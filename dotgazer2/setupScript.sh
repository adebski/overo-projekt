#!/bin/sh
#loading modules
insmod /lib/modules/3.2.0/kernel/drivers/iommu/omap-iovmm.ko
insmod /lib/modules/3.2.0/kernel/drivers/media/media.ko
insmod /lib/modules/3.2.0/kernel/drivers/media/video/videodev.ko
insmod /lib/modules/3.2.0/kernel/drivers/media/video/v4l2-common.ko
insmod /lib/modules/3.2.0/kernel/drivers/media/video/v4l2-int-device.ko
insmod /lib/modules/3.2.0/kernel/drivers/media/video/omap3isp/omap3-isp.ko
insmod /home/root/pwm.ko frequency=23000

#setting up media framework pipeline
media-ctl -v -l '"mt9v032 3-005c":0->"OMAP3 ISP CCDC":0[1]'
media-ctl -v -l '"OMAP3 ISP CCDC":1->"OMAP3 ISP CCDC output":0[1]'
media-ctl -v -f '"mt9v032 3-005c":0 [SGRBG10 752x480]'
media-ctl -v -f '"OMAP3 ISP CCDC":1 [SGRBG10 752x480]'
