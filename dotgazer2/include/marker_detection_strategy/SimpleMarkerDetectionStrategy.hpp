#ifndef SIMPLEMARKERDETECTIONSTRATEGY_HPP
#define	SIMPLEMARKERDETECTIONSTRATEGY_HPP

#include "marker_detection_strategy/MarkerDetectionStrategy.hpp"
#include <vector>
#include "dotgazer/Marker.hpp"
#include "opencv/cv.h"
#include "utils/ImageConverter.hpp"
#include "dotgazer/Tilt.hpp"
#include "dotgazer/AlgorithmSettings.hpp"

class SimpleMarkerDetectionStrategy : public MarkerDetectionStrategy {
private:
    std::vector<Marker> markers;
    bool verboseText, verboseImage;
    CvMemStorage *storage;
    CvSeq *contours;
    AlgorithmSettings* algorithmSettings;
public:
    static SimpleMarkerDetectionStrategy* getInstance(bool verboseText, bool verboseImage, AlgorithmSettings* algorithmSettings);
    std::vector<Marker> detectMarkers(std::vector<Dot> dots, Tilt tilt);
private:
    void assignDotToMarker(Dot dot);
    void removeBogusMarkers(void);
    void correctMarkersParallelToEdges(void);
    bool isMarkerPrallelToEdge(Marker m);
    Dot temporarilyTransformDot(Dot dot);
    std::vector<Marker> correctTilt(std::vector<Marker> inMarkers, Tilt tilt);
    Dot correctDot(Dot dot, Tilt tilt);
    std::vector<Marker> undistort(std::vector<Marker> inMarkers);

    SimpleMarkerDetectionStrategy() {
    };
    SimpleMarkerDetectionStrategy(SimpleMarkerDetectionStrategy const&);
    void operator=(SimpleMarkerDetectionStrategy const&);
};

#endif