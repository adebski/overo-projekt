#ifndef MARKERDETECTIONSTRATEGY_HPP
#define MARKERDETECTIONSTRATEGY_HPP

#include <vector>
#include "dotgazer/Marker.hpp"
#include "opencv/cv.h"
#include "utils/ImageConverter.hpp"
#include "dotgazer/Tilt.hpp"

class MarkerDetectionStrategy {
public:
    virtual std::vector<Marker> detectMarkers(std::vector<Dot> dots, Tilt tilt) = 0;
};

#endif