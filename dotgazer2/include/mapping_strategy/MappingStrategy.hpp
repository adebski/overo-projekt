#ifndef MAPPINGSTRATEGY_HPP
#define	MAPPINGSTRATEGY_HPP

#include "utils/Const.hpp"
#include "dotgazer/Coordinates.hpp"
#include <vector>
#include "dotgazer/Marker.hpp"
#include <map>

class MappingStrategy {
public:
    virtual int loadMapFromFile(const char* fileName) = 0;
    virtual void updateMap(std::vector<Marker> markers) = 0;
    virtual std::vector<Marker> updateMarkersUsingMap(std::vector<Marker> markers) = 0;
    virtual std::map<int, Coordinates> getMap() = 0;
    virtual std::map<int, Coordinates>* getPointerToMap() = 0;
    virtual void clearMap() = 0;
};

#endif