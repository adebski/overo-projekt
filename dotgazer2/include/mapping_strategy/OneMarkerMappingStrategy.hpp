#ifndef ONEMARKERMAPPINGSTRATEGY_HPP
#define	ONEMARKERMAPPINGSTRATEGY_HPP

#include "mapping_strategy/MappingStrategy.hpp"
#include "utils/Const.hpp"
#include "dotgazer/Coordinates.hpp"
#include <vector>
#include "dotgazer/Marker.hpp"
#include <map>
#include "dotgazer/AlgorithmSettings.hpp"

class OneMarkerMappingStrategy : public MappingStrategy {
private:
    bool verboseText, verboseImage;
    std::map<int, Coordinates> coords;
    AlgorithmSettings* algorithmSettings;
    const char* fileName;
public:
    OneMarkerMappingStrategy(bool verboseText, bool verboseImage, AlgorithmSettings* algorithmSettings);
    int loadMapFromFile(const char* fileName);
    void updateMap(std::vector<Marker> markers);
    std::vector<Marker> updateMarkersUsingMap(std::vector<Marker> markers);
    std::map<int, Coordinates> getMap();
    std::map<int, Coordinates> *getPointerToMap();
    void clearMap();
private:
    bool contains(int key);
    void createNewMap(std::vector<Marker> markers);
};

#endif