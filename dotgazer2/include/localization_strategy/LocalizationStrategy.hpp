#ifndef LOCALIZATIONSTRATEGY_HPP
#define	LOCALIZATIONSTRATEGY_HPP

#include "dotgazer/Coordinates.hpp"
#include <vector>
#include "dotgazer/Marker.hpp"

class LocalizationStrategy {
public:
    virtual Coordinates calculatePosition(std::vector<Marker> markers) = 0;
    virtual Marker getReferenceMarker() = 0;

    virtual ~LocalizationStrategy() {
    };
};

#endif