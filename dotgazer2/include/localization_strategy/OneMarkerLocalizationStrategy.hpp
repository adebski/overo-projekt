#ifndef ONEMARKERLOCALIZATIONSTRATEGY_HPP
#define	ONEMARKERLOCALIZATIONSTRATEGY_HPP

#include "localization_strategy/LocalizationStrategy.hpp"
#include "dotgazer/Coordinates.hpp"
#include <vector>
#include "dotgazer/Marker.hpp"
#include "dotgazer/AlgorithmSettings.hpp"

class OneMarkerLocalizationStrategy : public LocalizationStrategy {
private:
    bool verboseText, verboseImage;
    Marker referenceMarker;
    AlgorithmSettings* algorithmSettings;
public:
    OneMarkerLocalizationStrategy(bool verboseText, bool verboseImage, AlgorithmSettings* algorithmSettings);
    Coordinates calculatePosition(std::vector<Marker> markers);
    Marker getReferenceMarker();
private:
    Coordinates calculateRobotPosition(Marker m);
};

#endif