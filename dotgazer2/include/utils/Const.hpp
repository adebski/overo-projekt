#ifndef CONSTANTS_HPP
#define	CONSTANTS_HPP

#include "sys/types.h"
#include "libwebsockets.h"
#include "limits.h"
#include "time.h"
#include <stdint.h>
#include <opencv/cv.h>

class Const {
private:
    static float cameraMatrix[];
    static float distCoeffs[];
public:
    static const float PI = 3.14159;
    static const char* CONFIG_FILE;
    static const int IMAGE_WIDTH = 752;
    static const int IMAGE_HEIGHT = 480;
    static const int IMAGE_CENTER_X = 401;
    static const int IMAGE_CENTER_Y = 200;
    static const int BGR_DATA_SIZE = IMAGE_WIDTH * IMAGE_HEIGHT * 3;
    static const int OVERO_STREAM_BUFFER_SIZE = 2048;
    static const int OVERO_CONTROL_BUFFER_SIZE = 2048;
    static const int OVERO_STREAM_HEADER_PREAMBULE_SIZE = 4;
    static const int OVERO_CONTROL_HEADER_PREAMBULE_SIZE = 2;
    static const unsigned int OVERO_CONTROL_BRIGHTNESS_PACKET_SIZE = 4;
    static const unsigned int OVERO_CONTROL_ALGORITHM_PARAMETERS_PACKET_SIZE = 20;
    //number of possible epochTime values + 4 chars for(.acc or .img) and one char for NULL
    static const int FILENAME_BUFFER_SIZE = (CHAR_BIT * sizeof (unsigned long long) + 4 * sizeof (char) + sizeof (char));
    static const float INVALID_COORDS = 9999999.0f;
    static const char* GMOCK_OPTIONS;
    static const CvMat CAMERA_MATRIX;
    static const CvMat DIST_COEFFS;
    // was 0.00155f on in-room photos
    static const float METERS_PER_PIXEL_COEFF = 0.0016f; // meters per pixel when CEILING_HEIGHT=1
    static const float MARKER_REAL_SPAN_METERS = 0.224f; // marker's diagonal length
    static const int TRANSFORMATION_MATRIX_SCALE = 1; // the greater the scale, the more accurate (and bigger) the matrix will be
};

#endif
