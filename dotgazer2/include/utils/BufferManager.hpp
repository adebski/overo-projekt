#ifndef BUFFERMANAGER_HPP
#define	BUFFERMANAGER_HPP

#include <stdint.h>
#include <asm/types.h>          /* for videodev2.h */

#include <linux/videodev2.h>
#include <linux/media.h>
#include <linux/v4l2-subdev.h>

class BufferManager {
public:

    struct Buffer {
        unsigned int size;
        void *memory;
    };
private:
    const int _fileDescriptor;
    int _numberOfBuffers;
    const enum v4l2_memory _memoryType;
    struct Buffer *_buffers;
    const enum v4l2_buf_type _bufferType;
    struct v4l2_buffer *internalBuffer;
    bool _verbose;
public:
    BufferManager(const int fileDescriptor, const int numberOfBuffers, const enum v4l2_memory memoryType,
            const enum v4l2_buf_type bufferType, bool verbose);
    int requestBuffers();
    int queueBuffer(int bufferNumber);
    int queueBuffers();
    int freeBuffers();
    enum v4l2_memory getMemeoryType() const;
    enum v4l2_buf_type getBufferType() const;
    int getNumberOfBuffers() const;
    struct Buffer getBuffer(int bufferNumber);
    int dequeueBuffer(struct v4l2_buffer * buffer);
private:
    int queryForBuffer(struct v4l2_buffer *buffer, __u32 index);
    int mapMemoryMapBuffers();
    int freeMemoryMapBuffers();
};

#endif