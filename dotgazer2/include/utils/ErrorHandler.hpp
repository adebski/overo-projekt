#ifndef ERRORHANDLER_HPP
#define	ERRORHANDLER_HPP

#define MAXLINE 4096
#include<cstdarg>

class ErrorHandler {
public:
    /*
     * free allocated space
     */
    static void freeAllocated(int num, ...);

    /*
     * write to stderr, return to caller
     */
    static void write(int errnoflag, int error, const char *fmt, va_list ap);

    /*
     *nonfatal error related to system call
     *print message and return
     */
    static void systemNonfatal(const char *fmt, ...);

    /*
     * fatal error related to system call
     * print message and terminate
     */
    static void systemFatal(const char *fmt, ...);

    /*
     * fatal error related to system call
     * print message, dump core, and terminate
     */
    static void systemFatalDump(const char *fmt, ...);

    /*nonfatal error unrelated to system call
     * print message and return
     */
    static void nonsystemNonfatal(const char *fmt, ...);

    /*
     * fatal error unrelated to system call
     * error code passed as explicit parameter
     * print message and terminate
     */
    static void nonsystemFatalErrcode(int error, const char *fmt, ...);

    /*
     * fatal error unrelated to system call
     * print message and terminate
     */
    static void nonsystemFatal(const char *fmt, ...);
};

#endif