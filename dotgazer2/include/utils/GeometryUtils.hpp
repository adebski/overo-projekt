#ifndef GEOMETRYUTILS_HPP
#define	GEOMETRYUTILS_HPP

#include "dotgazer/Marker.hpp"
#include "dotgazer/Dot.hpp"
#include <vector>

class GeometryUtils {
public:
    static Dot removeBogusDot(Marker m);
    static int calculateMarkerId(Dot mainDot, Marker* m);
    static float dotsDistance(Dot a, Dot b);
    static float markerAngle(Marker m);
    static float vectorAngle(float xBegin, float yBegin, float xEnd, float yEnd);
    static float pointsDistance(float x1, float y1, float x2, float y2);
    static Marker findMarkerClosestToCenter(std::vector<Marker> markers);
private:
    static char oneEdgeDotIsTheSame(Marker m);
    static float max(float a, float b);
    static float min(float a, float b);
    static char areDifferentDots(Dot a, Dot b);
    static char dotExists(float x, float y, Marker m, float tolerance);
    static inline float normalizeAngle(float angle);
};

#endif