#ifndef CVUTILS_HPP
#define	CVUTILS_HPP

#include "opencv/cv.h"

class CVUtils {
public:
    virtual void drawCircle(IplImage* img, float x, float y, int r, int hue, int thickness);
    virtual void drawCircle(IplImage* img, float x, float y, int radius, int r, int g, int b, int thickness);
    virtual void undistortImage(IplImage* img);
};

#endif