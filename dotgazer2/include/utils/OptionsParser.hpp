#ifndef OPTIONSPARSER_HPP
#define	OPTIONSPARSER_HPP

#include "camera/Camera.hpp"
#include "accelerometer/Accelerometer.hpp"
#include <opencv/cv.h>
#include "ImageConverter.hpp"
#include "dotgazer/AlgorithmSettings.hpp"
#include "binarization_strategy/BinarizationStrategy.hpp"
#include "marker_detection_strategy/MarkerDetectionStrategy.hpp"

enum AccelerometerVersion {
    MINIMU_9, MINIMU_9V2, ACCELEROMETER_STUB, ACCELEROMETER_VERSION_MAX = ACCELEROMETER_STUB + 1
};

enum CameraModel {
    MT9V032, CAMERA_STUB, CAMERA_MODEL_MAX = CAMERA_STUB + 1
};

enum BinarizationStrategyType {
    RAW_BINARIZATION_STRATEGY, RAW_INTERPOLATED_BINARIZATION_STRATEGY,
    BGR_BINARIZATION_STRATEGY, BINARIZATION_STRATEGY_MAX = BGR_BINARIZATION_STRATEGY + 1
};

class OptionsParser {
private:
    long mainFunction;
    char* fileName;
    int accMeasurements;
    bool verboseText;
    bool verboseImage;
    int iterations;
    char* serialDevice;
    bool performDaemonization;
    char* parameterFileName;
    char* mapFileName;

    AccelerometerVersion accelerometerVersion;
    CameraModel cameraModel;
    BinarizationStrategyType binarizationStrategyType;

public:
    void parseCmdLine(int argc, char** argv);
    int invokeMain();
    OptionsParser();

private:
    void printUsage();
    Camera* createCamera();
    Accelerometer *createAccelerometer();
    BinarizationStrategy *createBinarizationStrategy(ImageConverter *imageConverter, CvSize maskSize,
            AlgorithmSettings *algorithSettings);
    int daemonize();
};

#endif