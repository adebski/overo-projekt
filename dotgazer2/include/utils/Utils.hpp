#ifndef UTILS_HPP
#define	UTILS_HPP

#include <sys/time.h>
#include <stdint.h>
#include "sys/types.h"
#include "dotgazer/AlgorithmSettings.hpp"
#include <map>

class Utils {
public:
    static timespec timeDiff(timespec start, timespec end);
    static uint64_t pack754_32(float f);
    static uint64_t pack754_64(float f);
    static long double unpack754_32(uint32_t i);
    static long double unpack754_64(uint64_t i);
    static bool isSystemBigEndian(void);
    static void setNewBrightness(uint32_t brightness);
    static void readParameterFile(char* parameterFileName, AlgorithmSettings* algorithmSettings);
    static void saveParameterAndMap(AlgorithmSettings *algorithmSettings, char* parameterFileName,
            std::map<int, Coordinates>* map, char* mapFileName);
};

#endif