#ifndef IMAGECONVERTER_HPP
#define	IMAGECONVERTER_HPP

#include "opencv/cv.h"
#include "camera/CameraImage.hpp"

class ImageConverter {
private:
    IplImage *rescaledImage;
    IplImage *yuvImage;
    IplImage *yChannel, *uChannel, *vChannel;
    IplImage *rawImage;
    CameraImage *cameraImage;
public:
    static ImageConverter* getInstance();
    IplImage* loadRawImage(const char* path);

    /* Reuses memory, no need to manually free() the CameraImage* */
    CameraImage* loadCameraImage(const char* path);

    IplImage* convertRawToBGR(IplImage *rawImage);
    IplImage* convertRawToBGR(CameraImage *rawImage, IplImage *bgrImage);
    IplImage* convertRawToBGR(IplImage *rawImage, IplImage *bgrImage);
    IplImage* createIplImageStructure(int width, int heigth, int depth, int channels);
    IplImage* getYFromBGR(IplImage *bgrImage);
private:
    char* loadRawData(const char *path, const int size);

    ImageConverter() {
    };
    ImageConverter(ImageConverter const&);
    void operator=(ImageConverter const&);
};

#endif

