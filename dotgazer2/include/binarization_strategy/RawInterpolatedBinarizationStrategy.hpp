#ifndef RAWINTERPOLATEDBINARIZATIONSTRATEGY_HPP
#define	RAWINTERPOLATEDBINARIZATIONSTRATEGY_HPP

#include "BinarizationStrategy.hpp"
#include "RawInterpolatedBinarizationStrategy.hpp" 
#include "dotgazer/AlgorithmSettings.hpp"
#include "camera/CameraImage.hpp"

class RawInterpolatedBinarizationStrategy : public BinarizationStrategy {
private:
    bool verboseText, verboseImage;
    ImageConverter* converter;
    IplImage* mask;
    AlgorithmSettings* algorithmSettings;
public:
    static RawInterpolatedBinarizationStrategy* getInstance(CvSize maskSize, bool verboseText,
            bool verboseImage, AlgorithmSettings* algorithmSettings);
    IplImage* binarizeImage(CameraImage* rawImage);
    IplImage* getIplImageAllocatedForBinarizedImage();
private:

    RawInterpolatedBinarizationStrategy() {
    };
    RawInterpolatedBinarizationStrategy(RawInterpolatedBinarizationStrategy const&);
    void operator=(RawInterpolatedBinarizationStrategy const&);
};

#endif