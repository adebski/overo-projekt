#ifndef RAWBINARIZATIONSTRATEGY_HPP
#define	RAWBINARIZATIONSTRATEGY_HPP

#include "BinarizationStrategy.hpp"
#include "RawBinarizationStrategy.hpp" 
#include "dotgazer/AlgorithmSettings.hpp"
#include "camera/CameraImage.hpp"

class RawBinarizationStrategy : public BinarizationStrategy {
private:
    bool verboseText, verboseImage;
    ImageConverter* converter;
    IplImage* mask;
    AlgorithmSettings* algorithmSettings;
public:
    static RawBinarizationStrategy* getInstance(CvSize maskSize, bool verboseText, bool verboseImage,
            AlgorithmSettings* algorithmSettings);
    IplImage* binarizeImage(CameraImage* rawImage);
    IplImage* getIplImageAllocatedForBinarizedImage();
private:

    RawBinarizationStrategy() {
    };
    RawBinarizationStrategy(RawBinarizationStrategy const&);
    void operator=(RawBinarizationStrategy const&);
};

#endif