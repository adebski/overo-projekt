#ifndef BINARIZATIONSTRATEGY_HPP
#define	BINARIZATIONSTRATEGY_HPP

#include "opencv/cv.h"
#include "utils/ImageConverter.hpp"
#include "camera/CameraImage.hpp"

class BinarizationStrategy {
public:
    virtual IplImage* binarizeImage(CameraImage* rawImage) = 0;
    virtual IplImage* getIplImageAllocatedForBinarizedImage() = 0;
};

#endif