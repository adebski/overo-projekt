#ifndef SIMPLEBINARIZATIONSTRATEGY_HPP
#define	SIMPLEBINARIZATIONSTRATEGY_HPP

#include "binarization_strategy/BinarizationStrategy.hpp"
#include "opencv/cv.h"
#include "utils/ImageConverter.hpp"
#include "dotgazer/AlgorithmSettings.hpp"
#include "camera/CameraImage.hpp"

class BgrBinarizationStrategy : public BinarizationStrategy {
private:
    bool verboseText, verboseImage;
    ImageConverter* converter;
    IplImage* mask;
    IplImage* bgrImage;
    AlgorithmSettings* algorithmSettings;
public:
    static BgrBinarizationStrategy* getInstance(ImageConverter* converter,
            CvSize maskSize, bool verboseText, bool verboseImage, AlgorithmSettings* algorithmSettings);
    IplImage* binarizeImage(CameraImage* rawImage);
    IplImage* getIplImageAllocatedForBinarizedImage();
private:

    BgrBinarizationStrategy() {
    };
    BgrBinarizationStrategy(BgrBinarizationStrategy const&);
    void operator=(BgrBinarizationStrategy const&);
};

#endif