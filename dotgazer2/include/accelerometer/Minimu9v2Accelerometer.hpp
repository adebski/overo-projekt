#ifndef MINIMU9V2ACCELEROMETER_HPP
#define	MINIMU9V2ACCELEROMETER_HPP

#include "accelerometer/Accelerometer.hpp"
#include <stdint.h>
#include "dotgazer/Tilt.hpp"

class Minimu9v2Accelerometer : public Accelerometer {
private:
    int fileDescriptor;
    Tilt initialTilt;
public:
    Minimu9v2Accelerometer(); // only for testing purposes; don't use in production!
    Minimu9v2Accelerometer(const char* deviceName);
    void calculateInitialTilt();
    Tilt getTilt();
    Tilt getAverageTilt(int mesurements, int milis);
private:
    uint8_t get(uint8_t addr);
    float convertToAngle(uint8_t h, uint8_t l);
    Tilt getRawTilt();
};

#endif