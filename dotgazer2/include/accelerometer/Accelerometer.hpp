#ifndef ACCELEROMETER_HPP
#define	ACCELEROMETER_HPP

#include "dotgazer/Tilt.hpp"

class Accelerometer {
public:
    virtual Tilt getTilt() = 0;
    virtual Tilt getAverageTilt(int mesurements, int milis) = 0;
    virtual void calculateInitialTilt() = 0;

    virtual ~Accelerometer() {
    };
};

#endif