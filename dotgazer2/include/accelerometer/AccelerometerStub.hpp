/* 
 * File:   AccelerometerStub.hpp
 * Author: adebksi
 *
 * Created on September 14, 2013, 12:38 PM
 */

#ifndef ACCELEROMETERSTUB_HPP
#define	ACCELEROMETERSTUB_HPP

#include "Accelerometer.hpp"
#include "dotgazer/Tilt.hpp"
#include <unistd.h>

class AccelerometerStub : public Accelerometer {
private:
    Tilt tilt;
public:
    virtual Tilt getTilt();
    virtual Tilt getAverageTilt(int mesurements, int milis);
    void calculateInitialTilt();

    virtual ~AccelerometerStub() {
    };
};

#endif	/* ACCELEROMETERSTUB_HPP */

