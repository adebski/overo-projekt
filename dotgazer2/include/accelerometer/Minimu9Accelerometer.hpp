#ifndef MINIMU9ACCELEROMETER_HPP
#define	MINIMU9ACCELEROMETER_HPP

#include "accelerometer/Accelerometer.hpp"
#include <stdint.h>
#include "dotgazer/Tilt.hpp"

class Minimu9Accelerometer : public Accelerometer {
private:
    int fileDescriptor;
public:
    Minimu9Accelerometer(); // only for testing purposes; don't use in production!
    Minimu9Accelerometer(const char* deviceName);
    void calculateInitialTilt();
    Tilt getTilt();
    Tilt getAverageTilt(int mesurements, int milis);
private:
    uint8_t get(uint8_t addr);
    float convertToAngle(uint8_t h, uint8_t l);
};

#endif