/* 
 * File:   OveroControlCallback.hpp
 * Author: adebksi
 *
 * Created on September 8, 2013, 9:16 PM
 */

#ifndef OVEROCONTROLCALLBACK_HPP
#define	OVEROCONTROLCALLBACK_HPP

enum OveroControlState {
    IDLE, DETECT, MAP_INIT, MAP_UPDATE
};

class OveroControlCallback {
public:
    void virtual onOveroControlState(OveroControlState overoControlState) = 0;
    void virtual onUpdateMapUsingThisImage(bool updateMapUsingThisImage) = 0;
    void virtual onCalculateCeilingHeightUsingThisImage() = 0;
};

#endif	/* OVEROCONTROLCALLBACK_HPP */

