/* 
 * File:   WebsocketService.hpp
 * Author: adebksi
 *
 * Created on September 8, 2013, 4:27 PM
 */

#ifndef MAINSERVERWEBSOCKETSERVICE_HPP
#define	MAINSERVERWEBSOCKETSERVICE_HPP

#include "opencv/cv.h"
#include "dotgazer/AlgorithmSettings.hpp"
#include "camera/Mt9v032Camera.hpp"
#include <libwebsockets.h>
#include "utils/CVUtils.hpp"
#include "dotgazer/Tilt.hpp"
#include "dotgazer/Coordinates.hpp"
#include "OveroControlCallback.hpp"
#include "utils/ImageConverter.hpp"
#include "dotgazer/Marker.hpp"
#include "dotgazer/Dot.hpp"
#include <vector>

enum ImageStreamFrameState {
    NO_FRAME, FIRST_FRAME, MIDDLE_FRAME, LAST_FRAME
};

enum ImageStreamType {
    NONE, NORMAL_IMAGE, BINARIZED_IMAGE
};

class MainServerWebsocketService {
private:
    struct libwebsocket_context * overoLibwebsocketContext;
    struct libwebsocket_protocols protocols[4];

    OveroControlCallback *overoControLCallback;

    CVUtils *cvUtils;
    ImageConverter *imageConverter;
    bool verboseText;
    bool verboseImage;

    //results to send
    Tilt tilt;
    Coordinates coordinates;
    int referenceMarkerId;
    std::vector<Marker> *markers;

    //pointers to shared memory
    AlgorithmSettings * algorithmSettings;
    IplImage *mask;
    CameraImage *rawCameraImage;
    std::map<int, Coordinates> *map;

    bool sendAlgorithmParameters;
    bool sendCurrentMap;

    //image that will be streamed
    IplImage *bgrImage;
    CvMat *compressedImageToSend;
    int imageDataLeft;
    int imageDataSent;

    enum ImageStreamFrameState imageStreamFrameState;
    enum ImageStreamType imageStreamType;
    bool undistortImage;
    bool coordinatesStream;
    bool accelerometerStream;
    bool paintMarkers;
    bool paintCenter;
    bool detectedMarkersStream;

    char* parameterFileName;
    char* mapFileName;

    MainServerWebsocketService() {
    };
    MainServerWebsocketService(MainServerWebsocketService const&);
    void operator=(MainServerWebsocketService const&);

    void printVerboseMessage();
    bool isImageStreamEnabled() const;
    bool isStreamEnabled() const;
    void initializeProtocol(int protocolIndex, const char* name, callback_function* callback,
            int perSessionDataSize, int rxBufferSize);
    void saveImageAndCoordinates();
    void prepareImageDataForStream();
    void websocketOveroControlReceiveTextData(libwebsocket_context* context, libwebsocket* wsi, void* in, size_t len);
    void websocketOveroControlReceiveBinaryData(void *in, size_t len);
    void websocketOveroStreamParseMessage(char* in, size_t len);
    int websocketOveroStreamSendStream(libwebsocket_context* context, libwebsocket* wsi);
public:
    static MainServerWebsocketService *getInstance(CVUtils *cvUtils, ImageConverter *imageConverter, bool verboseText, bool verboseImage,
            char* parameterFileName, char* mapFileName);
    virtual int initialzieWebsocket(int port);
    virtual void setSharedMemoryPointers(AlgorithmSettings *algorithmSettings, IplImage *mask, CameraImage *rawCameraImage, std::vector<Marker> *markers, std::map<int, Coordinates> *map);
    virtual void serviceWebsockets();
    virtual void setOveroControlCallback(OveroControlCallback *overoControlCallback);
    virtual void updateResultsToSend(Tilt tilt, Coordinates coordinates, int referenceMarkerId);
    virtual int websocketOveroControlCallback(libwebsocket_context* context, libwebsocket* wsi,
            libwebsocket_callback_reasons reason, void* user, void* in, size_t len);
    virtual int websocketOveroStreamCallback(libwebsocket_context* context, libwebsocket* wsi,
            libwebsocket_callback_reasons reason, void* user, void* in, size_t len);

    virtual ~MainServerWebsocketService();

};

extern MainServerWebsocketService *globalMainServerWebsocketService;

#endif	/* MAINSERVERWEBSOCKETSERVICE_HPP */

