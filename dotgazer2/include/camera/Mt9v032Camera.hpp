#ifndef MT9V032CAMERA_HPP
#define	MT9V032CAMERA_HPP

#include "camera/Camera.hpp"
#include <stdint.h>
#include <asm/types.h>          /* for videodev2.h */
#include <linux/videodev2.h>
#include <linux/media.h>
#include <linux/v4l2-subdev.h>

#include "utils/BufferManager.hpp"
#include "camera/CameraImage.hpp"

class Mt9v032Camera : public Camera {
private:
    static const char* cameraStatesStrings[];

    enum cameraStates {
        CREATED, OPENED, CONFIGURED, READY, CLOSED
    };

    static struct PixelFormat {
        const char *name;
        __u32 pixelFormat;
    } pixelFormats[];

    enum v4l2_memory _memoryType;
    int _numberOfBuffers;
    enum v4l2_buf_type _bufferType;
    BufferManager *_bufferManager;

    int _fileDescriptor;
    enum cameraStates _cameraState;

    __u32 _pixelFormat;
    __u32 _width;
    __u32 _height;
    __u32 _bytesperline;
    __u32 _imagesize;

    void *_pattern;
    unsigned int _patternsize;

    const char* _deviceName;
    bool _verbose;
public:
    Mt9v032Camera(const char* deviceName, bool verboseText);
    int prepareCameraForStream();
    struct CameraImage *getFrame(struct CameraImage * cameraImage);
    virtual ~Mt9v032Camera();
private:
    int closeDevice();
    int openDevice();
    int setFormat();
    int prepareBuffers(int numberOfBuffers, enum v4l2_memory memoryType = V4L2_MEMORY_MMAP);
    int startCapturing();
    int stopCapturing();
    const char* getV4L2PixelFormatName(const __u32 pixelFormat);
    void printFormatInformation(const struct v4l2_format format);
    int prepareBufferForImageCopy(struct CameraImage *cameraImage, int bytesUsed);
};

#endif