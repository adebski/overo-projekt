#ifndef CAMERA_HPP
#define	CAMERA_HPP

class Camera {
public:
    virtual int prepareCameraForStream() = 0;
    virtual struct CameraImage *getFrame(struct CameraImage * cameraImage) = 0;

    virtual ~Camera() {
    };
};

#endif