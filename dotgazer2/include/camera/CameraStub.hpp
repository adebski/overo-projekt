/* 
 * File:   CameraStub.hpp
 * Author: adebksi
 *
 * Created on September 14, 2013, 1:10 AM
 */

#ifndef CAMERASTUB_HPP
#define	CAMERASTUB_HPP

#include "Camera.hpp"
#include "CameraImage.hpp"

class CameraStub : public Camera {
private:
    CameraImage *cameraImage;
    char* stubImageFileName;

    int loadRawImage();
public:
    CameraStub(char *stubImageFileName);
    virtual int prepareCameraForStream();
    virtual struct CameraImage *getFrame(struct CameraImage * cameraImage);

    virtual ~CameraStub() {
    };
};


#endif	/* CAMERASTUB_HPP */

