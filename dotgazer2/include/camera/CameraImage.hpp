#ifndef CAMERAIMAGE_HPP
#define	CAMERAIMAGE_HPP

struct CameraImage {
    uint32_t length; // length of image in bytes
    void* image;
};

#endif