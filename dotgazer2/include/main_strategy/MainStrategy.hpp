#ifndef MAINSTRATEGY_HPP
#define	MAINSTRATEGY_HPP

class MainStrategy {
public:
    virtual int main() = 0;

    virtual ~MainStrategy() {
    };
};

#endif

