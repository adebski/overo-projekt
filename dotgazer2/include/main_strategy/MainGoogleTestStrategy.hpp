/* 
 * File:   MainGoogleTestStrategy.hpp
 * Author: adebksi
 *
 * Created on August 29, 2013, 11:19 AM
 */

#ifndef MAINGOOGLETESTSTRATEGY_HPP
#define	MAINGOOGLETESTSTRATEGY_HPP

#include <cstdio>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include "MainStrategy.hpp"

class MainGoogleTestStrategy : public MainStrategy {
public:
    int main();
};

#endif	/* MAINGOOGLETESTSTRATEGY_HPP */

