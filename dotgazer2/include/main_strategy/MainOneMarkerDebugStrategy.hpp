#ifndef MAINONEMARKERDEBUGSTRATEGY_HPP
#define	MAINONEMARKERDEBUGSTRATEGY_HPP

#include "main_strategy/MainStrategy.hpp"
#include "dotgazer/AlgorithmSettings.hpp"

class MainOneMarkerDebugStrategy : public MainStrategy {
private:
    char* fileName;
    bool verboseText, verboseImage;
    AlgorithmSettings* algorithmSettings;
public:
    MainOneMarkerDebugStrategy(char* fileName, bool verboseText, bool verboseImage,
            AlgorithmSettings* algorithmSettings);
    void process(char[]);
    int main();
};

#endif