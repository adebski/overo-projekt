#ifndef PERFORMANCETESTSTRATEGY_HPP
#define PERFORMANCETESTSTRATEGY_HPP

#include "accelerometer/Minimu9Accelerometer.hpp"
#include "camera/Mt9v032Camera.hpp"
#include "MainStrategy.hpp"
#include "dotgazer/AlgorithmSettings.hpp"
#include "binarization_strategy/BinarizationStrategy.hpp"
#include "localization_strategy/LocalizationStrategy.hpp"
#include "marker_detection_strategy/MarkerDetectionStrategy.hpp"
#include "mapping_strategy/MappingStrategy.hpp"
#include "websocket/MainServerWebsocketService.hpp"
#include "dot_detection_strategy/DotDetectionStrategy.hpp"

class MainPerformanceTestStrategy : public MainStrategy {
private:
    Camera *camera;
    Accelerometer *accelerometer;
    AlgorithmSettings* algorithmSettings;
    int iterations;
    int accMeasurements;
    LocalizationStrategy *localizationStrategy;
    MappingStrategy *mappingStrategy;
    BinarizationStrategy *binarizationStrategy;
    DotDetectionStrategy *dotDetectionStrategy;
    MarkerDetectionStrategy* markerDetectionStrategy;
    int serialDeviceDescriptor;
    MainServerWebsocketService *mainServerWebsocketService;

    char* parameterFileName;
    void writeCoordinatesToSerial(Coordinates coordinates);
public:
    int main();
    MainPerformanceTestStrategy(Camera* camera,
            Accelerometer* accelerometer, LocalizationStrategy *localizationStrategy, MappingStrategy *mappingStrategy,
            BinarizationStrategy *binarizationStrategy, DotDetectionStrategy *dotDetectionStrategy,
            MarkerDetectionStrategy* markerDetectionStrategy, int accMeasurements, AlgorithmSettings* algorithmSettings,
            int iterations, int serialDeviceDescriptor, MainServerWebsocketService *mainServerWebsocketService, char* parameterFileName);
};

#endif	/* PERFORMANCETESTSTRATEGY_HPP */

