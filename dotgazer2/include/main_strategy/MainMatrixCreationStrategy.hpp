#ifndef MAINMATRIXCREATIONSTRATEGY_HPP
#define MAINMATRIXCREATIONSTRATEGY_HPP

#include "main_strategy/MainStrategy.hpp"
#include "dotgazer/AlgorithmSettings.hpp"
#include "dotgazer/Dot.hpp"

class MainMatrixCreationStrategy : public MainStrategy {
private:
    char* fileName;
    bool verboseText, verboseImage;
    AlgorithmSettings* algorithmSettings;
public:
    MainMatrixCreationStrategy(char* fileName, bool verboseText, bool verboseImage,
            AlgorithmSettings* algorithmSettings);
    int main();
private:
    static bool compareDotsByDistanceFromCenter(const Dot& a, const Dot& b);
};

#endif
