#ifndef MAINRAWTOJGPCONVERTERSTRATEGY_HPP
#define MAINRAWTOJGPCONVERTERSTRATEGY_HPP

#include "MainStrategy.hpp"
#include "utils/ImageConverter.hpp"

class MainRawToJpgConverterStrategy : public MainStrategy {
private:
    ImageConverter *imageConverter;
    char* fileName;

public:
    int main();
    MainRawToJpgConverterStrategy(ImageConverter *imageConverter, char* fileName);
};


#endif