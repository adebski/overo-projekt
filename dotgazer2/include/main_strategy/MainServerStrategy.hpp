#ifndef MAINSERVERSTRATEGY_HPP
#define	MAINSERVERSTRATEGY_HPP

#include "localization_strategy/OneMarkerLocalizationStrategy.hpp"
#include "marker_detection_strategy/SimpleMarkerDetectionStrategy.hpp"
#include "binarization_strategy/BgrBinarizationStrategy.hpp"
#include "mapping_strategy/OneMarkerMappingStrategy.hpp"
#include "main_strategy/MainStrategy.hpp"
#include "websocket/OveroControlCallback.hpp"
#include <libwebsockets.h>
#include "opencv/cv.h"
#include "camera/Mt9v032Camera.hpp"
#include "utils/Const.hpp"
#include "utils/CVUtils.hpp"
#include "dotgazer/Tilt.hpp"
#include "dotgazer/Coordinates.hpp"
#include "accelerometer/Minimu9Accelerometer.hpp"
#include "dotgazer/AlgorithmSettings.hpp"
#include "websocket/MainServerWebsocketService.hpp"
#include "dot_detection_strategy/DotDetectionStrategy.hpp"

class MainServerStrategy : public MainStrategy, OveroControlCallback {
private:
    LocalizationStrategy *localizationStrategy;
    MappingStrategy *mappingStrategy;
    BinarizationStrategy *binarizationStrategy;
    DotDetectionStrategy *dotDetectionStrategy;
    MarkerDetectionStrategy* markerDetectionStrategy;
    MainServerWebsocketService *mainServerWebsocketService;

    IplImage *mask;
    Tilt tilt;
    Coordinates coordinates;
    int referenceMarkerId;

    OveroControlState overoControlState;
    bool updateMapUsingThisImage;

    int accMeasurements;
    bool verboseText, verboseImage;
    Camera *camera;
    Accelerometer *accelerometer;
    AlgorithmSettings* algorithmSettings;

    int serialDeviceDescriptor;

    bool daemonMode;
    void writeCoordinatesToSerial(Coordinates coordinates);
public:
    MainServerStrategy(Camera * camera, Accelerometer *accelerometer,
            LocalizationStrategy *localizationStrategy, MappingStrategy *mappingStrategy,
            BinarizationStrategy *binarizationStrategy, MarkerDetectionStrategy* markerDetectionStrategy,
            DotDetectionStrategy *dotDetectionStrategy, int accMeasurements, bool verboseText, bool verboseImage,
            AlgorithmSettings* algorithmSettings, MainServerWebsocketService *mainServerWebsocketService,
            int serialDeviceDescriptor, bool daemonMode);
    int main();

    virtual void onOveroControlState(OveroControlState overoControlState);
    virtual void onUpdateMapUsingThisImage(bool updateMapUsingThisImage);
    virtual void onCalculateCeilingHeightUsingThisImage();

    virtual ~MainServerStrategy();
};



#endif