#ifndef OPENCVDOTDETECTIONSTRATEGY_HPP
#define OPENCVDOTDETECTIONSTRATEGY_HPP

#include "dot_detection_strategy/DotDetectionStrategy.hpp"
#include "dotgazer/AlgorithmSettings.hpp"
#include "opencv/cv.h"
#include "dotgazer/Dot.hpp"
#include <vector>

class OpencvDotDetectionStrategy : public DotDetectionStrategy {
private:
    std::vector<Dot> dots;
    bool verboseText, verboseImage;
    CvMemStorage *storage;
    CvSeq *contours;
    AlgorithmSettings* algorithmSettings;
public:
    static OpencvDotDetectionStrategy* getInstance(bool verboseText, bool verboseImage, AlgorithmSettings* algorithmSettings);
    std::vector<Dot> detectDots(IplImage* mask);
private:

    OpencvDotDetectionStrategy() {
    };
    OpencvDotDetectionStrategy(OpencvDotDetectionStrategy const&);
    void operator=(OpencvDotDetectionStrategy const&);
};

#endif

