#ifndef DOTDETECTIONSTRATEGY_HPP
#define DOTDETECTIONSTRATEGY_HPP

#include "dotgazer/Dot.hpp"
#include "opencv/cv.h"
#include <vector>

class DotDetectionStrategy {
public:
    virtual std::vector<Dot> detectDots(IplImage* mask) = 0;
};

#endif
