#ifndef TILT_HPP
#define	TILT_HPP

class Tilt {
private:
    float x, y, z;
public:
    Tilt();
    Tilt(float x, float y, float z);
    float getX();
    float getY();
    float getZ();
    Tilt operator-(Tilt&) const;
};

#endif