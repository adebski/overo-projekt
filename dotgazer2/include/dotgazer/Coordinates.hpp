#ifndef COORDINATES_HPP
#define	COORDINATES_HPP

class Coordinates {
private:
    float x, y, angle;
public:
    Coordinates();
    Coordinates(float x, float y, float angle);
    float getX();
    float getY();
    float getAngle();
};

#endif