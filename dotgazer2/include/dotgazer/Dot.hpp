#ifndef DOT_HPP
#define	DOT_HPP

class Dot {
private:
    float x, y;
public:
    Dot();
    Dot(float x, float y);
    float getX() const;
    float getY() const;
    bool operator==(const Dot &other) const;
};

#endif

