#ifndef ALGORITHMSETTINGS_HPP
#define	ALGORITHMSETTINGS_HPP

#include <vector>
#include "dotgazer/Marker.hpp"

class AlgorithmSettings {
private:
    bool undistort;
    float minDotArea;
    float maxDotArea;
    float dotBrightnessThreshold;
    float ceilingHeightMeters;
    float metersPerPixel;
    float **mx;
    float **my;
public:
    AlgorithmSettings(long mainFunction);
    bool getUndistort();
    void setUndistort(bool undistort);
    float getMinDotArea();
    void setMinDotArea(float area);
    float getMaxDotArea();
    void setMaxDotArea(float area);
    float getDotBrightnessThreshold();
    void setDotBrightnessThreshold(float threshold);
    float getCeilingHeightMeters();
    float getMetersPerPixel();
    void setCeilingHeightMetersAndMetersPerPixel(float height);
    void setCeilingHeightMetersAndMetersPerPixel(std::vector<Marker> markers);
    void setCeilingHeightMetersAndMetersPerPixel(float height, float metersPerPixel);
    void printVerboseMessage();
    float** getMx();
    float** getMy();
};

#endif