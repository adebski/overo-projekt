#ifndef MARKER_HPP
#define	MARKER_HPP

#include "dotgazer/Dot.hpp"
#include <vector>
#include "dotgazer/Coordinates.hpp"

class Marker {
private:
    std::vector<Dot> dots; // pixels
    Dot north, south, west, east; // pixels
    Dot left, right, center, mainDot; // pixels
    float distanceFromScreenCenter; // pixels
    int id;
    Coordinates absoluteCoords; // meters
public:
    Marker();
    Marker(Dot dot);
    void addDot(Dot dot);
    std::vector<Dot> getDots();
    Dot getNorth();
    void setNorth(Dot dot);
    Dot getSouth();
    void setSouth(Dot dot);
    Dot getWest();
    void setWest(Dot dot);
    Dot getEast();
    void setEast(Dot dot);
    Dot getLeft();
    void setLeft(Dot dot);
    Dot getRight();
    void setRight(Dot dot);
    Dot getCenter();
    void setCenter(Dot dot);
    Dot getMainDot();
    void setMainDot(Dot dot);
    int getId();
    void setId(int id);
    float getDistanceFromScreenCenter();
    void setDistanceFromScreenCenter(float distance);
    Coordinates getAbsoluteCoords();
    void setAbsoluteCoords(Coordinates coords);
};

#endif