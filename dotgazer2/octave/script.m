% B = nan(20);
% B(3,2) = 5;
% B(17,4) = 3;
% B(16, 19) = 2.3;
% B(5, 18) = 4.5;
% Bhat = inpaint_nans(B);
% mesh(Bhat);

mx_sparse = load('mx_sparse.txt');
my_sparse = load('my_sparse.txt');
mx_filled = inpaint_nans(mx_sparse);
my_filled = inpaint_nans(my_sparse);
dlmwrite('mx_filled.txt', mx_filled, ' ');
dlmwrite('my_filled.txt', my_filled, ' ');
