#!/bin/bash

for file in `find $1`; do 
	base=`echo "$file" | cut -d'.' -f 1`
	echo $base
	./dotgazer.out -m 6 -f $file
done
