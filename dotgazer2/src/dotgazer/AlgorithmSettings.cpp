#include "dotgazer/AlgorithmSettings.hpp"
#include "utils/Const.hpp"
#include "utils/GeometryUtils.hpp"
#include <cmath>
#include "utils/ErrorHandler.hpp"

float** loadTransformationMatrixFromFile(const char* filename) {
    float** m = (float**) calloc(Const::IMAGE_WIDTH, sizeof(*m));
    for (int i=0; i<Const::IMAGE_WIDTH; i++) {
        m[i] = (float*) calloc(Const::IMAGE_HEIGHT, sizeof(*m[i]));
    }
    FILE *f = fopen(filename, "r");
    if (f == NULL) {
        ErrorHandler::systemFatal("Could not open file %s to load the matrix", filename);
    }
    for (int i=0; i<Const::IMAGE_WIDTH; i++) {
        for (int j=0; j<Const::IMAGE_HEIGHT; j++) {
            if (fscanf(f, "%f", &m[i][j]) == EOF) {
                ErrorHandler::systemFatal("Could not properly read file %s\n", filename);
            };
        }
    }
    return m;
}

AlgorithmSettings::AlgorithmSettings(long mainFunction) {
    undistort = true;
    minDotArea = 10.0f;
    maxDotArea = 250.0f;
    dotBrightnessThreshold = 250.0f;
    setCeilingHeightMetersAndMetersPerPixel(2.5f);
    // we load the translation matrices if it's not the matrix creation strategy
    if (mainFunction != 3) {
        mx = loadTransformationMatrixFromFile("octave/mx_filled.txt");
        my = loadTransformationMatrixFromFile("octave/my_filled.txt");
    }
}

bool AlgorithmSettings::getUndistort() {
    return undistort;
}

void AlgorithmSettings::setUndistort(bool undistort) {
    this->undistort = undistort;
}

float AlgorithmSettings::getMinDotArea() {
    return minDotArea;
}

void AlgorithmSettings::setMinDotArea(float area) {
    minDotArea = area;
}

float AlgorithmSettings::getMaxDotArea() {
    return maxDotArea;
}

void AlgorithmSettings::setMaxDotArea(float area) {
    maxDotArea = area;
}

float AlgorithmSettings::getDotBrightnessThreshold() {
    return dotBrightnessThreshold;
}

void AlgorithmSettings::setDotBrightnessThreshold(float threshold) {
    dotBrightnessThreshold = threshold;
}

float AlgorithmSettings::getCeilingHeightMeters() {
    return ceilingHeightMeters;
}

float AlgorithmSettings::getMetersPerPixel() {
    return metersPerPixel;
}

void AlgorithmSettings::setCeilingHeightMetersAndMetersPerPixel(float height) {
    ceilingHeightMeters = height;
    metersPerPixel = height * Const::METERS_PER_PIXEL_COEFF;
}

void AlgorithmSettings::setCeilingHeightMetersAndMetersPerPixel(std::vector<Marker> markers) {
    if (markers.size() == 0) {
        printf("Couldn't calibrate height because there were no markers");
        return;
    }
    Marker closest = GeometryUtils::findMarkerClosestToCenter(markers);
    float diagonalLengthPixels = MAX(GeometryUtils::dotsDistance(closest.getNorth(), closest.getSouth()),
            GeometryUtils::dotsDistance(closest.getWest(), closest.getEast()));
    metersPerPixel = Const::MARKER_REAL_SPAN_METERS / diagonalLengthPixels;
    ceilingHeightMeters = metersPerPixel / Const::METERS_PER_PIXEL_COEFF;
}

void AlgorithmSettings::setCeilingHeightMetersAndMetersPerPixel(float height, float metersPerPixel) {
    ceilingHeightMeters = height;
    this->metersPerPixel = metersPerPixel;
}

void AlgorithmSettings::printVerboseMessage() {
    printf("Alghoritm parameters: undistort %d metersPerPixel %f dotBrightnessThreshold %f minDotArea %f maxDotArea %f ceilingHeightMeters %f\n",
            undistort, metersPerPixel, dotBrightnessThreshold, minDotArea, maxDotArea, ceilingHeightMeters);
}

float** AlgorithmSettings::getMx() {
    return mx;
}

float** AlgorithmSettings::getMy() {
    return my;
}