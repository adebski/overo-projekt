#include "dotgazer/Dot.hpp"

Dot::Dot() {
    this->x = 0;
    this->y = 0;
}

Dot::Dot(float x, float y) {
    this->x = x;
    this->y = y;
}

float Dot::getX() const {
    return x;
}

float Dot::getY() const {
    return y;
}

bool Dot::operator==(const Dot &other) const {
    return ((x == other.x) && (y == other.y));
}