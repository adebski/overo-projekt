#include "dotgazer/Coordinates.hpp"

Coordinates::Coordinates() {
    x = 0;
    y = 0;
}

Coordinates::Coordinates(float x, float y, float angle) {
    this->x = x;
    this->y = y;
    this->angle = angle;
}

float Coordinates::getX() {
    return x;
}

float Coordinates::getY() {
    return y;
}

float Coordinates::getAngle() {
    return angle;
}