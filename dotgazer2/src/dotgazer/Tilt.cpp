#include "dotgazer/Tilt.hpp"

Tilt::Tilt() {
    this->x = 0;
    this->y = 0;
    this->z = 0;
}

Tilt::Tilt(float x, float y, float z) {
    this->x = x;
    this->y = y;
    this->z = z;
}

float Tilt::getX() {
    return x;
}

float Tilt::getY() {
    return y;
}

float Tilt::getZ() {
    return z;
}

Tilt Tilt::operator-(Tilt& rhs) const {
    Tilt res = Tilt(this->x - rhs.getX(),
            this->y - rhs.getY(),
            this->z - rhs.getZ());
    return res;
}