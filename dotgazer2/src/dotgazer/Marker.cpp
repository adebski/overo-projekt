#include "dotgazer/Marker.hpp"
#include "dotgazer/Coordinates.hpp"

Marker::Marker() {
}

Marker::Marker(Dot dot) {
    addDot(dot);
    setNorth(dot);
    setSouth(dot);
    setWest(dot);
    setEast(dot);
}

void Marker::addDot(Dot dot) {
    dots.push_back(dot);
}

std::vector<Dot> Marker::getDots() {
    return dots;
}

Dot Marker::getNorth() {
    return north;
}

void Marker::setNorth(Dot dot) {
    north = dot;
}

Dot Marker::getSouth() {
    return south;
}

void Marker::setSouth(Dot dot) {
    south = dot;
}

Dot Marker::getWest() {
    return west;
}

void Marker::setWest(Dot dot) {
    west = dot;
}

Dot Marker::getEast() {
    return east;
}

void Marker::setEast(Dot dot) {
    east = dot;
}

Dot Marker::getLeft() {
    return left;
}

void Marker::setLeft(Dot dot) {
    left = dot;
}

Dot Marker::getRight() {
    return right;
}

void Marker::setRight(Dot dot) {
    right = dot;
}

Dot Marker::getCenter() {
    return center;
}

void Marker::setCenter(Dot dot) {
    center = dot;
}

Dot Marker::getMainDot() {
    return mainDot;
}

void Marker::setMainDot(Dot dot) {
    mainDot = dot;
}

int Marker::getId() {
    return id;
}

void Marker::setId(int id) {
    this->id = id;
}

float Marker::getDistanceFromScreenCenter() {
    return distanceFromScreenCenter;
}

void Marker::setDistanceFromScreenCenter(float distance) {
    distanceFromScreenCenter = distance;
}

Coordinates Marker::getAbsoluteCoords() {
    return absoluteCoords;
}

void Marker::setAbsoluteCoords(Coordinates coords) {
    absoluteCoords = coords;
}