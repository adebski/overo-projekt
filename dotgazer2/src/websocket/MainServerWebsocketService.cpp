#define __STDC_FORMAT_MACROS
#include "websocket/MainServerWebsocketService.hpp"
#include "utils/Utils.hpp"
#include "opencv/cv.h"
#include "opencv/highgui.h"
#include "utils/CVUtils.hpp"
#include "utils/ImageConverter.hpp"
#include <libwebsockets.h>
#include "utils/ErrorHandler.hpp"
#include <fcntl.h>
#include <inttypes.h>
#include "sys/stat.h"
#include "sys/types.h"
#include "utils/Const.hpp"
#include "arpa/inet.h"

static const char* imageStreamTypeStrings [] = {"none", "normal image", "binarized image"};
MainServerWebsocketService *globalMainServerWebsocketService;

int _websocketHttpCallback(libwebsocket_context* context, libwebsocket* wsi,
        libwebsocket_callback_reasons reason, void* user, void* in, size_t len) {
    return 0;
}

int _websocketOveroControlCallback(libwebsocket_context* context, libwebsocket* wsi,
        libwebsocket_callback_reasons reason, void* user, void* in, size_t len) {
    return globalMainServerWebsocketService->websocketOveroControlCallback(context, wsi,
            reason, user, in, len);
}

int _websocketOveroStreamCallback(libwebsocket_context* context, libwebsocket* wsi,
        libwebsocket_callback_reasons reason, void* user, void* in, size_t len) {
    return globalMainServerWebsocketService->websocketOveroStreamCallback(context, wsi,
            reason, user, in, len);
}

void MainServerWebsocketService::printVerboseMessage() {
    printf("imageDataleft %d imageDataSent %d imageStreamState %s "
            "coordinatesStream %d accelerometerStream %d\n",
            imageDataLeft, imageDataSent,
            imageStreamTypeStrings[imageStreamType], coordinatesStream, accelerometerStream);
}

bool MainServerWebsocketService::isImageStreamEnabled() const {
    return imageStreamType != NONE;
}

bool MainServerWebsocketService::isStreamEnabled() const {
    return isImageStreamEnabled() || accelerometerStream || coordinatesStream || detectedMarkersStream;
}

void MainServerWebsocketService::initializeProtocol(int protocolIndex, const char* name, callback_function* callback,
        int perSessionDataSize, int rxBufferSize) {
    memset(&(protocols[protocolIndex]), 0, sizeof (struct libwebsocket_protocols));
    protocols[protocolIndex].name = name;
    protocols[protocolIndex].callback = callback;
    protocols[protocolIndex].per_session_data_size = perSessionDataSize;
    protocols[protocolIndex].rx_buffer_size = rxBufferSize;
}

void MainServerWebsocketService::saveImageAndCoordinates() {
    if (verboseText) {
        printf("Saving readings\n");
    }

    static char filenameBuffer[Const::FILENAME_BUFFER_SIZE];
    int fileDescriptor;
    FILE *fileHandle;
    time_t currentSeconds = time(NULL);

    snprintf(filenameBuffer, Const::FILENAME_BUFFER_SIZE, "%llu.img", (unsigned long long) currentSeconds);
    filenameBuffer[Const::FILENAME_BUFFER_SIZE - 1] = '\0';
    fileDescriptor = open(filenameBuffer, O_WRONLY | O_TRUNC | O_CREAT, 00777);
    if (fileDescriptor < 0) {
        ErrorHandler::systemNonfatal("Error opening file %s", filenameBuffer);
        return;
    }
    if (write(fileDescriptor, rawCameraImage->image, rawCameraImage->length) < 0) {
        ErrorHandler::systemNonfatal("Error writing raw image");
        close(fileDescriptor);
        return;
    }
    close(fileDescriptor);

    snprintf(filenameBuffer, Const::FILENAME_BUFFER_SIZE, "%llu.acc", (unsigned long long) currentSeconds);
    filenameBuffer[Const::FILENAME_BUFFER_SIZE - 1] = '\0';
    fileHandle = fopen(filenameBuffer, "w+");
    if (fileHandle == NULL) {
        ErrorHandler::systemNonfatal("Error opening file %s", filenameBuffer);
        return;
    }
    if (fprintf(fileHandle, "%g %g %g", tilt.getX(), tilt.getY(), tilt.getZ()) < 0) {
        ErrorHandler::systemNonfatal("Error writing coordinates to file");
        return;
    }
    fclose(fileHandle);

    if (verboseText) {
        printf("Finished saving readings\n");
    }
    return;
}

void MainServerWebsocketService::prepareImageDataForStream() {
    IplImage *imageToStream;


    if (compressedImageToSend != NULL) {
        cvReleaseMat(&compressedImageToSend);
        compressedImageToSend = NULL;
    }

    switch (imageStreamType) {
        case NORMAL_IMAGE:
            imageConverter->convertRawToBGR(rawCameraImage, bgrImage);
            imageToStream = bgrImage;
            break;
        case BINARIZED_IMAGE:
            imageToStream = mask;
            break;
        default:
            printf("Unrecognized value of imageStreamType\n");
            return;

    }
    if (undistortImage) {
        cvUtils->undistortImage(imageToStream);
    }
    if (paintMarkers) {
        for (size_t markerIndex = 0; markerIndex < markers->size(); ++markerIndex) {
            Marker marker = markers->at(markerIndex);
            std::vector<Dot> dots = marker.getDots();
            for (size_t dotIndex = 0; dotIndex < dots.size(); ++dotIndex) {
                cvUtils->drawCircle(imageToStream, dots[dotIndex].getX(), dots[dotIndex].getY(), 5, 150, 1);
            }
        }
    }

    if (paintCenter) {
        cvUtils->drawCircle(imageToStream, Const::IMAGE_WIDTH / 2, Const::IMAGE_HEIGHT / 2, 3, 255, 2);
    }

    compressedImageToSend = cvEncodeImage(".jpeg", imageToStream, 0);
    imageDataLeft = compressedImageToSend->rows * compressedImageToSend->step;
    imageDataSent = 0;
}

void MainServerWebsocketService::websocketOveroControlReceiveTextData(libwebsocket_context* context, libwebsocket* wsi, void* in, size_t len) {
    printf("OveroControl received %.*s\n", (int) len, (char*) in);
    if (!strncmp((char*) in, "START_DETECTION", len)) {
        overoControLCallback->onOveroControlState(DETECT);
    } else if (!strncmp((char*) in, "STOP_DETECTION", len)) {
        overoControLCallback->onOveroControlState(IDLE);
    } else if (!strncmp((char*) in, "START_MAP", len)) {
        overoControLCallback->onOveroControlState(MAP_INIT);
    } else if (!strncmp((char*) in, "STOP_MAP", len)) {
        overoControLCallback->onOveroControlState(IDLE);
    } else if (!strncmp((char*) in, "UPDATE_MAP_USING_THIS_IMAGE", len)) {
        overoControLCallback->onUpdateMapUsingThisImage(true);
    } else if (!strncmp((char*) in, "CALCULATE_CEILING_HEIGHT_USING_THIS_IMAGE", len)) {
        overoControLCallback->onCalculateCeilingHeightUsingThisImage();
    } else if (!strncmp((char*) in, "SEND_CURRENT_PARAMETERS", len)) {
        this->sendAlgorithmParameters = true;
        libwebsocket_callback_on_writable(context, wsi);
    } else if (!strncmp((char*) in, "SEND_CURRENT_MAP", len)) {
        this->sendCurrentMap = true;
        libwebsocket_callback_on_writable(context, wsi);
    } else if (!strncmp((char*) in, "SAVE_PARAMETERS_AND_MAP", len)) {
        Utils::saveParameterAndMap(algorithmSettings, parameterFileName, map, mapFileName);
    }
}

void MainServerWebsocketService::websocketOveroControlReceiveBinaryData(void *in, size_t len) {
    uint32_t *newParameters = (uint32_t*) in;
    printf("Overo control: received %zu bytes of binary data\n", len);

    //new brightness 
    if (len == Const::OVERO_CONTROL_BRIGHTNESS_PACKET_SIZE) {
        uint32_t newBrightnessValue = newParameters[0];
        newBrightnessValue = ntohl(newBrightnessValue);
        Utils::setNewBrightness(newBrightnessValue);
    } else if (len == Const::OVERO_CONTROL_ALGORITHM_PARAMETERS_PACKET_SIZE) {
        bool undistort = ntohl(newParameters[0]);
        float dotBrightnessThreshold = Utils::unpack754_32(ntohl(newParameters[1]));
        float minDotArea = Utils::unpack754_32(ntohl(newParameters[2]));
        float maxDotArea = Utils::unpack754_32(ntohl(newParameters[3]));
        float ceilingHeightMeters = Utils::unpack754_32(ntohl(newParameters[4]));

        algorithmSettings->setUndistort(undistort);
        algorithmSettings->setDotBrightnessThreshold(dotBrightnessThreshold);
        algorithmSettings->setMinDotArea(minDotArea);
        algorithmSettings->setMaxDotArea(maxDotArea);
        algorithmSettings->setCeilingHeightMetersAndMetersPerPixel(ceilingHeightMeters);

        algorithmSettings->printVerboseMessage();
    }
}

void MainServerWebsocketService::websocketOveroStreamParseMessage(char* message, size_t len) {
    if (!strncmp(message, "START_NORMAL_IMAGE_STREAM", len)) {
        imageStreamType = NORMAL_IMAGE;
    } else if (!strncmp(message, "STOP_NORMAL_IMAGE_STREAM", len)) {
        imageStreamType = NONE;
    } else if (!strncmp(message, "START_BINARIZED_IMAGE_STREAM", len)) {
        imageStreamType = BINARIZED_IMAGE;
    } else if (!strncmp(message, "STOP_BINARIZED_IMAGE_STREAM", len)) {
        imageStreamType = NONE;
    } else if (!strncmp(message, "START_UNDISTORT_IMAGE", len)) {
        undistortImage = true;
    } else if (!strncmp(message, "STOP_UNDISTORT_IMAGE", len)) {
        undistortImage = false;
    } else if (!strncmp(message, "START_COORDINATES_STREAM", len)) {
        coordinatesStream = true;
    } else if (!strncmp(message, "STOP_COORDINATES_STREAM", len)) {
        coordinatesStream = false;
    } else if (!strncmp(message, "START_ACC_STREAM", len)) {
        accelerometerStream = true;
    } else if (!strncmp(message, "STOP_ACC_STREAM", len)) {
        accelerometerStream = false;
    } else if (!strncmp(message, "START_PAINT_MARKERS", len)) {
        paintMarkers = true;
    } else if (!strncmp(message, "STOP_PAINT_MARKERS", len)) {
        paintMarkers = false;
    } else if (!strncmp(message, "START_PAINT_CENTER", len)) {
        paintCenter = true;
    } else if (!strncmp(message, "STOP_PAINT_CENTER", len)) {
        paintCenter = false;
    } else if (!strncmp(message, "SAVE_CURRENT", len)) {
        saveImageAndCoordinates();
    } else if (!strncmp(message, "START_DETECTED_MARKERS_STREAM", len)) {
        detectedMarkersStream = true;
    } else if (!strncmp(message, "STOP_DETECTED_MARKERS_STREAM", len)) {
        detectedMarkersStream = false;
    }
}

int MainServerWebsocketService::websocketOveroStreamSendStream(libwebsocket_context* context, libwebsocket* wsi) {
    static unsigned char buffer[Const::OVERO_STREAM_BUFFER_SIZE];
    static int bufferSizeWithoutPadding = Const::OVERO_STREAM_BUFFER_SIZE - LWS_SEND_BUFFER_PRE_PADDING - LWS_SEND_BUFFER_POST_PADDING;
    static unsigned char *dataPartOfBuffer = buffer + LWS_SEND_BUFFER_PRE_PADDING;
    uint32_t *headerBuffer = (uint32_t*) (dataPartOfBuffer + Const::OVERO_STREAM_HEADER_PREAMBULE_SIZE);
    int dataToSendSize = 0;
    int result = 0;

    if ((isStreamEnabled() == false) && imageStreamFrameState == NO_FRAME) {
        return 0;
    }

    do {

        if (verboseText) {
            printVerboseMessage();
        }

        switch (imageStreamFrameState) {
            case FIRST_FRAME:
            {
                if (verboseText) {
                    printf("First frame\n");
                }

                dataToSendSize = Const::OVERO_STREAM_HEADER_PREAMBULE_SIZE;
                //each byte will inform receiver about the contents of the message
                dataPartOfBuffer[0] = isImageStreamEnabled();
                dataPartOfBuffer[1] = accelerometerStream;
                dataPartOfBuffer[2] = coordinatesStream;
                dataPartOfBuffer[3] = detectedMarkersStream;

                if (accelerometerStream) {

                    if (verboseText) {
                        printf("acc data %g %g %g\n", tilt.getX(), tilt.getY(), tilt.getZ());
                    }

                    dataToSendSize += sizeof (uint32_t) * 3;
                    headerBuffer[0] = htonl(Utils::pack754_32(tilt.getX()));
                    headerBuffer[1] = htonl(Utils::pack754_32(tilt.getY()));
                    headerBuffer[2] = htonl(Utils::pack754_32(tilt.getZ()));
                    headerBuffer += 3;
                }

                if (coordinatesStream) {
                    dataToSendSize += sizeof (uint32_t) * 4;
                    headerBuffer[0] = htonl(Utils::pack754_32(coordinates.getX()));
                    headerBuffer[1] = htonl(Utils::pack754_32(coordinates.getY()));
                    headerBuffer[2] = htonl(Utils::pack754_32(coordinates.getAngle()));
                    headerBuffer[3] = htonl(referenceMarkerId);
                    headerBuffer += 4;
                }

                if (detectedMarkersStream) {
                    //for every marker we are sending center dot X,Y coords and marker id, also we are sending the number of markers
                    Marker marker;
                    uint32_t numberOfMarkers = markers->size();
                    dataToSendSize += sizeof (uint32_t) * 3 * numberOfMarkers + sizeof (uint32_t);

                    headerBuffer[0] = htonl(numberOfMarkers);
                    headerBuffer += 1;
                    for (uint32_t markerIndex = 0; markerIndex < numberOfMarkers; ++markerIndex) {
                        marker = markers->at(markerIndex);
                        headerBuffer[0] = htonl(marker.getId());
                        headerBuffer[1] = htonl(Utils::pack754_32(marker.getCenter().getX()));
                        headerBuffer[2] = htonl(Utils::pack754_32(marker.getCenter().getY()));
                        headerBuffer += 3;
                    }
                }

                result = libwebsocket_write(wsi, dataPartOfBuffer, dataToSendSize,
                        (libwebsocket_write_protocol) (LWS_WRITE_BINARY | LWS_WRITE_NO_FIN));

                if (verboseText) {
                    printf("Supposed to send %d sent %d\n", dataToSendSize, result);
                }

                if (result < 0) {
                    printf("Error while writing first frame\n");
                    return -1;
                }

                /*
                 * If we want to stream the image we are preparing data for it, otherwise we can sent LAST_FRAME
                 * directly after the first one.
                 * Because we are preparing data here we have an assurance that if during this frame we want to stream
                 * images, all the pointer will be initialized
                 */
                if (isImageStreamEnabled()) {
                    prepareImageDataForStream();
                    imageStreamFrameState = MIDDLE_FRAME;
                } else {
                    imageStreamFrameState = LAST_FRAME;
                }

                break;
            }
            case MIDDLE_FRAME:
            {
                if (verboseText) {
                    printf("Middle frame\n");
                }
                dataToSendSize = imageDataLeft > bufferSizeWithoutPadding ?
                        bufferSizeWithoutPadding : imageDataLeft;
                memcpy(dataPartOfBuffer,
                        compressedImageToSend->data.ptr + imageDataSent, dataToSendSize);

                result = libwebsocket_write(wsi, dataPartOfBuffer, dataToSendSize,
                        (libwebsocket_write_protocol) (LWS_WRITE_CONTINUATION | LWS_WRITE_NO_FIN));

                if (verboseText) {
                    printf("Supposed to send %d sent %d\n", dataToSendSize, result);
                }

                if (result < 0) {
                    printf("Error while writing middle frame\n");
                    return -1;
                } else if (result > dataToSendSize) {
                    result = dataToSendSize;
                }

                imageDataLeft -= result;
                imageDataSent += result;

                if (imageDataLeft <= 0) {
                    imageStreamFrameState = LAST_FRAME;
                }
                break;
            }
            case LAST_FRAME:
            {
                if (verboseText) {
                    printf("Last frame\n");
                }

                if (libwebsocket_write(wsi, dataPartOfBuffer, 1, LWS_WRITE_CONTINUATION) < 0) {
                    printf("Error while writing last frame\n");
                }
                imageStreamFrameState = NO_FRAME;

                if (verboseText) {
                    printf("Finished streaming image\n");
                }
                return 0;
            }
            default:
                break;

        }
        if (verboseText) {
            printf("stream loop end:data sent %d data left %d\n", imageDataSent, imageDataLeft);
        }
    } while (!lws_send_pipe_choked(wsi));

    if (imageDataLeft > 0 || imageStreamFrameState == LAST_FRAME) {
        libwebsocket_callback_on_writable(context, wsi);
    }

    return 0;
}

MainServerWebsocketService* MainServerWebsocketService::getInstance(CVUtils *cvUtils, ImageConverter *imageConverter, bool verboseText, bool verboseImage, char* parameterFileName, char* mapFileName) {
    MainServerWebsocketService *websocketService = new MainServerWebsocketService();

    if (websocketService == NULL) {
        ErrorHandler::systemNonfatal("Error while initializing new websocket servie");
    }

    websocketService->cvUtils = cvUtils;
    websocketService->imageConverter = imageConverter;
    websocketService->verboseText = verboseText;
    websocketService->verboseImage = verboseImage;
    websocketService->bgrImage = imageConverter->createIplImageStructure(Const::IMAGE_WIDTH, Const::IMAGE_HEIGHT, IPL_DEPTH_8U, 3);
    websocketService->compressedImageToSend = NULL;
    websocketService->parameterFileName = parameterFileName;
    websocketService->mapFileName = mapFileName;

    if (websocketService->bgrImage == NULL) {
        ErrorHandler::nonsystemNonfatal("Error while initializing bgr image for websocket service");
        return NULL;
    }

    websocketService->imageDataLeft = 0;
    websocketService->imageDataSent = 0;

    websocketService->imageStreamFrameState = NO_FRAME;
    websocketService->imageStreamType = NONE;
    websocketService->sendAlgorithmParameters = false;
    websocketService->sendCurrentMap = false;
    websocketService->undistortImage = false;
    websocketService->coordinatesStream = false;
    websocketService->accelerometerStream = false;
    websocketService->paintMarkers = false;
    websocketService->paintCenter = false;
    websocketService->detectedMarkersStream = false;

    websocketService->initializeProtocol(0, "http", _websocketHttpCallback, 0, 128);
    websocketService->initializeProtocol(1, "overoStream", _websocketOveroStreamCallback, 0, 0);
    websocketService->initializeProtocol(2, "overoControl", _websocketOveroControlCallback, 0, 0);
    websocketService->initializeProtocol(3, NULL, NULL, 0, 0);

    return websocketService;
}

int MainServerWebsocketService::initialzieWebsocket(int port) {
    struct lws_context_creation_info info;

    memset(&info, 0, sizeof (struct lws_context_creation_info));

    info.port = port;
    //we are listening on all interfaces
    info.iface = NULL;
    info.protocols = this->protocols;
#ifndef LWS_NO_EXTENSIONS
    info.extensions = libwebsocket_get_internal_extensions();
#endif
    info.gid = -1;
    info.uid = -1;

    this->overoLibwebsocketContext = libwebsocket_create_context(&info);
    if (this->overoLibwebsocketContext == NULL) {
        ErrorHandler::nonsystemNonfatal("error while creating websocket");
        return -1;
    }

    return 0;
}

void MainServerWebsocketService::setSharedMemoryPointers(AlgorithmSettings* algorithmSettings, IplImage* mask, CameraImage* rawCameraImage, std::vector<Marker> *markers, std::map<int, Coordinates> *map) {
    this->algorithmSettings = algorithmSettings;
    this->mask = mask;
    this->rawCameraImage = rawCameraImage;
    this->markers = markers;
    this->map = map;
}

void MainServerWebsocketService::serviceWebsockets() {
    libwebsocket_service(overoLibwebsocketContext, 0);

    if (isStreamEnabled() && imageStreamFrameState == NO_FRAME) {
        imageStreamFrameState = FIRST_FRAME;
        libwebsocket_callback_on_writable_all_protocol(&protocols[1]);
    }

}

void MainServerWebsocketService::setOveroControlCallback(OveroControlCallback* overoControlCallback) {
    this->overoControLCallback = overoControlCallback;
}

void MainServerWebsocketService::updateResultsToSend(Tilt tilt, Coordinates coordinates, int referenceMarkerId) {
    this->tilt = tilt;
    this->coordinates = coordinates;
    this->referenceMarkerId = referenceMarkerId;
}

int MainServerWebsocketService::websocketOveroControlCallback(libwebsocket_context* context, libwebsocket* wsi,
        libwebsocket_callback_reasons reason, void* user, void* in, size_t len) {

    static unsigned char buffer[Const::OVERO_CONTROL_BUFFER_SIZE];
    static unsigned char *dataPartOfBuffer = buffer + LWS_SEND_BUFFER_PRE_PADDING;
    static int bufferSizeWithoutPadding = Const::OVERO_CONTROL_BUFFER_SIZE - LWS_SEND_BUFFER_PRE_PADDING - LWS_SEND_BUFFER_POST_PADDING;
    uint32_t *controlDataBuffer = (uint32_t *) (dataPartOfBuffer + Const::OVERO_CONTROL_HEADER_PREAMBULE_SIZE);
    int dataToSendSize = Const::OVERO_CONTROL_HEADER_PREAMBULE_SIZE;
    int result;

    switch (reason) {
        case LWS_CALLBACK_ESTABLISHED:
            printf("OveroControl callback established\n");
            break;
        case LWS_CALLBACK_RECEIVE:
            if (lws_frame_is_binary(wsi)) {
                websocketOveroControlReceiveBinaryData(in, len);
            } else {
                websocketOveroControlReceiveTextData(context, wsi, in, len);
            }
            break;
        case LWS_CALLBACK_SERVER_WRITEABLE:

            dataPartOfBuffer[0] = sendAlgorithmParameters;
            dataPartOfBuffer[1] = sendCurrentMap;

            if (sendAlgorithmParameters) {
                controlDataBuffer[0] = htonl(algorithmSettings->getUndistort());
                controlDataBuffer[1] = htonl(Utils::pack754_32(algorithmSettings->getDotBrightnessThreshold()));
                controlDataBuffer[2] = htonl(Utils::pack754_32(algorithmSettings->getMinDotArea()));
                controlDataBuffer[3] = htonl(Utils::pack754_32(algorithmSettings->getMaxDotArea()));
                controlDataBuffer[4] = htonl(Utils::pack754_32(algorithmSettings->getCeilingHeightMeters()));
                controlDataBuffer += 5;
                dataToSendSize += 20;
                sendAlgorithmParameters = false;
            }

            if (sendCurrentMap) {
                controlDataBuffer[0] = htonl((uint32_t) map->size());
                controlDataBuffer += 1;
                dataToSendSize += 4 + map->size() * 4 * 4;
                typedef std::map<int, Coordinates>::iterator coordinatesMapIterator;
                for (coordinatesMapIterator iterator = map->begin(); iterator != map->end(); iterator++) {
                    controlDataBuffer[0] = htonl((uint32_t) iterator->first);
                    controlDataBuffer[1] = htonl(Utils::pack754_32(iterator->second.getX()));
                    controlDataBuffer[2] = htonl(Utils::pack754_32(iterator->second.getY()));
                    controlDataBuffer[3] = htonl(Utils::pack754_32(iterator->second.getAngle()));
                    controlDataBuffer += 4;
                }
            }

            result = libwebsocket_write(wsi, dataPartOfBuffer, dataToSendSize,
                    (libwebsocket_write_protocol) LWS_WRITE_BINARY);

            if (verboseText) {
                printf("Supposed to send %d sent %d\n", bufferSizeWithoutPadding, result);
            }

            if (result < 0) {
                printf("Error while sending alghoritm parameters\n");
                return -1;
            }

        default:
            break;
    }

    return 0;
}

int MainServerWebsocketService::websocketOveroStreamCallback(libwebsocket_context* context, libwebsocket* wsi, libwebsocket_callback_reasons reason, void* user, void* in, size_t len) {
    switch (reason) {
        case LWS_CALLBACK_ESTABLISHED:
        {
            printf("OveroStream callback established\n");
            break;
        }
        case LWS_CALLBACK_RECEIVE:
        {
            websocketOveroStreamParseMessage((char*) in, len);
            printf("OveroStream received %.*s\n", (int) len, (char*) in);
            break;
        }
        case LWS_CALLBACK_SERVER_WRITEABLE:
        {
            websocketOveroStreamSendStream(context, wsi);
            break;
        }
        default:
            break;
    }

    return 0;
}

MainServerWebsocketService::~MainServerWebsocketService() {
    libwebsocket_context_destroy(overoLibwebsocketContext);
}



