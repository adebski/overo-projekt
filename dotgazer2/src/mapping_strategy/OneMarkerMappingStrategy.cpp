#include "mapping_strategy/OneMarkerMappingStrategy.hpp"

#include "dotgazer/Coordinates.hpp"
#include <vector>
#include "dotgazer/Marker.hpp"
#include "utils/GeometryUtils.hpp"
#include "utils/ErrorHandler.hpp"
#include "utils/Utils.hpp"
#include <cstdlib>
#include <cstdio>
#include <cmath>

OneMarkerMappingStrategy::OneMarkerMappingStrategy(bool verboseText, bool verboseImage,
        AlgorithmSettings* algorithmSettings) {
    this->verboseText = verboseText;
    this->verboseImage = verboseImage;
    this->algorithmSettings = algorithmSettings;
}

void OneMarkerMappingStrategy::createNewMap(std::vector<Marker> markers) {
    if (verboseText) {
        printf("Creating a new map\n");
    }

    for (unsigned int i = 0; i < markers.size(); i++) {
        Marker m = markers[i];
        if (verboseText) {
            printf("Adding marker %d at (%f, %f), angle %f to map\n", m.getId(),
                    (m.getCenter().getX() - Const::IMAGE_WIDTH / 2) * algorithmSettings->getMetersPerPixel(),
                    (m.getCenter().getY() - Const::IMAGE_HEIGHT / 2) * algorithmSettings->getMetersPerPixel(),
                    GeometryUtils::markerAngle(m));
        }
        coords[m.getId()] = Coordinates((m.getCenter().getX() - Const::IMAGE_WIDTH / 2) * algorithmSettings->getMetersPerPixel(),
                (m.getCenter().getY() - Const::IMAGE_HEIGHT / 2) * algorithmSettings->getMetersPerPixel(),
                GeometryUtils::markerAngle(m));
    }
}

int OneMarkerMappingStrategy::loadMapFromFile(const char* fileName) {
    coords.clear();
    FILE* file;
    float tmpX, tmpY, tmpAngle;
    int num;
    if ((file = fopen(fileName, "r")) == NULL) {
        ErrorHandler::systemNonfatal("Could not open configuration file: %s", fileName);
        return -1;
    }
    while (fscanf(file, "%d %f %f %f", &num, &tmpX, &tmpY, &tmpAngle) != EOF) {
        coords[num] = Coordinates(tmpX, tmpY, tmpAngle);
    }
    fclose(file);
    return 0;
}

void OneMarkerMappingStrategy::updateMap(std::vector<Marker> markers) {
    if (verboseText) {
        printf("Received updateMap\n");
    }
    if (coords.size() == 0) {
        createNewMap(markers);
        return;
    }
    if (verboseText) {
        printf("Updating the exisitng map (creation omitted)\n");
    }
    Marker referenceMarker;
    bool foundReferenceMarker = false;
    for (unsigned int i = 0; i < markers.size(); i++) {
        Marker m = markers[i];
        // if this marker is already mapped and is closer to the center (or is the initial one)
        if (contains(m.getId()) && (!foundReferenceMarker ||
                m.getDistanceFromScreenCenter() < referenceMarker.getDistanceFromScreenCenter())) {
            foundReferenceMarker = true;
            referenceMarker = m;
        }
    }
    if (!foundReferenceMarker) {
        return;
    }
    // map unmapped markers in relation to the referenceMarker
    for (unsigned int i = 0; i < markers.size(); i++) {
        Marker m = markers[i];
        if (!contains(m.getId())) {
            float absCameraAngle = GeometryUtils::markerAngle(referenceMarker) - referenceMarker.getAbsoluteCoords().getAngle();
            float xShift = (m.getCenter().getX() - referenceMarker.getCenter().getX()) * algorithmSettings->getMetersPerPixel();
            float yShift = (m.getCenter().getY() - referenceMarker.getCenter().getY()) * algorithmSettings->getMetersPerPixel();
            float mAbsX = referenceMarker.getAbsoluteCoords().getX() + sin(absCameraAngle) * yShift + cos(absCameraAngle) * xShift;
            float mAbsY = referenceMarker.getAbsoluteCoords().getY() + cos(absCameraAngle) * yShift - sin(absCameraAngle) * xShift;
            float mAbsAngle = absCameraAngle + GeometryUtils::markerAngle(m);
            coords[m.getId()] = Coordinates(mAbsX, -mAbsY, mAbsAngle);
        }
    }
}

std::vector<Marker> OneMarkerMappingStrategy::updateMarkersUsingMap(std::vector<Marker> markers) {
    for (unsigned int i = 0; i < markers.size(); i++) {
        markers[i].setAbsoluteCoords(coords[markers[i].getId()]);
    }
    return markers;
}

std::map<int, Coordinates> OneMarkerMappingStrategy::getMap() {
    return coords;
}

std::map<int, Coordinates> *OneMarkerMappingStrategy::getPointerToMap() {
    return &coords;
}

void OneMarkerMappingStrategy::clearMap() {
    coords.clear();
}

bool OneMarkerMappingStrategy::contains(int key) {
    return coords.count(key) > 0;
}