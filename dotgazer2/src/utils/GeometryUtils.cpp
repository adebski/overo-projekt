#include "utils/GeometryUtils.hpp"
#include "dotgazer/Marker.hpp"
#include "dotgazer/Coordinates.hpp"
#include <cmath>
#include <cstdio>
#include "utils/Const.hpp"
#include "dotgazer/Dot.hpp"

Dot GeometryUtils::removeBogusDot(Marker m) {
    Dot mainDot;

    if (!oneEdgeDotIsTheSame(m)) {
        // if all edge dots are different
        float short1 = 0, long1 = 0, short2 = 0, long2 = 0;
        short1 = min(dotsDistance(m.getNorth(), m.getWest()),
                dotsDistance(m.getSouth(), m.getEast()));
        long1 = max(dotsDistance(m.getNorth(), m.getWest()),
                dotsDistance(m.getSouth(), m.getEast()));
        short2 = min(dotsDistance(m.getNorth(), m.getEast()),
                dotsDistance(m.getSouth(), m.getWest()));
        long2 = max(dotsDistance(m.getNorth(), m.getEast()),
                dotsDistance(m.getSouth(), m.getWest()));

        if (std::abs(short1 - long2) < std::abs(short2 - long1)) {
            if (dotsDistance(m.getNorth(), m.getWest()) < dotsDistance(m.getSouth(), m.getEast())) {
                if (dotsDistance(m.getNorth(), m.getEast()) > dotsDistance(m.getSouth(), m.getWest()))
                    mainDot = m.getNorth();
                else
                    mainDot = m.getWest();
            } else {
                if (dotsDistance(m.getNorth(), m.getEast()) > dotsDistance(m.getSouth(), m.getWest()))
                    mainDot = m.getEast();
                else
                    mainDot = m.getSouth();
            }
        } else {
            if (dotsDistance(m.getNorth(), m.getEast()) < dotsDistance(m.getSouth(), m.getWest())) {
                if (dotsDistance(m.getNorth(), m.getWest()) > dotsDistance(m.getSouth(), m.getEast()))
                    mainDot = m.getNorth();
                else
                    mainDot = m.getEast();
            } else {
                if (dotsDistance(m.getNorth(), m.getWest()) > dotsDistance(m.getSouth(), m.getEast()))
                    mainDot = m.getWest();
                else
                    mainDot = m.getSouth();
            }
        }
    } else {
        if (!areDifferentDots(m.getNorth(), m.getEast())) {
            if (dotsDistance(m.getWest(), m.getNorth()) < dotsDistance(m.getSouth(), m.getNorth()))
                mainDot = m.getWest();
            else
                mainDot = m.getSouth();
        } else if (!areDifferentDots(m.getEast(), m.getSouth())) {
            if (dotsDistance(m.getNorth(), m.getEast()) < dotsDistance(m.getWest(), m.getEast()))
                mainDot = m.getNorth();
            else
                mainDot = m.getWest();
        } else if (!areDifferentDots(m.getSouth(), m.getWest())) {
            if (dotsDistance(m.getNorth(), m.getSouth()) < dotsDistance(m.getEast(), m.getSouth()))
                mainDot = m.getNorth();
            else
                mainDot = m.getEast();
        } else if (!areDifferentDots(m.getWest(), m.getNorth())) {
            if (dotsDistance(m.getEast(), m.getWest()) < dotsDistance(m.getSouth(), m.getWest()))
                mainDot = m.getEast();
            else
                mainDot = m.getSouth();
        }
    }

    return mainDot;
}

int GeometryUtils::calculateMarkerId(Dot mainDot, Marker* m) {
    Dot left, right;
    float xLeft, yLeft, xRight, yRight;
    int ret = 0;

    if (!areDifferentDots(mainDot, m->getNorth())) {
        left = m->getEast();
        right = m->getWest();
    } else if (!areDifferentDots(mainDot, m->getEast())) {
        left = m->getSouth();
        right = m->getNorth();
    } else if (!areDifferentDots(mainDot, m->getSouth())) {
        left = m->getWest();
        right = m->getEast();
    } else if (!areDifferentDots(mainDot, m->getWest())) {
        left = m->getNorth();
        right = m->getSouth();
    }

    m->setMainDot(mainDot);
    m->setLeft(left);
    m->setRight(right);
    m->setCenter(Dot((left.getX() + right.getX()) / 2, (left.getY() + right.getY()) / 2));

    // wyznaczenie wektorow w lewo i prawo od mainDot
    xLeft = (left.getX() - mainDot.getX()) / 2;
    yLeft = (left.getY() - mainDot.getY()) / 2;
    xRight = (right.getX() - mainDot.getX()) / 2;
    yRight = (right.getY() - mainDot.getY()) / 2;

    // promien poszukiwan srodka kropki po przesunieciu o wektor
    float tolerance = sqrt(xLeft * xLeft + yLeft * yLeft) / 5;

    if (dotExists(mainDot.getX() + xRight, mainDot.getY() + yRight, *m, tolerance))
        ret += 64;
    if (dotExists(mainDot.getX() + xLeft, mainDot.getY() + yLeft, *m, tolerance))
        ret += 2;
    if (dotExists(mainDot.getX() + xLeft + xRight, mainDot.getY() + yLeft + yRight, *m,
            tolerance))
        ret += 32;
    if (dotExists(mainDot.getX() + xLeft + 2 * xRight,
            mainDot.getY() + yLeft + 2 * yRight, *m, tolerance))
        ret += 512;
    if (dotExists(mainDot.getX() + 2 * xLeft + xRight,
            mainDot.getY() + 2 * yLeft + yRight, *m, tolerance))
        ret += 16;

    return ret;
}

float GeometryUtils::dotsDistance(Dot a, Dot b) {
    return sqrt((a.getX() - b.getX()) * (a.getX() - b.getX()) + (a.getY() - b.getY()) * (a.getY() - b.getY()));
}

char GeometryUtils::oneEdgeDotIsTheSame(Marker m) {
    if (areDifferentDots(m.getNorth(), m.getSouth()) && areDifferentDots(m.getNorth(), m.getWest())
            && areDifferentDots(m.getNorth(), m.getEast())
            && areDifferentDots(m.getSouth(), m.getWest())
            && areDifferentDots(m.getSouth(), m.getEast())
            && areDifferentDots(m.getWest(), m.getEast()))
        return 0;
    else
        return 1;
}

float GeometryUtils::max(float a, float b) {
    if (a > b)
        return a;
    else
        return b;
}

float GeometryUtils::min(float a, float b) {
    if (a < b)
        return a;
    else
        return b;
}

char GeometryUtils::areDifferentDots(Dot a, Dot b) {
    if (a.getX() != b.getX() || a.getY() != b.getY())
        return 1;
    else
        return 0;
}

char GeometryUtils::dotExists(float x, float y, Marker m, float tolerance) {
    for (unsigned int i = 0; i < m.getDots().size(); i++) {
        float x1 = m.getDots()[i].getX();
        float y1 = m.getDots()[i].getY();
        if (GeometryUtils::pointsDistance(x, y, x1, y1) < tolerance) {
            return 1;
        }
    }
    return 0;
}

// kat markera == kat wektora left

float GeometryUtils::markerAngle(Marker m) {
    return vectorAngle(m.getMainDot().getX(), m.getMainDot().getY(), m.getLeft().getX(), m.getLeft().getY());
}

float GeometryUtils::vectorAngle(float xBegin, float yBegin, float xEnd, float yEnd) {
    float shiftX = xEnd - xBegin;
    float shiftY = yEnd - yBegin;
    if (shiftX > 0) {
        return normalizeAngle(atan(shiftY / shiftX));
    } else if (shiftX == 0) {
        if (shiftY > 0)
            return Const::PI / 2;
        else
            return 3 * Const::PI / 2;
    } else {
        return Const::PI - atan(shiftY / (-shiftX));
    }
}

float GeometryUtils::pointsDistance(float x1, float y1, float x2, float y2) {
    return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}

inline float GeometryUtils::normalizeAngle(float angle) {
    return angle < 0 ? 2 * Const::PI + angle : angle;
}

Marker GeometryUtils::findMarkerClosestToCenter(std::vector<Marker> markers) {
    Marker* closest = &markers[0];
    for (unsigned int i = 1; i < markers.size(); i++) {
        if (markers[i].getDistanceFromScreenCenter() < closest->getDistanceFromScreenCenter()) {
            closest = &markers[i];
        }
    }
    return *closest;
}