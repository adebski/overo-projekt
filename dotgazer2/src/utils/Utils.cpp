#define __STDC_FORMAT_MACROS
#include "utils/Utils.hpp"
#include "utils/Const.hpp"
#include <stdint.h>
#include "sys/types.h"
#include <inttypes.h>
#include "utils/ErrorHandler.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <map>

timespec Utils::timeDiff(timespec start, timespec end) {
    timespec temp;
    if ((end.tv_nsec - start.tv_nsec) < 0) {
        temp.tv_sec = end.tv_sec - start.tv_sec - 1;
        temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec - start.tv_sec;
        temp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    return temp;
}

//Functions for handling the conversion of floating point numbers to uint types for conversion to network order

uint64_t pack754(long double f, unsigned bits, unsigned expbits) {
    long double fnorm;
    int shift;
    long long sign, exp, significand;
    unsigned significandbits = bits - expbits - 1; // -1 for sign bit

    if (f == 0.0) return 0; // get this special case out of the way

    // check sign and begin normalization
    if (f < 0) {
        sign = 1;
        fnorm = -f;
    } else {
        sign = 0;
        fnorm = f;
    }

    // get the normalized form of f and track the exponent
    shift = 0;
    while (fnorm >= 2.0) {
        fnorm /= 2.0;
        shift++;
    }
    while (fnorm < 1.0) {
        fnorm *= 2.0;
        shift--;
    }
    fnorm = fnorm - 1.0;

    // calculate the binary form (non-float) of the significand data
    significand = fnorm * ((1LL << significandbits) + 0.5f);

    // get the biased exponent
    exp = shift + ((1 << (expbits - 1)) - 1); // shift + bias

    // return the final answer
    return (sign << (bits - 1)) | (exp << (bits - expbits - 1)) | significand;
}

long double unpack754(uint64_t i, unsigned bits, unsigned expbits) {
    long double result;
    long long shift;
    unsigned bias;
    unsigned significandbits = bits - expbits - 1; // -1 for sign bit

    if (i == 0) return 0.0;

    // pull the significand
    result = (i & ((1LL << significandbits) - 1)); // mask
    result /= (1LL << significandbits); // convert back to float
    result += 1.0f; // add the one back on

    // deal with the exponent
    bias = (1 << (expbits - 1)) - 1;
    shift = ((i >> significandbits)&((1LL << expbits) - 1)) - bias;
    while (shift > 0) {
        result *= 2.0;
        shift--;
    }
    while (shift < 0) {
        result /= 2.0;
        shift++;
    }

    // sign it
    result *= (i >> (bits - 1))&1 ? -1.0 : 1.0;

    return result;
}

uint64_t Utils::pack754_32(float f) {
    return pack754(f, 32, 8);
}

uint64_t Utils::pack754_64(float f) {
    return pack754(f, 64, 11);
}

long double Utils::unpack754_32(uint32_t i) {
    return unpack754(i, 32, 8);
}

long double Utils::unpack754_64(uint64_t i) {
    return unpack754(i, 64, 11);
}

bool Utils::isSystemBigEndian(void) {

    union {
        uint32_t i;
        char c[4];
    } bint = {0x01020304};
    return bint.c[0] == 1;
}

void Utils::setNewBrightness(uint32_t newBrightnessValue) {
    char stringifiedValue[4];
    printf("new brightness value is %"PRIu32"\n", newBrightnessValue);
    if (newBrightnessValue > 100) {
        ErrorHandler::nonsystemNonfatal("New brightness value should be in [0,100] range, it is %"PRIu32"\n", newBrightnessValue);
        newBrightnessValue = 100;
    }
    snprintf(stringifiedValue, 4, "%"PRIu32, newBrightnessValue);

    FILE* pwmNode = fopen("/dev/pwm11", "w");

    if (pwmNode == NULL) {
        ErrorHandler::systemNonfatal("Error while opening pwm node");
        return;
    }

    printf("written %d bytes\n", (int) fwrite(stringifiedValue, 4, 1, pwmNode));
    fclose(pwmNode);

    return;
}

void Utils::readParameterFile(char* parameterFileName, AlgorithmSettings* algorithmSettings) {
    std::ifstream parameterFile(parameterFileName, std::ios::in);
    std::string line;
    char* lineCString;
    char* token;
    char* value;
    int brightness;

    if (parameterFile.is_open() == false) {
        ErrorHandler::nonsystemNonfatal("Error while opening file %s", parameterFileName);
        return;
    }

    while (std::getline(parameterFile, line)) {
        lineCString = const_cast<char*> (line.c_str());
        token = strtok(lineCString, "=");
        value = strtok(NULL, "=");
        if (token == NULL || value == NULL) {
            ErrorHandler::nonsystemNonfatal("Parameter file %s has wrong format", parameterFileName);
            return;
        }

        if (!strncmp(token, "minDotArea", strlen(token))) {
            algorithmSettings->setMinDotArea(strtof(value, NULL));
        } else if (!strncmp(token, "maxDotArea", strlen(token))) {
            algorithmSettings->setMaxDotArea(strtof(value, NULL));
        } else if (!strncmp(token, "dotBrightnessThreshold", strlen(token))) {
            algorithmSettings->setDotBrightnessThreshold(strtof(value, NULL));
        } else if (!strncmp(token, "ceilingHeightMeters", strlen(token))) {
            algorithmSettings->setCeilingHeightMetersAndMetersPerPixel(strtof(value, NULL));
        } else if (!strncmp(token, "undistort", strlen(token))) {
            if (!strncmp(value, "true", strlen(value))) {
                algorithmSettings->setUndistort(true);
            } else {
                algorithmSettings->setUndistort(false);
            }
        } else if (!strncmp(token, "brightness", strlen(token))) {
            brightness = strtol(value, NULL, 10);

            if (brightness < 0) {
                brightness = 0;
            } else if (brightness > 100) {
                brightness = 100;
            }

            Utils::setNewBrightness(brightness);
        } else {
            ErrorHandler::nonsystemNonfatal("Option %s is unrecognized", token);
        }
    }

    parameterFile.close();
    algorithmSettings->printVerboseMessage();
}

void Utils::saveParameterAndMap(AlgorithmSettings *algorithmSettings, char* parameterFileName,
        std::map<int, Coordinates>* map, char* markerMapFileName) {

    if (parameterFileName != NULL) {
        std::ofstream parameterFile(parameterFileName, std::ios::out | std::ios::trunc);
        ;
        if (parameterFile.is_open() == false) {
            ErrorHandler::nonsystemNonfatal("Error while opening file %s for writing parameters", parameterFileName);
        } else {
            parameterFile << std::string("undistort=") << std::boolalpha << algorithmSettings->getUndistort() << '\n';
            parameterFile << std::string("minDotArea=") << algorithmSettings->getMinDotArea() << '\n';
            parameterFile << std::string("maxDotArea=") << algorithmSettings->getMaxDotArea() << '\n';
            parameterFile << std::string("dotBrightnessThreshold=") << algorithmSettings->getDotBrightnessThreshold() << '\n';
            parameterFile << std::string("ceilingHeightMeters=") << algorithmSettings->getCeilingHeightMeters() << '\n';
            parameterFile << std::string("brightness=") << algorithmSettings->getDotBrightnessThreshold();
            parameterFile.close();
        }
    } else {
        ErrorHandler::nonsystemNonfatal("Can't save parameters, no file given");
    }

    if (markerMapFileName != NULL) {
        std::ofstream markerMapFile(markerMapFileName, std::ios::out);
        if (markerMapFile.is_open() == false) {
            ErrorHandler::nonsystemNonfatal("Error while opening file %s for writing marker map", markerMapFileName);
        } else {
            typedef std::map<int, Coordinates>::iterator coordinatesMapIterator;
            for (coordinatesMapIterator iterator = map->begin(); iterator != map->end(); iterator++) {
                markerMapFile << iterator->first << ' ' << iterator->second.getX() << ' ' << iterator->second.getY() << ' ' <<
                        iterator->second.getAngle() << '\n';
            }
            markerMapFile.close();
        }
    } else {
        ErrorHandler::nonsystemNonfatal("Can't save marker map, no file given");
    }


}