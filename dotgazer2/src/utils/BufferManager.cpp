#include "utils/BufferManager.hpp"
#include <asm/types.h>          /* for videodev2.h */

#include <linux/videodev2.h>
#include <linux/media.h>
#include <linux/v4l2-subdev.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include "utils/ErrorHandler.hpp"

BufferManager::BufferManager(const int fileDescriptor, const int numberOfBuffers, const enum v4l2_memory memoryType,
        const enum v4l2_buf_type bufferType, bool verbose) :
_fileDescriptor(fileDescriptor), _numberOfBuffers(numberOfBuffers), _memoryType(memoryType), _bufferType(bufferType), _verbose(verbose) {
}

int BufferManager::requestBuffers() {
    struct v4l2_requestbuffers requestBuffers;

    requestBuffers.count = _numberOfBuffers;
    requestBuffers.type = _bufferType;
    requestBuffers.memory = _memoryType;
    if (ioctl(_fileDescriptor, VIDIOC_REQBUFS, &requestBuffers) < 0) {
        ErrorHandler::systemNonfatal("Unable to request buffers for device");
        return -1;
    }
    printf("%u buffers requested\n", _numberOfBuffers);

    switch (_memoryType) {
        case V4L2_MEMORY_MMAP:
        {
            if (_verbose) {
                printf("using MMAP buffers\n");
            }

            return mapMemoryMapBuffers();
            break;
        }
        default:
        {
            ErrorHandler::nonsystemNonfatal("Not supported memory type");
            return -1;
        }
    }

    return 0;
}

int BufferManager::queueBuffer(int bufferNumber) {
    struct v4l2_buffer buffer;

    if (_verbose) {
        printf("Queueing buffer %d\n", bufferNumber);
    }

    buffer.index = bufferNumber;
    buffer.type = _bufferType;
    buffer.memory = _memoryType;
    buffer.length = _buffers[bufferNumber].size;

    if (ioctl(_fileDescriptor, VIDIOC_QBUF, &buffer) < 0) {
        ErrorHandler::systemNonfatal("Unable to queue buffer %d", bufferNumber);
        return -1;
    }

    return 0;
}

int BufferManager::queueBuffers() {
    int returnValue;

    for (int i = 0; i < _numberOfBuffers; ++i) {
        returnValue = queueBuffer(i);
        if (returnValue < 0) {
            ErrorHandler::nonsystemNonfatal("Unable to queue all buffers \n");
            return returnValue;
        }
    }

    return 0;
}

int BufferManager::freeBuffers() {
    struct v4l2_requestbuffers requestBuffers;
    memset(&requestBuffers, 0, sizeof requestBuffers);


    switch (_memoryType) {
        case V4L2_MEMORY_MMAP:
            if (freeMemoryMapBuffers() < 0) {
                return -1;
            }
            break;
        default:
            ErrorHandler::nonsystemNonfatal("Unsupported memory type");
            break;
    }


    requestBuffers.count = 0;
    requestBuffers.type = _bufferType;
    requestBuffers.memory = _memoryType;
    if (ioctl(_fileDescriptor, VIDIOC_REQBUFS, &requestBuffers) < 0) {
        ErrorHandler::systemNonfatal("Unable to release buffers");
        return -1;
    }
    printf("%u buffers released.\n", _numberOfBuffers);

    free(_buffers);
    _buffers = NULL;
    _numberOfBuffers = 0;

    return 0;
}

enum v4l2_memory BufferManager::getMemeoryType() const {
    return _memoryType;
}

enum v4l2_buf_type BufferManager::getBufferType() const {
    return _bufferType;
}

int BufferManager::getNumberOfBuffers() const {
    return _numberOfBuffers;
}

struct BufferManager::Buffer BufferManager::getBuffer(int bufferNumber) {

    if (_verbose) {
        printf("Getting %d buffer\n", bufferNumber);
    }

    return _buffers[bufferNumber];
}

int BufferManager::dequeueBuffer(struct v4l2_buffer * buffer) {

    if (_verbose) {
        printf("Dequeing buffer\n");
    }

    memset(buffer, 0, sizeof (struct v4l2_buffer));
    buffer->type = _bufferType;
    buffer->memory = _memoryType;

    if (ioctl(_fileDescriptor, VIDIOC_DQBUF, buffer) < 0) {
        ErrorHandler::systemNonfatal("Error while dequeuing buffer");
        buffer = NULL;
        return -1;
    }

    return 0;
}

int BufferManager::queryForBuffer(struct v4l2_buffer *buffer, __u32 index) {

    if (_verbose) {
        printf("Querying buffer %u\n", index);
    }

    buffer->index = index;
    buffer->type = _bufferType;
    buffer->memory = _memoryType;

    return ioctl(_fileDescriptor, VIDIOC_QUERYBUF, buffer);
}

int BufferManager::mapMemoryMapBuffers() {
    struct v4l2_buffer buffer;
    struct Buffer *buffers;

    buffers = (struct Buffer*) malloc(sizeof (struct Buffer) * _numberOfBuffers);
    if (buffers == NULL) {
        ErrorHandler::systemNonfatal("Error while allocating buffer information array");
        return -1;
    }

    for (int i = 0; i < _numberOfBuffers; ++i) {
        memset(&buffer, 0, sizeof buffer);

        if (queryForBuffer(&buffer, i)) {
            ErrorHandler::systemNonfatal("Unable to query for buffer %u", i);
            return -1;
        }
        printf("Buffer information: length %u offset %u\n", buffer.length, buffer.m.offset);

        buffers[i].memory = mmap(0, buffer.length, PROT_READ | PROT_WRITE, MAP_SHARED, _fileDescriptor, buffer.m.offset);
        if (buffers[i].memory == MAP_FAILED) {
            ErrorHandler::systemNonfatal("Unable to map buffer %u", i);
            return -1;
        }
        buffers[i].size = buffer.length;
        printf("Buffer %u mapped at address %p.\n", i, buffers[i].memory);
    }
    _buffers = buffers;

    printf("Finished MMAP buffer mapping\n");

    return 0;
}

int BufferManager::freeMemoryMapBuffers() {
    for (int i = 0; i < _numberOfBuffers; ++i) {
        if (munmap(_buffers[i].memory, _buffers[i].size) < 0) {
            ErrorHandler::systemNonfatal("Unable to unmap buffer %u", i);
            return -1;
        }
        _buffers[i].memory = NULL;
    }

    printf("Finished freeing MMAP buffers\n");

    return 0;
}
