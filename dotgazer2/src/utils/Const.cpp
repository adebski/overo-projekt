#include "utils/Const.hpp"

float Const::cameraMatrix[] = {
    5.8895792365920408e+02, 0.0, 3.7550000000000000e+02,
    0.0, 5.8895792365920408e+02, 2.3950000000000000e+02,
    0.0, 0.0, 1.0
};

float Const::distCoeffs[] = {
    -3.4798304542437747e-01, 7.8413129351129526e-02, 0.0, 0.0, 1.2320627783150140e-01
};

const char* Const::CONFIG_FILE = "markers_config.txt";
const char* Const::GMOCK_OPTIONS = "-gmock_verbose=info";

const CvMat Const::CAMERA_MATRIX = cvMat(3, 3, CV_32FC1, cameraMatrix);
const CvMat Const::DIST_COEFFS = cvMat(1, 5, CV_32FC1, distCoeffs);