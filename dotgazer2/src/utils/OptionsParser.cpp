#include "utils/OptionsParser.hpp"
#include "main_strategy/MainStrategy.hpp"
#include "main_strategy/MainServerStrategy.hpp"
#include "main_strategy/MainOneMarkerDebugStrategy.hpp"
#include "main_strategy/MainGoogleTestStrategy.hpp"
#include "main_strategy/MainPerformanceTestStrategy.hpp"
#include "main_strategy/MainRawToJpgConverterStrategy.hpp"
#include "websocket/MainServerWebsocketService.hpp"
#include "utils/ErrorHandler.hpp"
#include "utils/CVUtils.hpp"
#include "accelerometer/Minimu9Accelerometer.hpp"
#include "accelerometer/Minimu9v2Accelerometer.hpp"
#include "accelerometer/AccelerometerStub.hpp"
#include <cstdlib>
#include <getopt.h>
#include <cstdio>
#include "dotgazer/AlgorithmSettings.hpp"
#include "binarization_strategy/RawBinarizationStrategy.hpp"
#include "binarization_strategy/RawInterpolatedBinarizationStrategy.hpp"
#include "camera/CameraStub.hpp"
#include "marker_detection_strategy/SimpleMarkerDetectionStrategy.hpp"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "utils/Utils.hpp"
#include "main_strategy/MainMatrixCreationStrategy.hpp"
#include "dot_detection_strategy/DotDetectionStrategy.hpp"
#include "dot_detection_strategy/OpencvDotDetectionStrategy.hpp"

Camera *OptionsParser::createCamera() {
    switch (cameraModel) {
        case MT9V032:
            return new Mt9v032Camera("/dev/video2", verboseText);
            break;
        case CAMERA_STUB:
            return new CameraStub(fileName);
            break;
        default:
            printf("Unrecognized camera model\n");
            return NULL;
            break;
    }
}

Accelerometer *OptionsParser::createAccelerometer() {
    switch (accelerometerVersion) {
        case MINIMU_9:
            return new Minimu9Accelerometer("/dev/i2c-3");
            break;
        case MINIMU_9V2:
            return new Minimu9v2Accelerometer("/dev/i2c-3");
            break;
        case ACCELEROMETER_STUB:
            return new AccelerometerStub();
            break;
        default:
            printf("Unrecognized accelerometer version\n");
            return NULL;
            break;
    }
}

BinarizationStrategy *OptionsParser::createBinarizationStrategy(ImageConverter *imageConverter, CvSize maskSize,
        AlgorithmSettings *algorithmSettings) {
    switch (binarizationStrategyType) {
        case RAW_BINARIZATION_STRATEGY:
            return RawBinarizationStrategy::getInstance(maskSize, verboseText, verboseImage, algorithmSettings);
            break;
        case RAW_INTERPOLATED_BINARIZATION_STRATEGY:
            return RawInterpolatedBinarizationStrategy::getInstance(maskSize, verboseText, verboseImage, algorithmSettings);
            break;
        case BGR_BINARIZATION_STRATEGY:
            return BgrBinarizationStrategy::getInstance(imageConverter, maskSize, verboseText, verboseImage,
                    algorithmSettings);
            break;
        default:
            printf("Unrecognized binarization strategy type\n");
            return NULL;
            break;
    }

    return NULL;
}

OptionsParser::OptionsParser() {
    verboseText = false;
    verboseImage = false;
    accMeasurements = 1;
    iterations = 1;
    accelerometerVersion = MINIMU_9V2;
    cameraModel = MT9V032;
    binarizationStrategyType = RAW_BINARIZATION_STRATEGY;
    serialDevice = NULL;
    performDaemonization = false;
    parameterFileName = NULL;
    mapFileName = NULL;
}

void OptionsParser::parseCmdLine(int argc, char** argv) {
    int option;

    if (argc < 2) {
        printUsage();
        exit(0);
    }

    while ((option = getopt(argc, argv, "f:m:r:a:c:b:d:t:s:p:n:ehvi")) != -1) {
        switch (option) {
            case 'f':
                fileName = optarg;
                break;
            case 'm':
                mainFunction = strtol(optarg, NULL, 10);
                break;
            case 'r':
                accMeasurements = strtol(optarg, NULL, 10);
                break;
            case 'h':
                printUsage();
                break;
            case 'v':
                verboseText = true;
                break;
            case 'i':
                verboseImage = true;
                break;
            case 'a':
                if (static_cast<int> (ACCELEROMETER_VERSION_MAX) <= strtol(optarg, NULL, 10)) {
                    printf("Unrecognized accelerometer version\n");
                    exit(-1);
                } else {
                    accelerometerVersion = static_cast<AccelerometerVersion> (strtol(optarg, NULL, 10));
                }
                break;
            case 'c':
                if (static_cast<int> (CAMERA_MODEL_MAX) <= strtol(optarg, NULL, 10)) {
                    printf("Unrecognized camera model\n");
                    exit(-1);
                } else {
                    cameraModel = static_cast<CameraModel> (strtol(optarg, NULL, 10));
                }
                break;
            case 'b':
                if (static_cast<int> (BINARIZATION_STRATEGY_MAX) <= strtol(optarg, NULL, 10)) {
                    printf("Unrecognized binarization strategy type\n");
                    exit(-1);
                } else {
                    binarizationStrategyType = static_cast<BinarizationStrategyType> (strtol(optarg, NULL, 10));
                }
                break;
            case 't':
                iterations = strtol(optarg, NULL, 10);
                break;
            case 's':
                serialDevice = optarg;
                break;
            case 'e':
                performDaemonization = true;
                break;
            case 'p':
                parameterFileName = optarg;
                break;
            case 'n':
                mapFileName = optarg;
                break;
            default:
                printf("Unsupported option - %c\n", option);
                exit(-1);
        }
    }
}

int OptionsParser::invokeMain() {
    MainStrategy* strategy = NULL;
    Camera *camera = NULL;
    Accelerometer *accelerometer = NULL;
    LocalizationStrategy *localizationStrategy = NULL;
    MappingStrategy *mappingStrategy = NULL;
    BinarizationStrategy *binarizationStrategy = NULL;
    DotDetectionStrategy *dotDetectionStrategy = NULL;
    MarkerDetectionStrategy *markerDetectionStrategy = NULL;
    CvSize maskSize = cvSize(Const::IMAGE_WIDTH, Const::IMAGE_HEIGHT);
    ImageConverter* converterForBinarizationStrategy = NULL;
    ImageConverter* converterForWebsocketService = NULL;
    CVUtils *cvUtils = NULL;
    int serialDeviceDescriptor = -1;

    AlgorithmSettings* algorithmSettings = new AlgorithmSettings(mainFunction);

     if (parameterFileName != NULL) {
                Utils::readParameterFile(parameterFileName, algorithmSettings);
    }

    switch (mainFunction) {
        case 1:
            accelerometer = createAccelerometer();
            camera = createCamera();
            converterForBinarizationStrategy = ImageConverter::getInstance();
            converterForWebsocketService = ImageConverter::getInstance();
            cvUtils = new CVUtils();

            if (accelerometer == NULL) {
                ErrorHandler::nonsystemFatal("Error during accelerometer initalization");
                return -1;
            }
            if (camera == NULL) {
                ErrorHandler::nonsystemFatal("Error during camera initalization");
                return -1;
            }
            if (converterForBinarizationStrategy == NULL) {
                ErrorHandler::nonsystemFatal("Error during image converter for binarization strategy initalization");
                return -1;
            }
            if (converterForWebsocketService == NULL) {
                ErrorHandler::nonsystemFatal("Error during image converter for websocket service initalization");
                return -1;
            }
            if (cvUtils == NULL) {
                ErrorHandler::nonsystemFatal("Error during cvUtils initalization");
                return -1;
            }
            globalMainServerWebsocketService = MainServerWebsocketService::getInstance(cvUtils, converterForWebsocketService,
                    verboseText, verboseImage, parameterFileName, mapFileName);
            if (globalMainServerWebsocketService == NULL) {
                ErrorHandler::nonsystemFatal("Error during websocket service initalization");
                return -1;
            }

            localizationStrategy = new OneMarkerLocalizationStrategy(verboseText, verboseImage, algorithmSettings);
            mappingStrategy = new OneMarkerMappingStrategy(verboseText, verboseImage, algorithmSettings);
            binarizationStrategy = createBinarizationStrategy(converterForBinarizationStrategy, maskSize, algorithmSettings);
            dotDetectionStrategy = OpencvDotDetectionStrategy::getInstance(verboseText, verboseImage, algorithmSettings);
            markerDetectionStrategy = SimpleMarkerDetectionStrategy::getInstance(verboseText, verboseImage, algorithmSettings);

            if (localizationStrategy == NULL) {
                ErrorHandler::nonsystemFatal("Error during localizationStrategy creation");
                return -1;
            }
            if (mappingStrategy == NULL) {
                ErrorHandler::nonsystemFatal("Error during mappingStrategy creation");
                return -1;
            }
            if (binarizationStrategy == NULL) {
                ErrorHandler::nonsystemNonfatal("Error while creating binarizationStrategy");
                return -1;
            }
            if (markerDetectionStrategy == NULL) {
                ErrorHandler::nonsystemNonfatal("Error while creating markerDetectionStrategy");
                return -1;
            }

            if (serialDevice != NULL) {
                serialDeviceDescriptor = open(serialDevice, O_RDWR);

                if (serialDeviceDescriptor < 0) {
                    ErrorHandler::systemNonfatal("Error while opening serial device %s", serialDevice);
                    return -1;
                }
            }

            if (mapFileName != NULL) {
                mappingStrategy->loadMapFromFile(mapFileName);
            }

            strategy = new MainServerStrategy(camera, accelerometer, localizationStrategy,
                    mappingStrategy, binarizationStrategy, markerDetectionStrategy, dotDetectionStrategy,
                    accMeasurements, verboseText, verboseImage, algorithmSettings,
                    globalMainServerWebsocketService, serialDeviceDescriptor, performDaemonization);
            break;
        case 2:
            strategy = new MainOneMarkerDebugStrategy(fileName, verboseText, verboseImage, algorithmSettings);
            break;
        case 3:
            strategy = new MainMatrixCreationStrategy(fileName, verboseText, verboseImage, algorithmSettings);
            break;
        case 4:
            strategy = new MainGoogleTestStrategy();
            break;
        case 5:
            accelerometer = createAccelerometer();
            camera = createCamera();
            converterForBinarizationStrategy = ImageConverter::getInstance();
            converterForWebsocketService = ImageConverter::getInstance();

            if (accelerometer == NULL) {
                ErrorHandler::nonsystemFatal("Error during accelerometer initalization");
                return -1;
            }
            if (camera == NULL) {
                ErrorHandler::nonsystemFatal("Error during camera initalization");
                return -1;
            }
            if (converterForBinarizationStrategy == NULL) {
                ErrorHandler::nonsystemFatal("Error during image converter initalization");
                return -1;
            }
            if (converterForWebsocketService == NULL) {
                ErrorHandler::nonsystemFatal("Error during image converter for websocket service initalization");
                return -1;
            }

            localizationStrategy = new OneMarkerLocalizationStrategy(verboseText, verboseImage, algorithmSettings);
            mappingStrategy = new OneMarkerMappingStrategy(verboseText, verboseImage, algorithmSettings);
            binarizationStrategy = createBinarizationStrategy(converterForBinarizationStrategy, maskSize, algorithmSettings);
            dotDetectionStrategy = OpencvDotDetectionStrategy::getInstance(verboseText, verboseImage, algorithmSettings);
            markerDetectionStrategy = SimpleMarkerDetectionStrategy::getInstance(verboseText, verboseImage, algorithmSettings);

            if (localizationStrategy == NULL) {
                ErrorHandler::nonsystemFatal("Error during localizationStrategy creation");
                return -1;
            }
            if (mappingStrategy == NULL) {
                ErrorHandler::nonsystemFatal("Error during mappingStrategy creation");
                return -1;
            }
            if (binarizationStrategy == NULL) {
                ErrorHandler::nonsystemNonfatal("Error while creating binarizationStrategy");
                return -1;
            }
            if (markerDetectionStrategy == NULL) {
                ErrorHandler::nonsystemNonfatal("Error while creating markerDetectionStrategy");
                return -1;
            }

            if (serialDevice != NULL) {
                serialDeviceDescriptor = open(serialDevice, O_RDWR);

                if (serialDeviceDescriptor < 0) {
                    ErrorHandler::systemNonfatal("Error while opening serial device %s", serialDevice);
                    return -1;
                }
            }

            globalMainServerWebsocketService = MainServerWebsocketService::getInstance(cvUtils, converterForWebsocketService,
                    verboseText, verboseImage, parameterFileName, mapFileName);
            if (globalMainServerWebsocketService == NULL) {
                ErrorHandler::nonsystemFatal("Error during websocket service initalization");
                return -1;
            }
            strategy = new MainPerformanceTestStrategy(camera, accelerometer, localizationStrategy, mappingStrategy,
                    binarizationStrategy, dotDetectionStrategy, markerDetectionStrategy, accMeasurements,
                    algorithmSettings, iterations, serialDeviceDescriptor, globalMainServerWebsocketService, parameterFileName);
            break;
        case 6:
            converterForBinarizationStrategy = ImageConverter::getInstance();
            strategy = new MainRawToJpgConverterStrategy(converterForBinarizationStrategy, fileName);
            break;
        default:
            return -1;
    }

    if (performDaemonization) {
        if (daemonize() != 0) {
            ErrorHandler::nonsystemNonfatal("Daemonize failed, exiting");
            delete strategy;
            return -1;
        }
    }

    int res = strategy->main();
    delete strategy;
    return res;
}

void OptionsParser::printUsage() {
    printf("-h print help\n");
    printf("-f image file\n");
    printf("-v verbose (text messages)\n");
    printf("-i verbose (images)\n");
    printf("-r [num] - number of accelerometer measurements per frame (0 - disabled, default - 1)\n");
    printf("-a [num] - accelerometer version\n");
    printf("\t0 - MinimuV1\n");
    printf("\t1 - MinimuV2 - default\n");
    printf("\t2 - Stub\n");
    printf("-c [num] camera model\n ");
    printf("\t0 - mt9v032 - default\n");
    printf("\t1 - stub\n");
    printf("-b [num] - binarization strategy type\n");
    printf("\t0 - raw image binarization strategy - default\n");
    printf("\t1 - raw interpolated binarization strategy\n");
    printf("\t2 - bgr image binarization strategy - low performance\n");
    printf("-t [num] - number of iterations for performance strategies\n");
    printf("-m main function:\n");
    printf("\t1 Start the server\n");
    printf("\t2 Debug using 1 marker localization\n");
    printf("\t3 Create image correcting matrix\n");
    printf("\t4 Run gtest/gmock tests\n");
    printf("\t5 Run performance test\n");
    printf("\t6 Convert given raw image to jpg image\n");
    printf("-s [serialDevice], if not given, results are not written to serial device\n");
    printf("-e start as daemon process\n");
    printf("-p file with algorithm parameters and default brightness\n");
    printf("-n file with markers configuration\n");
    return;
}

int OptionsParser::daemonize() {
    /* Our process ID and Session ID */
    pid_t pid, sid;

    /* Fork off the parent process */
    pid = fork();
    if (pid < 0) {
        ErrorHandler::systemNonfatal("Error while forking");
        return -1;
    }
    /* If we got a good PID, then
       we can exit the parent process. */
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    /* Change the file mode mask */
    umask(0);

    /* Create a new SID for the child process */
    sid = setsid();
    if (sid < 0) {
        ErrorHandler::systemNonfatal("Error while setting SID");
        return -1;
    }



    /* Change the current working directory */
    if ((chdir("/")) < 0) {
        ErrorHandler::systemNonfatal("Error while setting SID");
        return -1;
    }

    return 0;
}