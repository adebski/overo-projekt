#include "utils/ImageConverter.hpp"
#include "utils/ErrorHandler.hpp"
#include "utils/Const.hpp"
#include <fcntl.h>
#include <unistd.h>
#include <opencv2/highgui/highgui_c.h>

ImageConverter* ImageConverter::getInstance() {
    ImageConverter* converter = new ImageConverter();
    CvSize imageSize = cvSize(Const::IMAGE_WIDTH, Const::IMAGE_HEIGHT);

    if ((converter->rescaledImage = cvCreateImage(imageSize, IPL_DEPTH_8U, 1)) == NULL) {
        ErrorHandler::nonsystemNonfatal("error while allocating memory for rescaled image");
        return NULL;
    }

    if ((converter->yuvImage = cvCreateImage(imageSize, IPL_DEPTH_8U, 3)) == NULL) {
        ErrorHandler::nonsystemNonfatal("error while allocating memory for yuv image");
        return NULL;
    }

    if ((converter->yChannel = cvCreateImage(imageSize, IPL_DEPTH_8U, 1)) == NULL) {
        ErrorHandler::nonsystemNonfatal("error while allocating memory for y channel");
        return NULL;
    }

    if ((converter->uChannel = cvCreateImage(imageSize, IPL_DEPTH_8U, 1)) == NULL) {
        ErrorHandler::nonsystemNonfatal("error while allocating memory for u channel");
        return NULL;
    }

    if ((converter->vChannel = cvCreateImage(imageSize, IPL_DEPTH_8U, 1)) == NULL) {
        ErrorHandler::nonsystemNonfatal("error while allocating memory for v channel");
        return NULL;
    }

    if ((converter->rawImage = cvCreateImage(imageSize, IPL_DEPTH_16U, 1)) == NULL) {
        ErrorHandler::nonsystemNonfatal("error while allocating memory for raw image");
        return NULL;
    }

    converter->cameraImage = new CameraImage();

    return converter;
}

IplImage* ImageConverter::loadRawImage(const char* path) {
    IplImage *rawImage;
    CvSize size;
    size.height = Const::IMAGE_HEIGHT;
    size.width = Const::IMAGE_WIDTH;

    if ((rawImage = cvCreateImage(size, IPL_DEPTH_16U, 1)) == NULL) {
        ErrorHandler::nonsystemNonfatal("error while allocating memory for raw image");
        return NULL;
    }

    if ((rawImage->imageData = loadRawData(path, Const::IMAGE_HEIGHT * Const::IMAGE_WIDTH * 2)) == NULL) {
        cvReleaseImage(&rawImage);
        return NULL;
    }

    return rawImage;
}

CameraImage* ImageConverter::loadCameraImage(const char* path) {
    if (cameraImage->image != NULL) {
        free(cameraImage->image);
    }

    FILE* file = fopen(path, "rb");
    size_t lSize;
    void* buffer;
    size_t result;

    if (file == NULL) {
        ErrorHandler::systemFatal("couldn't open image file %s for reading", path);
    }
    // obtain file size:
    fseek(file, 0, SEEK_END);
    lSize = ftell(file);
    rewind(file);
    // allocate memory to contain the whole file:
    buffer = (void*) malloc(sizeof (char)*lSize);
    if (buffer == NULL) {
        ErrorHandler::systemFatal("Couldn't allocate buffer for reading file %s", path);
    }
    // copy the file into the buffer:
    result = fread(buffer, 1, lSize, file);
    if (result != lSize) {
        ErrorHandler::systemFatal("Couldn't read the whole file %s", path);
    }
    fclose(file);

    cameraImage->length = lSize;
    cameraImage->image = buffer;
    return cameraImage;
}

IplImage* ImageConverter::createIplImageStructure(int width, int heigth, int depth, int channels) {
    IplImage *image;
    CvSize size = {width, heigth};

    if ((image = cvCreateImage(size, depth, channels)) == NULL) {
        ErrorHandler::systemNonfatal("Error while allocating memory for image");
        return NULL;
    }

    return image;
}

IplImage* ImageConverter::convertRawToBGR(IplImage *rawImage, IplImage *bgrImage) {
    cvConvertScale(rawImage, rescaledImage, 0.25, 0.0);
    cvCvtColor(rescaledImage, bgrImage, CV_BayerGB2BGR);
    return bgrImage;
}

IplImage* ImageConverter::convertRawToBGR(CameraImage *rawImage, IplImage *bgrImage) {
    this->rawImage->imageData = (char*) rawImage->image;
    convertRawToBGR(this->rawImage, bgrImage);
    this->rawImage->imageData = NULL;
    return bgrImage;
}

IplImage* ImageConverter::convertRawToBGR(IplImage *rawImage) {
    IplImage *bgrImage;
    CvSize imageSize = cvGetSize(rawImage);

    if ((bgrImage = cvCreateImage(imageSize, IPL_DEPTH_8U, 3)) == NULL) {
        ErrorHandler::nonsystemNonfatal("error while allocating memory for rescaled image");
        return NULL;
    }

    return convertRawToBGR(rawImage, bgrImage);
}

IplImage* ImageConverter::getYFromBGR(IplImage *bgrImage) {
    cvCvtColor(bgrImage, yuvImage, CV_BGR2YCrCb);
    cvSplit(yuvImage, yChannel, uChannel, vChannel, NULL);
    return yChannel;
}

char* ImageConverter::loadRawData(const char *path, const int size) {
    int fd;
    char* buf;
    int ret;

    if ((fd = open(path, O_RDONLY)) == -1) {
        ErrorHandler::systemNonfatal("error while opening file %s", path);
        return NULL;
    }

    if ((buf = (char*) malloc(sizeof (char) * size)) == NULL) {
        ErrorHandler::systemNonfatal("error while allocating memory for buffer");
        close(fd);
        return NULL;
    }

    if ((ret = read(fd, buf, size)) != size) {
        ErrorHandler::systemNonfatal("read %d bytes, should %d", ret, size);
        free(buf);
        close(fd);
        return NULL;
    }

    close(fd);
    return buf;
}