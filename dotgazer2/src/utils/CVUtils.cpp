#include "utils/CVUtils.hpp"
#include "utils/Const.hpp"
#include "opencv/highgui.h"

void CVUtils::drawCircle(IplImage* img, float x, float y, int r, int hue, int thickness) {
    drawCircle(img, x, y, r, hue, hue, hue, thickness);
}

void CVUtils::drawCircle(IplImage* img, float x, float y, int radius, int r, int g, int b, int thickness) {
    cvCircle(img, cvPoint(cvRound(x), Const::IMAGE_HEIGHT - cvRound(y)), radius,
            CV_RGB(r, g, b), thickness, CV_AA, 0);
}

void CVUtils::undistortImage(IplImage* img) {
    IplImage* tmp = (IplImage*) cvClone(img);
    cvUndistort2(tmp, img, &Const::CAMERA_MATRIX, &Const::DIST_COEFFS);
    cvReleaseImage(&tmp);
}