#include <cstdlib>
#include "utils/OptionsParser.hpp"

using namespace std;

int main(int argc, char** argv) {
    OptionsParser optionsParser;
    optionsParser.parseCmdLine(argc, argv);
    int ret = optionsParser.invokeMain();
    return ret;
}

