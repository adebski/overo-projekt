#include "binarization_strategy/BgrBinarizationStrategy.hpp"

#include "opencv/cv.h"
#include "opencv/highgui.h"
#include "utils/ImageConverter.hpp"
#include "dotgazer/AlgorithmSettings.hpp"
#include "utils/Const.hpp"
#include <netinet/in.h>
#include "utils/Utils.hpp"

BgrBinarizationStrategy* BgrBinarizationStrategy::getInstance(ImageConverter* converter,
        CvSize maskSize, bool verboseText, bool verboseImage, AlgorithmSettings* algorithmSettings) {
    BgrBinarizationStrategy* instance = new BgrBinarizationStrategy();
    instance->verboseText = verboseText;
    instance->verboseImage = verboseImage;
    instance->converter = converter;
    instance->algorithmSettings = algorithmSettings;
    if ((instance->mask = cvCreateImage(maskSize, IPL_DEPTH_8U, 1)) == NULL) return NULL;
    if ((instance->bgrImage = cvCreateImage(maskSize, IPL_DEPTH_8U, 3)) == NULL) return NULL;
    return instance;
}

IplImage* BgrBinarizationStrategy::binarizeImage(CameraImage* rawImage) {
    IplImage* img;

    converter->convertRawToBGR(rawImage, bgrImage);

    if ((img = converter->getYFromBGR(bgrImage)) == NULL) {
        return NULL;
    }

    if (verboseText) {
        printf("Fetched Y channel from BGR image\n");
    }

    if (verboseImage) {
        cvNamedWindow("Image view", 1);
        cvShowImage("Image view", img);
        cvWaitKey(0);
    }

    cvInRangeS(img, cvScalar(algorithmSettings->getDotBrightnessThreshold(), 0, 0, 0), cvScalar(256, 0, 0, 0), mask);
    if (verboseImage) {
        cvShowImage("Image view", mask);
        cvWaitKey(0);
    }

    return mask;
}

IplImage* BgrBinarizationStrategy::getIplImageAllocatedForBinarizedImage() {
    return mask;
}