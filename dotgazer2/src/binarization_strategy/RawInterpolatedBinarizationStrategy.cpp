#include "binarization_strategy/RawInterpolatedBinarizationStrategy.hpp"
#include "opencv/cv.h"
#include "utils/Const.hpp"
#include "utils/Utils.hpp"
#include "arpa/inet.h"  
#include "opencv/highgui.h"

RawInterpolatedBinarizationStrategy* RawInterpolatedBinarizationStrategy::getInstance(
        CvSize maskSize, bool verboseText,
        bool verboseImage, AlgorithmSettings* algorithmSettings) {
    RawInterpolatedBinarizationStrategy* instance = new RawInterpolatedBinarizationStrategy();
    instance->verboseText = verboseText;
    instance->verboseImage = verboseImage;
    instance->algorithmSettings = algorithmSettings;
    if ((instance->mask = cvCreateImage(maskSize, 8, 1)) == NULL) return NULL;
    return instance;
}

IplImage* RawInterpolatedBinarizationStrategy::binarizeImage(CameraImage* rawImage) {
    uint8_t *rawPtr8 = (uint8_t*) rawImage->image;
    uint16_t *rawPtr16 = (uint16_t*) rawImage->image;
    uchar *maskPtr = (uchar*) mask->imageData;
    int widthStep = mask->widthStep;
    uint16_t rawValue1, rawValue2;
    uint16_t raw8_1;
    uint16_t raw8_2;
    int dotBrightnessThreshold = (algorithmSettings->getDotBrightnessThreshold());
    dotBrightnessThreshold = dotBrightnessThreshold << 2;


    if (!Utils::isSystemBigEndian()) {
        for (int y = 0; y < Const::IMAGE_HEIGHT; y++) {
            for (int x = 0; x < Const::IMAGE_WIDTH; x += 2) {
                rawValue1 = *rawPtr16;
                ++rawPtr16;
                rawValue2 = *rawPtr16;
                ++rawPtr16;
                if (y % 2 == 0) {
                    rawValue2 = rawValue1;
                } else {
                    rawValue1 = rawValue2;
                }
                maskPtr[x] = (rawValue1) >= dotBrightnessThreshold ? 255 : 0;
                maskPtr[x + 1] = (rawValue2) >= dotBrightnessThreshold ? 255 : 0;
            }
            maskPtr += widthStep;
        }
    } else {
        for (int y = 0; y < Const::IMAGE_HEIGHT; y++) {
            for (int x = 0; x < Const::IMAGE_WIDTH; x++) {
                raw8_1 = *rawPtr8;
                ++rawPtr8;
                raw8_2 = (*rawPtr8) << 8;
                ++rawPtr8;
                maskPtr[x] = (((raw8_1 | raw8_2))) >= dotBrightnessThreshold ? 255 : 0;
            }
            maskPtr += widthStep;
        }
    }

    if (verboseImage) {
        cvShowImage("Image view", mask);
        cvWaitKey(0);
    }
    return mask;
}

IplImage* RawInterpolatedBinarizationStrategy::getIplImageAllocatedForBinarizedImage() {
    return mask;
}