#include "accelerometer/Minimu9v2Accelerometer.hpp"
#include "utils/ErrorHandler.hpp"
#include "utils/Const.hpp"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>

Minimu9v2Accelerometer::Minimu9v2Accelerometer() {
}

Minimu9v2Accelerometer::Minimu9v2Accelerometer(const char* deviceName) {
    // connect to i2c
    if ((fileDescriptor = open(deviceName, O_RDWR)) == -1) {
        ErrorHandler::systemFatal("Accelerometer - open");
        exit(EXIT_FAILURE);
    };

    // set the other side as slave
    // najprawodpodobniej 0x19, opisane na stronie 20
    if (ioctl(fileDescriptor, I2C_SLAVE, 0x19) == -1) {
        ErrorHandler::systemFatal("Accelerometer - ioctl");
        exit(EXIT_FAILURE);
    }

    // set the control registry
    // rejest kontrolny jest pod 0x20 (strona 24)
    // 10Hz - 0x27
    // 25Hz - 0x37
    // 50Hz - 0x47
    // istnieja jeszcze inne rejestry kontrolne, opisane na str. 25-28
    uint8_t ctrl[] = {0x20, 0x27};
    if (write(fileDescriptor, ctrl, 2) == -1) {
        ErrorHandler::systemFatal("Accelerometer - write");
        exit(EXIT_FAILURE);
    }

    uint8_t ctrl2[] = {0x23, 0x08};
    if (write(fileDescriptor, ctrl2, 2) == -1) {
        ErrorHandler::systemFatal("Accelerometer - write");
        exit(EXIT_FAILURE);
    }
    //    uint8_t ctrl2[] = {0x21, 0x03};
    //    if (write(fileDescriptor, ctrl2, 2) == -1) {
    //        ErrorHandler::systemFatal("Accelerometer - write");
    //        exit(EXIT_FAILURE);
    //    }

    initialTilt = Tilt(0, 0, 0);
}

void Minimu9v2Accelerometer::calculateInitialTilt() {
    initialTilt = getRawTilt();
}

Tilt Minimu9v2Accelerometer::getTilt() {
    return getRawTilt() - initialTilt;
}

Tilt Minimu9v2Accelerometer::getRawTilt() {
    return Tilt(
            -(float) convertToAngle(get(0x29), get(0x28)), //X
            -(float) convertToAngle(get(0x2b), get(0x2a)), //Y
            (float) convertToAngle(get(0x2d), get(0x2c))); //Z    
}

Tilt Minimu9v2Accelerometer::getAverageTilt(int mesurements, int milis) {
    Tilt tilt;
    float x = 0;
    float y = 0;
    float z = 0;

    if (mesurements == 0) {
        return tilt;
    }

    for (int i = 0; i < mesurements; ++i) {
        tilt = getTilt();
        x += tilt.getX();
        y += tilt.getY();
        z += tilt.getZ();
        if (i < mesurements - 1) {
            usleep(1000 * milis);
        }
    }

    return Tilt(x / mesurements, y / mesurements, z / mesurements);
}

uint8_t Minimu9v2Accelerometer::get(uint8_t addr) {
    uint8_t data[] = {addr};
    if (write(fileDescriptor, data, 1) == -1) {
        ErrorHandler::systemNonfatal("Accelerometer - get.write");
    }
    if (read(fileDescriptor, data, 1) == -1) {
        ErrorHandler::systemNonfatal("Accelerometer - get.read");
    }
    return data[0];
}

float Minimu9v2Accelerometer::convertToAngle(uint8_t h, uint8_t l) {
    // combine high and low bytes, then shift right to discard lowest 4 bits (which are meaningless)
    int16_t intRes = ((int16_t) (h << 8 | l)) >> 4;
    float res = ((float) intRes) / 2048.0f * Const::PI;
    return res;
}