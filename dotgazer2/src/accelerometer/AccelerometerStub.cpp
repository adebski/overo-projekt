#include "accelerometer/AccelerometerStub.hpp"
#include "dotgazer/Tilt.hpp" 

Tilt AccelerometerStub::getTilt() {
    return tilt;
}

Tilt AccelerometerStub::getAverageTilt(int mesurements, int milis) {


    if (mesurements <= 1) {
        return tilt;
    }

    usleep(1000 * milis * (mesurements - 1));
    return tilt;
}

void AccelerometerStub::calculateInitialTilt() {
}