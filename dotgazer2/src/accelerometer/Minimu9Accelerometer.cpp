#include "accelerometer/Minimu9Accelerometer.hpp"
#include "utils/ErrorHandler.hpp"
#include "utils/Const.hpp"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>

Minimu9Accelerometer::Minimu9Accelerometer() {
}

Minimu9Accelerometer::Minimu9Accelerometer(const char* deviceName) {
    // connect to i2c
    if ((fileDescriptor = open(deviceName, O_RDWR)) == -1) {
        ErrorHandler::systemFatal("Accelerometer - open");
        exit(EXIT_FAILURE);
    };

    // set the other side as slave
    if (ioctl(fileDescriptor, I2C_SLAVE, 0x18) == -1) {
        ErrorHandler::systemFatal("Accelerometer - ioctl");
        exit(EXIT_FAILURE);
    }

    // set the control registry
    uint8_t ctrl[] = {0x20, 0x27};
    if (write(fileDescriptor, ctrl, 2) == -1) {
        ErrorHandler::systemFatal("Accelerometer - write");
        exit(EXIT_FAILURE);
    }
    //    uint8_t ctrl2[] = {0x21, 0x03};
    //    if (write(fileDescriptor, ctrl2, 2) == -1) {
    //        ErrorHandler::systemFatal("Accelerometer - write");
    //        exit(EXIT_FAILURE);
    //    }
}

void Minimu9Accelerometer::calculateInitialTilt() {
}

Tilt Minimu9Accelerometer::getTilt() {
    return Tilt(
            -(float) convertToAngle(get(0x29), get(0x28)), //X
            -(float) convertToAngle(get(0x2b), get(0x2a)), //Y
            (float) convertToAngle(get(0x2d), get(0x2c))); //Z
}

Tilt Minimu9Accelerometer::getAverageTilt(int mesurements, int milis) {
    Tilt tilt;
    float x = 0;
    float y = 0;
    float z = 0;

    if (mesurements == 0) {
        return tilt;
    }

    for (int i = 0; i < mesurements; ++i) {
        tilt = getTilt();
        x += tilt.getX();
        y += tilt.getY();
        z += tilt.getZ();
        if (i < mesurements - 1) {
            usleep(1000 * milis);
        }
    }

    return Tilt(x / mesurements, y / mesurements, z / mesurements);
}

uint8_t Minimu9Accelerometer::get(uint8_t addr) {
    uint8_t data[] = {addr};
    if (write(fileDescriptor, data, 1) == -1) {
        ErrorHandler::systemNonfatal("Accelerometer - get.write");
    }
    if (read(fileDescriptor, data, 1) == -1) {
        ErrorHandler::systemNonfatal("Accelerometer - get.read");
    }
    return data[0];
}

float Minimu9Accelerometer::convertToAngle(uint8_t h, uint8_t l) {
    //    int16_t intRes = convert(h, l);
    int16_t intRes = ((int16_t) (h << 8 | l)) >> 4;
    float res = ((float) intRes) / 2048.0f * Const::PI;
    return res;
}