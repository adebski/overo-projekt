#define __STDC_FORMAT_MACROS
#include "accelerometer/Minimu9Accelerometer.hpp"
#include <gtest/gtest.h>
#include <stdint.h>
#include <inttypes.h>
#include <cstdio>

class AccelerometerTest : public ::testing::Test {
  protected:

    AccelerometerTest() {

    }

    Minimu9Accelerometer accelerometer;
};

TEST_F(AccelerometerTest, TiltSubtractionTest) {
    Tilt rawTilt = Tilt(5, 6, 7);
    Tilt initialTilt = Tilt(3, 2, 1);
    Tilt resultTilt = rawTilt - initialTilt;
    EXPECT_EQ(resultTilt.getX(), 2);
    EXPECT_EQ(resultTilt.getY(), 4);
    EXPECT_EQ(resultTilt.getZ(), 6);
}