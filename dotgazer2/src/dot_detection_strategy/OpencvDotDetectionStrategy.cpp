#include "dot_detection_strategy/OpencvDotDetectionStrategy.hpp"
#include "utils/Const.hpp"

OpencvDotDetectionStrategy* OpencvDotDetectionStrategy::getInstance(bool verboseText,
        bool verboseImage, AlgorithmSettings* algorithmSettings) {
    OpencvDotDetectionStrategy* instance = new OpencvDotDetectionStrategy();
    instance->verboseText = verboseText;
    instance->verboseImage = verboseImage;
    instance->algorithmSettings = algorithmSettings;
    if ((instance->storage = cvCreateMemStorage(0)) == NULL) return NULL;
    if ((instance->contours = cvCreateSeq(0, sizeof (CvSeq), sizeof (CvPoint), instance->storage)) == NULL) return NULL;
    return instance;
}

std::vector<Dot> OpencvDotDetectionStrategy::detectDots(IplImage* mask) {
    dots.clear();
    cvFindContours(mask, storage, &this->contours, sizeof (CvContour), CV_RETR_LIST,
            CV_CHAIN_APPROX_SIMPLE, cvPoint(0, 0));
    CvSeq *contours = this->contours;
    for (; contours != NULL; contours = contours->h_next) {
        CvPoint2D32f center;
        float radius;
        double area = cvContourArea(contours, CV_WHOLE_SEQ, 0);

        if (area < algorithmSettings->getMinDotArea() || area > algorithmSettings->getMaxDotArea()) {
            continue;
        }

        cvMinEnclosingCircle(contours, &center, &radius);
        Dot dot = Dot(center.x, Const::IMAGE_HEIGHT - center.y);
        dots.push_back(dot);
        if (verboseText) {
            printf("detected dot (%f, %f), area = %f\n", dot.getX(), dot.getY(), area);
        }
    }
    cvClearMemStorage(storage);

    return dots;
}