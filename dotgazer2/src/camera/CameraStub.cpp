
#include "utils/Const.hpp"
#include "camera/CameraStub.hpp"
#include <cstdlib>
#include "utils/ErrorHandler.hpp"

int CameraStub::loadRawImage() {
    FILE* file = fopen(stubImageFileName, "rb");
    size_t lSize;
    void* buffer;
    size_t result;

    if (file == NULL) {
        ErrorHandler::systemNonfatal("couldn't open image file %s for reading", stubImageFileName);
        return -1;
    }
    // obtain file size:
    fseek(file, 0, SEEK_END);
    lSize = ftell(file);
    rewind(file);
    // allocate memory to contain the whole file:
    buffer = (void*) malloc(sizeof (char)*lSize);
    if (buffer == NULL) {
        ErrorHandler::systemNonfatal("Couldn't allocate buffer for reading file %s", stubImageFileName);
        fclose(file);
        return -1;
    }
    // copy the file into the buffer:
    result = fread(buffer, 1, lSize, file);
    if (result != lSize) {
        ErrorHandler::systemNonfatal("Couldn't read the whole file %s", stubImageFileName);
        fclose(file);
        return -1;
    }
    fclose(file);

    cameraImage = new CameraImage();
    cameraImage->length = lSize;
    cameraImage->image = buffer;
    return 0;
}

CameraStub::CameraStub(char* stubImageFileName) {
    this->stubImageFileName = stubImageFileName;
    this->cameraImage = NULL;
}

int CameraStub::prepareCameraForStream() {
    return 0;
}

CameraImage *CameraStub::getFrame(CameraImage* cameraImage) {
    if (this->cameraImage == NULL) {
        if (loadRawImage() < 0) {
            return NULL;
        }
    }

    cameraImage->image = this->cameraImage->image;
    cameraImage->length = this->cameraImage->length;
    return cameraImage;
}