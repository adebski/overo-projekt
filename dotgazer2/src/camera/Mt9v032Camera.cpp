#include "camera/Mt9v032Camera.hpp"
#include <stdint.h>
#include <string.h>
#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <malloc.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <cstdio>

#include <asm/types.h>          /* for videodev2.h */

#include <linux/videodev2.h>
#include <linux/media.h>
#include <linux/v4l2-subdev.h>

#include "utils/ErrorHandler.hpp"
#include "utils/BufferManager.hpp"

#define ARRAY_SIZE(a) sizeof(a)/sizeof((a)[0])

const char* Mt9v032Camera::cameraStatesStrings[] = {"CREATED", "OPENED", "CONFIGURED", "READY", "CLOSED", "UNDEFINED"};

Mt9v032Camera::PixelFormat Mt9v032Camera::pixelFormats[] = {
    { "RGB332", V4L2_PIX_FMT_RGB332},
    { "RGB555", V4L2_PIX_FMT_RGB555},
    { "RGB565", V4L2_PIX_FMT_RGB565},
    { "RGB555X", V4L2_PIX_FMT_RGB555X},
    { "RGB565X", V4L2_PIX_FMT_RGB565X},
    { "BGR24", V4L2_PIX_FMT_BGR24},
    { "RGB24", V4L2_PIX_FMT_RGB24},
    { "BGR32", V4L2_PIX_FMT_BGR32},
    { "RGB32", V4L2_PIX_FMT_RGB32},
    { "Y8", V4L2_PIX_FMT_GREY},
    { "Y10", V4L2_PIX_FMT_Y10},
    { "Y12", V4L2_PIX_FMT_Y12},
    { "Y16", V4L2_PIX_FMT_Y16},
    { "YUYV", V4L2_PIX_FMT_YUYV},
    { "UYVY", V4L2_PIX_FMT_UYVY},
    { "NV12", V4L2_PIX_FMT_NV12},
    { "NV21", V4L2_PIX_FMT_NV21},
    { "NV16", V4L2_PIX_FMT_NV16},
    { "NV61", V4L2_PIX_FMT_NV61},
    { "SBGGR8", V4L2_PIX_FMT_SBGGR8},
    { "SGBRG8", V4L2_PIX_FMT_SGBRG8},
    { "SGRBG8", V4L2_PIX_FMT_SGRBG8},
    { "SRGGB8", V4L2_PIX_FMT_SRGGB8},
    { "SGRBG10_DPCM8", V4L2_PIX_FMT_SGRBG10DPCM8},
    { "SBGGR10", V4L2_PIX_FMT_SBGGR10},
    { "SGBRG10", V4L2_PIX_FMT_SGBRG10},
    { "SGRBG10", V4L2_PIX_FMT_SGRBG10},
    { "SRGGB10", V4L2_PIX_FMT_SRGGB10},
    { "SBGGR12", V4L2_PIX_FMT_SBGGR12},
    { "SGBRG12", V4L2_PIX_FMT_SGBRG12},
    { "SGRBG12", V4L2_PIX_FMT_SGRBG12},
    { "SRGGB12", V4L2_PIX_FMT_SRGGB12},
    { "DV", V4L2_PIX_FMT_DV},
    { "MJPEG", V4L2_PIX_FMT_MJPEG},
    { "MPEG", V4L2_PIX_FMT_MPEG},
};

Mt9v032Camera::Mt9v032Camera(const char* deviceName, bool verboseText) {
    _cameraState = CREATED;
    _memoryType = V4L2_MEMORY_MMAP;
    _bufferType = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    _pixelFormat = V4L2_PIX_FMT_SGRBG10;
    _width = 752;
    _height = 480;
    _numberOfBuffers = 1;
    _deviceName = deviceName;
    _verbose = verboseText;
}

int Mt9v032Camera::closeDevice() {
    if (close(_fileDescriptor) < 0) {
        ErrorHandler::systemNonfatal("Error closing device %s", _deviceName);
        return -1;
    }
    printf("Device %s closed\n", _deviceName);

    _cameraState = CLOSED;
    return 0;
}

int Mt9v032Camera::openDevice() {
    struct v4l2_capability capabilities;

    memset(&capabilities, 0, sizeof (capabilities));

    printf("Opening device %s\n", _deviceName);

    if (_cameraState != CREATED) {
        ErrorHandler::nonsystemNonfatal("Camera may be opened only if it is in CREATED state, now it is in %s",
                cameraStatesStrings[_cameraState]);
        return -1;
    }

    _fileDescriptor = open(_deviceName, O_RDWR);
    if (_fileDescriptor == -1) {
        ErrorHandler::systemNonfatal("Error opening device %s: %s", _deviceName);
        return -1;
    }
    printf("Device %s opened\n", _deviceName);

    if (ioctl(_fileDescriptor, VIDIOC_QUERYCAP, &capabilities) == -1) {
        ErrorHandler::systemNonfatal("Error querying capabilities of %s", _deviceName);
        return -1;
    }

    printf("---%d---\n", capabilities.capabilities & V4L2_CAP_VIDEO_CAPTURE);

    if ((capabilities.capabilities & V4L2_CAP_VIDEO_CAPTURE) == 0) {
        ErrorHandler::nonsystemNonfatal("Error opening device %s: video capture not supported", _deviceName);
        closeDevice();
        return -1;
    }

    _cameraState = OPENED;

    printf("Device %s opened\n", _deviceName);

    return 0;
}

int Mt9v032Camera::setFormat() {
    struct v4l2_format format;
    memset(&format, 0, sizeof (format));

    printf("Setting format for camera\n");

    format.type = _bufferType;
    format.fmt.pix.width = _width;
    format.fmt.pix.height = _height;
    format.fmt.pix.pixelformat = _pixelFormat;
    format.fmt.pix.bytesperline = 0; //0 because it is unused
    format.fmt.pix.field = V4L2_FIELD_ANY; //up to the driver to decide

    if (ioctl(_fileDescriptor, VIDIOC_S_FMT, &format) < 0) {
        ErrorHandler::systemNonfatal("Unable to set format");
        return -1;
    }

    printf("Format set successfully\n");

    return 0;
}

int Mt9v032Camera::prepareBuffers(int numberOfBuffers, enum v4l2_memory memoryType) {
    int returnValue;

    printf("Preparing buffers\n");

    _bufferManager = new BufferManager(_fileDescriptor, numberOfBuffers, memoryType, _bufferType, _verbose);

    if (_bufferManager == NULL) {
        ErrorHandler::systemNonfatal("Error while allocating memory for buffer manager");
        return -1;
    }

    returnValue = _bufferManager->requestBuffers();
    if (returnValue < 0) {
        ErrorHandler::nonsystemNonfatal("Error while preparing buffers for video capture");
        return returnValue;
    }

    returnValue = _bufferManager->queueBuffers();
    if (returnValue < 0) {
        ErrorHandler::nonsystemNonfatal("Error while preparing buffers for video capture");
        return returnValue;
    }

    _cameraState = CONFIGURED;

    printf("Buffers set successfully, camera in %s state\n", cameraStatesStrings[_cameraState]);

    return 0;
}

int Mt9v032Camera::startCapturing() {
    int resultValue = ioctl(_fileDescriptor, VIDIOC_STREAMON, &_bufferType);

    if (resultValue == 0) {
        _cameraState = READY;
        printf("Camera in %s state\n", cameraStatesStrings[_cameraState]);
    } else {
        ErrorHandler::systemNonfatal("Error while starting streaming");
    }

    return resultValue;
}

int Mt9v032Camera::stopCapturing() {
    int resultValue = ioctl(_fileDescriptor, VIDIOC_STREAMOFF, &_bufferType);

    if (resultValue == 0) {
        _cameraState = CONFIGURED;
        printf("Camera in %s state\n", cameraStatesStrings[_cameraState]);
    } else {
        ErrorHandler::systemNonfatal("Error while stopping streaming");
    }

    return resultValue;
}

int Mt9v032Camera::prepareCameraForStream() {

    if (openDevice() < 0) {
        return -1;
    }

    if (setFormat() < 0) {
        return -1;
    }

    if (prepareBuffers(1) < 0) {
        return -1;
    }

    if (startCapturing() < 0) {
        return -1;
    }

    printf("Camera is prepared for stream\n");

    return 0;
}

struct CameraImage* Mt9v032Camera::getFrame(struct CameraImage * cameraImage) {
    struct v4l2_buffer buffer;
    struct BufferManager::Buffer internalBuffer;

    if (_cameraState != READY) {
        ErrorHandler::nonsystemNonfatal("You can only try to grab a frame when camera is in READY state, now it is in %s", cameraStatesStrings[_cameraState]);
        return NULL;
    }

    memset(&buffer, 0, sizeof buffer);

    if (_bufferManager->dequeueBuffer(&buffer) < 0) {
        ErrorHandler::nonsystemNonfatal("Error while getting a frame");
        return NULL;
    }

    if (_verbose) {
        printf("Dequeued buffer\n");
    }

    internalBuffer = _bufferManager->getBuffer(buffer.index);

    if (cameraImage->image == NULL) {

        if (_verbose) {
            printf("CameraImage->image is NULL, need to initalize memory\n");
        }

        if (prepareBufferForImageCopy(cameraImage, buffer.bytesused) < 0) {
            ErrorHandler::systemNonfatal("Error while allocating space for image copy");
            return NULL;
        }
    }

    if (_verbose) {
        printf("Starting to copy image data from internal buffer\n");
    }

    memcpy(cameraImage->image, internalBuffer.memory, cameraImage->length);

    if (_verbose) {
        printf("Queueing buffer %d\n", buffer.index);
    }

    if (_bufferManager->queueBuffer(buffer.index) < 0) {
        ErrorHandler::nonsystemNonfatal("Error while requeuing buffer");
        return NULL;
    }

    return cameraImage;
}

Mt9v032Camera::~Mt9v032Camera() {
    if (_cameraState == READY) {
        if (_verbose) {
            printf("Camera in ready state, stopping capture\n");
        }
        stopCapturing();
    }

    _bufferManager->freeBuffers();
    delete (_bufferManager);

    closeDevice();
}

const char* Mt9v032Camera::getV4L2PixelFormatName(const __u32 pixelFormat) {
    for (unsigned int i = 0; i < ARRAY_SIZE(pixelFormats); ++i) {
        if (pixelFormats[i].pixelFormat == pixelFormat)
            return pixelFormats[i].name;
    }

    return "unrecognized";
}

void Mt9v032Camera::printFormatInformation(const struct v4l2_format format) {
    printf("Video format set: %s (%08x) %ux%u (stride %u) buffer size %u\n", getV4L2PixelFormatName(format.fmt.pix.pixelformat),
            format.fmt.pix.pixelformat, format.fmt.pix.width, format.fmt.pix.height, format.fmt.pix.bytesperline,
            format.fmt.pix.sizeimage);
}

int Mt9v032Camera::prepareBufferForImageCopy(struct CameraImage *cameraImage, int bytesUsed) {
    cameraImage->length = bytesUsed;
    cameraImage->image = malloc(cameraImage->length);

    if (cameraImage->image == NULL) {
        ErrorHandler::systemNonfatal("Error while allocating memory for image copy");
        return -1;
    }

    if (_verbose) {
        printf("Memory for image copy was initialized\n");
    }

    return 0;
}