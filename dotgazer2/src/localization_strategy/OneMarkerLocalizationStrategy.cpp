#include "localization_strategy/OneMarkerLocalizationStrategy.hpp"
#include "dotgazer/Coordinates.hpp"
#include <vector>
#include "dotgazer/Marker.hpp"
#include <cstdlib>
#include <cstdio>
#include "utils/Const.hpp"
#include "utils/GeometryUtils.hpp"
#include "utils/Utils.hpp"
#include "utils/ErrorHandler.hpp"
#include <cmath>

OneMarkerLocalizationStrategy::OneMarkerLocalizationStrategy(bool verboseText,
        bool verboseImage, AlgorithmSettings* algorithmSettings) {
    this->verboseText = verboseText;
    this->verboseImage = verboseImage;
    this->algorithmSettings = algorithmSettings;

}

Coordinates OneMarkerLocalizationStrategy::calculatePosition(std::vector<Marker> markers) {
    // ubezpieczenie, metoda findMarkerClosestToCenter zaklada istnienie co najmniej jednego markera
    if (markers.size() == 0) {
        return Coordinates(Const::INVALID_COORDS, Const::INVALID_COORDS, 0.0f);
    }

    referenceMarker = GeometryUtils::findMarkerClosestToCenter(markers);
    if (verboseText) {
        printf("Marker closest to center: no: %d,  image coords=(%f, %f), angle: %f\n",
                referenceMarker.getId(),
                referenceMarker.getCenter().getX(),
                referenceMarker.getCenter().getY(),
                GeometryUtils::markerAngle(referenceMarker));
    }
    Coordinates result = calculateRobotPosition(referenceMarker);

    return result;
}

Coordinates OneMarkerLocalizationStrategy::calculateRobotPosition(Marker m) {
    float absCameraAngle = GeometryUtils::markerAngle(m) - m.getAbsoluteCoords().getAngle();
    // shifts are vectors from the marker to the image center
    //    float xShift = (Const::IMAGE_WIDTH / 2 - m.getCenter().getX()) * algorithmSettings->getMetersPerPixel();
    //    float yShift = (Const::IMAGE_HEIGHT / 2 - m.getCenter().getY()) * algorithmSettings->getMetersPerPixel();
//    float xShift = (Const::IMAGE_CENTER_X - m.getCenter().getX()) * algorithmSettings->getMetersPerPixel();
//    float yShift = (Const::IMAGE_CENTER_Y - m.getCenter().getY()) * algorithmSettings->getMetersPerPixel();
    int matrixRow = m.getCenter().getX() * Const::TRANSFORMATION_MATRIX_SCALE;
    int matrixColumn = m.getCenter().getY() * Const::TRANSFORMATION_MATRIX_SCALE;
    float xShift = algorithmSettings->getMx()[matrixRow][matrixColumn];
    float yShift = algorithmSettings->getMy()[matrixRow][matrixColumn];
    if (verboseText) {
        printf("M=(%f, %f), xShift=%f, yShift=%f\n",
                m.getAbsoluteCoords().getX(), m.getAbsoluteCoords().getY(),
                xShift, yShift);
    }
    float absCameraX = m.getAbsoluteCoords().getX() + sin(absCameraAngle) * yShift + cos(absCameraAngle) * xShift;
    float absCameraY = m.getAbsoluteCoords().getY() + cos(absCameraAngle) * yShift - sin(absCameraAngle) * xShift;
    // nagating Y because camera's Y points in the opposite direction than robot's Y
    return Coordinates(absCameraX, -absCameraY, absCameraAngle);
}

Marker OneMarkerLocalizationStrategy::getReferenceMarker() {
    return referenceMarker;
}