#include "marker_detection_strategy/SimpleMarkerDetectionStrategy.hpp"
#include <vector>
#include "opencv/cv.h"
#include "opencv/highgui.h"
#include "utils/CVUtils.hpp"
#include "utils/ImageConverter.hpp"
#include "utils/Const.hpp"
#include "dotgazer/Dot.hpp"
#include "utils/GeometryUtils.hpp"
#include "utils/Utils.hpp"

SimpleMarkerDetectionStrategy* SimpleMarkerDetectionStrategy::getInstance(bool verboseText,
        bool verboseImage, AlgorithmSettings* algorithmSettings) {
    SimpleMarkerDetectionStrategy* instance = new SimpleMarkerDetectionStrategy();
    instance->verboseText = verboseText;
    instance->verboseImage = verboseImage;
    instance->algorithmSettings = algorithmSettings;
    if ((instance->storage = cvCreateMemStorage(0)) == NULL) return NULL;
    if ((instance->contours = cvCreateSeq(0, sizeof (CvSeq), sizeof (CvPoint), instance->storage)) == NULL) return NULL;
    return instance;
}

std::vector<Marker> SimpleMarkerDetectionStrategy::detectMarkers(std::vector<Dot> dots, Tilt tilt) {
    markers.clear();
    for (unsigned int i = 0; i < dots.size(); i++) {
        assignDotToMarker(dots[i]);
    }

    if (verboseText) {
        printf("Markers before removing bogus markers:\n");
        for (unsigned int i = 0; i < markers.size(); i++) {
            Marker m = markers[i];
            printf("[%d] (size=%zu), N=(%f, %f), W=(%f, %f), S=(%f, %f), E=(%f, %f)\n",
                    i, markers[i].getDots().size(),
                    m.getNorth().getX(), m.getNorth().getY(),
                    m.getWest().getX(), m.getWest().getY(),
                    m.getSouth().getX(), m.getSouth().getY(),
                    m.getEast().getX(), m.getEast().getY());
        }
    }

    removeBogusMarkers();

    if (algorithmSettings->getUndistort()) {
        markers = undistort(markers);
    }

    // moved from main strategies, which had to invoke tilt correction separately
    markers = correctTilt(markers, tilt);

    if (verboseText) {
        printf("Markers before correction:\n");
        for (unsigned int i = 0; i < markers.size(); i++) {
            Marker m = markers[i];
            printf("[%d] (size=%zu), N=(%f, %f), W=(%f, %f), S=(%f, %f), E=(%f, %f)\n",
                    i, markers[i].getDots().size(),
                    m.getNorth().getX(), m.getNorth().getY(),
                    m.getWest().getX(), m.getWest().getY(),
                    m.getSouth().getX(), m.getSouth().getY(),
                    m.getEast().getX(), m.getEast().getY());
        }
    }
    correctMarkersParallelToEdges();
    if (verboseText) {
        printf("Markers after correction:\n");
        for (unsigned int i = 0; i < markers.size(); i++) {
            Marker m = markers[i];
            printf("[%d] (size=%zu), N=(%f, %f), W=(%f, %f), S=(%f, %f), E=(%f, %f)\n",
                    i, markers[i].getDots().size(),
                    m.getNorth().getX(), m.getNorth().getY(),
                    m.getWest().getX(), m.getWest().getY(),
                    m.getSouth().getX(), m.getSouth().getY(),
                    m.getEast().getX(), m.getEast().getY());
        }
    }

    for (unsigned int i = 0; i < markers.size(); i++) {
        std::vector<Dot> dots = markers[i].getDots();
        if (verboseText) {
            for (unsigned int j = 0; j < dots.size(); j++) {
                printf("marker %d, dot %d/%zu (%f, %f)\n", i, j + 1, dots.size(), dots[j].getX(), dots[j].getY());
            }
        }
    }

    // moved from localization strategy
    //    Dot screenCenter(Const::IMAGE_WIDTH / 2, Const::IMAGE_HEIGHT / 2);
    Dot screenCenter(Const::IMAGE_CENTER_X, Const::IMAGE_CENTER_Y);
    for (unsigned int i = 0; i < markers.size(); i++) {
        Dot mainDot = GeometryUtils::removeBogusDot(markers[i]);
        markers[i].setId(GeometryUtils::calculateMarkerId(mainDot, &markers[i]));
        markers[i].setDistanceFromScreenCenter(GeometryUtils::dotsDistance(screenCenter, markers[i].getCenter()));
    }

    return markers;
}

void SimpleMarkerDetectionStrategy::assignDotToMarker(Dot dot) {
    // try to assign the Dot to some of the existing Markers
    int markerSpan = (Const::MARKER_REAL_SPAN_METERS) / algorithmSettings->getMetersPerPixel() * 2;
    for (unsigned int i = 0; i < markers.size(); i++) {
        if (fabs(dot.getX() - markers[i].getDots()[0].getX()) < markerSpan
                && fabs(dot.getY() - markers[i].getDots()[0].getY()) < markerSpan) {
            markers[i].addDot(dot);
            if (dot.getX() < markers[i].getWest().getX())
                markers[i].setWest(dot);
            if (dot.getX() > markers[i].getEast().getX())
                markers[i].setEast(dot);
            if (dot.getY() > markers[i].getNorth().getY())
                markers[i].setNorth(dot);
            if (dot.getY() < markers[i].getSouth().getY())
                markers[i].setSouth(dot);
            return;
        }
    }
    // if none of the Markers is acceptable, create a new one
    Marker newMarker = Marker(dot); // sets N,W,S,E of the created Marker to 'dot'
    markers.push_back(newMarker);
}

void SimpleMarkerDetectionStrategy::removeBogusMarkers(void) {
    std::vector<Marker>::iterator it = markers.begin();
    while (it != markers.end()) {
        it->getDots();
        if (it->getDots().size() < 3) {
            it = markers.erase(it);
        } else {
            ++it;
        }
    }
}

void SimpleMarkerDetectionStrategy::correctMarkersParallelToEdges(void) {
    for (std::vector<Marker>::iterator it = markers.begin(); it != markers.end(); ++it) {
        if (isMarkerPrallelToEdge(*it)) {
            Marker tmpMarker;
            std::vector<Dot> dots = it->getDots();
            for (std::vector<Dot>::iterator dotIter = dots.begin(); dotIter != dots.end(); ++dotIter) {
                Dot newTmpDot = temporarilyTransformDot(*dotIter);
                if (dotIter == dots.begin()) {
                    tmpMarker.setNorth(newTmpDot);
                    tmpMarker.setWest(newTmpDot);
                    tmpMarker.setSouth(newTmpDot);
                    tmpMarker.setEast(newTmpDot);
                    it->setNorth(*dotIter);
                    it->setWest(*dotIter);
                    it->setSouth(*dotIter);
                    it->setEast(*dotIter);
                } else {
                    if (newTmpDot.getY() > tmpMarker.getNorth().getY()) {
                        tmpMarker.setNorth(newTmpDot);
                        it->setNorth(*dotIter);
                    }
                    if (newTmpDot.getY() < tmpMarker.getSouth().getY()) {
                        tmpMarker.setSouth(newTmpDot);
                        it->setSouth(*dotIter);
                    }
                    if (newTmpDot.getX() < tmpMarker.getWest().getX()) {
                        tmpMarker.setWest(newTmpDot);
                        it->setWest(*dotIter);
                    }
                    if (newTmpDot.getX() > tmpMarker.getEast().getX()) {
                        tmpMarker.setEast(newTmpDot);
                        it->setEast(*dotIter);
                    }
                }
            }
            if (verboseText) {
                printf("Marker is parallel to edges\n");
            }
        }
    }
}

bool SimpleMarkerDetectionStrategy::isMarkerPrallelToEdge(Marker m) {
    int tolerance = 5.0f;
    // we have to find two premises to regard the marker as parallel
    // one is not enough, e.g. for marker 560 turned 45 degrees
    int premises = 0;
    std::vector<Dot> dots = m.getDots();
    for (std::vector<Dot>::iterator it = dots.begin(); it != dots.end(); ++it) {
        if (*it == m.getNorth()) {
            continue;
        }
        if (m.getNorth().getY() - it->getY() < tolerance) {
            if (verboseText) {
                printf("North dot (%f, %f) is too close to (%f, %f)\n",
                        m.getNorth().getX(), m.getNorth().getY(), it->getX(), it->getY());
            }
            premises++;
        }
    }
    for (std::vector<Dot>::iterator it = dots.begin(); it != dots.end(); ++it) {
        if (*it == m.getSouth()) {
            continue;
        }
        if (it->getY() - m.getSouth().getY() < tolerance) {
            if (verboseText) {
                printf("South dot (%f, %f) is too close to (%f, %f)\n",
                        m.getSouth().getX(), m.getSouth().getY(), it->getX(), it->getY());
            }
            premises++;
        }
    }
    for (std::vector<Dot>::iterator it = dots.begin(); it != dots.end(); ++it) {
        if (*it == m.getEast()) {
            continue;
        }
        if (m.getEast().getX() - it->getX() < tolerance) {
            if (verboseText) {
                printf("East dot (%f, %f) is too close to (%f, %f)\n",
                        m.getEast().getX(), m.getEast().getY(), it->getX(), it->getY());
            }
            premises++;
        }
    }
    for (std::vector<Dot>::iterator it = dots.begin(); it != dots.end(); ++it) {
        if (*it == m.getWest()) {
            continue;
        }
        if (it->getX() - m.getWest().getX() < tolerance) {
            if (verboseText) {
                printf("West dot (%f, %f) is too close to (%f, %f)\n",
                        m.getWest().getX(), m.getWest().getY(), it->getX(), it->getY());
            }
            premises++;
        }
    }

    return premises >= 2;
}

Dot SimpleMarkerDetectionStrategy::temporarilyTransformDot(Dot dot) {
    float angle = Const::PI / 4;
    float x = cos(angle) * dot.getX() - sin(angle) * dot.getY();
    float y = sin(angle) * dot.getX() + cos(angle) * dot.getY();
    return Dot(x, y);
}

std::vector<Marker> SimpleMarkerDetectionStrategy::correctTilt(std::vector<Marker> inMarkers, Tilt tilt) {
    std::vector<Marker> outMarkers;
    for (unsigned int i = 0; i < inMarkers.size(); i++) {
        Marker m;
        for (unsigned int j = 0; j < inMarkers[i].getDots().size(); j++) {
            Dot dot = inMarkers[i].getDots()[j];
            Dot correctedDot = correctDot(dot, tilt);
            m.addDot(correctedDot);
            if (dot == inMarkers[i].getNorth()) m.setNorth(correctedDot);
            if (dot == inMarkers[i].getEast()) m.setEast(correctedDot);
            if (dot == inMarkers[i].getSouth()) m.setSouth(correctedDot);
            if (dot == inMarkers[i].getWest()) m.setWest(correctedDot);
        }
        outMarkers.push_back(m);
    }
    return outMarkers;
}

Dot SimpleMarkerDetectionStrategy::correctDot(Dot dot, Tilt tilt) {
    float x = dot.getX() + algorithmSettings->getCeilingHeightMeters() / algorithmSettings->getMetersPerPixel() * tan(tilt.getX());
    float y = dot.getY() + algorithmSettings->getCeilingHeightMeters() / algorithmSettings->getMetersPerPixel() * tan(tilt.getY());
    return Dot(x, y);
}

std::vector<Marker> SimpleMarkerDetectionStrategy::undistort(std::vector<Marker> inMarkers) {
    if (inMarkers.empty()) {
        return inMarkers;
    }
    std::vector<CvPoint2D32f> points;
    for (unsigned int i = 0; i < inMarkers.size(); i++) {
        for (unsigned int j = 0; j < inMarkers[i].getDots().size(); j++) {
            CvPoint2D32f point;
            point.x = inMarkers[i].getDots()[j].getX();
            // transform y back to image coordinates. Undistort operates on image properties
            point.y = Const::IMAGE_HEIGHT - inMarkers[i].getDots()[j].getY();
            points.push_back(point);
        }
    }
    CvMat tmp = cvMat(1, points.size(), CV_32FC2, &points[0]);
    cvUndistortPoints(&tmp, &tmp, &Const::CAMERA_MATRIX, &Const::DIST_COEFFS);

    // load interesting cells from camera matrix
    double fx = CV_MAT_ELEM(Const::CAMERA_MATRIX, float, 0, 0);
    double fy = CV_MAT_ELEM(Const::CAMERA_MATRIX, float, 1, 1);
    double cx = CV_MAT_ELEM(Const::CAMERA_MATRIX, float, 0, 2);
    double cy = CV_MAT_ELEM(Const::CAMERA_MATRIX, float, 1, 2);
    float* dstPtr = (float*) tmp.data.ptr;
    for (unsigned int pi = 0; pi < points.size(); ++pi) {
        float& px = *(dstPtr + pi * 2);
        float& py = *(dstPtr + pi * 2 + 1);
        // perform transformation. 
        // In fact this is equivalent to multiplication to camera matrix
        px = px * fx + cx;
        py = py * fy + cy;
    }

    dstPtr = (float*) tmp.data.ptr;
    std::vector<Marker> outMarkers;
    for (unsigned int i = 0; i < inMarkers.size(); i++) {
        Marker m;
        for (unsigned int j = 0; j < inMarkers[i].getDots().size(); j++) {
            Dot dot = inMarkers[i].getDots()[j];
            // take x & y of the next undistorted point and move to another point
            // transform y back to internal coordinates system
            Dot correctedDot = Dot(*dstPtr, Const::IMAGE_HEIGHT - *(dstPtr + 1));
            dstPtr += 2;
            m.addDot(correctedDot);
            if (dot == inMarkers[i].getNorth()) m.setNorth(correctedDot);
            if (dot == inMarkers[i].getEast()) m.setEast(correctedDot);
            if (dot == inMarkers[i].getSouth()) m.setSouth(correctedDot);
            if (dot == inMarkers[i].getWest()) m.setWest(correctedDot);
        }
        outMarkers.push_back(m);
    }
    return outMarkers;
}