#include "main_strategy/MainPerformanceTestStrategy.hpp"
#include "camera/Mt9v032Camera.hpp"
#include "main_strategy/MainServerStrategy.hpp"
#include "localization_strategy/OneMarkerLocalizationStrategy.hpp"
#include "marker_detection_strategy/SimpleMarkerDetectionStrategy.hpp"
#include "binarization_strategy/BgrBinarizationStrategy.hpp"
#include "mapping_strategy/OneMarkerMappingStrategy.hpp"
#include "utils/ErrorHandler.hpp"
#include "utils/ImageConverter.hpp"
#include <libwebsockets.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include "opencv/cv.h"
#include "utils/Const.hpp"
#include "dotgazer/Coordinates.hpp"
#include "accelerometer/Minimu9Accelerometer.hpp"
#include "utils/CVUtils.hpp"
#include <ctime>
#include <cstring>
#include <cstdio>
#include <sys/time.h>
#include "utils/Utils.hpp"
#include "dot_detection_strategy/DotDetectionStrategy.hpp"
#include <arpa/inet.h>

MainPerformanceTestStrategy::MainPerformanceTestStrategy(Camera* camera,
        Accelerometer* accelerometer, LocalizationStrategy *localizationStrategy, MappingStrategy *mappingStrategy,
        BinarizationStrategy *binarizationStrategy, DotDetectionStrategy *dotDetectionStrategy,
        MarkerDetectionStrategy* markerDetectionStrategy, int accMeasurements, AlgorithmSettings* algorithmSettings,
        int iterations, int serialDeviceDescriptor, MainServerWebsocketService *mainServerWebsocketService, char* parameterFileName) {
    this->camera = camera;
    this->accelerometer = accelerometer;
    this->algorithmSettings = algorithmSettings;
    this->iterations = iterations;
    this->accMeasurements = accMeasurements;
    this->binarizationStrategy = binarizationStrategy;
    this->dotDetectionStrategy = dotDetectionStrategy;
    this->markerDetectionStrategy = markerDetectionStrategy;
    this->localizationStrategy = localizationStrategy;
    this->mappingStrategy = mappingStrategy;
    this->serialDeviceDescriptor = serialDeviceDescriptor;
    this->mainServerWebsocketService = mainServerWebsocketService;
    this->parameterFileName = parameterFileName;
}

int MainPerformanceTestStrategy::main() {
    struct CameraImage cameraImage = {};
    Tilt tilt;
    std::vector<Marker> markers;
    std::vector<Dot> dots;
    struct timespec begin, end, difference;
    struct Coordinates coordinates;
    int referenceMarkerId;

    ImageConverter* converter = ImageConverter::getInstance();
    if (converter == NULL) {
        ErrorHandler::nonsystemFatal("Error during image converter initalization");
        return -1;
    }

    IplImage *mask = binarizationStrategy->getIplImageAllocatedForBinarizedImage();

    if (camera->prepareCameraForStream() < 0) {
        ErrorHandler::nonsystemNonfatal("Error while setting up camera");
        return -1;
    }

    if (mainServerWebsocketService->initialzieWebsocket(15000) < 0) {
        ErrorHandler::nonsystemNonfatal("Error while initalizing websockets");
        return -1;
    }
    mainServerWebsocketService->setSharedMemoryPointers(algorithmSettings, mask, &cameraImage, &markers, mappingStrategy->getPointerToMap());

    //giving time for connecting with client app
    getchar();

    clock_gettime(CLOCK_REALTIME, &begin);
    for (int i = 0; i < iterations; ++i) {
        camera->getFrame(&cameraImage); // get image from the camera
        tilt = accelerometer->getAverageTilt(accMeasurements, 25);
        binarizationStrategy->binarizeImage(&cameraImage);
        dots = dotDetectionStrategy->detectDots(mask);
        markers = markerDetectionStrategy->detectMarkers(dots, tilt);
        markers = mappingStrategy->updateMarkersUsingMap(markers);
        coordinates = localizationStrategy->calculatePosition(markers);
        referenceMarkerId = localizationStrategy->getReferenceMarker().getId();
        mainServerWebsocketService->updateResultsToSend(tilt, coordinates, referenceMarkerId);
        mainServerWebsocketService->serviceWebsockets();
        if (serialDeviceDescriptor > 0) {
            writeCoordinatesToSerial(coordinates);
        }
    }
    clock_gettime(CLOCK_REALTIME, &end);
    difference = Utils::timeDiff(begin, end);

    printf("elapsed: %ld:%f\n", difference.tv_sec, (double) difference.tv_nsec / 1000.0);

    return 0;
}

void MainPerformanceTestStrategy::writeCoordinatesToSerial(Coordinates coordinates) {
    static uint32_t serialDeviceBuffer[4];

    serialDeviceBuffer[0] = 0xFFFFFFFF;
    serialDeviceBuffer[1] = htonl(Utils::pack754_32(coordinates.getX()));
    serialDeviceBuffer[2] = htonl(Utils::pack754_32(coordinates.getY()));
    serialDeviceBuffer[3] = 0xFFFFFFFF;

    if (write(serialDeviceDescriptor, serialDeviceBuffer, sizeof (uint32_t) * 4) < 0) {
        ErrorHandler::systemNonfatal("Error while writing results to serial device");
    }
}