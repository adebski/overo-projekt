#include "main_strategy/MainOneMarkerDebugStrategy.hpp"
#include "utils/CVUtils.hpp"
#include "utils/ImageConverter.hpp"
#include <cstdlib>
#include "opencv/highgui.h"
#include "marker_detection_strategy/SimpleMarkerDetectionStrategy.hpp"
#include <vector>
#include "dotgazer/Coordinates.hpp"
#include "localization_strategy/OneMarkerLocalizationStrategy.hpp"
#include "utils/ErrorHandler.hpp"
#include <stdio.h>
#include <string.h>
#include "utils/Const.hpp"
#include "time.h"
#include "mapping_strategy/OneMarkerMappingStrategy.hpp"
#include <sys/time.h>
#include "utils/Utils.hpp"
#include "utils/CVUtils.hpp"
#include "binarization_strategy/BgrBinarizationStrategy.hpp"
#include "binarization_strategy/RawBinarizationStrategy.hpp"
#include "binarization_strategy/RawInterpolatedBinarizationStrategy.hpp"
#include "dot_detection_strategy/OpencvDotDetectionStrategy.hpp"
#include <netinet/in.h>
#include <dirent.h>
#include <unistd.h>

MainOneMarkerDebugStrategy::MainOneMarkerDebugStrategy(char* fileName,
        bool verboseText, bool verboseImage, AlgorithmSettings* algorithmSettings) {
    this->fileName = fileName;
    this->verboseText = verboseText;
    this->verboseImage = verboseImage;
    this->algorithmSettings = algorithmSettings;
}

void showUndistortedImage(IplImage* img) {
    CVUtils utils;
    utils.undistortImage(img);
}

void MainOneMarkerDebugStrategy::process(char imgFile[]) {    
    std::vector<Marker> markers;
    std::vector<Dot> dots;
    ImageConverter* imageConverter = ImageConverter::getInstance();
    CvSize maskSize = cvSize(Const::IMAGE_WIDTH, Const::IMAGE_HEIGHT);
    DotDetectionStrategy* dotDetectionStrategy = OpencvDotDetectionStrategy::getInstance(
            verboseText, verboseImage, algorithmSettings);
    MarkerDetectionStrategy* markerDetectionStrategy = SimpleMarkerDetectionStrategy::getInstance(
            verboseText, verboseImage, algorithmSettings);
    OneMarkerMappingStrategy mappingStrategy(verboseText, verboseImage, algorithmSettings);
    LocalizationStrategy* localizationStrategy = new OneMarkerLocalizationStrategy(
            verboseText, verboseImage, algorithmSettings);
//    BinarizationStrategy* binarizationStrategy = RawBinarizationStrategy::getInstance(
//            maskSize, verboseText, verboseImage, algorithmSettings);
    BinarizationStrategy* binarizationStrategy = RawInterpolatedBinarizationStrategy::getInstance(
            maskSize, verboseText, verboseImage, algorithmSettings);
//    BinarizationStrategy* binarizationStrategy = BgrBinarizationStrategy::getInstance(
//            imageConverter, maskSize, verboseText, verboseImage, algorithmSettings);

    mappingStrategy.loadMapFromFile(Const::CONFIG_FILE);
//    algorithmSettings->setUndistort(true);    
    
    CameraImage* cameraImage = imageConverter->loadCameraImage(imgFile);
    printf("processing %s\n", imgFile);

//    cvShowImage("original", imageConverter->convertRawToBGR(imageConverter->loadRawImage(imgFile)));
//    cvWaitKey(0);        

    IplImage* mask = binarizationStrategy->binarizeImage(cameraImage);
    IplImage* colorMask = cvCreateImage(cvSize(mask->width, mask->height), mask->depth, 3);
    cvCvtColor(mask, colorMask, CV_GRAY2BGR);
    
    dots = dotDetectionStrategy->detectDots(mask);
    markers = markerDetectionStrategy->detectMarkers(dots, Tilt());

    CVUtils cvUtils;
//        cvUtils.drawCircle(mask, Const::IMAGE_WIDTH / 2, Const::IMAGE_HEIGHT / 2, 3, 255, 2);
//    for (unsigned int j = 0; j < dots.size(); j++) {
//        Dot dot = dots[j];
//        cvUtils.drawCircle(colorMask, dot.getX(), dot.getY(), 10, 255, 0, 0, 1);
//    }
    for (unsigned int j = 0; j < markers.size(); j++) {
//        cvUtils.drawCircle(colorMask, markers[j].getCenter().getX(), markers[j].getCenter().getY(), 40, 0, 255, 0, 1);
//        cvUtils.drawCircle(colorMask, markers[j].getNorth().getX(), markers[j].getNorth().getY(), 10, 255, 0, 0, 1);
//        cvUtils.drawCircle(colorMask, markers[j].getEast().getX(), markers[j].getEast().getY(), 10, 255, 0, 0, 1);
//        cvUtils.drawCircle(colorMask, markers[j].getSouth().getX(), markers[j].getSouth().getY(), 10, 255, 0, 0, 1);
//        cvUtils.drawCircle(colorMask, markers[j].getWest().getX(), markers[j].getWest().getY(), 10, 255, 0, 0, 1);
//        for (unsigned int k = 0; k < markers[j].getDots().size(); k++) {
//            Dot dot = markers[j].getDots()[k];
//            cvUtils.drawCircle(colorMask, dot.getX(), dot.getY(), 10, 255, 0, 0, 1);
//        }
    }
    
    cvShowImage("maskAfterBinarization", colorMask);
    cvWaitKey(0);

    if (verboseImage) {
        cvShowImage("maskFromRaw", mask);
        cvWaitKey(0);
    }

//        algorithmSettings->setCeilingHeightMetersAndMetersPerPixel(markers);
    markers = mappingStrategy.updateMarkersUsingMap(markers);
    Coordinates result = localizationStrategy->calculatePosition(markers);

    if (verboseText) {
        printf("Auto-calibrated ceiling height = %fm\n", algorithmSettings->getCeilingHeightMeters());
    }
    printf("%f %f %f\n", -(result.getX()-0.08), result.getY()+0.08, result.getAngle());
}

int MainOneMarkerDebugStrategy::main() {
    char imgFile[256];
    DIR *dir;
    struct dirent *ent;
    
    // try to open a single file
    if (strlen(fileName) >= 5 && (!strcmp(fileName + strlen(fileName) - 4, ".bin") 
                || !strcmp(fileName + strlen(fileName) - 4, ".img"))) {
        sprintf(imgFile, "%s", fileName);
        process(imgFile);
    } 
    // try to open files from folder
    else if ((dir = opendir(fileName)) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (strlen(ent->d_name) < 5 || (strcmp(ent->d_name + strlen(ent->d_name) - 4, ".bin") 
                    && strcmp(ent->d_name + strlen(ent->d_name) - 4, ".img"))) {
                continue;
            }
            sprintf(imgFile, "%s/%s", fileName, ent->d_name);
            process(imgFile);
        }
    } else {
        ErrorHandler::nonsystemFatal("Could not open folder '%s'", fileName);
    }
    
    return 0;
}