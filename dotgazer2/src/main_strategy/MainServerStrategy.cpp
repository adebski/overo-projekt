#include "main_strategy/MainServerStrategy.hpp"
#include "localization_strategy/OneMarkerLocalizationStrategy.hpp"
#include "marker_detection_strategy/SimpleMarkerDetectionStrategy.hpp"
#include "binarization_strategy/BgrBinarizationStrategy.hpp"
#include "mapping_strategy/OneMarkerMappingStrategy.hpp"
#include "utils/ErrorHandler.hpp"
#include "utils/ImageConverter.hpp"
#include "camera/Mt9v032Camera.hpp"
#include <libwebsockets.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include "opencv/cv.h"
#include "utils/Const.hpp"
#include "dotgazer/Coordinates.hpp"
#include "accelerometer/Minimu9Accelerometer.hpp"
#include "utils/CVUtils.hpp"
#include "utils/Utils.hpp"
#include "dot_detection_strategy/DotDetectionStrategy.hpp"
#include <arpa/inet.h>

MainServerStrategy::MainServerStrategy(Camera * camera, Accelerometer *accelerometer,
        LocalizationStrategy *localizationStrategy, MappingStrategy *mappingStrategy,
        BinarizationStrategy *binarizationStrategy, MarkerDetectionStrategy* markerDetectionStrategy,
        DotDetectionStrategy *dotDetectionStrategy, int accMeasurements, bool verboseText, bool verboseImage,
        AlgorithmSettings* algorithmSettings, MainServerWebsocketService *mainServerWebsocketService,
        int serialDeviceDescriptor, bool daemonMode) {
    this->camera = camera;
    this->accelerometer = accelerometer;
    this->localizationStrategy = localizationStrategy;
    this->mappingStrategy = mappingStrategy;
    this->binarizationStrategy = binarizationStrategy;
    this->dotDetectionStrategy = dotDetectionStrategy;
    this->markerDetectionStrategy = markerDetectionStrategy;
    this->accMeasurements = accMeasurements;
    this->verboseText = verboseText;
    this->verboseImage = verboseImage;
    this->algorithmSettings = algorithmSettings;
    this->mainServerWebsocketService = mainServerWebsocketService;
    this->serialDeviceDescriptor = serialDeviceDescriptor;
    this->daemonMode = daemonMode;
}

int MainServerStrategy::main() {
    accelerometer->calculateInitialTilt();

    //Initializing all struct members to zero of respective type
    struct CameraImage cameraImage = {};
    std::vector<Dot> dots;
    std::vector<Marker> markers;
    ImageConverter* converter = ImageConverter::getInstance();
    if (converter == NULL) {
        ErrorHandler::nonsystemFatal("Error during image converter initalization");
        return -1;
    }

    mask = binarizationStrategy->getIplImageAllocatedForBinarizedImage();

    if (mainServerWebsocketService->initialzieWebsocket(15000) < 0) {
        ErrorHandler::nonsystemNonfatal("Error while initalizing websockets");
        return -1;
    }

    if (camera->prepareCameraForStream() < 0) {
        ErrorHandler::nonsystemNonfatal("Error while setting up camera");
        return -1;
    }

    mainServerWebsocketService->setSharedMemoryPointers(algorithmSettings, mask, &cameraImage, &markers, mappingStrategy->getPointerToMap());
    mainServerWebsocketService->setOveroControlCallback(this);
    if (daemonMode) {
        overoControlState = DETECT;
    }
    while (true) {
        camera->getFrame(&cameraImage); // get image from the camera
        tilt = accelerometer->getAverageTilt(accMeasurements, 25);
        binarizationStrategy->binarizeImage(&cameraImage);

        switch (overoControlState) {
            case DETECT:
                dots = dotDetectionStrategy->detectDots(mask);
                markers = markerDetectionStrategy->detectMarkers(dots, tilt);
                markers = mappingStrategy->updateMarkersUsingMap(markers);
                coordinates = localizationStrategy->calculatePosition(markers);
                referenceMarkerId = localizationStrategy->getReferenceMarker().getId();
                if (serialDeviceDescriptor > 0) {
                    writeCoordinatesToSerial(coordinates);
                }
                break;
            case MAP_INIT:
                mappingStrategy->clearMap();
                overoControlState = MAP_UPDATE;
                break;
            case MAP_UPDATE:
                if (updateMapUsingThisImage) {
                    updateMapUsingThisImage = false;
                    dots = dotDetectionStrategy->detectDots(mask);
                    markers = markerDetectionStrategy->detectMarkers(dots, tilt);
                    mappingStrategy->updateMap(markers);
                }
                break;
            case IDLE:
                break;
            default:
                break;
        }
        mainServerWebsocketService->updateResultsToSend(tilt, coordinates, referenceMarkerId);
        mainServerWebsocketService->serviceWebsockets();
        //usleep(1000 * 1000);
    }
    return 0;
}

void MainServerStrategy::onOveroControlState(OveroControlState overoControlState) {
    this->overoControlState = overoControlState;
}

void MainServerStrategy::onUpdateMapUsingThisImage(bool updateMapUsingThisImage) {
    this->updateMapUsingThisImage = updateMapUsingThisImage;
}

void MainServerStrategy::onCalculateCeilingHeightUsingThisImage() {
    printf("Calculating ceiling height using current image\n");
    std::vector<Dot> dots = dotDetectionStrategy->detectDots(mask);
    std::vector<Marker> markers = markerDetectionStrategy->detectMarkers(dots, tilt);
    markers = mappingStrategy->updateMarkersUsingMap(markers);
    algorithmSettings->setCeilingHeightMetersAndMetersPerPixel(markers);
    algorithmSettings->printVerboseMessage();
}

void MainServerStrategy::writeCoordinatesToSerial(Coordinates coordinates) {
    static uint32_t serialDeviceBuffer[4];

    serialDeviceBuffer[0] = 0xFFFFFFFF;
    serialDeviceBuffer[1] = htonl(Utils::pack754_32(coordinates.getX()));
    serialDeviceBuffer[2] = htonl(Utils::pack754_32(coordinates.getY()));
    serialDeviceBuffer[3] = 0xFFFFFFFF;

    if (write(serialDeviceDescriptor, serialDeviceBuffer, sizeof (uint32_t) * 4) < 0) {
        ErrorHandler::systemNonfatal("Error while writing results to serial device");
    }
}

MainServerStrategy::~MainServerStrategy() {
    delete camera;
}
