#include "main_strategy/MainRawToJpgConverterStrategy.hpp"
#include "utils/ImageConverter.hpp"
#include <cstring>
#include "opencv/cv.h"
#include "opencv/highgui.h"
#include "utils/ErrorHandler.hpp"

MainRawToJpgConverterStrategy::MainRawToJpgConverterStrategy(ImageConverter *imageConverter, char*fileName) {
    this->imageConverter = imageConverter;
    this->fileName = fileName;
}

int MainRawToJpgConverterStrategy::main() {
    IplImage* rawImage = imageConverter->loadRawImage(fileName);
    IplImage* bgrImage = imageConverter->convertRawToBGR(rawImage);
    int newFileNameLengthWithoutNullByte = strlen(fileName) + 4;
    char *newFileName = (char*) malloc(sizeof (char) * newFileNameLengthWithoutNullByte + 1);
    snprintf(newFileName, newFileNameLengthWithoutNullByte + 1, "%s.jpg", fileName);

    return cvSaveImage(newFileName, bgrImage);
}