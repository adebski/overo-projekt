#include "main_strategy/MainMatrixCreationStrategy.hpp"
#include <cstdlib>
#include <algorithm>
#include <stdio.h>
#include "opencv/highgui.h"
#include "camera/CameraImage.hpp"
#include "utils/ErrorHandler.hpp"
#include "utils/Const.hpp"
#include "binarization_strategy/RawBinarizationStrategy.hpp"
#include "dot_detection_strategy/OpencvDotDetectionStrategy.hpp"
#include "marker_detection_strategy/SimpleMarkerDetectionStrategy.hpp"
#include "utils/CVUtils.hpp"
#include "utils/GeometryUtils.hpp"
#include <dirent.h>
#include <unistd.h>

std::vector<Dot> removeInvalidDots(std::vector<Dot> dots) {
    GeometryUtils geometryUtils;
    std::vector<Dot> newDots;
    // we can safely assume, that the Dot closes to the center is valid and that we have at least 1 Dot
    newDots.push_back(dots[0]);
    for (unsigned int i=1; i<dots.size(); i++) {
        // consecutive dots shouldn't be too far away from each other
        if (geometryUtils.dotsDistance(dots[i], dots[i-1]) < 30) {
            newDots.push_back(dots[i]);
        }
    }
    return newDots;
}

MainMatrixCreationStrategy::MainMatrixCreationStrategy(char* fileName,
        bool verboseText, bool verboseImage, AlgorithmSettings* algorithmSettings) {
    this->fileName = fileName;
    this->verboseText = verboseText;
    this->verboseImage = verboseImage;
    this->algorithmSettings = algorithmSettings;
}

int MainMatrixCreationStrategy::main() {
    ImageConverter* imageConverter = ImageConverter::getInstance();
    GeometryUtils geometryUtils;
    CameraImage* cameraImage;
    IplImage* rawImage;
    DIR *dir;
    struct dirent *ent;
    char imgFile[256];
    
    // initialize transformation matrices with invalid coords
    // the rows and columns are mx[image x coordinate][image y coordinate]
    std::vector<std::vector<float> > mx(Const::IMAGE_WIDTH * Const::TRANSFORMATION_MATRIX_SCALE, 
            std::vector<float>(Const::IMAGE_HEIGHT * Const::TRANSFORMATION_MATRIX_SCALE, Const::INVALID_COORDS));
    std::vector<std::vector<float> > my(Const::IMAGE_WIDTH * Const::TRANSFORMATION_MATRIX_SCALE, 
            std::vector<float>(Const::IMAGE_HEIGHT * Const::TRANSFORMATION_MATRIX_SCALE, Const::INVALID_COORDS));

    if ((dir = opendir(fileName)) == NULL) {
        ErrorHandler::nonsystemFatal("Could not open folder '%s'", fileName);
    }

    while ((ent = readdir(dir)) != NULL) {
        if (strlen(ent->d_name) < 5 || strcmp(ent->d_name + strlen(ent->d_name) - 4, ".bin")) {
            continue;
        }

        sprintf(imgFile, "%s/%s", fileName, ent->d_name);
        cameraImage = imageConverter->loadCameraImage(imgFile);
        // rawImage used only for "-i" option, safe to comment out to save memory
        rawImage = imageConverter->convertRawToBGR(imageConverter->loadRawImage(imgFile));
        CvSize maskSize = cvSize(Const::IMAGE_WIDTH, Const::IMAGE_HEIGHT);

        BinarizationStrategy* binarizationStrategy = RawBinarizationStrategy::getInstance(
                maskSize, verboseText, verboseImage, algorithmSettings);
        DotDetectionStrategy* dotDetectionStrategy = OpencvDotDetectionStrategy::getInstance(
                verboseText, verboseImage, algorithmSettings);

        IplImage* mask = binarizationStrategy->binarizeImage(cameraImage);
        std::vector<Dot> dots = dotDetectionStrategy->detectDots(mask);

        CVUtils cvUtils;
        if (verboseImage) {
            for (unsigned int i = 0; i < dots.size(); i++) {
                Dot dot = dots[i];
                cvUtils.drawCircle(rawImage, dot.getX(), dot.getY(), 1, 0, 1);
            }
            cvShowImage("maskFromRaw", rawImage);
            cvWaitKey(0);
        }

        if (verboseText) {
            printf("before sorting:\n");
            for (unsigned int i = 0; i < dots.size(); i++) {
                Dot dot = dots[i];
                printf("(%f, %f)\n", dot.getX(), dot.getY());
            }
        }
        sort(dots.begin(), dots.end(), compareDotsByDistanceFromCenter);
        dots = removeInvalidDots(dots);
        if (verboseText) {
            printf("after sorting:\n");
            for (unsigned int i = 0; i < dots.size(); i++) {
                Dot dot = dots[i];
                printf("(%f, %f)\n", dot.getX(), dot.getY());
            }
        }

        Dot d1 = dots[0];
        Dot d2 = dots[dots.size() - 1];
        float angle = geometryUtils.vectorAngle(d1.getX(), d1.getY(), d2.getX(), d2.getY());

        // each dot was separated by 8cm, first one being in the center
        for (unsigned int i=0; i<dots.size(); i++) {
            float realDist = 0.08 * i;
            float realX = realDist * cos(angle);
            float realY = realDist * sin(angle);
            int matrixRow = dots[i].getX() * Const::TRANSFORMATION_MATRIX_SCALE;
            int matrixColumn = dots[i].getY() * Const::TRANSFORMATION_MATRIX_SCALE;
            // in LocalizationStrategy we'll shift from marker to the image center, so we neet to negate the coordinates
            mx[matrixRow][matrixColumn] = -realX;
            my[matrixRow][matrixColumn] = -realY;
        }
    }
    closedir(dir);

    const char *mxFilename = "octave/mx_sparse.txt";
    const char *myFilename = "octave/my_sparse.txt";
    FILE *mxFile = fopen(mxFilename, "w");
    FILE *myFile = fopen(myFilename, "w");
    if (mxFile == NULL) {
        ErrorHandler::systemFatal("Could not open file %s to write the matrix", mxFilename);
    }
    for (int i=0; i<Const::IMAGE_WIDTH; i++) {
        printf("writing row %d to file\n", i);
        for (int j=0; j<Const::IMAGE_HEIGHT; j++) {
            if (j == Const::IMAGE_HEIGHT - 1) {
                fprintf(mxFile, "%f\n", mx[i][j] == Const::INVALID_COORDS ? NAN : mx[i][j]);
                fprintf(myFile, "%f\n", my[i][j] == Const::INVALID_COORDS ? NAN : my[i][j]);
            } else {
                fprintf(mxFile, "%f,", mx[i][j] == Const::INVALID_COORDS ? NAN : mx[i][j]);
                fprintf(myFile, "%f,", my[i][j] == Const::INVALID_COORDS ? NAN : my[i][j]);
            }
        }
    }
    
    return 0;
}

bool MainMatrixCreationStrategy::compareDotsByDistanceFromCenter(const Dot& a, const Dot& b) {
    GeometryUtils geometryUtils;
    float aDistFromCenter = geometryUtils.pointsDistance(a.getX(), a.getY(), Const::IMAGE_CENTER_X, Const::IMAGE_CENTER_Y);
    float bDistFromCenter = geometryUtils.pointsDistance(b.getX(), b.getY(), Const::IMAGE_CENTER_X, Const::IMAGE_CENTER_Y);
    return (aDistFromCenter < bDistFromCenter);
}