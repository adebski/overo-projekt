#include "main_strategy/MainGoogleTestStrategy.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "utils/Const.hpp"
#include <iostream>

int MainGoogleTestStrategy::main() {
    int fakeArgc = 1;
    //+1 for null byte
    char *fakeArgv = (char*) malloc(sizeof (char) * strlen(Const::GMOCK_OPTIONS) + 1);
    strcpy(fakeArgv, Const::GMOCK_OPTIONS);
    testing::InitGoogleMock(&fakeArgc, &fakeArgv);
    return RUN_ALL_TESTS();
}