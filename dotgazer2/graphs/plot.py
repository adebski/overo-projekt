#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import os
from subprocess import call

DATA_FOLDER = 'data/'
GNUPLOT_SCRIPTS_FOLDER = 'gnuplot_scripts/'
IMAGES_FOLDER = 'images/'


GNUPLOT_TEMPLATE = '''set autoscale
set style line 1 lt 1 lc rgb "red" lw 4 pt 3
set style line 2 lt 1 lc rgb "blue" lw 4 pt 3
unset log
unset label
set grid
show grid
set border 15 lw 4
set xtics auto
set ytics auto
set grid lw 3
unset title
set xlabel "%s"
set ylabel "%s"
set term png size 2560,1920 font 'Verdana,48'
set output "%s"
plot %s'''


def createNecessarySubdirectories():
  if not os.path.exists(DATA_FOLDER):
    os.makedirs(DATA_FOLDER)
  if not os.path.exists(GNUPLOT_SCRIPTS_FOLDER):
    os.makedirs(GNUPLOT_SCRIPTS_FOLDER)
  if not os.path.exists(IMAGES_FOLDER):
    os.makedirs(IMAGES_FOLDER)


def prepareMeasurements(resultsFilePath):
  measurements = {}

  resultsFile = open(resultsFilePath, 'r')

  for line in resultsFile:
    xPosition = float(line[:3]) / 100
    key = line[:4]
    if key not in measurements:
      measurements[key] = []

    tokens = line.split(' ')
    x = float(tokens[1])
    y = float(tokens[2])
    measurement = {
      'x': x,
      'y': y,
      'angle': float(tokens[3]),
      'xError': xPosition - x,
      'yError': y # yPosition should be 0
    }
    measurements[key].append(measurement)

  resultsFile.close()

  for key in measurements:
    measurements[key].sort(key = lambda measurement: measurement['angle'])
  
  return measurements


def prepareFiles(measurements, sortedResultsFilePath):
  sortedResultsFile = open(sortedResultsFilePath, 'w')
  avgStepErrorDatafile = open(DATA_FOLDER + 'avg_step_error.txt', 'w')
  avgContinuousErrorDatafile = open(DATA_FOLDER + 'avg_continuous_error.txt', 'w')
  maxStepErrorDatafile = open(DATA_FOLDER + 'max_step_error.txt', 'w')
  maxContinuousErrorDatafile = open(DATA_FOLDER + 'max_continuous_error.txt', 'w')

  for key in sorted(measurements):
    xPosition = float(key[:3]) / 100
    datafile = DATA_FOLDER + key + '.txt'
    errorSumX = 0.0
    errorSumY = 0.0
    errorMaxX = 0.0
    errorMaxY = 0.0
    denominator = 0

    f = open(datafile, 'w')
    for measurement in sorted(measurements[key], key=lambda measurement: measurement['angle']):
      sortedResultsFile.write('%s %s %s %s\n' % (key, measurement['angle'], measurement['x'], measurement['y']))
      f.write('%s %s %s %s %s %s %s\n' % (measurement['angle'], measurement['x'], measurement['y'],
          measurement['xError'], abs(measurement['xError']),
          measurement['yError'], abs(measurement['yError'])))
      errorSumX += abs(measurement['xError'])
      errorSumY += abs(measurement['yError'])
      errorMaxX = max(errorMaxX, abs(measurement['xError']))
      errorMaxY = max(errorMaxY, abs(measurement['yError']))
      denominator += 1
    f.close()

    for measurement in sorted(measurements[key], key=lambda measurement: measurement['angle']):
      sortedResultsFile.write('%s %s %s %s\n' % (key, measurement['angle'], measurement['x'], measurement['y']))

    if key[3] == 'a':
      avgStepErrorDatafile.write('%s %s %s\n' % (xPosition, errorSumX/denominator, errorSumY/denominator))
      maxStepErrorDatafile.write('%s %s %s\n' % (xPosition, errorMaxX, errorMaxY))
    else:
      avgContinuousErrorDatafile.write('%s %s %s\n' % (xPosition, errorSumX/denominator, errorSumY/denominator))
      maxContinuousErrorDatafile.write('%s %s %s\n' % (xPosition, errorMaxX, errorMaxY))

    # errors plot script
    f = open(GNUPLOT_SCRIPTS_FOLDER + key + '_error.p', 'w')
    f.write(GNUPLOT_TEMPLATE % (
      'Angle [rad]',
      'Error [m]',
      IMAGES_FOLDER + key + '_error.png',
      '"%s" using 1:5 title "X error" with linespoints ls 1, "%s" using 1:7 title "Y error" with linespoints ls 2' % (datafile, datafile)))
    f.close()

  sortedResultsFile.close()
  avgStepErrorDatafile.close()
  avgContinuousErrorDatafile.close()
  maxStepErrorDatafile.close()
  maxContinuousErrorDatafile.close()

  # average step error sript
  f = open(GNUPLOT_SCRIPTS_FOLDER + 'avg_step_error.p', 'w')
  datafile = DATA_FOLDER + 'avg_step_error.txt'
  f.write(GNUPLOT_TEMPLATE % (
    'Physical camera position in X [m]',
    'Average error [m]',
    IMAGES_FOLDER + 'avg_step_error.png',
    '"%s" using 1:2 title "X error" with linespoints ls 1, "%s" using 1:3 title "Y error" with linespoints ls 2' % (datafile, datafile)))
  f.close()

  # average continuous error sript
  f = open(GNUPLOT_SCRIPTS_FOLDER + 'avg_continuous_error.p', 'w')
  datafile = DATA_FOLDER + 'avg_continuous_error.txt'
  f.write(GNUPLOT_TEMPLATE % (
    'Physical camera position in X [m]',
    'Average error [m]',
    IMAGES_FOLDER + 'avg_continuous_error.png',
    '"%s" using 1:2 title "X error" with linespoints ls 1, "%s" using 1:3 title "Y error" with linespoints ls 2' % (datafile, datafile)))
  f.close()

  # max step error script
  f = open(GNUPLOT_SCRIPTS_FOLDER + 'max_step_error.p', 'w')
  datafile = DATA_FOLDER + 'max_step_error.txt'
  f.write(GNUPLOT_TEMPLATE % (
    'Physical camera position in X [m]',
    'Maximum error [m]',
    IMAGES_FOLDER + 'max_step_error.png',
    '"%s" using 1:2 title "X error" with linespoints ls 1, "%s" using 1:3 title "Y error" with linespoints ls 2' % (datafile, datafile)))
  f.close()

  # max continuous error sript
  f = open(GNUPLOT_SCRIPTS_FOLDER + 'max_continuous_error.p', 'w')
  datafile = DATA_FOLDER + 'max_continuous_error.txt'
  f.write(GNUPLOT_TEMPLATE % (
    'Physical camera position in X [m]',
    'Maximum error [m]',
    IMAGES_FOLDER + 'max_continuous_error.png',
    '"%s" using 1:2 title "X error" with linespoints ls 1, "%s" using 1:3 title "Y error" with linespoints ls 2' % (datafile, datafile)))
  f.close()


def gnuplot(measurements):
  for key in measurements:
    call(['gnuplot', GNUPLOT_SCRIPTS_FOLDER + key + '_error.p'])
    call(['gnuplot', GNUPLOT_SCRIPTS_FOLDER + 'avg_step_error.p'])
    call(['gnuplot', GNUPLOT_SCRIPTS_FOLDER + 'avg_continuous_error.p'])
    call(['gnuplot', GNUPLOT_SCRIPTS_FOLDER + 'max_step_error.p'])
    call(['gnuplot', GNUPLOT_SCRIPTS_FOLDER + 'max_continuous_error.p'])


if __name__ == "__main__":
  if len(sys.argv) != 2:
    print "Usage: ./plot.py <results_file_path>"
    exit()

  resultsFilePath = sys.argv[1]  

  createNecessarySubdirectories()
  measurements = prepareMeasurements(resultsFilePath)
  prepareFiles(measurements, resultsFilePath + '_sorted')
  gnuplot(measurements)
  