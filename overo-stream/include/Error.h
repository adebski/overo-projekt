#ifndef ERRHANDLING
#define ERRHANDLING
#define MAXLINE 4096
#include<stdarg.h>
/*
 * free allocated space
 */
void error_free(int num,...);

/*
 * write to stderr, return to caller
 */
void writeErrorMessage(int errnoflag,int error,const char *fmt,va_list ap);

/*
 *nonfatal error related to system call
 *print message and return
 */
void nonFatalSystemCallError(const char *fmt, ...);

/*
 * fatal error related to system call
 * print message and terminate
 */
void fatalSystemCallError(const char *fmt, ...);

/*
 * fatal error related to system call
 * print message, dump core, and terminate
 */
void error_sys_d(const char *fmt, ...);

/*nonfatal error unrelated to system call
 * print message and return
 */
void nonFatalNonSystemCallError(const char *fmt, ...);

/*
 * fatal error unrelated to system call
 * error code passed as explicit parameter
 * print message and terminate
 */
void error_nsys_fp(int error, const char *fmt, ...);

/*
 * fatal error unrelated to system call
 * print message and terminate
 */
void fatalNonSytemCallError(const char *fmt, ...);
#endif
