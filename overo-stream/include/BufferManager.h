/*
 * BufferManager.h
 *
 *  Created on: Feb 16, 2013
 *      Author: andrzej
 */

#ifndef BUFFERMANAGER_H_
#define BUFFERMANAGER_H_
#include <asm/types.h>          /* for videodev2.h */

#include <linux/videodev2.h>
#include <linux/media.h>
#include <linux/v4l2-subdev.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include "Error.h"

namespace overoStream
{

    struct Buffer
    {
            unsigned int padding;
            unsigned int size;
            int bytesUsed;
            void *memory;
    };

    class BufferManager
    {

        private:
            const int _fileDescriptor;
            int _numberOfBuffers;
            const enum v4l2_memory _memoryType;
            struct Buffer *_buffers;
            const enum v4l2_buf_type _bufferType;
            struct v4l2_buffer *internalBuffer;


            int queryForBuffer(struct v4l2_buffer *buffer, __u32 index)
            {
                buffer->index = index;
                buffer->type = _bufferType;
                buffer->memory = _memoryType;

                return ioctl(_fileDescriptor, VIDIOC_QUERYBUF, buffer);
            }

            int mapMemoryMapBuffers()
            {
                struct v4l2_buffer buffer;
                struct Buffer *buffers;

                buffers = (struct Buffer*) malloc(sizeof(struct Buffer) * _numberOfBuffers);
                if (buffers == NULL)
                {
                    nonFatalSystemCallError("Error while allocating buffer information array");
                    return -1;
                }

                for (int i = 0; i < _numberOfBuffers; ++i)
                {
                    memset(&buffer, 0, sizeof buffer);

                    if (queryForBuffer(&buffer, i))
                    {
                        nonFatalSystemCallError("Unable to query for buffer %u", i);
                        return -1;
                    }
                    printf("Buffer information: length %u offset %u\n", buffer.length, buffer.m.offset);

                    buffers[i].memory = mmap(0, buffer.length, PROT_READ | PROT_WRITE, MAP_SHARED, _fileDescriptor, buffer.m.offset);
                    if (buffers[i].memory == MAP_FAILED)
                    {
                        nonFatalSystemCallError("Unable to map buffer %u", i);
                        return -1;
                    }
                    buffers[i].size = buffer.length;
                    buffers[i].padding = 0;
                    printf("Buffer %u mapped at address %p.\n", i, buffers[i].memory);
                }
                _buffers = buffers;

                return 0;
            }

            int freeMemoryMapBuffers()
            {
                for (int i = 0; i < _numberOfBuffers; ++i)
                {
                    if (munmap(_buffers[i].memory, _buffers[i].size) < 0)
                    {
                        nonFatalSystemCallError("Unable to unmap buffer %u", i);
                        return -1;
                    }
                    _buffers[i].memory = NULL;
                }

                return 0;
            }

            void freeUserPointerBuffers()
            {
                for (int i = 0; i < _numberOfBuffers; ++i)
                {
                    free(_buffers[i].memory);
                    _buffers[i].memory = NULL;
                }
            }

        public:
            BufferManager(const int fileDescriptor, const int numberOfBuffers, const enum v4l2_memory memoryType,
                    const enum v4l2_buf_type bufferType) :
                    _fileDescriptor(fileDescriptor), _numberOfBuffers(numberOfBuffers), _memoryType(memoryType), _bufferType(bufferType)
            {
            }

            int requestBuffers()
            {
                struct v4l2_requestbuffers requestBuffers;

                requestBuffers.count = _numberOfBuffers;
                requestBuffers.type = _bufferType;
                requestBuffers.memory = _memoryType;
                if (ioctl(_fileDescriptor, VIDIOC_REQBUFS, &requestBuffers) < 0)
                {
                    nonFatalSystemCallError("Unable to request buffers for device");
                    return -1;
                }
                printf("%u buffers requested\n", _numberOfBuffers);

                switch (_memoryType)
                {
                    case V4L2_MEMORY_MMAP:
                    {
                        return mapMemoryMapBuffers();
                        break;
                    }
                    default:
                    {
                        nonFatalNonSystemCallError("Not supported memory type");
                        return -1;
                    }
                }

                return 0;
            }

            int queueBuffer(int bufferNumber)
            {
                struct v4l2_buffer buffer;

                buffer.index = bufferNumber;
                buffer.type = _bufferType;
                buffer.memory = _memoryType;
                buffer.length = _buffers[bufferNumber].size;

                if (_memoryType == V4L2_MEMORY_USERPTR)
                {
                    buffer.m.userptr = (long unsigned int) _buffers[bufferNumber].memory;
                }

                if (ioctl(_fileDescriptor, VIDIOC_QBUF, &buffer) < 0)
                {
                    nonFatalSystemCallError("Unable to queue buffer %d", bufferNumber);
                    return -1;
                }

                return 0;
            }

            int queueBuffers()
            {
                int returnValue;

                for (int i = 0; i < _numberOfBuffers; ++i)
                {
                    returnValue = queueBuffer(i);
                    if (returnValue < 0)
                    {
                        nonFatalNonSystemCallError("Unable to queue all buffers \n");
                        return returnValue;
                    }
                }

                return 0;
            }

            int freeBuffers()
            {
                struct v4l2_requestbuffers requestBuffers;
                memset(&requestBuffers, 0, sizeof requestBuffers);


                switch (_memoryType)
                {
                    case V4L2_MEMORY_MMAP:
                        if (freeMemoryMapBuffers() < 0)
                        {
                            return -1;
                        }
                        break;
                    case V4L2_MEMORY_USERPTR:
                        freeUserPointerBuffers();
                        break;
                    default:
                        nonFatalNonSystemCallError("Unsupported memory type");
                        break;
                }


                requestBuffers.count = 0;
                requestBuffers.type = _bufferType;
                requestBuffers.memory = _memoryType;
                if (ioctl(_fileDescriptor, VIDIOC_REQBUFS, &requestBuffers) < 0)
                {
                    nonFatalSystemCallError("Unable to release buffers");
                    return -1;
                }
                printf("%u buffers released.\n", _numberOfBuffers);

                free(_buffers);
                _buffers = NULL;
                _numberOfBuffers = 0;

                return 0;
            }

            enum v4l2_memory getMemeoryType() const
            {
                return _memoryType;
            }

            enum v4l2_buf_type getBufferType() const
            {
                return _bufferType;
            }

            int getNumberOfBuffers() const
            {
                return _numberOfBuffers;
            }

            struct Buffer getBuffer(int bufferNumber)
            {
                return _buffers[bufferNumber];
            }

            int dequeueBuffer(struct v4l2_buffer * buffer)
            {
                memset(buffer, 0, sizeof (struct v4l2_buffer));
                buffer->type = _bufferType;
                buffer->memory = _memoryType;

                if (ioctl(_fileDescriptor, VIDIOC_DQBUF, buffer) < 0)
                {
                    nonFatalSystemCallError("Error while dequeuing buffer");
                    buffer = NULL;
                    return -1;
                }

                return 0;
            }
    };

} /* namespace overoStream */
#endif /* BUFFERMANAGER_H_ */
