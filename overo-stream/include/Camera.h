/*
 * Camera.h
 *
 *  Created on: Feb 10, 2013
 *      Author: andrzej
 */

#ifndef CAMERA_H_
#define CAMERA_H_

#include <stdint.h>
#include <string.h>
#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <malloc.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <cstdio>

#include <asm/types.h>          /* for videodev2.h */

#include <linux/videodev2.h>
#include <linux/media.h>
#include <linux/v4l2-subdev.h>

#include "Error.h"
#include "BufferManager.h"

namespace overoStream
{
#define ARRAY_SIZE(a) sizeof(a)/sizeof((a)[0])

    enum cameraStates
    {
        CREATED, OPENED, CONFIGURED, READY, CLOSED, UNDEFINED
    };

    struct CameraImage
    {
            uint32_t length;
            void* image;
    };

    static struct
    {
            const char *name;
            __u32 pixelFormat;
    } pixelFormats[] =
    {
    { "RGB332", V4L2_PIX_FMT_RGB332 },
    { "RGB555", V4L2_PIX_FMT_RGB555 },
    { "RGB565", V4L2_PIX_FMT_RGB565 },
    { "RGB555X", V4L2_PIX_FMT_RGB555X },
    { "RGB565X", V4L2_PIX_FMT_RGB565X },
    { "BGR24", V4L2_PIX_FMT_BGR24 },
    { "RGB24", V4L2_PIX_FMT_RGB24 },
    { "BGR32", V4L2_PIX_FMT_BGR32 },
    { "RGB32", V4L2_PIX_FMT_RGB32 },
    { "Y8", V4L2_PIX_FMT_GREY },
    { "Y10", V4L2_PIX_FMT_Y10 },
    { "Y12", V4L2_PIX_FMT_Y12 },
    { "Y16", V4L2_PIX_FMT_Y16 },
    { "YUYV", V4L2_PIX_FMT_YUYV },
    { "UYVY", V4L2_PIX_FMT_UYVY },
    { "NV12", V4L2_PIX_FMT_NV12 },
    { "NV21", V4L2_PIX_FMT_NV21 },
    { "NV16", V4L2_PIX_FMT_NV16 },
    { "NV61", V4L2_PIX_FMT_NV61 },
    { "SBGGR8", V4L2_PIX_FMT_SBGGR8 },
    { "SGBRG8", V4L2_PIX_FMT_SGBRG8 },
    { "SGRBG8", V4L2_PIX_FMT_SGRBG8 },
    { "SRGGB8", V4L2_PIX_FMT_SRGGB8 },
    { "SGRBG10_DPCM8", V4L2_PIX_FMT_SGRBG10DPCM8 },
    { "SBGGR10", V4L2_PIX_FMT_SBGGR10 },
    { "SGBRG10", V4L2_PIX_FMT_SGBRG10 },
    { "SGRBG10", V4L2_PIX_FMT_SGRBG10 },
    { "SRGGB10", V4L2_PIX_FMT_SRGGB10 },
    { "SBGGR12", V4L2_PIX_FMT_SBGGR12 },
    { "SGBRG12", V4L2_PIX_FMT_SGBRG12 },
    { "SGRBG12", V4L2_PIX_FMT_SGRBG12 },
    { "SRGGB12", V4L2_PIX_FMT_SRGGB12 },
    { "DV", V4L2_PIX_FMT_DV },
    { "MJPEG", V4L2_PIX_FMT_MJPEG },
    { "MPEG", V4L2_PIX_FMT_MPEG }, };

    class Camera
    {
        private:
            static const char *cameraStatesStrings[];

            enum v4l2_memory _memoryType;
            int _numberOfBuffers;
            enum v4l2_buf_type _bufferType;
            BufferManager *_bufferManager;

            int _fileDescriptor;
            enum cameraStates _cameraState;

            __u32 _pixelFormat;
            __u32 _width;
            __u32 _height;
            __u32 _bytesperline;
            __u32 _imagesize;

            void *_pattern;
            unsigned int _patternsize;

            const char* _deviceName;

            const char* getV4L2PixelFormatName(const __u32 pixelFormat)
            {
                for (unsigned int i = 0; i < ARRAY_SIZE(pixelFormats); ++i)
                {
                    if (pixelFormats[i].pixelFormat == pixelFormat)
                        return pixelFormats[i].name;
                }

                return "unrecognized";
            }

            void printFormatInformation(const struct v4l2_format format)
            {
                printf("Video format set: %s (%08x) %ux%u (stride %u) buffer size %u\n", getV4L2PixelFormatName(format.fmt.pix.pixelformat),
                        format.fmt.pix.pixelformat, format.fmt.pix.width, format.fmt.pix.height, format.fmt.pix.bytesperline,
                        format.fmt.pix.sizeimage);
            }

            void initialize(__u32 pixelFormat, __u32 width, __u32 height, const char* deviceName)
            {
                _cameraState = CREATED;
                _memoryType = V4L2_MEMORY_MMAP;
                _bufferType = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                _pixelFormat = pixelFormat;
                _width = width;
                _height = height;
                _numberOfBuffers = 1;
                _deviceName = deviceName;
            }

            int prepareBufferForImageCopy(struct CameraImage *cameraImage, int bytesUsed)
            {
                cameraImage->length = bytesUsed;
                cameraImage->image = malloc(cameraImage->length);

                if (cameraImage->image == NULL)
                {
                    nonFatalSystemCallError("Error while allocating memory for image copy");
                    return -1;
                }

                return 0;
            }

        public:
            Camera(__u32 pixelFormat, __u32 width, __u32 height, const char* deviceName)
            {
                initialize(pixelFormat, width, height, deviceName);
            }

            Camera(const char* deviceName)
            {
                initialize(V4L2_PIX_FMT_SGRBG10, 752, 480, deviceName);
            }

            int closeDevice()
            {
                if (close(_fileDescriptor) < 0)
                {
                    nonFatalSystemCallError("Error closing device %s", _deviceName);
                    return -1;
                }
                printf("Device %s closed\n", _deviceName);

                _cameraState = CLOSED;
                return 0;
            }

            int openDevice()
            {
                struct v4l2_capability capabilities;

                memset(&capabilities, 0, sizeof(capabilities));

                if (_cameraState != CREATED)
                {
                    nonFatalNonSystemCallError("Camera may be opened only if it is in CLOSED state, now it is in %s",
                            cameraStatesStrings[_cameraState]);
                }

                _fileDescriptor = open(_deviceName, O_RDWR);
                if (_fileDescriptor == -1)
                {
                    nonFatalSystemCallError("Error opening device %s: %s", _deviceName);
                    return -1;
                }
                printf("Device %s opened\n", _deviceName);

                if (ioctl(_fileDescriptor, VIDIOC_QUERYCAP, &capabilities) == -1)
                {
                    nonFatalSystemCallError("Error querying capabilities of %s", _deviceName);
                    return -1;
                }

                printf("---%d---\n", capabilities.capabilities & V4L2_CAP_VIDEO_CAPTURE);

                if ((capabilities.capabilities & V4L2_CAP_VIDEO_CAPTURE) == 0)
                {
                    nonFatalNonSystemCallError("Error opening device %s: video capture not supported", _deviceName);
                    closeDevice();
                    return -1;
                }

                _cameraState = OPENED;

                return 0;
            }

            int setFormat()
            {
                struct v4l2_format format;
                memset(&format, 0, sizeof(format));

                format.type = _bufferType;
                format.fmt.pix.width = _width;
                format.fmt.pix.height = _height;
                format.fmt.pix.pixelformat = _pixelFormat;
                format.fmt.pix.bytesperline = 0; //0 because it is unused
                format.fmt.pix.field = V4L2_FIELD_ANY; //up to the driver to decide

                if (ioctl(_fileDescriptor, VIDIOC_S_FMT, &format) < 0)
                {
                    nonFatalSystemCallError("Unable to set format");
                    return -1;
                }

                return 0;
            }

            int getFormat(struct v4l2_format *format)
            {
                if (ioctl(_fileDescriptor, VIDIOC_G_FMT, format) < 0)
                {
                    nonFatalSystemCallError("Unable to get format");
                    return -1;
                }

                return 0;
            }

            int prepareBuffers(int numberOfBuffers, enum v4l2_memory memoryType = V4L2_MEMORY_MMAP)
            {
                int returnValue;
                _bufferManager = new BufferManager(_fileDescriptor, numberOfBuffers, memoryType, _bufferType);

                if (_bufferManager == NULL)
                {
                    nonFatalSystemCallError("Error while allocating memory for buffer manager");
                    return -1;
                }

                returnValue = _bufferManager->requestBuffers();
                if (returnValue < 0)
                {
                    nonFatalNonSystemCallError("Error while preparing buffers for video capture");
                    return returnValue;
                }

                returnValue = _bufferManager->queueBuffers();
                if (returnValue < 0)
                {
                    nonFatalNonSystemCallError("Error while preparing buffers for video capture");
                    return returnValue;
                }

                _cameraState = CONFIGURED;

                return 0;
            }

            int startCapturing()
            {
                int resultValue = ioctl(_fileDescriptor, VIDIOC_STREAMON, &_bufferType);

                if (resultValue == 0)
                {
                    _cameraState = READY;
                } else
                {
                    nonFatalSystemCallError("Error while starting streaming");
                }

                return resultValue;
            }

            int stopCapturing()
            {
                int resultValue = ioctl(_fileDescriptor, VIDIOC_STREAMOFF, &_bufferType);

                if (resultValue == 0)
                {
                    _cameraState = CONFIGURED;
                } else
                {
                    nonFatalSystemCallError("Error while stopping streaming");
                }

                return resultValue;
            }

            struct CameraImage *getFrame(struct CameraImage * cameraImage)
            {
                struct v4l2_buffer buffer;
                struct Buffer internalBuffer;

                memset(&buffer, 0, sizeof buffer);

                if (_bufferManager->dequeueBuffer(&buffer) < 0)
                {
                    nonFatalNonSystemCallError("Error while getting a frame");
                    return NULL;
                }

                internalBuffer = _bufferManager->getBuffer(buffer.index);

                if (cameraImage->image == NULL)
                {
                    if (prepareBufferForImageCopy(cameraImage, buffer.bytesused) < 0)
                    {
                        nonFatalSystemCallError("Error while allocating space for image copy");
                        return NULL;
                    }
                }

                memcpy(cameraImage->image, internalBuffer.memory, cameraImage->length);

                if (_bufferManager->queueBuffer(buffer.index) < 0)
                {
                    nonFatalNonSystemCallError("Error while requeuing buffer");
                    return NULL;
                }

                return cameraImage;
            }

            ~Camera()
            {
                if (_cameraState == READY)
                {
                    stopCapturing();
                }

                _bufferManager->freeBuffers();
                delete (_bufferManager);

                closeDevice();
            }
    };

}

#endif /* CAMERA_H_ */
