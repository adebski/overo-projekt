/*
 * Options.h
 *
 *  Created on: Feb 10, 2013
 *      Author: andrzej
 */

#ifndef OPTIONS_H_
#define OPTIONS_H_

#include<iostream>
#include<stdint.h>
#include<arpa/inet.h>

namespace overoStream
{
    struct SenderOptions
    {
            uint16_t receiverPort;
            bool portGiven;
            struct in_addr receiverIPAddress;
            bool addressGiven;
    };

    struct ReceiverOptions
    {
            uint16_t port;
            bool portGiven;
    };
}



#endif /* OPTIONS_H_ */
