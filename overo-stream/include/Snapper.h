/*
 * Snapper.h
 *
 *  Created on: Jan 31, 2013
 *      Author: andrzej
 */

#ifndef SNAPPER_H_
#define SNAPPER_H_

#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <malloc.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <asm/types.h>          /* for videodev2.h */

#include <linux/videodev2.h>
#include <linux/media.h>
#include <linux/v4l2-subdev.h>

namespace overoStream
{
    class Snapper
    {
        public:
            Snapper();
            ~Snapper();
    };

}


#endif /* SNAPPER_H_ */
