/*
 * RawImageConverter.h
 *
 *  Created on: Feb 18, 2013
 *      Author: andrzej
 */

#ifndef RAWIMAGECONVERTER_H_
#define RAWIMAGECONVERTER_H_

#include <opencv/cv.h>
#include <cstdio>
#include <cstdlib>
#include "Error.h"
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

namespace overoStream
{

    class RawImageConverter
    {
        private:
            char* loadRawData(char *path, int size)
            {
                int fileDescriptor;
                char* buffer;
                int returnValue;

                if ((fileDescriptor = open(path, O_RDONLY)) == -1)
                {
                    nonFatalSystemCallError("Error while opening file %s", path);
                    return NULL;
                }

                if ((buffer = (char*) malloc(sizeof(char) * size)) == NULL)
                {
                    nonFatalSystemCallError("Error while allocating memory for buffer");
                    close(fileDescriptor);
                    return NULL;
                }

                if ((returnValue = read(fileDescriptor, buffer, size)) != size)
                {
                    nonFatalSystemCallError("read %d bytes, should %d", returnValue, size);
                    free(buffer);
                    close(fileDescriptor);
                    return NULL;
                }

                close(fileDescriptor);
                return buffer;
            }

        public:
            IplImage* convertRawToBGR(IplImage *rawImage, IplImage *bgrImage)
            {
                CvSize imageSize = cvGetSize(rawImage);
                IplImage *rescaledImage;

                if ((rescaledImage = cvCreateImage(imageSize, IPL_DEPTH_8U, 1)) == NULL)
                {
                    nonFatalNonSystemCallError("Error while allocating memory for rescaled image");
                    return NULL;
                }

                cvConvertScale(rawImage, rescaledImage, 0.25, 0.0);
                cvCvtColor(rescaledImage, bgrImage, CV_BayerGB2BGR);

                cvReleaseImage(&rescaledImage);
                return bgrImage;
            }

            IplImage* createIplImageStructure( int width, int height, int depth, int channels)
            {
                IplImage *image;
                CvSize size =
                { width, height };

                if ((image = cvCreateImage(size, depth, channels)) == NULL)
                {
                    nonFatalNonSystemCallError("Error while allocating memory for image");
                    return NULL;
                }

                return image;
            }

            IplImage* loadRawImage(char* path, int width, int height)
            {
                IplImage *rawImage;
                CvSize size =
                { width, height };

                if ((rawImage = cvCreateImage(size, IPL_DEPTH_16U, 1)) == NULL)
                {
                    nonFatalNonSystemCallError("Error while allocating memory for raw image");
                    return NULL;
                }

                if ((rawImage->imageData = loadRawData(path, width * height * 2)) == NULL)
                {
                    cvReleaseImage(&rawImage);
                    return NULL;
                }

                return rawImage;
            }
    };

} /* namespace overoStream */
#endif /* RAWIMAGECONVERTER_H_ */
