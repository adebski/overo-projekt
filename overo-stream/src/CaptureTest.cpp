/*
 * CaptureTest.cpp
 *
 *  Created on: Feb 17, 2013
 *      Author: andrzej
 */
#include<cstdlib>
#include"Error.h"
#include<cstdio>
#include <Camera.h>
#include <sys/stat.h>
#include<fcntl.h>

int writeToDisk(char* path, void* buffer, int size)
{
    int fileDescriptor = open(path, O_RDWR | O_APPEND | O_CREAT );

    if (fileDescriptor < 0)
    {
        nonFatalSystemCallError("Error creating file %s", path);
        return -1;
    }

    if (write(fileDescriptor, buffer, size) < 0)
    {
        nonFatalSystemCallError("Error writing to file %s", path);
        close(fileDescriptor);
        return -1;
    }

    return 0;
}

int main(int argc, char **argv)
{
    overoStream::Camera *camera;
    overoStream::CameraImage cameraImage;

    camera = new overoStream::Camera("/dev/video2");

    if (camera->openDevice() < 0)
    {
        return -1;
    }

    if (camera->setFormat() < 0)
    {
        return -1;
    }

    if (camera->prepareBuffers(1) < 0)
    {
        return -1;
    }

    if (camera->startCapturing() < 0)
    {
        return -1;
    }
    if (camera->getFrame(&cameraImage) < 0)
    {
        return -1;
    }

    writeToDisk("testImage.bin",cameraImage.image, cameraImage.length);

    if (camera->stopCapturing() < 0)
    {
        return -1;
    }

    delete(camera);

    printf("%d\n",cameraImage.length);

}
