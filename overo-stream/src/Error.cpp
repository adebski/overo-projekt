#include"Error.h"
#include<stdio.h>
#include<errno.h>
#include<stdarg.h>
#include<string.h>
#include<stdlib.h>

/*
 * free allocated space
 */
void error_free(int num,...)
{
    va_list list;
    int i;
    va_start(list,num);
    for (i=0;i<num;++i)
        free(va_arg(list,void*));
    return ;
} 

/*
 * write to stderr
 * return to caller
 */
void writeErrorMessage(int errnoflag,int error,const char *fmt,va_list ap)
{
    char buf[MAXLINE];
    vsnprintf(buf, MAXLINE, fmt, ap);
    if (errnoflag)
        snprintf(buf+strlen(buf), MAXLINE-strlen(buf), ": %s",strerror(error));
    strcat(buf, "\n");
    fflush(stdout);     /* in case stdout and stderr are the same */
    fputs(buf, stderr);
    fflush(NULL);       /* flushes all stdio output streams */
}

/*
 *nonfatal error related to system call
 *print message and return
 */
void nonFatalSystemCallError(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    writeErrorMessage(1, errno, fmt, ap);
    va_end(ap);
}

/*
 * fatal error related to system call
 * print message and terminate
 */
void fatalSystemCallError(const char *fmt, ...)
{
        
    va_list ap;
    va_start(ap, fmt);
    writeErrorMessage(1, errno, fmt, ap);
    va_end(ap);
    exit(1);
}

/*
 * fatal error related to system call
 * print message, dump core, and terminate
 */
void error_sys_d(const char *fmt, ...)
{    
    va_list ap;
    va_start(ap, fmt);
    writeErrorMessage(1, errno, fmt, ap);
    va_end(ap);
    abort(); //dump core and terminate
    exit(1);
}

/*nonfatal error unrelated to system call
 * print message and return
 */
void nonFatalNonSystemCallError(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    writeErrorMessage(0, 0, fmt, ap);
    va_end(ap);
}

/*
 * fatal error unrelated to system call
 * error code passed as explicit parameter
 * print message and terminate
 */
void error_nsys_fp(int error, const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    writeErrorMessage(1, error, fmt, ap);
    va_end(ap);
    exit(1);
}

/*
 * fatal error unrelated to system call
 * print message and terminate
 */
void fatalNonSytemCallError(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    writeErrorMessage(0, 0, fmt, ap);
    va_end(ap);
    exit(1);
}
