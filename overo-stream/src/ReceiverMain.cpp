/*
 * SenderMain.cpp
 *
 *  Created on: Feb 10, 2013
 *      Author: andrzej
 */
#include "Options.h"
#include <getopt.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include"Error.h"
#include "RawImageConverter.h"
#include "Camera.h"
#include"opencv/cv.h"
#include"opencv/highgui.h"

extern char *optarg;

#define WIDTH 752
#define HEIGHT 480
#define WINDOW_NAME "Window"

void usage()
{
    printf("-p port number\n");
    printf("-h print help \n");
}

int readOptions(int argc, char** argv, struct overoStream::ReceiverOptions &options)
{
    int option;
    long int portNumber;

    while ((option = getopt(argc, argv, "p:a:h:")) != -1)
    {
        switch (option)
        {
            case 'p':
                portNumber = strtol(optarg, NULL, 10);
                options.port = htons(portNumber);
                options.portGiven = true;
                break;
            case 'h':
                usage();
                break;
            case '?':
                printf("Unrecognized option\n");
                return -1;
                break;
            default:
                printf("Undefined error\n");
                break;
        }
    }
    return 0;
}

int receiveMessage(int socketDescriptor, uint32_t messageSize, void* buffer)
{
    char *temp;
    int bytesRead = 0;
    uint32_t bytesToBeRead = messageSize;

    temp = (char*) buffer;
    bytesToBeRead = messageSize;

    while (bytesToBeRead > 0)
    {
        bytesRead = read(socketDescriptor, temp, bytesToBeRead);

        if (bytesRead >= 0)
        {
            temp += bytesRead;
            bytesToBeRead -= bytesRead;
        } else
        {
            nonFatalSystemCallError("Error while receiving message");
            return -1;
        }
    }

    return 0;
}

int receiveStream(int clientDescriptor)
{
    void* message;
    uint32_t messageSize;
    overoStream::RawImageConverter rawImageConverter;
    IplImage *rawImage = rawImageConverter.createIplImageStructure(WIDTH, HEIGHT, IPL_DEPTH_16U, 1);
    IplImage *bgrImage = rawImageConverter.createIplImageStructure(WIDTH, HEIGHT, IPL_DEPTH_8U, 3);
    cvNamedWindow(WINDOW_NAME, 1);

    if (rawImage == NULL)
    {
        nonFatalNonSystemCallError("Error while creating IplImage structure for raw images");
        return -1;
    }

    if (bgrImage == NULL)
    {
        nonFatalNonSystemCallError("Error while creating IplImage structure for bgr images");
        return -1;
    }

    //corner case - first message
    if (receiveMessage(clientDescriptor, sizeof(uint32_t), &messageSize) < 0)
    {
        nonFatalSystemCallError("Error while receiving message size");
        return -1;
    }
    messageSize = ntohl(messageSize);

    message = malloc(messageSize);
    if (message == NULL)
    {
        nonFatalSystemCallError("Error while allocating space for messages: %u", messageSize);
        return -1;
    }

    if (receiveMessage(clientDescriptor, messageSize, message) < 0)
    {
        nonFatalSystemCallError("Error while receiving message");
        return -1;
    }
    rawImage->imageData = (char*) message;

    if (rawImageConverter.convertRawToBGR(rawImage, bgrImage) < 0)
    {
        nonFatalNonSystemCallError("Error while converting raw to bgr image");
        return -1;
    }
    cvShowImage(WINDOW_NAME, bgrImage);
    cvWaitKey(100);
    printf("frame 0\n");

    for (int i = 1; i < 10000000 - 1; ++i)
    {
        if (receiveMessage(clientDescriptor, messageSize, message) < 0)
        {
            return -1;
        }

        if (rawImageConverter.convertRawToBGR(rawImage, bgrImage) == NULL)
        {
            return -1;
        }
        cvShowImage(WINDOW_NAME, bgrImage);
        cvWaitKey(100);
        printf("frame %i\n", i);
    }

    return 0;
}

int main(int argc, char**argv)
{
    struct overoStream::ReceiverOptions options;
    struct sockaddr_in socketAddress;
    struct sockaddr_in clientAddress;
    unsigned int clientAddressSize = sizeof(clientAddress);
    int socketDescriptor;
    int clientDescriptor;

    options.portGiven = false;

    if (readOptions(argc, argv, options) != 0)
    {
        return -1;
    }

    if (options.portGiven == false)
    {
        usage();
        return -1;
    }

    socketDescriptor = socket(AF_INET, SOCK_STREAM, 0);
    if (socket < 0)
    {
        perror("Error while creating socket");
        return -1;
    }

    socketAddress.sin_addr.s_addr = INADDR_ANY;
    socketAddress.sin_family = AF_INET;
    socketAddress.sin_port = options.port;

    if (bind(socketDescriptor, (struct sockaddr *) &socketAddress, sizeof(socketAddress)) < 0)
    {
        nonFatalSystemCallError("Error while binding socket to address");
        close(socketDescriptor);
        return -1;
    }

    if (listen(socketDescriptor, 5) < 0)
    {
        nonFatalSystemCallError("Error while marking socket as passive");
        close(socketDescriptor);
        return -1;
    }

    clientDescriptor = accept(socketDescriptor, (struct sockaddr*) &clientAddress, &clientAddressSize);
    if (clientDescriptor < 0)
    {
        nonFatalSystemCallError("Error while accepting connection");
        close(socketDescriptor);
    }
    printf("Connection accepted\n");

    receiveStream(clientDescriptor);

    close(clientDescriptor);
    close(socketDescriptor);

    return 0;
}
