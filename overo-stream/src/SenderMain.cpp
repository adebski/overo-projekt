/*
 * SenderMain.cpp
 *
 *  Created on: Feb 10, 2013
 *      Author: andrzej
 */

#include "Camera.h"
#include "Options.h"
#include <getopt.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "Error.h"

extern char *optarg;

void usage()
{
    printf("-p receiver port number\n");
    printf("-a receiver IP address\n");
    printf("-h print help \n");
}

int readOptions(int argc, char** argv, struct overoStream::SenderOptions &options)
{
    int option;
    int returnValue;
    long int portNumber;

    while ((option = getopt(argc, argv, "p:a:h:")) != -1)
    {
        switch (option)
        {
            case 'p':
                portNumber = strtol(optarg, NULL, 10);
                options.receiverPort = htons(portNumber);
                options.portGiven = true;
                break;
            case 'a':
                returnValue = inet_aton(optarg, &options.receiverIPAddress);
                if (returnValue == 0)
                {
                    perror("Invalid IP address");
                    return -1;
                }
                options.addressGiven = true;
                break;
            case 'h':
                usage();
                break;
            case '?':
                printf("Unrecognized option\n");
                return -1;
                break;
            default:
                printf("Undefined error\n");
                break;
        }
    }
    return 0;
}

int sendMessage(int socketDescriptor, uint32_t messageSize, void* message)
{
    int returnValue;

    if ((returnValue = write(socketDescriptor, message, messageSize)) < 0)
    {
        return -1;
    }

    return 0;
}

int prepareCameraForStream(overoStream::Camera *camera)
{
    if (camera->openDevice() < 0)
    {
        return -1;
    }

    if (camera->setFormat() < 0)
    {
        return -1;
    }

    if (camera->prepareBuffers(1) < 0)
    {
        return -1;
    }

    if (camera->startCapturing() < 0)
    {
        return -1;
    }

    return 0;
}

int startStreaming(int socketDescriptor)
{
    overoStream::Camera *camera = new overoStream::Camera("/dev/video2");
    overoStream::CameraImage cameraImage;
    uint32_t imageSizeNetworkOrder;

    cameraImage.image = NULL;

    if (camera == NULL)
    {
        nonFatalSystemCallError("Error while creating camera object");
    }

    if (prepareCameraForStream(camera) < 0)
    {
        nonFatalNonSystemCallError("Error while preparing camera for streaming");
        return -1;
    }

    if (camera->getFrame(&cameraImage) < 0)
    {
        nonFatalNonSystemCallError("Error while getting first frame");
        delete (camera);
        return -1;
    }

    imageSizeNetworkOrder = htonl(cameraImage.length);

    if (sendMessage(socketDescriptor,sizeof(uint32_t),&imageSizeNetworkOrder) < 0)
    {
        nonFatalSystemCallError("Error while sending image size");
        delete (camera);
        return -1;
    }

    if (sendMessage(socketDescriptor, cameraImage.length, cameraImage.image) < 0)
    {
        nonFatalSystemCallError("Error while sending first image");
        delete (camera);
        return -1;
    }

    for (int i = 1; i < 10000000; ++i)
    {
        if (camera->getFrame(&cameraImage) < 0)
        {
            nonFatalNonSystemCallError("Error while getting first frame");
            delete (camera);
            return -1;
        }

        if (sendMessage(socketDescriptor, cameraImage.length, cameraImage.image) < 0)
        {
            nonFatalNonSystemCallError("Error while sending %d frame", i);
            delete (camera);
            return -1;
        }
    }

    if (camera->stopCapturing() < 0)
    {
        nonFatalNonSystemCallError("Error while stopping streaming");
        delete (camera);
        return -1;
    }

    delete (camera);
    return 0;
}

int main(int argc, char**argv)
{
    struct overoStream::SenderOptions options;
    struct sockaddr_in socketAddress;

    int socketDescriptor;

    if (readOptions(argc, argv, options) != 0)
    {
        return -1;
    }

    if ((options.portGiven == false) || (options.addressGiven == false))
    {
        usage();
        return -1;
    }

    socketDescriptor = socket(AF_INET, SOCK_STREAM, 0);
    if (socket < 0)
    {
        perror("Error while creating socket");
        return -1;
    }

    socketAddress.sin_addr = options.receiverIPAddress;
    socketAddress.sin_family = AF_INET;
    socketAddress.sin_port = options.receiverPort;

    if (connect(socketDescriptor, (struct sockaddr *) &socketAddress, sizeof(socketAddress)) < 0)
    {
        perror("Error while connecting");
        return -1;
    }

    printf("Connection established\n");

    startStreaming(socketDescriptor);

    close(socketDescriptor);

    return 0;
}
