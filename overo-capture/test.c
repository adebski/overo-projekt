#include<stdio.h>
#include<stdlib.h>
#include<linux/videodev2.h>
#include"minunit.h"
#include<getopt.h>
#include<unistd.h>

#include"main.h"

extern int optind;

#include"options.h"
int tests_run = 0;

char *test_SGRBG10_parse_format()
{
    struct capture_options opt;
    struct device dev;

    char *cmd_line[] =
    { "test", "-f", "SGRBG10" };
    parse_cmd_line(3, cmd_line, &opt,&dev);
    mu_assert("format is not set properly", dev.pixelformat==V4L2_PIX_FMT_SGRBG10);
    return NULL;
}

char* test_invalid_parse_format()
{
    struct capture_options opt;
    struct device dev;

    char *cmd_line[] =
    { "test", "-f", "S" };
    int ret=parse_cmd_line(3, cmd_line, &opt,&dev);
    mu_assert("format was recognized even if it was invalid", ret == -1);

    return NULL;
}

static char* run_all_tests()
{
    mu_run_test(test_SGRBG10_parse_format);
    optind=0;
    mu_run_test(test_invalid_parse_format);
    return 0;
}

int main()
{
    char* result = run_all_tests();
    if (result)
        printf("%s\n", result);
    else
        printf("all tests passed\n");
    return 0;
}

