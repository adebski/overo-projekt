#ifndef OPTIONS_H
#define OPTIONS_H
#include<linux/videodev2.h>
#include<asm/types.h>

#include"main.h"

struct capture_options
{
    unsigned int do_print_info:1,
        do_set_format:1,
        do_save:1,
        do_capture:1,
        verbose:1;
    char * dev_name;
    char * file_name;
    __u32 format;
    long frames;
    long microseconds;
};

int parse_cmd_line(int argc, char **argv, struct capture_options *options, struct device *dev);
const char *v4l2_format_name(unsigned int );
#endif
