#include<stdio.h>
#include<stdlib.h>
#include<linux/videodev2.h>
#include<unistd.h>
#include<strings.h>
#include<asm/types.h>
#include"options.h"
#include<getopt.h>

#include"main.h"
#include"errhandling.h"

extern char *optarg;

static struct
{
    const char *name;
    __u32 fourcc;
} pixel_formats[] =
{
{ "RGB332", V4L2_PIX_FMT_RGB332 },
{ "RGB555", V4L2_PIX_FMT_RGB555 },
{ "RGB565", V4L2_PIX_FMT_RGB565 },
{ "RGB555X", V4L2_PIX_FMT_RGB555X },
{ "RGB565X", V4L2_PIX_FMT_RGB565X },
{ "BGR24", V4L2_PIX_FMT_BGR24 },
{ "RGB24", V4L2_PIX_FMT_RGB24 },
{ "BGR32", V4L2_PIX_FMT_BGR32 },
{ "RGB32", V4L2_PIX_FMT_RGB32 },
{ "Y8", V4L2_PIX_FMT_GREY },
{ "Y10", V4L2_PIX_FMT_Y10 },
{ "Y12", V4L2_PIX_FMT_Y12 },
{ "Y16", V4L2_PIX_FMT_Y16 },
{ "YUYV", V4L2_PIX_FMT_YUYV },
{ "UYVY", V4L2_PIX_FMT_UYVY },
{ "NV12", V4L2_PIX_FMT_NV12 },
{ "NV21", V4L2_PIX_FMT_NV21 },
{ "NV16", V4L2_PIX_FMT_NV16 },
{ "NV61", V4L2_PIX_FMT_NV61 },
{ "SBGGR8", V4L2_PIX_FMT_SBGGR8 },
{ "SGBRG8", V4L2_PIX_FMT_SGBRG8 },
{ "SGRBG8", V4L2_PIX_FMT_SGRBG8 },
{ "SRGGB8", V4L2_PIX_FMT_SRGGB8 },
{ "SGRBG10_DPCM8", V4L2_PIX_FMT_SGRBG10DPCM8 },
{ "SBGGR10", V4L2_PIX_FMT_SBGGR10 },
{ "SGBRG10", V4L2_PIX_FMT_SGBRG10 },
{ "SGRBG10", V4L2_PIX_FMT_SGRBG10 },
{ "SRGGB10", V4L2_PIX_FMT_SRGGB10 },
{ "SBGGR12", V4L2_PIX_FMT_SBGGR12 },
{ "SGBRG12", V4L2_PIX_FMT_SGBRG12 },
{ "SGRBG12", V4L2_PIX_FMT_SGRBG12 },
{ "SRGGB12", V4L2_PIX_FMT_SRGGB12 },
{ "DV", V4L2_PIX_FMT_DV },
{ "MJPEG", V4L2_PIX_FMT_MJPEG },
{ "MPEG", V4L2_PIX_FMT_MPEG }, };

#define ARRAY_SIZE(a) sizeof(a)/sizeof((a)[0])

__u32 v4l2_format_code(char *format)
{
    for (int i = 0; i < ARRAY_SIZE(pixel_formats); ++i)
    {
        if (strcasecmp(pixel_formats[i].name, format) == 0)
            return pixel_formats[i].fourcc;
    }
    return 0;
}

const char *v4l2_format_name(unsigned int fourcc)
{
    for (int i = 0; i < ARRAY_SIZE(pixel_formats); ++i)
    {
        if (pixel_formats[i].fourcc == fourcc)
            return pixel_formats[i].name;
    }

    return "unrecognized";
}

void printUsage()
{
    printf("-f format - specify format\n");
    printf("-o file - save image to a file\n");
    printf("-p - print format and time frame of the device\n");
    printf("-s wxh - set width and height of an image\n");
    printf("-c - capture images\n");
    printf("-h - print help\n");
    printf("-v - be verbose\n");
    printf("-n frames - number of frames to capture\n");
    printf("-t microseconds - number of microseconds to wait between each frame\n");
    return;
}

int parse_cmd_line(int argc, char **argv, struct capture_options *options, struct device *dev)
{
    int opt;
    char* endptr;

    while ((opt = getopt(argc, argv, "f:s:o:n:t:phvc")) != -1)
    {
        switch (opt)
        {
            case 'f':
                options->do_set_format = 1;
                dev->pixelformat = v4l2_format_code(optarg);
                if (dev->pixelformat == 0)
                {
                    printf("Unsupported video format: %s\n", optarg);
                    return -1;
                }
                break;
            case 'h':
                printUsage();
                exit(0);
            case 'o':
                options->do_save = 1;
                options->file_name = optarg;
                break;
            case 'p':
                options->do_print_info = 1;
                break;
            case 'v':
                options->verbose = 1;
                break;
            case 's':
                options->do_set_format = 1;
                dev->width = strtol(optarg, &endptr, 10);
                if (*endptr != 'x' || endptr == optarg)
                {
                    error_nsys_nf("Invalid size %s", optarg);
                    return -1;
                }
                dev->height = strtol(endptr + 1, &endptr, 10);
                if (*endptr != 0)
                {
                    error_nsys_nf("Invalid size %s", optarg);
                    return -1;
                }
                break;
            case 'c':
                options->do_capture=1;
                break;
            case 'n':
                options->frames = strtol(optarg,NULL,10);
                break;
            case 't':
                options->microseconds = strtol(optarg,NULL,10);
                break;
            default:
                printf("Unsupported option -%c\n", opt);
                return -1;
        }
    }

    if (optind >= argc)
    {
        printUsage();
        return -1;
    }

    options->dev_name = argv[optind];
    return 0;
}
