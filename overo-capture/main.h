#ifndef MAIN_H_
#define MAIN_H_

#include<sys/types.h>

struct device
{
    int fd;

    enum v4l2_buf_type type;
    enum v4l2_memory memtype;
    unsigned int nbufs;
    struct buffer *buffers;

    __u32 pixelformat;
    __u32 width;
    __u32 height;
    __u32 bytesperline;
    __u32 imagesize;

    void *pattern;
    unsigned int patternsize;
};

struct buffer
{
    unsigned int padding;
    unsigned int size;
    void *mem;
};

#endif
