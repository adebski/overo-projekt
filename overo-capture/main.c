#define _BSD_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>


#include <getopt.h>             /* getopt_long() */

#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <malloc.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <asm/types.h>          /* for videodev2.h */

#include <linux/videodev2.h>
#include <linux/media.h>
#include <linux/v4l2-subdev.h>

#include"options.h"
#include"errhandling.h"
#include"main.h"

#define CLEAR(x) memset (&(x), 0, sizeof (x))

int open_device(struct capture_options *opt, struct device *dev)
{

    struct v4l2_capability cap;

    dev->fd = -1;
    dev->memtype = V4L2_MEMORY_MMAP;
    dev->buffers = NULL;
    dev->type = (enum v4l2_buf_type) -1;

    dev->fd = open(opt->dev_name, O_RDWR);
    if (dev->fd == -1)
    {
        error_nsys_nf("Error opening device %s", opt->dev_name);
        return -1;
    }

    printf("Device %s opened.\n", opt->dev_name);

    memset(&cap, 0, sizeof cap);
    if (ioctl(dev->fd, VIDIOC_QUERYCAP, &cap) == -1)
    {
        error_sys_nf("Error querying capabilities of %s", opt->dev_name);
        close(dev->fd);
        return -1;
    }

    if (cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)
        dev->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    else
    {
        error_sys_nf("Error opening device %s: video capture not supported", opt->dev_name);
        close(dev->fd);
        return -1;
    }

    printf("Successfully opened device %s which is video capture device\n", opt->dev_name);
    return 0;
}

int close_device(struct capture_options *opt, struct device *dev)
{
    printf("Closing device %s\n", opt->dev_name);
    close(dev->fd);
    return 0;
}

int print_info(struct device *dev)
{
    struct v4l2_format fmt;
    struct v4l2_streamparm parm;

    memset(&fmt, 0, sizeof fmt);
    fmt.type = dev->type;

    if (ioctl(dev->fd, VIDIOC_G_FMT, &fmt) < 0)
    {
        error_sys_nf("Unable to get format");
        return -1;
    }

    printf("Video format: %s (%08x) %ux%u (B/per line %u) buffer size %u\n", v4l2_format_name(fmt.fmt.pix.pixelformat),
            fmt.fmt.pix.pixelformat, fmt.fmt.pix.width, fmt.fmt.pix.height, fmt.fmt.pix.bytesperline,
            fmt.fmt.pix.sizeimage);

    memset(&parm, 0, sizeof parm);
    parm.type = dev->type;

    if (ioctl(dev->fd, VIDIOC_G_PARM, &parm) < 0)
    {
        error_sys_nf("Unable to get params");
        return -1;
    }
    return 0;
}

int video_set_format(struct device *dev)
{
    struct v4l2_format fmt;
    memset(&fmt, 0, sizeof fmt);

    fmt.type = dev->type;
    fmt.fmt.pix.width = dev->width;
    fmt.fmt.pix.height = dev->height;
    fmt.fmt.pix.pixelformat = dev->pixelformat;
    fmt.fmt.pix.bytesperline = dev->bytesperline;
    fmt.fmt.pix.field = V4L2_FIELD_ANY;

    if (ioctl(dev->fd, VIDIOC_S_FMT, &fmt) == -1)
    {
        error_sys_nf("Unable to set format");
        return -1;
    }

    if (ioctl(dev->fd, VIDIOC_G_FMT, &fmt) < 0)
    {
        error_sys_nf("Unable to get format");
        return -1;
    }

    printf("Video format set: %s (%08x) %ux%u (stride %u) buffer size %u\n", v4l2_format_name(fmt.fmt.pix.pixelformat),
            fmt.fmt.pix.pixelformat, fmt.fmt.pix.width, fmt.fmt.pix.height, fmt.fmt.pix.bytesperline,
            fmt.fmt.pix.sizeimage);
    return 0;
}

int video_allocate_buffers(struct device *dev)
{
    struct v4l2_requestbuffers rb;
    struct v4l2_buffer buf;
    struct buffer *buffers;

    memset(&rb, 0, sizeof rb);
    rb.count = dev->nbufs;
    rb.type = dev->type;
    rb.memory = dev->memtype;

    if (ioctl(dev->fd, VIDIOC_REQBUFS, &rb) == -1)
    {
        error_sys_nf("Unable to request buffers");
        return -1;
    }

    printf("%u buffers requested.\n", rb.count);

    buffers = malloc(sizeof(struct buffer) * rb.count);
    if (buffers == NULL)
    {
        error_sys_nf("Error while allocating buffer information array");
        return -1;
    }

    /* Map the buffers. */
    for (int i = 0; i < rb.count; ++i)
    {
        memset(&buf, 0, sizeof buf);
        buf.index = i;
        buf.type = dev->type;
        buf.memory = dev->memtype;

        if (ioctl(dev->fd, VIDIOC_QUERYBUF, &buf) == -1)
        {
            error_sys_nf("Unable to query buffer %u", i);
            return -1;
        }
        printf("length: %u offset: %u\n", buf.length, buf.m.offset);

        switch (dev->memtype)
        {
            case V4L2_MEMORY_MMAP:

                if ((buffers[i].mem = mmap(0, buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, dev->fd, buf.m.offset))
                        == MAP_FAILED)
                {
                    error_sys_nf("Unable to map buffer %u");
                    return -1;
                }
                buffers[i].size = buf.length;
                buffers[i].padding = 0;
                printf("Buffer %u mapped at address %p.\n", i, buffers[i].mem);
                break;

                /*case V4L2_MEMORY_USERPTR:
                 ret = posix_memalign(&buffers[i].mem, page_size, buf.length + offset + padding);
                 if (ret < 0)
                 {
                 printf("Unable to allocate buffer %u (%d)\n", i, ret);
                 return -ENOMEM;
                 }
                 buffers[i].mem += offset;
                 buffers[i].size = buf.length;
                 buffers[i].padding = padding;
                 printf("Buffer %u allocated at address %p.\n", i, buffers[i].mem);
                 break;*/

            default:
                break;
        }
    }

    dev->buffers = buffers;
    dev->nbufs = rb.count;
    return 0;
}

int video_queue_buffer(struct device *dev, int index)
{
    struct v4l2_buffer buf;

    memset(&buf, 0, sizeof buf);
    buf.index = index;
    buf.type = dev->type;
    buf.memory = dev->memtype;
    buf.length = dev->buffers[index].size;

    if (dev->memtype == V4L2_MEMORY_USERPTR)
        buf.m.userptr = (unsigned long) dev->buffers[index].mem;

    if (ioctl(dev->fd, VIDIOC_QBUF, &buf) == -1)
    {
        error_sys_nf("Unable to queue buffer %d", index);
        return -1;
    }

    return 0;
}

int video_free_buffers(struct device * dev)
{
    struct v4l2_requestbuffers rb;

    for (int i = 0; i < dev->nbufs; ++i)
    {
        switch (dev->memtype)
        {
            case V4L2_MEMORY_MMAP:
                if (munmap(dev->buffers[i].mem, dev->buffers[i].size) < 0)
                {
                    error_sys_nf("Unable to unmap buffer %u");
                    return -1;
                }
                break;
            case V4L2_MEMORY_USERPTR:
                free(dev->buffers[i].mem);
                break;
            default:
                break;
        }
        dev->buffers[i].mem = NULL;
    }

    memset(&rb, 0, sizeof rb);
    rb.count = 0;
    rb.type = dev->type;
    rb.memory = dev->memtype;

    if (ioctl(dev->fd, VIDIOC_REQBUFS, &rb) == -1)
    {
        error_sys_nf("Unable to release buffers");
        return -1;
    }

    printf("%u buffers released.\n", dev->nbufs);

    free(dev->buffers);
    dev->nbufs = 0;
    dev->buffers = NULL;

    return 0;
}

int video_prepare_capture(struct device *dev)
{
    if (video_allocate_buffers(dev) == -1)
        return -1;

    for (int i = 0; i < dev->nbufs; ++i)
    {
        if (video_queue_buffer(dev, i) == -1)
            return -1;
    }

    return 0;
}

int video_enable(struct device *dev, int enable)
{
    int type = dev->type;

    if (ioctl(dev->fd, enable ? VIDIOC_STREAMON : VIDIOC_STREAMOFF, &type) == -1)
    {
        error_sys_nf("Unable to %s streaming", enable ? "start" : "stop");
        return -1;
    }

    return 0;
}

int video_save_image(struct device *dev, struct capture_options * opt, struct v4l2_buffer *buf, unsigned int image_no)
{
    unsigned int filename_len = 6;
    char *filename = malloc(sizeof(char) * filename_len);
    int fd;

    if (filename == NULL)
    {
        error_sys_nf("Error while mallocing memory for file name");
        return -1;
    }

    snprintf(filename, filename_len, "%u", image_no);

    if ((fd = open(filename, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH)) == -1)
    {
        error_sys_nf("Error while opening/creating file %s", filename);
        return -1;
    }
    free(filename);

    if (write(fd, dev->buffers[buf->index].mem, buf->bytesused) == -1)
    {
        close(fd);
        error_sys_nf("Error while writing image to a file %s", filename);
        return -1;
    }
    close(fd);

    return 0;
}

int video_do_capture(struct device * dev, struct capture_options * opt)
{
    struct v4l2_buffer buf;
    unsigned int size;
    unsigned int i;

    /* Start streaming. */
    if (video_enable(dev, 1) == -1)
        return video_free_buffers(dev);

    size = 0;

    for (i = 0; i < opt->frames; ++i)
    {
        /* Dequeue a buffer. */
        memset(&buf, 0, sizeof buf);
        buf.type = dev->type;
        buf.memory = dev->memtype;

        if (ioctl(dev->fd, VIDIOC_DQBUF, &buf) == -1)
        {
            if (errno != EIO)
            {
                printf("Unable to dequeue buffer");
                return video_free_buffers(dev);
            }
            /* If this is user ptr buffers */
            buf.type = dev->type;
            buf.memory = dev->memtype;
            if (dev->memtype == V4L2_MEMORY_USERPTR)
                buf.m.userptr = (unsigned long) dev->buffers[i].mem;
        }

        if (dev->type == V4L2_BUF_TYPE_VIDEO_CAPTURE && dev->imagesize != 0 && buf.bytesused != dev->imagesize)
            printf("Warning: bytes used %u != image size %u\n", buf.bytesused, dev->imagesize);

        size += buf.bytesused;

        /* Save the image. */
        if (opt->do_save)
        {
            video_save_image(dev, opt, &buf, i);
        }

        fflush(stdout);

        if (video_queue_buffer(dev, buf.index) == -1)
        {
            error_sys_nf("Unable to requeue buffer");
            return video_free_buffers(dev);
        }

        /*sleep interval*/
        if (opt->microseconds >0)
        {
            usleep(1000*opt->microseconds);
        }
    }

    /* Stop streaming. */
    video_enable(dev, 0);

    printf("Captured %u frames", i);

    return 0;
}

int main(int argc, char ** argv)
{
    struct capture_options opt;
    struct device dev;

    memset(&dev, 0, sizeof(struct device));

    opt.frames = 5;

    dev.memtype = V4L2_MEMORY_MMAP;
    dev.pixelformat = V4L2_PIX_FMT_SGRBG10;
    dev.width = 752;
    dev.height = 480;
    dev.nbufs = 4;

    if (parse_cmd_line(argc, argv, &opt, &dev) == -1)
        exit(-1);

    if (open_device(&opt, &dev) == -1)
        exit(-1);

    if (opt.do_print_info)
        if (print_info(&dev) == -1)
        {
            close_device(&opt, &dev);
            exit(-1);
        }
    if (opt.do_set_format)
        if (video_set_format(&dev) == -1)
        {
            close_device(&opt, &dev);
            exit(-1);
        }

    if (!opt.do_capture)
    {
        close_device(&opt, &dev);
        exit(0);
    }

    if (video_prepare_capture(&dev))
    {
        printf("Error while preparing everything for a capture\n");
        exit(-1);
    }

    if (video_do_capture(&dev, &opt) == -1)
    {
        printf("Error while capturing images\n");
        exit(-1);
    }

    close_device(&opt, &dev);
    exit(0);
}
