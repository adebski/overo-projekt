#ifndef ERRHANDLING
#define ERRHANDLING
#define MAXLINE 4096
#include<stdarg.h>
/*
 * free allocated space
 */
void error_free(int num,...);

/*
 * write to stderr, return to caller
 */
void error_write(int errnoflag,int error,const char *fmt,va_list ap);

/*
 *nonfatal error related to system call
 *print message and return
 */
void error_sys_nf(const char *fmt, ...);

/*
 * fatal error related to system call
 * print message and terminate
 */
void error_sys_f(const char *fmt, ...);

/*
 * fatal error related to system call
 * print message, dump core, and terminate
 */
void error_sys_d(const char *fmt, ...);

/*nonfatal error unrelated to system call
 * print message and return
 */
void error_nsys_nf(const char *fmt, ...);

/*
 * fatal error unrelated to system call
 * error code passed as explicit parameter
 * print message and terminate
 */
void error_nsys_fp(int error, const char *fmt, ...);

/*
 * fatal error unrelated to system call
 * print message and terminate
 */
void error_nsys_f(const char *fmt, ...);
#endif
