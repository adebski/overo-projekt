#include<stdlib.h>
#include<stdio.h>
#include"errhandling.h"
#include"opencv/cv.h"
#include"opencv/highgui.h"

void printInteractiveUsage()
{
	printf("0. quit\n");
	printf("1. normalize histogram\n");
	printf("2. get one channel\n");
}

int copyImage(IplImage *src, IplImage **dst)
{
	CvSize size = cvGetSize(src);
	(*dst) = cvCreateImage(size, src->depth, src->nChannels);
	if ((*dst) == NULL)
	{
		error_nsys_nf("error while allocating space for image copy");
		return -1;
	}

	cvCopy(src, *dst, NULL);

	return 0;
}

int normalizeHistogram(IplImage **bgrImage)
{
	int imageWidth = (*bgrImage)->width;
	int imageHeigth = (*bgrImage)->height;
	int imageDepth = (*bgrImage)->depth;
	CvSize imageSize = cvGetSize(*bgrImage);
	IplImage *bChannel = cvCreateImage(imageSize, imageDepth, 1);
	IplImage *gChannel = cvCreateImage(imageSize, imageDepth, 1);
	IplImage *rChannel = cvCreateImage(imageSize, imageDepth, 1);

	IplImage *normalizedBChannel = cvCreateImage(imageSize, imageDepth, 1);
	IplImage *normalizedGChannel = cvCreateImage(imageSize, imageDepth, 1);
	IplImage *normalizedRChannel = cvCreateImage(imageSize, imageDepth, 1);

	IplImage *normalizedImage = cvCreateImage(imageSize, imageDepth, 3);
	if (bChannel == NULL || gChannel == NULL || rChannel == NULL)
	{
		error_nsys_nf("error while allocating space for separate channels");
		return -1;
	}

	if (normalizedBChannel == NULL || normalizedGChannel == NULL
			|| normalizedRChannel == NULL)
	{
		error_nsys_nf("error while allocating space for normalized channels");
		return -1;
	}

	if (normalizedImage == NULL)
	{
		error_nsys_nf("error while allocating space for normalized image");
	}

	int numBins = 256;
	float range[] =
	{ 0, 255 };
	float *ranges[] =
	{ range };

	CvHistogram *bHist = cvCreateHist(1, &numBins, CV_HIST_ARRAY, ranges, 1);
	CvHistogram *gHist = cvCreateHist(1, &numBins, CV_HIST_ARRAY, ranges, 1);
	CvHistogram *rHist = cvCreateHist(1, &numBins, CV_HIST_ARRAY, ranges, 1);

	if (bHist == NULL || gHist == NULL || rHist == NULL)
	{
		error_nsys_nf("error while allocating histograms");
		return -1;
	}

	cvSplit(*bgrImage, bChannel, gChannel, rChannel, NULL);
	cvCalcHist(&bChannel, bHist, 0, NULL);
	cvCalcHist(&gChannel, bHist, 0, NULL);
	cvCalcHist(&rChannel, bHist, 0, NULL);

	cvEqualizeHist(bChannel, normalizedBChannel);
	cvEqualizeHist(gChannel, normalizedGChannel);
	cvEqualizeHist(rChannel, normalizedRChannel);

	unsigned char* normalizedData = (unsigned char*) normalizedImage->imageData;
	unsigned char* normalizedBChannelData =
			(unsigned char*) normalizedBChannel->imageData;
	unsigned char* normalizedGChannelData =
			(unsigned char*) normalizedGChannel->imageData;
	unsigned char* normalizedRChannelData =
			(unsigned char*) normalizedRChannel->imageData;

	for (int row = 0; row < imageHeigth; ++row)
	{
		for (int column = 0; column < imageWidth; ++column)
		{
			normalizedData[column * 3] = normalizedBChannelData[column];
			normalizedData[column * 3 + 1] = normalizedGChannelData[column];
			normalizedData[column * 3 + 2] = normalizedRChannelData[column];
		}
		normalizedData += normalizedImage->widthStep;
		normalizedBChannelData += normalizedBChannel->widthStep;
		normalizedGChannelData += normalizedGChannel->widthStep;
		normalizedRChannelData += normalizedRChannel->widthStep;
	}

	cvNamedWindow("Image view", 1);
	cvShowImage("Image view", normalizedImage);
	cvWaitKey(0);
	return 0;
}

int getOneChannel(IplImage *bgrImage, int channel)
{
	int imageDepth = (bgrImage)->depth;
	CvSize imageSize = cvGetSize(bgrImage);
	IplImage *bChannel = cvCreateImage(imageSize, imageDepth, 1);
	IplImage *gChannel = cvCreateImage(imageSize, imageDepth, 1);
	IplImage *rChannel = cvCreateImage(imageSize, imageDepth, 1);

	cvSplit(bgrImage, bChannel, gChannel, rChannel, NULL);


	cvNamedWindow("Image view", 1);
	if (channel == 1)
		cvShowImage("Image view", bChannel);
	else if (channel == 2)
		cvShowImage("Image view", gChannel);
	else if (channel == 3)
		cvShowImage("Image view", rChannel);
	cvWaitKey(0);
	return 0;
}

int interactiveMainRoutine(IplImage *bgrImage)
{
	IplImage *bgrImageCopy;
	int opt;
	int secondOpt;
	if (copyImage(bgrImage, &bgrImageCopy) == -1)
		return -1;

	printInteractiveUsage();
	int proceed = 1;
	while (proceed)
	{
		if (system("clear") == -1)
		{
		    error_sys_nf("error while clearing terminal screen");
		    return -1;
		}
		printInteractiveUsage();
		if (scanf("%d", &opt) == EOF)
		{
		    error_sys_nf("error while reading option value");
		    return -1;
		}
		switch (opt)
		{
		case 0:
			proceed = 0;
			break;
		case 1:
			normalizeHistogram(&bgrImageCopy);
			break;
		case 2:
			printf("\t1 b\n\t2 g\n\t3 r\n");
			if (scanf("%d",&secondOpt) == EOF)
			{
			    error_sys_nf("error while reading channel number");
			    return -1;
			}
			getOneChannel(bgrImageCopy,secondOpt);
			break;
		}
	}

	return 0;
}
