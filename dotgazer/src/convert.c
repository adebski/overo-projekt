#include "opencv/cv.h"
#include<stdlib.h>
#include<stdio.h>
#include"errhandling.h"
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>

#include"convert.h"

char* loadRawData(char *path, int size)
{
    int fd;
    char* buf;
    int ret;

    if ((fd = open(path, O_RDONLY)) == -1)
    {
        error_sys_nf("error while opening file %s", path);
        return NULL;
    }

    if ((buf = malloc(sizeof(char) * size)) == NULL)
    {
        error_sys_nf("error while allocating memory for buffer");
        close(fd);
        return NULL;
    }

    if ((ret = read(fd, buf, size)) != size)
    {
        error_sys_nf("read %d bytes, should %d", ret, size);
        free(buf);
        close(fd);
        return NULL;
    }

    close(fd);
    return buf;
}

IplImage* loadRawImage(char* path)
{
    IplImage *rawImage;
    CvSize size =
    { IMAGE_WIDTH, IMAGE_HEIGHT };

    if ((rawImage = cvCreateImage(size, IPL_DEPTH_16U, 1)) == NULL)
    {
        error_nsys_nf("error while allocating memory for raw image");
        return NULL;
    }

    if ((rawImage->imageData = loadRawData(path, IMAGE_WIDTH * IMAGE_HEIGHT * 2)) == NULL)
    {
        cvReleaseImage(&rawImage);
        return NULL;
    }

    return rawImage;
}

IplImage* convertRawToBGR(IplImage *rawImage)
{
    CvSize imageSize = cvGetSize(rawImage);
    IplImage *rescaledImage, *bgrImage;

    if ((rescaledImage = cvCreateImage(imageSize, IPL_DEPTH_8U, 1)) == NULL)
    {
        error_nsys_nf("error while allocating memory for rescaled image");
        return NULL;
    }

    if ((bgrImage = cvCreateImage(imageSize, IPL_DEPTH_8U, 3)) == NULL)
    {
        error_nsys_nf("error while allocating memory for rescaled image");
        cvReleaseImage(&rescaledImage);
        return NULL;
    }

    cvConvertScale(rawImage, rescaledImage, 0.25, 0.0);
    cvCvtColor(rescaledImage, bgrImage, CV_BayerGB2BGR);

    cvReleaseImage(&rescaledImage);
    return bgrImage;
}

IplImage * getYFromRaw(IplImage *rawImage)
{
    CvSize imageSize = cvGetSize(rawImage);
    IplImage *bgrImage, *yuvImage;
    IplImage *yChannel, *uChannel, *vChannel;

    uChannel = cvCreateImage(imageSize, IPL_DEPTH_8U, 1);
    vChannel = cvCreateImage(imageSize, IPL_DEPTH_8U, 1);

    if ((yuvImage = cvCreateImage(imageSize, IPL_DEPTH_8U, 3)) == NULL)
    {
        error_nsys_nf("error while allocating memory for yuv image");
        return NULL;
    }

    if ((yChannel = cvCreateImage(imageSize, IPL_DEPTH_8U, 1)) == NULL)
    {
        error_nsys_nf("error while allocating memory for y channel");
        cvReleaseImage(&yuvImage);
        return NULL;
    }

    if ((uChannel = cvCreateImage(imageSize, IPL_DEPTH_8U, 1)) == NULL)
    {
        error_nsys_nf("error while allocating memory for u channel");
        cvReleaseImage(&yuvImage);
        cvReleaseImage(&yChannel);
        return NULL;
    }

    if ((vChannel = cvCreateImage(imageSize, IPL_DEPTH_8U, 1)) == NULL)
    {
        error_nsys_nf("error while allocating memory for v channel");
        cvReleaseImage(&yuvImage);
        cvReleaseImage(&yChannel);
        cvReleaseImage(&uChannel);
        return NULL;
    }

    bgrImage = convertRawToBGR(rawImage);
    cvCvtColor(bgrImage, yuvImage, CV_BGR2YCrCb);
    cvSplit(yuvImage, yChannel, uChannel, vChannel, NULL);

    cvReleaseImage(&yuvImage);
    cvReleaseImage(&uChannel);
    cvReleaseImage(&vChannel);

    return yChannel;
}
