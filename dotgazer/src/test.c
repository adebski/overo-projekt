#include <stdio.h>
#include <math.h>
#include "opencv/cv.h"
#include "opencv/highgui.h"

#include"options.h"
#include"convert.h"
#include"interactive.h"
#include"test.h"

#define PI 3.14159
#define CONFIG_FILE "markers_config.txt"
#define SCREEN_WIDTH 752
#define SCREEN_HEIGHT 480
#define METERS_PER_PIXEL 1
#define MAX_MARKERS_NUM 20

struct s_marker marker[MAX_MARKERS_NUM]; // maksymalnie 10 markerow po 9 kropek
int markersNum = 0;
IplImage *rawImage;
IplImage *img;

void determineMyPositionUsingThreeMarkers(void) {
	// maksymalny numer markera to 626, wiec robimy taka jakby mape z koordynatami
	float xCoords[627];
	float yCoords[627];
	float tmpX, tmpY, robotX, robotY, dummy;
	int num;
	FILE* file;
	struct s_dot screenCenter;

	screenCenter.x = SCREEN_WIDTH / 2;
	screenCenter.y = SCREEN_HEIGHT / 2;

	if ((file = fopen(CONFIG_FILE, "r")) == NULL) {
		printf("Nie udalo sie otworzyc pliku %s\n", CONFIG_FILE);
		exit(1);
	}
	while (fscanf(file, "%d %f %f %f", &num, &tmpX, &tmpY, &dummy) != EOF) {
		xCoords[num] = tmpX;
		yCoords[num] = tmpY;
	}
	fclose(file);

	for (int i = 0; i < markersNum; i++) {
//		printf("N %lf %lf, S %lf %lf, W %lf %lf, E %lf %lf\n",
//				marker[i].north.x, marker[i].north.y, marker[i].south.x, marker[i].south.y,
//				marker[i].west.x, marker[i].west.y, marker[i].east.x, marker[i].east.y);
		struct s_dot mainDot = removeBogusDot(marker[i]);
		marker[i].id = calculateMarkerId(mainDot, i);
//		printf("marker nr %d, center = %lf %lf\n", marker[i].id,
//				marker[i].center.x, marker[i].center.y);

//		drawCircle(img, marker[i].center.x, marker[i].center.y, 50, 255, 1);
//		cvShowImage("Image view", img);
//		cvWaitKey(0);
//		cvCircle(img, cvPoint(cvRound(marker[i].center.x), cvRound(marker[i].center.y)),
//				1, CV_RGB(0,255,0), 2, CV_AA, 0);
		float distanceInPixels = dotsDistance(screenCenter, marker[i].center);
		float distanceInMeters = distanceInPixels * METERS_PER_PIXEL;
		marker[i].distanceFromScreenCenterInMeters = distanceInMeters;
		marker[i].distanceFromScreenCenterInPixels = distanceInPixels;
		marker[i].absoluteX = xCoords[marker[i].id];
		marker[i].absoluteY = yCoords[marker[i].id];
//		cvCircle(img, cvPoint(cvRound(marker[i].center.x), cvRound(marker[i].center.y)),
//				cvRound(marker[i].distanceFromScreenCenterInPixels), CV_RGB(0,0,255), 1, CV_AA, 0);
//		printf("odleglosc od srodka ekranu = %lf\n", distanceInPixels);
	}

	// obliczamy punkt przeciecia trzech markerow
	sortMarkersByDistanceFromScreenCenter();
	calculateCrossingPoints(&robotX, &robotY, 0, 1, 2);
	drawCircle(img, SCREEN_WIDTH - robotX, SCREEN_HEIGHT - robotY, 1, 0, 4);
	printf("polozenie robota = %lf %lf\n", robotX, robotY);

}

void calculateCrossingPoints(float* resX, float* resY, int idx1, int idx2,
		int idx3) {
	drawCircle(img, marker[idx1].center.x, marker[idx1].center.y,
			marker[idx1].distanceFromScreenCenterInPixels, 255, 1);
	drawCircle(img, marker[idx2].center.x, marker[idx2].center.y,
			marker[idx2].distanceFromScreenCenterInPixels, 150, 1);
	drawCircle(img, marker[idx3].center.x, marker[idx3].center.y,
			marker[idx3].distanceFromScreenCenterInPixels, 50, 1);

	struct s_marker m1, m2, m3;
	m1 = marker[idx1];
	m2 = marker[idx2];
	m3 = marker[idx3];
	float r1 = m1.distanceFromScreenCenterInMeters;
	float x1 = m1.absoluteX;
	float y1 = m1.absoluteY;
	float r2 = m2.distanceFromScreenCenterInMeters;
	float x2 = m2.absoluteX;
	float y2 = m2.absoluteY;
	float r3 = m3.distanceFromScreenCenterInMeters;
	float x3 = m3.absoluteX;
	float y3 = m3.absoluteY;
//	printf("dane (%lf, %lf) r=%lf, (%lf, %lf) r=%lf, (%lf, %lf) r=%lf\n",
//			x1, y1, r1, x2, y2, r2, x3, y3, r3);

	float d = pointsDistance(m1.absoluteX, m1.absoluteY, m2.absoluteX,
			m2.absoluteY);
	float a = (r1 * r1 - r2 * r2 + d * d) / (2 * d);
	float h = sqrt(r1 * r1 - a * a);
//	printf("d=%lf, a=%lf, h=%lf\n", d, a, h);
	float x4 = x1 + a * (x2 - x1) / d;
	float y4 = y1 + a * (y2 - y1) / d;
	float x5 = x4 + h * (y2 - y1) / d;
	float y5 = y4 - h * (x2 - x1) / d;
	float x6 = x4 - h * (y2 - y1) / d;
	float y6 = y4 + h * (x2 - x1) / d;
//	printf("dupa %lf %lf %lf %lf\n",x5, y5,x6, y6);
//	drawCircle(img, x5, y5, 1, 0, 5);
//	drawCircle(img, x6, y6, 1, 0, 5);
	if (fabs(pointsDistance(x5, y5, x3, y3) - r3)
			< fabs(pointsDistance(x6, y6, x3, y3) - r3)) {
		*resX = x5;
		*resY = y5;
	} else {
		*resX = x6;
		*resY = y6;
	}
}

void determineMyPositionUsingOneMarker(void) {
	// maksymalny numer markera to 626, wiec robimy taka jakby mape z koordynatami
	float xCoords[627];
	float yCoords[627];
	float angle[627];
	float tmpX, tmpY, robotX, robotY, tmpAngle;
	int num;
	FILE* file;
	struct s_dot screenCenter;

	screenCenter.x = SCREEN_WIDTH / 2;
	screenCenter.y = SCREEN_HEIGHT / 2;

	if ((file = fopen(CONFIG_FILE, "r")) == NULL) {
		printf("Nie udalo sie otworzyc pliku %s\n", CONFIG_FILE);
		exit(1);
	}
	while (fscanf(file, "%d %f %f %f", &num, &tmpX, &tmpY, &tmpAngle) != EOF) {
		xCoords[num] = tmpX;
		yCoords[num] = tmpY;
		angle[num] = tmpAngle;
	}
	fclose(file);

	for (int i = 0; i < markersNum; i++) {
//		printf("N %lf %lf, S %lf %lf, W %lf %lf, E %lf %lf\n",
//				marker[i].north.x, marker[i].north.y, marker[i].south.x, marker[i].south.y,
//				marker[i].west.x, marker[i].west.y, marker[i].east.x, marker[i].east.y);
		struct s_dot mainDot = removeBogusDot(marker[i]);
		marker[i].id = calculateMarkerId(mainDot, i);
//		printf("marker nr %d, center = %lf %lf\n", marker[i].id,
//				marker[i].center.x, marker[i].center.y);

//		drawCircle(img, marker[i].center.x, marker[i].center.y, 50, 255, 1);
//		cvShowImage("Image view", img);
//		cvWaitKey(0);
//		cvCircle(img, cvPoint(cvRound(marker[i].center.x), cvRound(marker[i].center.y)),
//				1, CV_RGB(0,255,0), 2, CV_AA, 0);
		float distanceInPixels = dotsDistance(screenCenter, marker[i].center);
		float distanceInMeters = distanceInPixels * METERS_PER_PIXEL;
		marker[i].distanceFromScreenCenterInMeters = distanceInMeters;
		marker[i].distanceFromScreenCenterInPixels = distanceInPixels;
		marker[i].absoluteX = xCoords[marker[i].id];
		marker[i].absoluteY = yCoords[marker[i].id];
		marker[i].calibratedAngle = angle[marker[i].id];
//		cvCircle(img, cvPoint(cvRound(marker[i].center.x), cvRound(marker[i].center.y)),
//				cvRound(marker[i].distanceFromScreenCenterInPixels), CV_RGB(0,0,255), 1, CV_AA, 0);
//		printf("odleglosc od srodka ekranu = %lf\n", distanceInPixels);
	}

	// obliczamy punkt przeciecia trzech markerow
	sortMarkersByDistanceFromScreenCenter();
	calculateRobotPosition(&robotX, &robotY, 0);
	drawCircle(img, SCREEN_WIDTH - robotX, SCREEN_HEIGHT - robotY, 1, 0, 4);
	printf("polozenie robota = %lf %lf\n", robotX, robotY);

}

void calculateRobotPosition(float* robotX, float* robotY, int idx) {
	drawCircle(img, marker[idx].center.x, marker[idx].center.y,
			marker[idx].distanceFromScreenCenterInPixels, 255, 1);

	struct s_marker m = marker[idx];
//	float angleDiff = markerAngle(m) - m.calibratedAngle;
	float angleDiff = m.calibratedAngle - markerAngle(m);
	float xShift = (SCREEN_WIDTH / 2 - m.center.x) * METERS_PER_PIXEL;
	float yShift = (SCREEN_HEIGHT / 2 - m.center.y) * METERS_PER_PIXEL;
	*robotX = m.absoluteX + sin(angleDiff) * yShift + cos(angleDiff) * xShift;
	*robotY = m.absoluteY + cos(angleDiff) * yShift + sin(angleDiff) * xShift;
}

void removeBogusMarkers(void) {
	for (int i = markersNum - 1; i >= 0; i--) {
		if (marker[i].dotsNum < 3) {
			for (int j = i + 1; j < markersNum; j++)
				marker[j - 1] = marker[j];
			markersNum--;
		}
	}
}

int assignDotToMarker(struct s_dot dot) {
	// probujemy dopasowac kropke do jakiegos juz istniejacego markera
	for (int i = 0; i < markersNum; i++) {
		// 40 - empirycznie wyliczony promien markera
		if (fabs(dot.x - marker[i].dot[0].x) < 70
				&& fabs(dot.y - marker[i].dot[0].y) < 70) {
			marker[i].dot[marker[i].dotsNum] = dot;
			if (dot.x < marker[i].west.x)
				marker[i].west = dot;
			if (dot.x > marker[i].east.x)
				marker[i].east = dot;
			if (dot.y > marker[i].north.y)
				marker[i].north = dot;
			if (dot.y < marker[i].south.y)
				marker[i].south = dot;
			marker[i].dotsNum++;
			return i;
		}
	}
	// jezeli sie nie udalo, to tworzymy nowy marker
	marker[markersNum].dot[0] = dot;
	marker[markersNum].dotsNum = 1;
	marker[markersNum].west = dot;
	marker[markersNum].east = dot;
	marker[markersNum].north = dot;
	marker[markersNum].south = dot;
	++markersNum;
	return markersNum - 1;
}

float dotsDistance(struct s_dot a, struct s_dot b) {
	return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
}

float pointsDistance(float x1, float y1, float x2, float y2) {
	return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}

struct s_dot removeBogusDot(struct s_marker m) {
	struct s_dot mainDot;

	if (!oneEdgeDotIsTheSame(m)) {
		float short1 = 0, long1 = 0, short2 = 0, long2 = 0;
		short1 = min(dotsDistance(m.north, m.west),
				dotsDistance(m.south, m.east));
		long1 = max(dotsDistance(m.north, m.west),
				dotsDistance(m.south, m.east));
		short2 = min(dotsDistance(m.north, m.east),
				dotsDistance(m.south, m.west));
		long2 = max(dotsDistance(m.north, m.east),
				dotsDistance(m.south, m.west));

		if (abs(short1 - long2) < abs(short2 - long1)) {
			if (dotsDistance(m.north, m.west) < dotsDistance(m.south, m.east)) {
				if (dotsDistance(m.north, m.east)
						> dotsDistance(m.south, m.west))
					mainDot = m.north;
				else
					mainDot = m.west;
			} else {
				if (dotsDistance(m.north, m.east)
						> dotsDistance(m.south, m.west))
					mainDot = m.east;
				else
					mainDot = m.south;
			}
		} else {
			if (dotsDistance(m.north, m.east) < dotsDistance(m.south, m.west)) {
				if (dotsDistance(m.north, m.west)
						> dotsDistance(m.south, m.east))
					mainDot = m.north;
				else
					mainDot = m.east;
			} else {
				if (dotsDistance(m.north, m.west)
						> dotsDistance(m.south, m.east))
					mainDot = m.west;
				else
					mainDot = m.south;
			}
		}
		//		printf("byly 4, mainDot = %lf %lf\n", mainDot.x, mainDot.y);
	} else {
		if (!areDifferentDots(m.north, m.east)) {
			if (dotsDistance(m.west, m.north) < dotsDistance(m.south, m.north))
				mainDot = m.west;
			else
				mainDot = m.south;
		} else if (!areDifferentDots(m.east, m.south)) {
			if (dotsDistance(m.north, m.east) < dotsDistance(m.west, m.east))
				mainDot = m.north;
			else
				mainDot = m.west;
		} else if (!areDifferentDots(m.south, m.west)) {
			if (dotsDistance(m.north, m.south) < dotsDistance(m.east, m.south))
				mainDot = m.north;
			else
				mainDot = m.east;
		} else if (!areDifferentDots(m.west, m.north)) {
			if (dotsDistance(m.east, m.west) < dotsDistance(m.south, m.west))
				mainDot = m.east;
			else
				mainDot = m.south;
		}
		//		printf("byly 3 mainDot = %lf %lf\n", mainDot.x, mainDot.y);
	}

	return mainDot;
}

int calculateMarkerId(struct s_dot mainDot, int i) {
	struct s_marker m = marker[i];
	struct s_dot left, right;
	float xLeft, yLeft, xRight, yRight;
	int ret = 0;

	if (!areDifferentDots(mainDot, m.north)) {
		left = m.east;
		right = m.west;
	} else if (!areDifferentDots(mainDot, m.east)) {
		left = m.south;
		right = m.north;
	} else if (!areDifferentDots(mainDot, m.south)) {
		left = m.west;
		right = m.east;
	} else if (!areDifferentDots(mainDot, m.west)) {
		left = m.north;
		right = m.south;
	}

	marker[i].mainDot = mainDot;
	marker[i].left = left;
	marker[i].right = right;
	marker[i].center.x = (left.x + right.x) / 2;
	marker[i].center.y = (left.y + right.y) / 2;
	drawCircle(img, left.x, left.y, 1, 33, 2);
	drawCircle(img, right.x, right.y, 1, 100, 2);
	drawCircle(img, marker[i].center.x, marker[i].center.y, 1, 0, 2);
	drawCircle(img, mainDot.x, mainDot.y, 1, 67, 2);

	// wyznaczenie wektorow w lewo i prawo od mainDot
	xLeft = (left.x - mainDot.x) / 2;
	yLeft = (left.y - mainDot.y) / 2;
	xRight = (right.x - mainDot.x) / 2;
	yRight = (right.y - mainDot.y) / 2;

	//	printf("mainDot = %lf %lf, left = %lf %lf, right = %lf %lf\n",
	//			mainDot.x, mainDot.y, left.x, left.y, right.x, right.y);

	// promien poszukiwan srodka kropki po przesunieciu o wektor
	float tolerance = sqrt(xLeft * xLeft + yLeft * yLeft) / 5;

	if (dotExists(mainDot.x + xRight, mainDot.y + yRight, i, tolerance))
		ret += 64;
	if (dotExists(mainDot.x + xLeft, mainDot.y + yLeft, i, tolerance))
		ret += 2;
	if (dotExists(mainDot.x + xLeft + xRight, mainDot.y + yLeft + yRight, i,
			tolerance))
		ret += 32;
	if (dotExists(mainDot.x + xLeft + 2 * xRight,
			mainDot.y + yLeft + 2 * yRight, i, tolerance))
		ret += 512;
	if (dotExists(mainDot.x + 2 * xLeft + xRight,
			mainDot.y + 2 * yLeft + yRight, i, tolerance))
		ret += 16;

	return ret;
}

char oneEdgeDotIsTheSame(struct s_marker m) {
	if (areDifferentDots(m.north, m.south) && areDifferentDots(m.north, m.west)
			&& areDifferentDots(m.north, m.east)
			&& areDifferentDots(m.south, m.west)
			&& areDifferentDots(m.south, m.east)
			&& areDifferentDots(m.west, m.east))
		return 0;
	else
		return 1;
}

float max(float a, float b) {
	if (a > b)
		return a;
	else
		return b;
}

float min(float a, float b) {
	if (a < b)
		return a;
	else
		return b;
}

char areDifferentDots(struct s_dot a, struct s_dot b) {
	if (a.x != b.x || a.y != b.y)
		return 1;
	else
		return 0;
}

char dotExists(float x, float y, int markerNum, float tolerance) {
	for (int i = 0; i < marker[markerNum].dotsNum; i++) {
		float x1 = marker[markerNum].dot[i].x;
		float y1 = marker[markerNum].dot[i].y;
		if (pointsDistance(x, y, x1, y1) < tolerance) {
//			printf("w %lf %lf istnieje kropka\n", x, y);
			return 1;
		}
	}
//	printf("w %lf %lf NIE istnieje kropka\n", x, y);
	return 0;
}

int convertImageAndDisplay(char *fileName) {
	IplImage *rawImage;
	IplImage *bgrImage;

	if ((rawImage = loadRawImage(fileName)) == NULL)
		return -1;

	if ((bgrImage = convertRawToBGR(rawImage)) == NULL) {
		cvReleaseImage(&rawImage);
		return -1;
	}

	drawCircle(bgrImage, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 1, 0, 3);

	cvReleaseImage(&rawImage);

	cvNamedWindow("Image view", 1);
	cvShowImage("Image view", bgrImage);
	cvWaitKey(0);

	return 0;
}

int interactiveManipulation(char *fileName) {
	printf("Loading image and showing it\n");

	IplImage *rawImage;
	IplImage *bgrImage;

	if ((rawImage = loadRawImage(fileName)) == NULL)
		return -1;

	if ((bgrImage = convertRawToBGR(rawImage)) == NULL) {
		cvReleaseImage(&rawImage);
		return -1;
	}

	cvReleaseImage(&rawImage);

	cvNamedWindow("Image view", 1);
	cvShowImage("Image view", bgrImage);
	cvWaitKey(0);

	interactiveMainRoutine(bgrImage);

	return 0;
}

void drawCircle(IplImage* img, float x, float y, int r, int hue, int thickness) {
	cvCircle(img, cvPoint(cvRound(x), SCREEN_HEIGHT - cvRound(y)), r,
			CV_RGB(hue,hue,hue), thickness, CV_AA, 0);
}

void quickSortMarkers(struct s_marker* t, int l, int r) {
	struct s_marker pivot = t[(l + r) / 2];
	int i = l;
	int j = r;

	do {
		while (t[i].distanceFromScreenCenterInMeters
				< pivot.distanceFromScreenCenterInMeters)
			++i;
		while (t[j].distanceFromScreenCenterInMeters
				> pivot.distanceFromScreenCenterInMeters)
			--j;
		if (i <= j) {
			struct s_marker tmp = t[i];
			t[i] = t[j];
			t[j] = tmp;
			++i;
			--j;
		}
	} while (i < j);
	if (l < j)
		quickSortMarkers(t, l, j);
	if (r > i)
		quickSortMarkers(t, i, r);
}

void sortMarkersByDistanceFromScreenCenter() {
	quickSortMarkers(marker, 0, markersNum - 1);
	printf("posortowane markery:\n");
	for (int i = 0; i < markersNum; i++)
		printf("%d %f %f %f\n", marker[i].id,
				marker[i].center.x * METERS_PER_PIXEL,
				marker[i].center.y * METERS_PER_PIXEL, markerAngle(marker[i]));
}

// kat markera == kat wektora left
float markerAngle(struct s_marker m) {
	float shiftX = m.left.x - m.mainDot.x;
	float shiftY = m.left.y - m.mainDot.y;
//	printf("%lf %lf\n",shiftX,shiftY);
	if (shiftX > 0) {
//		printf("shiftX>0\n");
		return normalizeAngle(atan(shiftY / shiftX));
	} else if (shiftX == 0) {
//		printf("shiftX==0\n");
		if (shiftY > 0)
			return PI / 2;
		else
			return 3 * PI / 2;
	} else {
//		printf("shiftX<0\n");
		return PI - atan(shiftY / (-shiftX));
	}
}

inline float normalizeAngle(float angle) {
	return angle < 0 ? 2 * PI + angle : angle;
}

int threeMarkersMain(struct programOptions * options) {
	if ((rawImage = loadRawImage(options->fileName)) == NULL)
		return -1;

	if ((img = getYFromRaw(rawImage)) == NULL) {
		cvReleaseImage(&rawImage);
		return -1;
	}

	if (options->verbose) {
		cvNamedWindow("Image view", 1);
		cvShowImage("Image view", img);
		cvWaitKey(0);
	}

	IplImage *mask = cvCreateImage(cvGetSize(img), 8, 1);
	cvInRangeS(img, cvScalar(250, 0, 0, 0), cvScalar(256, 0, 0, 0), mask);
	if (options->verbose) {
		cvShowImage("Image view", mask);
		cvWaitKey(0);
	}

	CvMemStorage *storage = cvCreateMemStorage(0);
	CvSeq *contours = cvCreateSeq(0, sizeof(CvSeq), sizeof(CvPoint), storage);
	cvFindContours(mask, storage, &contours, sizeof(CvContour), CV_RETR_LIST,
			CV_CHAIN_APPROX_SIMPLE, cvPoint(0, 0));

	for (; contours != NULL; contours = contours->h_next) {
		CvPoint2D32f center;
		float radius;
		double area = cvContourArea(contours, CV_WHOLE_SEQ, 0);
		
		// was 100.0, updated for new markers
		if (area > 200.0)
			continue;

		cvMinEnclosingCircle(contours, &center, &radius);
		struct s_dot dot;
		dot.x = center.x;
		dot.y = SCREEN_HEIGHT - center.y;
		int tmp = assignDotToMarker(dot);
		if (options->verbose) {
//			cvDrawContours(img, contours, CV_RGB( 255, 0, 0 ),
//					CV_RGB(0,0,0), -1, CV_FILLED, 8, cvPoint(0, 0));
		}
	}
//	if (options->verbose)
//	{
//		cvShowImage("Image view", img);
//		cvWaitKey(0);
//	}

	removeBogusMarkers();
	for (int i = 0; i < markersNum; i++)
		for (int j = 0; j < marker[i].dotsNum; j++)
			drawCircle(img, marker[i].dot[j].x, marker[i].dot[j].y, 1, 200, 2);
	determineMyPositionUsingThreeMarkers();

	if (options->verbose) {
		cvShowImage("Image view", img);
		cvWaitKey(0);
	}

	return 0;
}

int oneMarkerMain(struct programOptions * options) {
	if ((rawImage = loadRawImage(options->fileName)) == NULL)
		return -1;

	if ((img = getYFromRaw(rawImage)) == NULL) {
		cvReleaseImage(&rawImage);
		return -1;
	}

	if (options->verbose) {
		cvNamedWindow("Image view", 1);
		cvShowImage("Image view", img);
		cvWaitKey(0);
	}

	IplImage *mask = cvCreateImage(cvGetSize(img), 8, 1);
	cvInRangeS(img, cvScalar(250, 0, 0, 0), cvScalar(256, 0, 0, 0), mask);
	if (options->verbose) {
		cvShowImage("Image view", mask);
		cvWaitKey(0);
	}

	CvMemStorage *storage = cvCreateMemStorage(0);
	CvSeq *contours = cvCreateSeq(0, sizeof(CvSeq), sizeof(CvPoint), storage);
	cvFindContours(mask, storage, &contours, sizeof(CvContour), CV_RETR_LIST,
			CV_CHAIN_APPROX_SIMPLE, cvPoint(0, 0));

	for (; contours != NULL; contours = contours->h_next) {
		CvPoint2D32f center;
		float radius;
		double area = cvContourArea(contours, CV_WHOLE_SEQ, 0);

		if (area > 100.0)
			continue;

		cvMinEnclosingCircle(contours, &center, &radius);
		struct s_dot dot;
		dot.x = center.x;
		dot.y = SCREEN_HEIGHT - center.y;
		int tmp = assignDotToMarker(dot);
		if (options->verbose) {
//			cvDrawContours(img, contours, CV_RGB( 255, 0, 0 ),
//					CV_RGB(0,0,0), -1, CV_FILLED, 8, cvPoint(0, 0));
		}
	}
//	if (options->verbose)
//	{
//		cvShowImage("Image view", img);
//		cvWaitKey(0);
//	}

	removeBogusMarkers();
	for (int i = 0; i < markersNum; i++)
		for (int j = 0; j < marker[i].dotsNum; j++)
			drawCircle(img, marker[i].dot[j].x, marker[i].dot[j].y, 1, 200, 2);
	determineMyPositionUsingOneMarker();

	if (options->verbose) {
		cvShowImage("Image view", img);
		cvWaitKey(0);
	}

//	if (options->verbose)
//	{
//		for(int i=0; i<markersNum; i++)
//		{
//			drawCircle(img, marker[i].center.x, marker[i].center.y, 50, 255, 2);
//			for(int j=0; j<marker[i].dotsNum; j++)
//			{
//				drawCircle(img, marker[i].dot[j].x, marker[i].dot[j].y, 1, 0, 3);
//			}
//		}
//		cvShowImage("Image view", img);
//		cvWaitKey(0);
//	}

	return 0;
}

int findCentreAndSaveToDisk(struct programOptions *options) {
	IplImage *rawImage;
	IplImage *bgrImage;
	char* fileName = options->fileName;

	if ((rawImage = loadRawImage(fileName)) == NULL)
		return -1;

	if ((bgrImage = convertRawToBGR(rawImage)) == NULL) {
		cvReleaseImage(&rawImage);
		return -1;
	}

	drawCircle(bgrImage, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 1, 0, 3);

	cvReleaseImage(&rawImage);

	int outputFileNameLength = strlen(fileName)+5;//fileName.jpg
	char*outputFileName = malloc(outputFileNameLength);

	snprintf(outputFileName,outputFileNameLength,"%s.jpg",fileName);

	cvSaveImage(outputFileName, bgrImage,0);

	return 0;
}

int selectMainFunction(int argc, char ** argv, struct programOptions *options) {
	int ret;
	switch (options->mainFunction) {
	case 1:
		ret = convertImageAndDisplay(options->fileName);
		break;
	case 2:
		ret = interactiveManipulation(options->fileName);
		break;
	case 3:
		ret = threeMarkersMain(options);
		break;
	case 4:
		ret = oneMarkerMain(options);
		break;
	case 5:
		ret = findCentreAndSaveToDisk(options);
		break;
	default:
		ret = -1;
		break;
	}
	return ret;
}
int main(int argc, char** argv) {
	struct programOptions options;

	options.fileName = NULL;
	options.mainFunction = -1;
	options.verbose = 0;

	if (parseCmdLine(argc, argv, &options) == -1)
		exit(-1);

	return selectMainFunction(argc, argv, &options);
}
