#include <stdio.h>
#include <math.h>
#include "opencv/cv.h"
#include "opencv/highgui.h"

#include"options.h"
#include"convert.h"
#include"interactive.h"

#define PI 3.14159

int tenisBallMain(int argc, char** argv)
{
    IplImage* img;
    if ((img = cvLoadImage("newPhotos/red-000010.ppm", 1)) == 0)
    {
        perror("cvLoadImage");
        return 1;
    }

    cvNamedWindow("Image view", 1);
    cvShowImage("Image view", img);
    cvWaitKey(0);

    CvSize size = cvGetSize(img);
    // 8-bitowy unsigned int na kanał, 3 kanały
//    IplImage *hsv = cvCreateImage(size, IPL_DEPTH_8U, 3);
//    cvCvtColor(img, hsv, CV_BGR2HSV);
//    cvShowImage("Image view", hsv);
//    cvWaitKey(0);

    // 8 bitów, unsigned, 1 kanał
    CvMat *mask = cvCreateMat(size.height, size.width, CV_8UC1);
    cvInRangeS(img, cvScalar(80, 140, 175, 0),
            cvScalar(140, 180, 235, 0), mask);
    cvShowImage("Image view", mask);
    cvWaitKey(0);

    IplConvKernel *se21 = cvCreateStructuringElementEx(21, 21, 10, 10, CV_SHAPE_RECT, NULL);
    IplConvKernel *se11 = cvCreateStructuringElementEx(11, 11, 5, 5, CV_SHAPE_RECT, NULL);
    cvClose(mask, mask, se21, 1);
    cvOpen(mask, mask, se11, 1);
    cvShowImage("Image view", mask);
    //cvWaitKey(0);

    /* Copy mask into a grayscale image */
    IplImage *hough_in = cvCreateImage(size, 8, 1);
    cvCopy(mask, hough_in, NULL);
    cvSmooth(hough_in, hough_in, CV_GAUSSIAN, 15, 15, 0, 0);
    cvShowImage("Image view", hough_in);
//    cvWaitKey(0);

    /* Run the Hough function */
    CvMemStorage *storage = cvCreateMemStorage(0);
    CvSeq *circles = cvHoughCircles(hough_in, storage, CV_HOUGH_GRADIENT, 4, size.height / 10, 100, 40, 0, 0);

    int i;
    printf("circles == %d\n", circles->total);
    for (i = 0; i < circles->total; i++)
    {
        float *p = (float*) cvGetSeqElem(circles, i);
        CvPoint center = cvPoint(cvRound(p[0]), cvRound(p[1]));
        CvScalar val = cvGet2D(mask, center.y, center.x);
        if (val.val[0] < 1)
            continue;
        printf("%d %d %d\n", cvRound(p[0]), cvRound(p[1]), cvRound(p[2]));
        cvCircle(img, center, 3, CV_RGB(0,255,0), 2, CV_AA, 0);
    }
    cvShowImage("Image view", img);
//    cvWaitKey(0);

    return 0;
}

int houghTransformMain(int argc, char** argv)
{
    IplImage* img;
    if ((img = cvLoadImage("photos/img-000012.ppm", 1)) == 0)
    {
        perror("cvLoadImage");
        return 1;
    }
    cvNamedWindow("Image view", 1);
    cvShowImage("Image view", img);
//    cvWaitKey(0);

    IplImage* gray = cvCreateImage(cvGetSize(img), 8, 1); // allocate a 1 channel byte image
    CvMemStorage* storage = cvCreateMemStorage(0);
    cvCvtColor(img, gray, CV_BGR2GRAY);
    cvShowImage("Image view", gray);
//    cvWaitKey(0);

    cvSmooth(gray, gray, CV_GAUSSIAN, 3, 3, 0, 0);
    cvShowImage("Image view", gray);
    cvWaitKey(0);

    CvSeq* circles = cvHoughCircles(gray, storage, CV_HOUGH_GRADIENT, 4, // inverse ratio of the accumulator resolution
            1, // minimum distance between circle centres
            100, // higher threshold value for Canny
            20, // accumulator threshold for the circle centers; smaller->more false circles
            1, // minimum radius
            10); // maximum radius

    printf("circles == %d\n", circles->total);
    int i;
    for (i = 0; i < circles->total; i++)
    {
        float *p = (float*) cvGetSeqElem(circles, i);
        CvPoint center = cvPoint(cvRound(p[0]), cvRound(p[1]));
        CvScalar val = cvGet2D(gray, center.y, center.x);
        if (val.val[0] < 1)
            continue;
        printf("%d %d %d\n", cvRound(p[0]), cvRound(p[1]), cvRound(p[2]));
        cvCircle(img, center, cvRound(p[2]), CV_RGB(0,255,0), 1, CV_AA, 0);
    }
    cvShowImage("Image view", img);
    cvWaitKey(0);

    return 0;
}

int adaptiveThresholdMain(struct programOptions * options)
{
    if ((rawImage = loadRawImage(options->fileName)) == NULL)
        return -1;

    if ((img = convertRawToRGB(rawImage)) == NULL)
    {
        cvReleaseImage(&rawImage);
        return -1;
    }

    if (options->verbose)
    {
        cvNamedWindow("Image view", 1);
        cvShowImage("Image view", img);
        cvWaitKey(0);
    }

    IplImage* gray = cvCreateImage(cvGetSize(img), 8, 1);
    IplImage* gray2 = cvCreateImage(cvGetSize(img), 8, 1);// allocate a 1 channel byte image
    cvCvtColor(img, gray2, CV_BGR2GRAY);
    cvInRangeS(gray2, cvScalar(0, 0, 0, 0),
            cvScalar(120, 0, 0  , 0), gray);
    if (options->verbose)
    {
        cvShowImage("Image view", gray);
        cvWaitKey(0);
    }
    //
    //    cvAdaptiveThreshold(gray, gray, 255, //  Non-zero value assigned to the pixels for which the condition is satisfied
    //            CV_ADAPTIVE_THRESH_MEAN_C, // adaptiveMethod
    //            CV_THRESH_BINARY_INV, // thresholdType
    //           11, // blockSize
    //            40); // Constant subtracted from the mean or weighted mean
    //    if (options->verbose)
    //  {
    //      cvShowImage("Image view", gray);
    //      cvWaitKey(0);
    //  }
    //  cvSmooth(gray, gray, CV_GAUSSIAN, 3, 3, 0, 0);
    //IplImage *canny_out = cvCreateImage(cvGetSize(gray), 8, 1);
    //cvCanny(gray, gray, 175, 255, 3);
    //if (options->verbose)
    //{
    //  cvShowImage("Image view", gray);
    //  cvWaitKey(0);
    //}

    CvMemStorage *storage = cvCreateMemStorage(0);
    CvSeq *contours = cvCreateSeq(0, sizeof(CvSeq), sizeof(CvPoint), storage);
    cvFindContours(gray, storage, &contours, sizeof(CvContour), CV_RETR_LIST,
            CV_CHAIN_APPROX_SIMPLE, cvPoint(0, 0));
    //    printf("contours->total = %d\n", contours->total);

    for (; contours != NULL; contours = contours->h_next)
    {
        CvPoint2D32f center;
        float radius;
        double area = cvContourArea(contours, CV_WHOLE_SEQ, 0);
        // zeby niepotrzebnie nie wywolywac cvMinEnclosingCircle to najmniejsze punkciki odrzucamy juz tutaj
        if (area < 10.0)
            continue;
        //        cvDrawContours(img, contours, CV_RGB( 255, 0, 0 ),
        //                          CV_RGB(0,0,0), -1, CV_FILLED, 8, cvPoint(0, 0));
        cvMinEnclosingCircle(contours, &center, &radius);
        double circleArea = radius * radius * PI;
        //        printf("x=%lf, y=%lf, radius=%lf, area=%lf, circleArea=%lf\n",
        //              center.x, center.y, area, radius, circleArea);
        if (/*1.0 < area && area < 50.0 &&*/circleArea < area * 2.0)
        {
            //            printf("x=%lf, y=%lf, radius=%lf, area=%lf, circleArea=%lf\n",
            //                  center.x, center.y, area, radius, circleArea);
            struct s_dot dot;
            dot.x = center.x;
            dot.y = center.y;
            int tmp = assignDotToMarker(dot);
            if (options->verbose)
            {
                cvDrawContours(img, contours, CV_RGB( 255, 0, 0 ),
                        CV_RGB(0,0,0), -1, CV_FILLED, 8, cvPoint(0, 0));
            }

        }
    }
    removeBogusMarkers();
    determineMyPosition();

    if (options->verbose)
    {
        cvShowImage("Image view", img);
        cvWaitKey(0);
    }

    return 0;
}
