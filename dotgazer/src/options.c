#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<strings.h>
#include"options.h"
#include<getopt.h>

#include"errhandling.h"
#include"options.h"

void printUsage()
{
    printf("-h print help\n");
    printf("-f image file\n");
    printf("-v verbose\n");
    printf("-m main function:\n");
    printf("\t1 load raw image and display\n");
    printf("\t2 interactive manipulation\n");
    printf("\t3 determine position using 3 markers\n");
    printf("\t4 determine position using 1 marker\n");
    printf("\t5 draw circle at the center of image and save to disk\n");
    return;
}

int parseCmdLine(int argc, char** argv, struct programOptions * options)
{
    int opt;

    if (argc<2)
    {
        printUsage();
        return 0;
    }

    while ((opt = getopt(argc, argv, "f:m:hv")) != -1)
    {
        switch (opt)
        {
            case 'f':
                options->fileName = optarg;
                break;
            case 'm':
                options->mainFunction = strtol(optarg, NULL, 10);
                break;
            case 'h':
                printUsage();
                break;
            case 'v':
            	options->verbose=1;
            	break;
            default:
                printf("Unsupported option - %c\n", opt);
                return -1;
        }
    }
    return 0;
}
