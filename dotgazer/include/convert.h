#ifndef CONVERT_H_
#define CONVERT_H_

#include "opencv/cv.h"

#define IMAGE_WIDTH 752
#define IMAGE_HEIGHT 480

char* loadRawData(char *path,int size);
IplImage* loadRawImage(char* path);
IplImage* convertRawToBGR(IplImage *rawImage);
IplImage * getYFromRaw(IplImage *rawImage);
#endif /* CONVERT_H_ */
