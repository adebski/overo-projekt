#ifndef TEST_H_
#define TEST_H_

struct s_dot
{
    float x;
    float y;
} s_dot;

struct s_marker
{
    float distanceFromScreenCenterInMeters;
    float distanceFromScreenCenterInPixels;
    float absoluteX;
    float absoluteY;
    float calibratedAngle;
    int id;
    int dotsNum;
    struct s_dot dot[9];
    struct s_dot north;
    struct s_dot south;
    struct s_dot east;
    struct s_dot west;
    struct s_dot center;
    struct s_dot mainDot;
    struct s_dot left;
    struct s_dot right;
};


int assignDotToMarker(struct s_dot);
float dotsDistance(struct s_dot, struct s_dot);
float pointsDistance(float, float, float, float);
struct s_dot removeBogusDot(struct s_marker);
float max(float, float);
float min(float, float);
char areDifferentDots(struct s_dot, struct s_dot);
char oneEdgeDotIsTheSame(struct s_marker);
int calculateMarkerId(struct s_dot, int);
char dotExists(float, float, int, float);
void removeBogusMarkers(void);
void determineMyPositionUsingThreeMarkers(void);
void calculateCrossingPoints(float*, float*, int, int, int);
void determineMyPositionUsingOneMarker(void);
void calculateRobotPosition(float*, float*, int);
void drawCircle(IplImage*, float, float, int, int, int);
void sortMarkersByDistanceFromScreenCenter();
float markerAngle(struct s_marker);
float normalizeAngle(float);

#endif /* TEST_H_ */
