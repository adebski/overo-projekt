#ifndef OPTIONS_H
#define OPTIONS_H

struct programOptions
{
    long mainFunction;
    char * fileName;
    int verbose;
};

int parseCmdLine(int argc, char** argv, struct programOptions * options);
#endif

