# README #

This repository contains the source code of the Open-source Localization Device for Indoor Mobile Robots developed at AGH-UST. This readme is complementary with the article published by the authors (link to the article will be provided here) which is referenced here as *the article*.

## Deployment ##

### Setting up the platform ###

During our tests we were using custom built [Angstrom](http://www.angstrom-distribution.org/) distribution with 3.2. kernel. As far as we know it is possible now to use newer kernels on Overo™ boards that support MT9V032 sensor. 

For up-to-date information about Gumstix™ hardware and compatible software check:

* [Gumstix wiki](http://wiki.gumstix.org/index.php?title=Main_Page)
* [Gumstix Developer Center](http://www.gumstix.org/)
* [Gumstix Mailing List](http://gumstix.8.x6.nabble.com)  

There should be a lot information how to correctly set up Gumstix with Caspa™ cameras.

Additionaly one should install [omap3-pwm-driver](https://github.com/scottellis/omap3-pwm) by Scott Ellis to be able to control PWM lines from userspace.  

### Starting the localization program ###

The code of the localization algorithm and the necessary utility programs is contained within `dotgazer2` folder. Running `make` in this directory compiles the main program. Running the generated executable `dotgazer.out` prints to stdout the list of available options. 

The first step is to create the translation matrices as described in the article in section 4.1. A series of pictures at different device orientations should be taken beforehand where the camera is right beneath the first dot and the other dots are spaced by 8cm in the outward direction. The 8cm gap is not configurable in this version and has to be adhered to. To take the series of pictures you can use [yavta](http://git.ideasonboard.org/yavta.git) tool

Having the pictures saved in some folder `/path/to/pics/` the initial elements of the translation matrices can be filled by running:

    ./dotgazer.out -m 3 -f /path/to/pics/

The missing elements of the matrices can then be filled by running (GNU Octave has to be installed):

    cd octave
    octave script.m
    cd ..

After this step the matrices are set up. Next, markers configuration should be defined in an arbitrary text file. Each row of the file should be in the form of:

    marker_id x_position y_position orientation

All entries in the file are expressed in Absolute Coordinates System as defined in section 4 of the article. This file is later passed to the algorithm using `-n` option.

Another (optional) configuration file exists - algorithm configuration file. An example of it may be:

    undistort=true
    minDotArea=15
    maxDotArea=250
    dotBrightnessThreshold=32
    ceilingHeightMeters=2.5
    brightness=10

The initial algorithm settings can later be fine-tuned at runtime using the provided webapp.

To simplify things we omit a detailed description of all the program options. Their meaning should be clear after reading their short descriptions and consulting the article. To start the localization algorithm (`-r 0` disables the accelerometer-based image correction - we use the translation matrices) you should run:

    ./dotgazer.out -m 1 -r 0 -n /path/to/markers_config.txt -p /path/to/algorithm_settings.txt

### Setting up the webapp ###

In addition to C++ application there exists web application that can be used to control some of the behaviours of the localization program. Also it contains an option to stream images from the camera in the browser which can be useful during debuging/testing.

The application is written in pure JavaScript and uses Websockets technology to connect to the localization program. 

To start the application firstly install [npm](https://www.npmjs.com/) and then execute in `overo-webapp` directory

```
npm install
bower install
grunt serve
```

Application will be available under `localhost:9000`

## Libraries ##

The C++ code depends on the following libraries:

* [OpenCV](http://opencv.org/) - [BSD License](http://en.wikipedia.org/wiki/BSD_licenses#4-clause_license_.28original_.22BSD_License.22.29)
* [googlemock](https://code.google.com/p/googlemock/) - [New BSD License](http://en.wikipedia.org/wiki/BSD_licenses#3-clause_license_.28.22Revised_BSD_License.22.2C_.22New_BSD_License.22.2C_or_.22Modified_BSD_License.22.29)
* [libwebsockets](https://libwebsockets.org/) - [Modified LGPL 2.1 License](http://git.libwebsockets.org/cgi-bin/cgit/libwebsockets/tree/LICENSE)

Web application depends on:

* [AngularJS](https://angularjs.org/) - [MIT License](https://github.com/angular/angular.js/blob/master/LICENSE)
* [Twitter Bootstrap 2.3.2](http://getbootstrap.com/2.3.2/) - [Apache License v.2.0](http://getbootstrap.com/2.3.2/)

## Authors ##

Development: Andrzej Dębski, Wojciech Grajewski  
Under the supervision of: Wojciech Zaborowski, Wojciech Turek

Department of Computer Science  
AGH University of Science and Technology, Krakow, Poland

## License ##

The MIT License (MIT)

Copyright (c) 2014 Andrzej Dębski, Wojciech Grajewski, Wojciech Turek, Wojciech Zaborowski

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.