\chapter{MappingStrategy - Uzupełnienie danych markerów o dane zapisane w mapie}

\begin{lstlisting}
class MappingStrategy {
public:
    virtual int loadMapFromFile(const char* filename) = 0;
    virtual void updateMap(std::vector<Marker> markers) = 0;
    virtual std::vector<Marker> updateMarkersUsingMap(std::vector<Marker> markers) = 0;
    virtual std::map<int, Coordinates> getMap() = 0;
    virtual std::map<int, Coordinates>* getPointerToMap() = 0;
    virtual void clearMap() = 0;
\end{lstlisting}

Interfejs ten dostarcza metod umożliwiających operacje na mapie markerów. Chodzi tu zarówno o stworzenie mapy podczas procesu mapowania, jak również o uzupełnienie pól markerów o dane z mapy w procesie pozycjonowania. 

Jako, że powstała tylko jedna implementacja tego interfejsu, znaczenie metod zostało wyjaśnione podczas opisywania ich implementacji w następnym podrozdziale.

\section{OneMarkerMappingStrategy}

Klasa ta implementuje powyższy interfejs, dlatego nie została tutaj powtórzona deklaracja jej metod. Nie dodaje on żadnych metod publicznych.

Również pola klasy są standardowe, podobne jak w innych strategiach. Nowym polem jest 
\begin{lstlisting}
std::map<int, Coordinates> coords;
\end{lstlisting}
Jest to mapa, w której kluczami są numery markerów, a wartościami (wcześniej zmapowane) współrzędne tych markerów w układzie współrzędnych absolutnych.

\subsection{Metoda loadMapFromFile}

Metoda ta otwiera wskazany w argumencie plik, który następnie jest czytany linia po linii. Plik zawiera mapowania markerów w formacie

\begin{center}
\texttt{nr\_markera współrzędna\_x współrzędna\_y kąt\_nachylenia}
\end{center}

Wczytane z pliku dane następnie są zapisywane w mapie, a plik jest zamykany.

\subsection{Metoda updateMap}

Metoda ta dodaje nowe markery do mapy. Szczególnym przypadkiem jest sytuacja, gdy mapa nie zawiera żadnych markerów:
\begin{lstlisting}
if(coords.size() == 0) {
    createNewMap(markers);
    return;
}
\end{lstlisting}

\hspace{\textwidth}

W takim wypadku metoda \texttt{createNewMap} przyjmuje wektor aktualnie widocznych markerów i inicjalizuje mapę tymi właśnie markerami. W procesie tym jako środek układu współrzędnych absolutnych brany jest środek układu współrzędnych obrazu. Kamera znajduje się więc w punkcie $(0, 0)$ i~ma zerowy kąt obrotu, a współrzędne absolutne markerów są obliczane w~odniesieniu do niej.

Sprowadza się to do relatywnie prostego wzoru
\begin{lstlisting}
coords[m.getId()] = Coordinates(
    (m.getCenter().getX() - Const::IMAGE_WIDTH/2) * 
        algorithmSettings->getMetersPerPixel(), 
    (m.getCenter().getY() - Const::IMAGE_HEIGHT/2) * 
        algorithmSettings->getMetersPerPixel(), 
    GeometryUtils::markerAngle(m));
\end{lstlisting}

Współrzędna X markera jest wyliczana jako przesunięcie w pikselach od środka obrazu do środka tego markera pomnożone przez współczynnik \texttt{metersPerPixel} w celu konwersji na metry. Analogicznie współrzędna Y. Absolutny kąt obrotu markera to po prostu aktualny kąt obrotu markera, gdyż z założenia obserwowany układ ma zerowy kąt obrotu.\\

Jeżeli podany wyżej warunek nie był jednak spełniony, to mapa uzupełniana jest przez nowe markery, jeżeli takowe znajdują się na obrazie.

Na początku poszukiwany jest \texttt{referenceMarker}, który znajduje się najbliżej środka obrazu i jest już zmapowany. Poszukiwanie to odbywa się standardowym sposobem poprzez iterację po wszystkich markerach i porównywanie odległości od środka obrazu.

Jeżeli \texttt{referenceMarker} nie zostanie odnaleziony (dzieje się tak tylko, gdy żaden z widocznych markerów nie był zmapowany), to następuje powrót z metody i nic nie zostaje zmapowane.

Jeżeli znaleźliśmy \texttt{referenceMarker} to przechodzimy do właściwego mapowania nowych markerów:

\begin{lstlisting}
for (unsigned int i = 0; i < markers.size(); i++) {
    Marker m = markers[i];
    if (!contains(m.getId())) {
        float absCameraAngle = GeometryUtils::markerAngle(referenceMarker) - referenceMarker.getAbsoluteCoords().getAngle();
        float xShift = (m.getCenter().getX() - referenceMarker.getCenter().getX()) * algorithmSettings->getMetersPerPixel();
        float yShift = (m.getCenter().getY() - referenceMarker.getCenter().getY()) * algorithmSettings->getMetersPerPixel();
        float mAbsX = referenceMarker.getAbsoluteCoords().getX() + sin(absCameraAngle) * yShift + cos(absCameraAngle) * xShift;
        float mAbsY = referenceMarker.getAbsoluteCoords().getY() + cos(absCameraAngle) * yShift - sin(absCameraAngle) * xShift;
        float mAbsAngle = absCameraAngle + GeometryUtils::markerAngle(m);
        coords[m.getId()] = Coordinates(mAbsX, -mAbsY, mAbsAngle);
    }
}
\end{lstlisting}

Same wzory użyte tutaj dużo wygodniej czyta się w kodzie źródłowym, gdyż ograniczone miejsce na kartce uniemożliwia przedstawienie ich w czytelnej formie. Kod źródłowy został jednak zamieszczony w celu odwoływania się do niego w opisie słownym.

Iterujemy po wszystkich markerach, za każdym razem przypisując aktualny marker do zmiennej \texttt{m} (2) w celu skrócenia zapisu i tak skomplikowanych wzorów.

Jeżeli nie posiadamy w mapie danego markera (3), postępujemy według rozumowania przedstawionego w rozdziale \ref{sec:opis_algorytmu_wyznaczania_pozycji}:

\texttt{absCameraAngle} to kąt obrotu kamery w układzie współrzędnych absolutnych. Możemy go wyliczyć jako różnicę między aktualnie obserwowanym kątem \texttt{referenceMarker}a, a zapisanym w mapie kątem tegoż markera w~układzie współrzędnych absolutnych.

\texttt{xShift} i \texttt{yShift}, inaczej niż w rozdziale \ref{sec:opis_algorytmu_wyznaczania_pozycji}, oznaczają przesunięcie od \texttt{referenceMarker}a do aktualnie przetwarzanego markera \texttt{m}. Różnica ta wynika z faktu, że w rozdziale \ref{sec:opis_algorytmu_wyznaczania_pozycji} wytłumaczone zostało wyliczanie pozycji kamery, a nie wybranego markera na obrazie, jednak zasada działania i podstawy matematyczne pozostają niezmienione.

\texttt{mAbsX} i \texttt{mAbsY} to współrzędne markera \texttt{m} w układzie współrzędnych absolutnych. Wzory na te współrzędne wynikają bezpośrednio z rozumowania przedstawionego w rozdziale \ref{sec:opis_algorytmu_wyznaczania_pozycji}.

\texttt{mAbsAngle} to kąt obrotu markera \texttt{m} w układzie współrzędnych absolutnych. Jest to suma kąta obrotu kamery w układzie współrzędnych absolutnych i kąta obrotu markera \texttt{m} w układzie współrzędnych kamery.

Na koniec (10) marker \texttt{m} dodawany jest do mapy. Współrzędna Y jest negowana ze względu na fakt, że kamera obserwuje sufit z podłogi, a my rozważamy markery tak, jakbyśmy patrzyli na sufit z góry.

\subsection{Metoda updateMarkersUsingMap}

Metoda ta w markerach przekazanych w argumencie wywołania ustawia pole \texttt{absoluteCoords} na dane zapisane w mapie.

\subsection{Metoda getMap}

Metoda ta zwraca aktualną mapę, czyli wartość pola \texttt{coords}.

\subsection{Metoda getPointerToMap}

Metoda ta zwraca wskaźnik do aktualnej mapy, czyli adres pola \texttt{coords}.

\subsection{Metoda clearMap}

Metoda ta czyści dane zapisane w mapie.





















