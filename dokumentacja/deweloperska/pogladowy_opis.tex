\chapter{Teoretyczne podstawy działania algorytmu pozycjonowania}

W poniższym rozdziale został przedstawiony całościowo algorytm, który jest wykorzystywany do obliczania pozycji robota. Ze względu na duży stopień skomplikowania algorytmu, przedstawiono go tutaj na wysokim poziomie abstrakcji, a w następnych rozdziałach zostaną dokładniej omówione poszczególne etapy.

Pomijając szczegóły implementacyjne, takie jak np. konwersja obrazu z~kamery na format umożliwiający jego łatwą analizę, bądź komunikacja z~urządzeniami zewnętrznymi, sam algorytm opiera się na podstawowych zadach geometrii analitycznej. W takiej też formie, przekształceń i zależności geometrycznych, przedstawiono go w tym rozdziale.

\section{Sztuczne znaczniki}
\label{sec:sztuczne_znaczniki}

Zintegrowana z modułem kamera skierowana jest pionowo w stronę sufitu w pomieszczeniu. Na suficie znajdują się specjalne znaczniki (markery) składające się z odpowiednio rozmieszczonych na nich kropek, tak jak na rysunku~\ref{fig:marker}. Układ kropek na markerze pozwala zidentyfikować zarówno sam marker, jak i jego orientację (kąt obrotu). 

Ogólny schemat markera wygląda następująco:

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.4]{img/marker.png}
  \caption{Schemat przykładowego markera}
  \label{fig:marker}
\end{figure}

Wartości podane w rzędach i kolumnach to liczby w zapisie heksadecymalnym. Wymnożenie numeru rzędu i kolumny dla danej kropki daje w~wyniku jej numer. Zsumowane numery kropek na markerze dają numer markera. Na markerze zawsze znajdują się trzy kropki narysowane na obrazku białym kolorem, pozwalają one na ustalenie orientacji markera (na markerze nigdy nie ma czwartej narożnej kropki). Numery trzech narożnych kropek nie wliczają się do numeru markera. Z pozostałych pięciu kropek narysowanych na czerwono każda może się znaleźć na markerze lub nie, co daje 32 możliwe kombinacje i w rezultacie możliwość użycia 32 różnych markerów.

Moduł wyszukuje markery na obrazie z kamery, oblicza ich numery (dzięki temu może pobrać z pliku konfiguracyjnego współrzędne ich położenia) i na podstawie analizy obrazu wyznacza odległość urządzenia, na którym jest zamontowany, od markerów. Dzięki temu potrafi ustalić pozycję samego urządzenia w pomieszczeniu.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.5]{img/raw_ceiling.png}
  \caption{Przykładowa klatka obrazu zarejestrowanego przez kamerę.}
  \label{fig:raw_ceiling}
\end{figure}

Na rysunku \ref{fig:raw_ceiling} wyraźnie widoczne są różne rodzaje markerów. Warto zauważyć, że wszystkie, tak jak zostało wcześniej wspomniane, posiadają zawsze dokładnie trzy skrajne kropki. Obraz taki zostaje następnie przetworzony przez algorytm i na jego podstawie obliczana jest pozycja.

\section{Opis algorytmu wyznaczania pozycji}
\label{sec:opis_algorytmu_wyznaczania_pozycji}

W algorytmie wyznaczania położenia wykorzystywana jest informacja o~położeniu i orientacji markera. Znajomość rzeczywistych wymiarów markera służy również do obliczenia współczynnika, który używany jest do przekonwertowania otrzymanych wyników z jednostek używanych na obrazie (pikseli) do jednostek rzeczywistych (metry). 

Sam sposób obliczania wynikowej pozycji robota oraz jego orientacji dosyć trudno wytłumaczyć na samych zmiennych matematycznych, ponieważ pojawia się ich relatywnie wiele. Operacje odbywają się głównie na wektorach, dlatego ważne są też ich zwroty (czy też znaki przy wartościach), co dodatkowo utrudnia zrozumienie. Co więcej, niezbędne jest wprowadzenie dwóch układów współrzędnych. 

Z tego powodu postanowiono przedstawić zasadę działania algorytmu na przykładzie. Starano się przedstawić jak najbardziej złożoną sytuację, żeby móc uwzględnić i pokazać wszystkie aspekty działania algorytmu wyznaczania pozycji. Przedstawioną poniżej symulację działania można łatwo uogólnić na inne położenia, w których może znaleźć się robot.

\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{inkscape/robot_marker_diagram_legend/robot_marker_diagram_legend.png}
  \caption{Schemat przedstawiający obliczanie pozycji robota na podstawie obrazu zarejestrowanego przez kamerę. UWA to układ współrzędnych absolutnych, UWK - układ współrzędnych kamery.}
  \label{fig:robot_marker_diagram}
\end{figure}

Rysunek \ref{fig:robot_marker_diagram} przedstawia widok z góry na pomieszczenie (prostokąt narysowany linią ciągłą). W centrum pomieszczenia widoczny jest marker, według którego zostanie wyznaczona pozycja robota.

Przed wyjaśnieniem dalszych etapów algorytmu niezbędne jest wprowadzenie dwóch pojęć. Pierwszym z nich jest \textit{mapowanie}. Jest to proces, który wykonywany jest na początku użytkowania systemu wyznaczania pozycji. Po powieszeniu markerów w pomieszczeniu trzeba przypisać im współrzędne w~tym pomieszczeniu. Odbywa się to automatycznie poprzez ustawienie robota w pozycji, którą użytkownik chce uznać za $(0,0)$, a następnie wykrycie na obrazie istniejących markerów i przypisanie im pozycji obliczonej względem wybranego środka układu.

W tym momencie można wprowadzić drugie pojęcie - \textit{układu współrzędnych absolutnych}. Współrzędne w tym układzie są ściśle powiązane z danym pomieszczeniem. Układ ten definiowany jest podczas rozpoczęcia procesu mapowania w pomieszczeniu. Położenie i orientacja robota na początku procesu mapowania zostają przyjęte jako początek układu współrzędnych absolutnych. Tak długo, jak używana jest dana mapa markerów i pozostają one fizycznie w tych samych miejscach na suficie, istnieje ciągle ten sam układ współrzędnych absolutnych. Wyniki zwracane przez system zwracane są właśnie w układzie współrzędnych absolutnych.

Na rysunku \ref{fig:robot_marker_diagram} układ współrzędnych absolutnych został zaznaczony kolorem zielonym. W laboratorium znajduje się też robot, którego pozycja i~orientacja zostały przedstawione przy pomocy dużego, zielonego grotu strzałki. Naszym celem jest wyznaczenie pozycji robota w układzie współrzędnych absolutnych.

Na robocie zamontowana jest kamera, która rejestruje obraz sufitu. Jej zasięg został przedstawiony symbolicznie jako prostokąt o bokach rysowanych przerywanymi liniami. Obraz, na którego podstawie algorytm ma wyznaczyć pozycję robota to właśnie ten zaznaczony wycinek sufitu.

Drugim układem współrzędnych, jaki został wprowadzony na potrzeby algorytmu jest \textit{układ współrzędnych kamery}. W tym układzie operuje algorytm i wykonywane są obliczenia. Jego środkiem jest środek obrazu; położenie wszystkich elementów na obrazie jest wyrażane właśnie w odniesieniu do tego układu. Układ współrzędnych kamery i układ współrzędnych absolutnych zazwyczaj nie są zorientowane w tym samym kierunku. Tak jest i~w~przedstawionym przykładzie, gdzie są one nachylone względem siebie pod \linebreak kątem $\alpha$.

Narysowane fioletowym kolorem wektory pokazują, jaka jest relatywna pozycja robota względem markera w układzie współrzędnych kamery. Innymi słowy, o jakie wartości współrzędnych $x$ i $y$ należy się przesunąć od markera, żeby dojść do robota. Wartości te oznaczałyby pozycję robota, jeżeli szukalibyśmy jej w układzie współrzędnych kamery. Na rysunku \ref{fig:robot_marker_diagram} należy przesunąć się o $a$ zgodnie z osią $X$ oraz $b$ przeciwnie do osi $Y$. Zwroty wektorów znajdą odwzorowanie w dalszej części przykładu.

Oznaczając środek markera w układzie współrzędnych absolutnych przez $M=(x_m,y_m)$, wektory od markera do robota w układzie współrzędnych kamery przez $a$ i $b$, kąt nachylenia układu współrzędnych absolutnych do układu współrzędnych kamery przez $\alpha$ i poszukiwaną pozycję robota w układzie współrzędnych absolutnych przez $R=(x_r,y_r)$ można wyprowadzić następujące wzory:

$$x_r = x_m + b \sin \alpha + a \cos \alpha$$
$$y_r = y_m + b \cos \alpha - a \sin \alpha$$

Na potrzeby przykładu możemy przyjąć następujące wartości: 

$$M=(-40,-30)$$
$$a=40$$
$$b=-50$$
$$\alpha=120^{\circ}$$

Warte uwagi są w szczególności wartości $a$ i $b$. Jeżeli dany wektor ma zwrot przeciwny do odpowiadającej mu osi układu współrzędnych, to jego wartość brana jest z minusem, jak w przypadku $b$. Stosując podane wcześniej wzory można obliczyć:

$$x_r = -40 -50 \sin 120^{\circ} + 40 \cos 120^{\circ} \approx -103$$
$$y_r = -30 -50 \cos 120^{\circ} - 40 \sin 120^{\circ} \approx -50$$

Porównując powyższe wyniki z rysunkiem można wnioskować, że użyte wzory są poprawne. Są również wystarczająco ogólne, by móc działać w dowolnym położeniu robota oraz nachyleniu układów względem siebie.

\subsection{Orientacja układu współrzędnych absolutnych względem markerów}

Na przedstawionym wcześniej przykładzie założono, że układ współrzędnych absolutnych jest zorientowany tak samo, jak marker. Istnieje jednak możliwość ustalenia orientacji układu niezgodnie ze znacznikami. Co więcej, znaczniki nie muszą być parami zorientowane w taki sam sposób. Z tego wynika, że nie ma żadnych ograniczeń co do sposobu umieszczania markerów na suficie.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{inkscape/robot_marker_diagram_complex_angle_legend/robot_marker_diagram_complex_angle_legend.png}
  \caption{Obliczanie kąta w przypadku niezgodnej orientacji układu współrzędnych absolutnych i markera. UWA to układ współrzędnych absolutnych, UWK - układ współrzędnych kamery.}
  \label{fig:robot_marker_diagram_complex}
\end{figure}

Przypadek, w którym algorytm oblicza pozycję robota na podstawie markera niezgodnego z układem współrzędnych absolutnych, wymaga dodatkowego wyjaśnienia. Nie różni się on jednak bardzo od wcześniej przedstawionego sposobu wyznaczania pozycji. Dostosowania wymaga tylko etap obliczania kąta między układem współrzędnych kamery a układem współrzędnych absolutnych.

W sytuacji przedstawionej na rysunku \ref{fig:robot_marker_diagram_complex}, podobnie jak poprzednio, kąt nachylenia układu współrzędnych absolutnych względem układu współrzędnych kamery to $\alpha$, który trzeba będzie wyliczyć. Kąt $\beta$ to kąt nachylenia markera w układzie kamery, można go łatwo obliczyć. Kąt $\gamma$ wyznaczany jest, wraz z koordynatami, podczas mapowania danego markera. Jak łatwo policzyć zachodzi zależność

$$\alpha = \beta - \gamma$$

\section{Opis procesu mapowania}

Proces mapowania został już krótko przedstawiony w części dotyczącej opisu algorytmu znajdowania pozycji. Teraz zostanie on opisany nieco szerzej, jednak bez szczegółów implementacyjnych, które można znaleźć w dalszej części tej dokumentacji.

Proces mapowania zaczyna się od umieszczenia robota w pozycji, którą użytkownik chce zdefiniować jako początek układu współrzędnych absolutnych. Jak zostało wcześniej wspomniane, układy mogą być też nachylone względem siebie. Na początku mapowania układ współrzędnych kamery jest zorientowany tak samo jak układ współrzędnych absolutnych.

Aby zmapować marker, warunkiem wstępnym jest znajomość aktualnej pozycji robota w układzie współrzędnych absolutnych. Mając tę wiedzę, system potrafi obliczyć względne położenie jeszcze niezmapowanych markerów i~przypisać im koordynaty oraz orientację w układzie współrzędnych absolutnych.

Markery widoczne z pozycji $(0,0)$ zostają natychmiastowo zmapowane. Dzięki temu wstępnemu krokowi można przemieścić robota, jednocześnie cały czas posiadając informację o jego położeniu, dopóki znajduje się on w zasięgu co najmniej jednego ze zmapowanych markerów. Przemieszczając robota w~obszary, w których znajdują się nowe markery, można je (te nowe markery) zmapować. Czynność tę powtarza się, aż zmapowane zostaną wszystkie znaczniki.