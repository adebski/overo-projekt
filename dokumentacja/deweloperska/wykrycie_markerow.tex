\chapter{DetectionStrategy - Wykrycie markerów na obrazie}

Po binaryzacji obrazu następuje etap wykrycia markerów widocznych na obrazie. Funkcjonalność tę zapewniają klasy implementujące interfejs \texttt{Detection\-Strategy}:

\begin{lstlisting}
class DetectionStrategy {
public:
    virtual std::vector<Marker> detectMarkers(IplImage* mask, Tilt tilt) = 0;
\end{lstlisting}

Interfejs ten jest bardzo prosty. Jego jedyna metoda przyjmuje zbinaryzowany obraz oraz pomiary wychylenia układu i zwraca \texttt{vector} obiektów \texttt{Marker} wypełnionych podstawowymi danymi (dokładniej - takimi, które można odczytać bezpośrednio z obrazu).

Mimo bardzo prostej deklaracji interfejsu, implementacja jego funkcjonalności jest relatywnie skomplikowana. Proponowana przez autorów implementacja to \texttt{SimpleDetectionStrategy}. Korzysta ona w dużej części z biblioteki OpenCV do analizy zbinaryzowanego obrazu. 

Próbowano również zaimplementować alternatywną strategię - \texttt{Cvblob\-Detection\-Strategy}, która korzystałaby z biblioteki cvBlob. Jednak już w~pierwszych testach wydajnościowych okazało się, że słowa autora biblioteki o jej przewadze w szybkości działania nad OpenCV nie sprawdziły się. Implementację tej strategii zakończono więc na bardzo wczesnym etapie, zostawiono ją jednak w celach poglądowych i do testów wydajnościowych. Być może w~przyszłości implementacja tej biblioteki okaże się szybsza.

\section{SimpleDetectionStrategy}

\begin{lstlisting}
class SimpleDetectionStrategy : public DetectionStrategy {
private:
    std::vector<Marker> markers;
    bool verboseText, verboseImage;
    CvMemStorage *storage;
    CvSeq *contours;
    AlgorithmSettings* algorithmSettings;
public:
    static SimpleDetectionStrategy* getInstance(bool verboseText, bool verboseImage, AlgorithmSettings* algorithmSettings);
    std::vector<Marker> detectMarkers(IplImage* mask, Tilt tilt);
\end{lstlisting}

Klasa ta, podobnie jak implementacje pozostałych strategii, pozwala na instancjonowanie przez metodę \texttt{getInstance}. Oprócz standardowych pól, opisanych już w poprzednim rozdziale, pojawiają się pola, w których metody OpenCV będą zapisywać wyniki wykonania - \texttt{storage} i \texttt{contours}. Pamięć dla nich jest alokowana w \texttt{getInstance}. W polu \texttt{markers} będą przechowywane wykryte markery.

\subsection{Metoda detectMarkers}

Skrócony kod implementacji tej metody (pominięte zostały komunikaty do debuggingu, komentarze, itp.) wygląda następująco:

\begin{lstlisting}
markers.clear();
cvFindContours(mask, storage, &this->contours, sizeof (CvContour), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, cvPoint(0, 0));
CvSeq *contours = this->contours;
for (;contours!=NULL; contours = contours->h_next) {
    CvPoint2D32f center;
    float radius;
    double area = cvContourArea(contours, CV_WHOLE_SEQ, 0);
    if (area < algorithmSettings->getMinDotArea() || area > algorithmSettings->getMaxDotArea()) {
        continue;
    }
    cvMinEnclosingCircle(contours, &center, &radius);
    Dot dot = Dot(center.x, Const::IMAGE_HEIGHT - center.y);
    assignDotToMarker(dot);
}
cvClearMemStorage(storage);
removeBogusMarkers();
if (algorithmSettings->getUndistort()) {
    markers = undistort(markers);
}
markers = correctTilt(markers, tilt);
correctMarkersParallelToEdges();
Dot screenCenter(Const::IMAGE_WIDTH / 2, Const::IMAGE_HEIGHT / 2);
for (unsigned int i = 0; i < markers.size(); i++) {
    Dot mainDot = GeometryUtils::removeBogusDot(markers[i]);
    markers[i].setId(GeometryUtils::calculateMarkerId(mainDot, &markers[i]));
    markers[i].setDistanceFromScreenCenter(GeometryUtils::dotsDistance(screenCenter, markers[i].getCenter()));
}
return markers;
\end{lstlisting}

Z racji tego, że ta sama instancja strategii jest używana do przetworzenia wielu klatek, na początku należy wyczyścić wektor markerów z poprzedniej iteracji (1).

Następnie (2) wyszukiwane są kontury obiektów na obrazie wejściowym \texttt{mask} przy użyciu funkcji \texttt{cvFindContours}. \texttt{storage} to tymczasowa pamięć używana przez OpenCV. Do zmiennej \texttt{contours} zostanie zapisany wynik wyszukiwania. Następnie przekazywany jest rozmiar pojedyńczego konturu - \texttt{sizeof(CvContour)}. \texttt{CV\_RETR\_LIST} sprawia, że w wyniku otrzymywana jest lista konturów, a nie struktura hierarchiczną. \texttt{CV\_CHAIN\_APPROX\_SIMPLE} powoduje zwrócenie konturów w formie skompresowanej, dokładne wyjaśnienie kompresji znajduje się w dokumentacji do OpenCV\cite{opencv_docs}. Ostatni argument to przesunięcie początkowe (offset).

Znalezione kontury zostają przypisane do tymczasowej zmiennej \texttt{contours} (3). Dzięki temu można iterować używając tego wskaźnika (4), jednocześnie zachowując adres początkowy struktury w polu \texttt{contours}. Przez to może ona zostać użyta w nasępnym wykonaniu metody \texttt{detectMarkers}.

Iterując po każdym konturze tworzone są pomocnicze zmienne \texttt{center} i~\texttt{radius} (5-6). Od razu, korzystając z \texttt{cvContourArea}, obliczane jest też pole danego konturu (7).

Jeżeli pole konturu nie mieści się w granicach określonych w \texttt{Algorithm\-Settings} (8), to dany kontur jest ignorowany i rozpoczyna się analiza następnego (9).

Jeżeli kontur jest prawidłowy (spełnia warunek (8)), to, używając funkcji \texttt{cvMinEnclosingCircle}, znajdowany jest jego środek i promień (11). Następnie, na tej podstawie, tworzony jest obiekt \texttt{Dot} o środku w tym konturze (12) i przypisywany do odpowiedniego markera (13). Użyta funkcja \texttt{assignDotToMarker} zostanie wyjaśniona w dalszej części rozdziału.

Gdy sprawdzono już wszystkie kontury na obrazie i przypisano kropki do markerów, można wyczyścić podręczną pamięć OpenCV (15). Następnie, z użyciem funkcji \texttt{removeBogusMarkers}, usuwane są niepoprawne markery (16). Również ta i pozostałe funkcje nie pochodzące z pakietu OpenCV zostały wyjaśnione poniżej.

Jeżeli użytkownik włączył usuwanie zniekształceń obrazu (17), to znalezione markery transformowane są funkcją \texttt{undistort}. Następnie, używając informacji o wychyleniu układu, ich współrzędne poprawiane są metodą \texttt{correctTilt} (20).

Jeżeli markery są ustawione równolegle do krawędzi obrazu, to wykryte na nich skrajne kropki (północna, południowa, wschodnia, zachodnia) mogą być niepoprawne, gdyż różnice między ich współrzędnymi w danym kierunku są zbyt małe. Z tego względu wszystkie markery przetwarzane są jeszcze przez funkcję \texttt{correctMarkersParallelToEdges} (21). Jeżeli marker jest równoległy do krawędzi obrazu, zostaje on tymczasowo obrócony o 45 stopni w celu wyznaczenia skrajnych kropek.

Tworzona jest zmienna \texttt{screenCenter} (22) zawierająca współrzędne środka obrazu, która zostanie użyta w kolejnej pętli (23). Pętla ta przebiega po wszystkich markerach. Na początku znajdowana jest \texttt{mainDot} markera (24), a następnie ustawiane jest jego \texttt{id} (i implicite \texttt{mainDot}) (25) oraz odległość od środka obrazu (26). Do wykonania tych operacji wykorzystywane są metody z opisywanej wcześniej klasy \texttt{GeometryUtils}.

Na końcu zwracane są wykryte markery (28).

\subsection{Metoda assignDotToMarker}

Metoda ta dodaje kropkę do istniejącego markera lub tworzy nowy marker z tą kropką. Jest to zależne, czy kropka jest w zasięgu któregoś z istniejących markerów - zasięg ten jest określony przez \texttt{markerSpan}.

Dla każdego z istniejących markerów sprawdzane jest, czy współrzędne tej kropki są oddalone o nie więcej niż \texttt{markerSpan} od pierwszej kropki tego markera. Możnaby wybrać dowolną jego kropkę, jednak mamy pewność, że pierwsza zawsze istnieje.

Jeżeli warunek ten jest spełniony to dana kropka dodawana jest do znalezionego markera, a następnie sprawdzamy, czy kwalifikuje się ona na którąś ze skrajnych kropkek. Jeżeli tak jest, to ustawiane jest odpowiednie pole markera. Na koniec następuje powrót z funkcji.

Jeżeli kropka nie jest w zasięgu żadnego z markerów, to tworzony jest nowy marker przez przekazanie do konstruktora tej kropki, co automatycznie ustawi wszystkie skrajne kropki markera na tę kropkę. Następnie stworzony marker dodawany jest do wektora \texttt{markers}.

\subsection{Metoda removeBogusMarkers}

Jest to bardzo prosta metoda, która usuwa z wektora \texttt{markers} wszystkie markery, które mają mniej niż trzy kropki. W tym celu użyty jest iterator \texttt{it} z~biblioteki STL. Dzięki takiemu sposobowi iterowania po elementach wektora można usunąć aktualnie analizowany markera metodą \texttt{erase} z klasy \texttt{vector}.

\subsection{Metoda undistort}

Obraz rejestrowany przez kamerę, szczególnie słabszej jakości, jest zniekształcony. Widoczna jest na nim dystorsja, kolokwialnie nazywana beczką. Jest to spowodowane faktem, że obraz jest prostokątny, a obszar świata rzeczywistego rejestrowany przez kamerę okrągły.

Zniekształcenie obrazu powoduje problemy szczególnie w przypadkach, gdy chcemy go użyć do relatywnie precyzyjnych obliczeń. Z tego powodu pakiet OpenCV dostarcza zestaw funkcji pozwalających na eliminację zniekształceń obrazu. Zasada ich działania oparta jest na zaawansowanych własnościach matematycznych obrazu i soczewki w kamerze. 

Dokładnie proces usuwania dystorsji obrazu został opisany w odpowiednim tutorialu na stronie projektu OpenCV\cite{undistort}. Z tego tutoriala pochodzą też obrazy widoczne na rysunku \ref{fig:distorted_undistorted}. Po lewej stronie znajduje się obraz przed usunięciem zniekształceń - wyraźnie widoczne są zaokrąglone boki stołu. Po prawej stronie znajduje się obraz poprawiony - linie proste są rzeczywiście proste na obrazie.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.6]{img/distorted_undistorted.png}
  \caption{Przykładowe obrazy z tutoriala OpenCV\cite{undistort} dotyczącego usuwania dystorsji obrazu.}
  \label{fig:distorted_undistorted}
\end{figure}

Aby skorzystać z funkcji poprawiających zniekształcenia obrazu niezbędna jest znajomość dwóch macierzy - macierzy współczynników kamery i~macierzy współczynników dystorsji. Ręczne wyliczenie tych macierzy jest praktycznie niewykonalne, ale istnieją funkcje wbudowane w OpenCV, które analizując serię obrazów potrafią wyliczyć współczynniki w tych macierzach. Dokładny opis całego procesu i kod źródłowy, pozwalający na wygodne jego przeprowadzenie, znajduje się w wyżej wspomnianym tutorialu\cite{undistort}.

Upraszczając, niezbędne jest stworzenie szachownicy podobnej jak ta na rysunku \ref{fig:distorted_undistorted} i uruchomienie programu z tutoriala na serii zdjęć szachownicy. Po zarejestrowaniu jej wystarczającą liczbę razy pod różnymi kątami nachylenia, OpenCV jest w stanie wyliczyć współczynniki w macierzach. Jest to operacja jednorazowa, więc znalezione raz współczynniki można wpisać bezpośrednio do kodu źródłowego. Tak też postąpili autorzy, tworząc w klasie \texttt{Const} stałe \texttt{CAMERA\_MATRIX} i \texttt{DIST\_COEFFS}.

W tym momencie można zacząć analizę kodu źródłowego metody \texttt{undistort}. Na początku tworzony jest wektor punktów wszystkich markerów - \texttt{points}. Przed wpisaniem do wektora, współrzędne każdej kropki są konwertowane z~powrotem do reprezentacji obrazu (wartości Y rosnące w dół obrazu).

Następnie tworzony jest tymczasowy obiekt klasy \texttt{CvMat} o nazwie \texttt{tmp}, który będzie wskazywał na tę samą pamięć co \texttt{points}. Obiekt ten, wraz ze wspomnianymi wcześniej macierzami, jest przekazywany do funkcji \texttt{cv\-Undistort\-Points} i w nim też są zwracane rezultaty przekształcenia.

Otrzymane rezultaty należy jeszcze przemnożyć przez macierz współczynników kamery, co jest wykonywane w następnym bloku kodu tam, gdzie zostały wprowadzone zmienne \texttt{fx}, \texttt{fy}, \texttt{cx} i \texttt{cy}.

Na końcu tworzony jest wynikowy wektor markerów - \texttt{outMarkers}, do którego zostaną wpisane poprawione markery. Następuje pętla po wszystkich markerach wejściowych, a wewnątrz niej po wszystkich kropkach markera. Na podstawie przetransformowanych punktów tworzone są nowe obiekty \texttt{Dot}, dodawane do nowego markera i ewentualnie ustawiane jako skrajne kropki, jeżeli ich odpowiedniki były którymiś z nich. Na końcu marker dodawany jest do \texttt{outMarkers}, który, po zakończeniu zewnętrznej pętli, jest zwracany jako wynik wykonania metody.

\subsection{Metoda correctTilt}

Implementacja tej metody jest oparta na podobnym schemacie co ostatnia część implementacji metody \texttt{undistort}. Tworzony jest nowy wektor markerów \texttt{outMarkers}, a następnie transformowane są kropki z markerów wejściowych i dodawane nowe markery z przetransformowanymi kropkami do \texttt{outMarkers}. Tak samo, jeżeli oryginalna kropka była którąś ze skrajnych, to prztransformowana również ustawiana jest jako ta skrajna kropka.

Inny jest jedynie sposób tranformowania kropek. Operacja ta odbywa się w funkcji \texttt{correctDot}. Do oryginalnych współrzędnych kropki jest w niej dodawane przesunięcie w pikselach wynikające ze zmierzonego wychylenia układu (\texttt{tilt}). Wartość przesunięcia w pikselach jest wyliczana przez konwersję wysokości sufitu na piksele, a następnie pomnożenie jej przez tangens wychylenia układu. Dociekliwy czytelnik bardzo łatwo wyprowadzi ten wzór z zależności trygonometrycznych w trójkącie.

\subsection{Metoda correctMarkersParallelToEdges}

Całe ciało tej metody to pętla po markerach przy pomocy iteratora \texttt{it}. Interesują nas tylko markery równoległe do krawędzi obrazu, dlatego na samym początku pętli sprawdzany jest warunek
\begin{lstlisting}
if (isMarkerPrallelToEdge(*it)) {
\end{lstlisting}

\hspace{\textwidth}

Funkcja \texttt{isMarkerPrallelToEdge} analizuje wszystkie skrajne kropki danego markera i porównuje ich współrzędne z pozostałymi kropkami tegoż markera. Jeżeli aktualna skrajna kropka nie jest wysunięta w danym kierunku o odległość większą niż \texttt{tolerance} od wszystkich pozostałych kropek, to zwiększany jest licznik \texttt{premises}. Jeżeli istnieją co najmniej dwie przesłanki (\texttt{premises}), że marker jest równoległy do krawędzi obrazu, to zwracane jest \texttt{true}, w przeciwnym przypadku \texttt{false}.

Wymagane jest wystąpienie dwóch przesłanek, gdyż w przeciwnym przypadku niektóre markery mogą zostać niepoprawnie zaklasyfikowane jako równoległe do krawędzi obrazu. Przykładem jest marker o numerze 560 obrócony o 45 stopni. Ze względu na ułożenie kropek na takim markerze, w jednym kierunku spełniony jest warunek na zwiększenie \texttt{premises}, jednak w pozostałych kierunkach bardzo wyraźnie można odróżnić kropki skrajne.

Z drugiej strony, jeżeli marker rzeczywiście jest równoległy do krawędzi obrazu, to zawsze wystąpią co najmniej dwie przesłanki. Można tak wnioskować z faktu, że w którychś dwóch kierunkach wystąpi konflikt między kropkami \texttt{mainDot} i \texttt{left} oraz \texttt{mainDot} i \texttt{right}.\\

Po stwierdzeniu, że dany marker jest równoległy do krawędzi obrazu, tworzony jest tymczasowy marker \texttt{tmpMarker} i wykonywana jest iteracja po wszystkich kropkach oryginalnego markera. Każda kropka jest transformowana za pomocą funkcji \texttt{temporarilyTransformDot} i zapisywana w \texttt{newTmpDot}. 

Funkcja \texttt{temporarilyTransformDot} obraca daną kropkę wokół środka układu współrzędnych o 45 stopni. Dzięki temu, jeżeli przetransformowane zostaną tak wszystkie kropki markera, sam marker staje się obrócony o 45 stopni i nie jest już równoległy do krawędzi obrazu.

Sam wzór na obrót wynika bezpośrednio z mnożenia współrzędnych przez macierz rotacji. Dla kąta obrotu $\theta$ macierz ta ma postać:

$$ R = \left[\begin{array}{cc} \cos \theta & - \sin \theta \\ \sin \theta & \cos \theta \end{array}\right] $$

\hspace{\textwidth}

Pozostała część algorytmu w metodzie \texttt{correctMarkersParallelToEdges} polega na jednoczesnym ustawianiu skrajnych kropek w oryginalnym markerze wskazywanym przez iterator \texttt{it}, jak i tymczasowym markerze \texttt{tmpMarker}. Przykładowo:
\begin{lstlisting}
if (newTmpDot.getY() > tmpMarker.getNorth().getY()) {
    tmpMarker.setNorth(newTmpDot);
    it->setNorth(*dotIter);
}
\end{lstlisting}
Porównywanie współrzędnych aktualnej i skrajnej kropki odbywa się na markerze tymczasowym i na kropce przetransformowanej (1). Jeżeli warunek jest spełniony, to w markerze tymczasowym ustawiana jest odpowiednia przetransformowana kropka tymczasowa (2). Jednocześnie, w oryginalnym markerze ustawiana jest jako ta sama skrajna kropka aktualnie analizowana kropka markera oryginalnego (3).


\section{CvblobDetectionStrategy}

Klasa ta posiada tylko częściową implementację. Została ona wprowadzona eksperymentalnie w celu wstępnego porównania wydajności z OpenCV. Okazało się, że wydajność jej jest jednak słabsza, przynajmniej w aktualnej implementacji, dlatego kod został porzucony na poziomie poszukiwania "blobów", czyli obiektów na zbinaryzowanym obrazie.

Metoda \texttt{getInstance} standardowo inicjalizuje pola klasy. Jedynym nowym polem, w porównaniu z OpenCV jest \texttt{labelImage}, które przechowuje obraz, na którym zostaną później zaznaczone znalezione obiekty.

\subsection{Metoda detectMarkers}

\begin{lstlisting}
markers.clear();
CvBlobs blobs;
cvLabel(mask, labelImage, blobs);
cvFilterByArea(blobs, algorithmSettings->getMinDotArea(), algorithmSettings->getMaxDotArea());
for (CvBlobs::const_iterator it = blobs.begin(); it != blobs.end(); ++it) {
    float area = it->second->area;
    CvPoint2D64f center = it->second->centroid;
}
return markers;
\end{lstlisting}

Podobnie jak w implementacji OpenCV, zaczynamy od wyczyszczenia wektora \texttt{markers} (1). Następnie tworzony jest obiekt \texttt{blobs}, w którym zostaną zapisane znalezione bloby (2).

Metoda \texttt{cvLabel} (3) przyjmuje na wejściu zbinaryzowany obraz (\texttt{mask}) i~znajduje na nim bloby. Umieszcza je w obiekcie \texttt{blobs}, jednocześnie tworząc obraz \texttt{labelImage} i zaznaczając na nim znalezione bloby.

Znalezione bloby są następnie filtrowane pod względem wielkości powierzchni. Używana jest do tego funkcja \texttt{cvFilterByArea} (4).

Potem następuje iteracja po znalezionych blobach i wyłuskanie ich powierzchni i współrzędnych środka (6-7). Gdyby implementować tę metodę w całości, należałoby dopisać tutaj tworzenie kropek i przypisywanie ich do markerów, podobnie jak w \texttt{SimpleDetectionStrategy}.

W przypadku Dotgazera zaimplementowana część algorytmu wystarczyła już by stwierdzić, że metoda ta będzie wolniejsza niż ta z \texttt{Simple\-Detection\-Strategy}.

%TODO porównanie wydajności tu wrzucić?