\chapter{BinarizationStrategy - Binaryzacja obrazu}
\label{sec:binarization_strategy}

Pierwszym etapem algorytmu obliczania pozycji jest odpowiednie zbinaryzowanie obrazu. Dzięki temu w kolejnych krokach algorytmu możliwe jest bardziej dokładne i niezawodnie znalezienie markerów i ich współrzędnych. Pomimo pozornej prostoty problemu, w toku prac zostały zaimplementowane aż trzy strategie implementujące interfejs \texttt{BinarizationStrategy}, który jest zadeklarowany następująco:

\begin{lstlisting}
class BinarizationStrategy {
public:
    virtual IplImage* binarizeImage(CameraImage* rawImage) = 0;
    virtual IplImage* getIplImageAllocatedForBinarizedImage() = 0;
};
\end{lstlisting}

Metoda \texttt{binarizeImage} przyjmuje wskaźnik na strukturę przechowującą dane binarne obrazu zarejestrowanego przez kamerę:

\begin{lstlisting}
struct CameraImage {
    uint32_t length;
    void* image;
};
\end{lstlisting}

Jak zostało już wcześniej wspomniane, obraz rejestrowany przez kamerę jest w formacie GRBG i jasność każdego piksela reprezentowana jest na dwóch bajtach, z czego znaczących jest tylko 10 młodszych bitów. W strukturze \texttt{CameraImage} dane obrazu znajdują się w pamięci wskazywanej przez \texttt{image}, a \texttt{length} wyraża rozmiar obrazu w bajtach.

Zwracany jest wskaźnik na obiekt klasy \texttt{IplImage} z pakietu OpenCV, który reprezentuje zbinaryzowany obraz i posłuży do wykonania kolejnego kroku algorytmu.

Ponadto zadeklarowana jest metoda \texttt{get\-Ipl\-Image\-Allocated\-For\-Binarized\-Image}, która również zwraca obraz zbinaryzowany. Metoda ta nie przeprowadza jednak binaryzacji, a jedynie zwraca wskaźnik na zapamiętany rezultat wykonania \texttt{binarizeImage}. Jest ona przydatna, jeżeli z jakiegoś powodu chcemy po raz drugi otrzymać wskaźnik na zbinaryzowany obraz, np. w celu streamowania go do użytkownika.

\section{BgrBinarizationStrategy}

Pierwszą implementacją binaryzacji obrazu była \texttt{BgrBinarizationStrategy}:

\begin{lstlisting}
class BgrBinarizationStrategy : public BinarizationStrategy {
private:
    bool verboseText, verboseImage;
    ImageConverter* converter;
    IplImage* mask;
    IplImage* bgrImage;
    AlgorithmSettings* algorithmSettings;
public:
    static BgrBinarizationStrategy* getInstance(
            ImageConverter* converter, CvSize maskSize, 
            bool verboseText, bool verboseImage, 
            AlgorithmSettings* algorithmSettings);
    IplImage* binarizeImage(CameraImage* rawImage);
    IplImage* getIplImageAllocatedForBinarizedImage();
\end{lstlisting}

Pola \texttt{verboseText} i \texttt{verboseImage} pojawiają się w każdej z implementacji strategii i służą do sterowania wypisywaniem logów. Nie sterują one w żaden sposób wykonaniem algorytmu, służą jedynie do wyspecyfikowania, czy użytkownik chce otrzymywać w konsoli logi w formie tekstowej (\texttt{verboseText}) oraz w formie graficznej (\texttt{verboseImage}), gdzie okna renderowane są przez OpenCV. Ta druga opcja dostępna jest tylko w środowiskach graficznych.

\texttt{ImageConverter} oraz \texttt{AlgorithmSettings} są wstrzykiwane przez konstruktor z poziomu głównego kodu serwera. Podobnie w innych strategiach, klasy użytkowe, które skupiają się głównie na dostarczeniu przydatnych funkcji, są wstrzykiwane przez konstruktor.

\texttt{bgrImage} oraz \texttt{mask} są alokowane w metodzie \texttt{getInstance}. Służą one odpowiednio do przechowywania obrazu przekonwertowanego do formatu BGR oraz do przechowywania maski (zbinaryzowanego obrazu) otrzymanego na podstawie \texttt{bgrImage}.

\subsection{Metoda binarizeImage}

Implementacja metody \texttt{binarizeImage} jest dosyć prosta i zaczyna się od konwersji obrazu do formatu BGR 
\begin{lstlisting}
converter->convertRawToBGR(rawImage, bgrImage);
\end{lstlisting}

Tak skonwertowany obraz jest następnie, przy użyciu metody \texttt{cvInRangeS} z pakietu OpenCV, binaryzowany. Wartość progowa jasności piksela pobierana jest z obiektu klasy \texttt{AlgorithmSettings} przy pomocy metody \texttt{getDot\-Brightness\-Threshold}:
\begin{lstlisting}
cvInRangeS(img, cvScalar( algorithmSettings->getDotBrightnessThreshold(), 0, 0, 0), cvScalar(256, 0, 0, 0), mask);
\end{lstlisting}

Wynik binaryzacji zapamiętywany jest w polu \texttt{mask}. Wskaźnik do tego pola jest też wartością zwracaną z metody. Aby ponownie pobrać ten wskaźnik z~zewnątrz klasy wystarczy już tylko wywołanie metody \texttt{getIplImage\-Allocated\-For\-BinarizedImage}.

Przedstawione w powyższym opisie obiekty i metody użytkowe (utility methods) będą wykorzystywane również w innych implementacjach, jednak nie będą już szerzej opisywane, gdyż spowodowałoby to nadmierny rozrost dokumentacji. Tak samo metody zaczynające się od \texttt{cv*} są częścią biblioteki OpenCV i na stronie tegoż projektu należy szukać ich dokumentacji.


\section{RawBinarizationStrategy}

W celu optymalizacji szybkości binaryzacji stworzono strategię, która operuje na obrazie surowym, a nie GRBG - \texttt{RawBinarizationStrategy}. Metoda \texttt{getIplImageAllocatedForBinarizedImage}, jeżeli chodzi o implementację, nie zmieniła się:

\begin{lstlisting}
class RawBinarizationStrategy: public BinarizationStrategy {
private:
    bool verboseText, verboseImage;
    ImageConverter* converter;
    IplImage* mask;
    AlgorithmSettings* algorithmSettings;
public:
    static RawBinarizationStrategy* getInstance(CvSize maskSize, bool verboseText, bool verboseImage, AlgorithmSettings* algorithmSettings);
    IplImage* binarizeImage(CameraImage* rawImage);
    IplImage* getIplImageAllocatedForBinarizedImage();
\end{lstlisting}

\subsection{Metoda binarizeImage}

W implementacji tej metody, każdy piksel surowego obrazu i maski jest przetwarzany osobno. Iteracja po pikselach odbywa się przez użyciu kilku wskaźników. 

\texttt{rawPtr8} wykorzystywany jest do iteracji po kolejnych bajtach obrazu, jeżeli liczby są reprezentowane w procesorze jako big endian. Konieczność taka zachodzi, ponieważ wartości pikseli na obrazie zapisywane są w little endian, więc niezbędna jest możliwość wyłuskania każdego z dwóch bajtów, na których zapisywana jest informacja o pikselu.

\texttt{rawPtr16} wykorzystywany jest do iteracji po kolejnych pikselach obrazu (dwa bajty). Jest on używany, gdy reprezentacja liczb w procesorze zgadza się z reprezentacją na obrazie (little endian).

\texttt{maskPtr} służy do ustawiania kolejnych pikseli w zbinaryzowanym obrazie. Binaryzacja w tym przypadku będzie oznaczać ustawienie minimalnej bądź maksymalnej jasności piksela.

Znaczenie pozostałych zmiennych zostanie wyjaśnione podczas opisu samego algorytmu binaryzacji.\\

Zastosowany algorytm binaryzacji zależy od kolejności bajtów (endianness) w reprezentacji procesora. Jest to sprawdzane w warunku

\begin{lstlisting}
if (!Utils::isSystemBigEndian()) {
\end{lstlisting}

Jeżeli procesor posiada reprezentację little endian, to jest ona zgodna z~reprezentacją pikseli na obrazie i wystarczające jest pobranie wartości kolejnego piksela (1), przesunięcie wskaźnika na następny piksel (2) i ustawienie wartości piksela w masce na biały lub czarny kolor (3).

\begin{lstlisting}
rawValue = *rawPtr16;
++rawPtr16;
maskPtr[x] = (rawValue) >= dotBrightnessThreshold ? 255 : 0;
\end{lstlisting}

Z racji tego, że wartości pikseli na obrazie reprezentowane są na 10 bitach, a użytkownik ustawia wartość \texttt{dotBrightness\-Threshold} w zakresie 0-255, czyli wartości mieszczące się na 8 bitach, to przy deklaracji \texttt{dot\-Brightness\-Threshold} wykonywane jest jeszcze dwubitowe przesunięcie w lewo, aby porównywanie wartości pikseli miało sens:

\begin{lstlisting}
int dotBrightnessThreshold = (algorithmSettings->getDotBrightnessThreshold());
dotBrightnessThreshold = dotBrightnessThreshold << 2;
\end{lstlisting}

Po przetworzeniu wszystkich pikseli w danym wierszu na obrazie, następuje przesunięcie wskaźnika maski na kolejny wiersz:

\begin{lstlisting}
maskPtr += widthStep;
\end{lstlisting}

Jeżeli procesor posiada reprezentację big endian, to trzeba dane o wartościach pikseli wyłuskiwać analizując kolejne bajty i zamieniając kolejność w~parach. Służą do tego dwie dodatkowe zmienne pomocnicze \texttt{raw8\_1} i~\texttt{raw8\_2}.

\begin{lstlisting}
raw8_1 = *rawPtr8;
++rawPtr8;
raw8_2 = (*rawPtr8) << 8;
++rawPtr8;
maskPtr[x] = (((raw8_1 | raw8_2))) >= dotBrightnessThreshold ? 255 : 0;
\end{lstlisting}

Iteracja po surowym obrazie odbywa się wskaźnikiem co 8 bitów. Starszy bajt obrazu zapamiętywany jest w \texttt{raw8\_1}, staje się przez to młodszym bajtem. Młodszy bajt obrazu zapamiętywany jest w \texttt{raw8\_2} i przesuwany o 8 bitów, stając się tym samym starszym bajtem. Na tak przygotowanych zmiennych wykonywana jest operacja sumy bitowej, dzięki czemu zostają one ustawione w takiej samej reprezentacji, jak reprezentacja procesora. Reszta algorytmu przebiega podobnie jak w przypadku procesorów little endian.

\subsection{Jakość binaryzacji}

Podczas testowania implementacji \texttt{RawBinarizationStrategy} okazało się, że, pomimo znacznej poprawy szybkości wykrywania, spadła jakość binaryzowanego obrazu. Fakt, że rozpatrywane były tylko pojedyńcze piksele prowadził do "poszarpanych" krawędzi na obrazie w miejscach, gdzie jasność pikseli wahała się w granicach progu binaryzacji, co można zauważyć na rysunku \ref{fig:raw_binarization_noninterpolated}.

\begin{figure}[H]
  \centering
  \includegraphics[scale=1]{img/raw_binarization_noninterpolated_closeup.png}
  \caption{Fragment obrazu zbinaryzowanego przy użyciu RawBinarizationStrategy. Widoczne są "poszarpane" krawędzie na brzegu lampy.}
  \label{fig:raw_binarization_noninterpolated}
\end{figure}

Niejednorodność binaryzacji w obszarach takich, jak widoczny na rysunku \ref{fig:raw_binarization_noninterpolated} brzeg lampy prowadziły do trudności w odróżnieniu prawdziwych markerów od jasnych punktów na suficie. Piksele na brzegu lamp w zbinaryzowanym obrazie często łączyły się w grupy, które były wystarczająco duże, aby móc zaklasyfikować je jako kropkę markera. Jeżeli co najmniej trzy takie grupy pikseli znalazły się blisko siebie, to wykrywany był nieistniejący w rzeczywistości marker. Na dłuższą metę takie rozwiązanie było niedopuszczalne.


\section{RawInterpolatedBinarizationStrategy} 

Strategia \texttt{RawInterpolatedBinarization\-Strategy} jest kompromisem pomiędzy jakością zbinaryzowanego obrazu dostępną w \texttt{BgrBinarization\-Strategy} a szybkością działania, którą udało się osiągnąć w \texttt{RawBinarizationStrategy}.

Nie było możliwe stworzenie zaawansowanej metody interpolującej, gdyż znacznie zmniejszała ona szybkość działania. W szczególności operowanie na pikselach z innych wierszy niż aktualnie przetwarzany (np. górny i dolny sąsiad danego piksela) oznaczała sięganie do pamięci, która nie znajdowała się w cache procesora. Z tego powodu zaimplementowano prostą interpolację, która została opisana poniżej.

\begin{lstlisting}
class RawInterpolatedBinarizationStrategy: public BinarizationStrategy{
private:
    bool verboseText, verboseImage;
    ImageConverter* converter;
    IplImage* mask;
    AlgorithmSettings* algorithmSettings;
public:
    static RawInterpolatedBinarizationStrategy* getInstance(CvSize maskSize, bool verboseText, bool verboseImage, AlgorithmSettings* algorithmSettings);
    IplImage* binarizeImage(CameraImage* rawImage);
    IplImage* getIplImageAllocatedForBinarizedImage();
\end{lstlisting}

\subsection{Metoda binarizeImage}

W aktualnej wersji systemu interpolacja została zaimplementowana tylko dla architektur typu little endian. Zasada progowania wartości pikseli i tworzenia zbinaryzowanej maski jest podobna jak w \texttt{RawBinarizationStrategy}.

Zmieniło się to, że w każdej iteracji następuje przesunięcie o dwa piksele w masce:
\begin{lstlisting}
for (int x = 0; x < Const::IMAGE_WIDTH; x+=2) {
\end{lstlisting}

Również piksele surowego obrazu sczytywane są parami w każdej iteracji:
\begin{lstlisting}
rawValue1 = *rawPtr16;
++rawPtr16;
rawValue2 = *rawPtr16;
++rawPtr16;
\end{lstlisting}

Interpolacja jest bardzo prosta i nie wymaga odwoływania się do pamięci innej, niż wczytane wartości dwóch pikseli. Polega ona na tym, że w parzystych wierszach wartość pierwszego piksela z pary ustawiana jest na taką samą, jak wartość drugiego piksela. W nieparzystych wierszach postępowanie jest odwrotne:
\begin{lstlisting}
if (y % 2 == 0) {
    rawValue2 = rawValue1;
} else {
    rawValue1 = rawValue2;
}
\end{lstlisting}          

Zinterpolowane w ten sposób piksele są porównywane z wartościami progowymi i na ich podstawie zbinaryzowany obraz zapisywany jest w masce:
\begin{lstlisting}
maskPtr[x] = (rawValue1) >= dotBrightnessThreshold ? 255 : 0;
maskPtr[x+1] = (rawValue2) >= dotBrightnessThreshold ? 255 : 0;
\end{lstlisting}

\subsection{Jakość binaryzacji}

Jakość zbinaryzowanego obrazu została znacznie poprawiona w stosunku do \texttt{RawBinarizationStrategy}. Markery nadal wykrywane są poprawnie, a niepożądne obiekty, jak przykładowo lampy na suficie, tworzą jedną spójną grupę pikseli (jak na rysunku \ref{fig:raw_binarization_interpolated}), które łatwo jest odfiltrować jako nie-markery.

\begin{figure}[H]
  \centering
  \includegraphics[scale=1]{img/raw_binarization_interpolated_closeup.png}
  \caption{Fragment obrazu zbinaryzowanego przy użyciu RawInterpolatedBinarizationStrategy. Udało się zneutralizować "poszarpane" krawędzie na brzegu lampy.}
  \label{fig:raw_binarization_interpolated}
\end{figure}






















