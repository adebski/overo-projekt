\chapter{LocalizationStrategy - Obliczenie aktualnej pozycji}

Posiadając wszystkie informacje o markerach można przystąpić do wyznaczenia pozycji kamery (a zarazem i robota) w pomieszczeniu. Strategia deklarująca metody do tego służące to \texttt{LocalizationStrategy}:

\begin{lstlisting}
class LocalizationStrategy {
public:
    virtual Coordinates calculatePosition(std::vector<Marker> markers) = 0;
    virtual Marker getReferenceMarker() = 0;
\end{lstlisting}

Metoda \texttt{calculatePosition} na podstawie przekazanych w argumencie markerów oblicza współrzędne kamery. Jest to główna metoda w tej strategii.

Metoda \texttt{getReferenceMarker} została wprowadzona w celach diagnostycznych. Pozwala ona po przeprowadzeniu wyznaczania pozycji pobrać marker, według którego została wyznaczona pozycja kamery. Dzięki temu można np. zaznaczyć ten marker na obrazie streamowanym do użytkownika. Sygnatura tej metody zakłada, że wyznaczanie pozycji odbywa się na podstawie jednego markera. W przypadku implementacji tej strategii korzystającej z więcej niż jednego markera konieczne byłoby zmienienie tej metody w interfejsie.

\section{OneMarkerLocalizationStrategy}

Implementacja ta wyznacza współrzędne kamery korzystając z jednego, najbliższego środkowi obrazu, markera. Strategia ta nie dodaje żadnych nowych metod publicznych. Jej pola również są podobne jak w innych strategiach, dlatego została tutaj bliżej przedstawiona jej deklaracja.

\subsection{Metoda calculatePosition}

Jeżeli przekazany wektor markerów jest pusty, to zwracane są współrzędne \texttt{INVALID\_COORDS}. Sygnalizuje to użytkownikowi, że nie udało się poprawnie wyznaczyć pozycji kamery.

Jeżeli istnieją markery, to przy pomocy metody \texttt{findMarkerClosest\-To\-Center} znajdowany jest \texttt{referenceMarker}. Wynika to z założenia, że marker najbliższy środkowi obrazu jest najmniej zniekształcony i najlepiej nadaje się do wyznaczania pozycji.

Samo znajdowanie pozycji odbywa się przy użyciu pomocniczej metody \texttt{calculateRobotPosition}, która na wejściu otrzymuje \texttt{referenceMarker} i~zwraca wyliczone współrzędne kamery. Jej implementacja jest podobna do opisywanego w poprzednim rozdziale mapowania nowego markera:

\begin{lstlisting}
float absCameraAngle = GeometryUtils::markerAngle(m) - m.getAbsoluteCoords().getAngle();
float xShift = (Const::IMAGE_WIDTH / 2 - m.getCenter().getX()) * algorithmSettings->getMetersPerPixel();
float yShift = (Const::IMAGE_HEIGHT / 2 - m.getCenter().getY()) * algorithmSettings->getMetersPerPixel();
float absCameraX = m.getAbsoluteCoords().getX() + sin(absCameraAngle) * yShift + cos(absCameraAngle) * xShift;
float absCameraY = m.getAbsoluteCoords().getY() + cos(absCameraAngle) * yShift - sin(absCameraAngle) * xShift;
return Coordinates(absCameraX, -absCameraY, absCameraAngle);
\end{lstlisting}

Wykorzystywany tutaj marker \texttt{m} to \texttt{referenceMarker}, który został przekazany w argumencie wywołania.

\texttt{absCameraAngle} to kąt obrotu kamery w układzie współrzędnych absolutnych. Można wyliczyć jako różnicę między aktualnie obserwowanym kątem \texttt{referenceMarker}a, a zapisanym w mapie kątem tegoż markera w układzie współrzędnych absolutnych.

\texttt{xShift} i \texttt{yShift}, zostały przedstawione w rozdziale \ref{sec:opis_algorytmu_wyznaczania_pozycji}. Oznaczają one przesunięcie od \texttt{referenceMarker}a do środka obrazu.

\texttt{absCameraX} i \texttt{absCameraY} to współrzędne kamery w układzie współrzędnych absolutnych. Wzory na te współrzędne wynikają bezpośrednio z rozumowania przedstawionego w rozdziale \ref{sec:opis_algorytmu_wyznaczania_pozycji}.

Obliczone współrzędne kamery w układzie współrzędnych absolutnych zwracane są w obiekcie klasy \texttt{Coordinates}. Współrzędna Y jest negowana ze względu na fakt, że kamera obserwuje sufit z podłogi, a my rozważamy markery tak, jakbyśmy patrzyli na sufit z góry.

\subsection{Metoda getReferenceMarker}

Metoda ta to zwykły getter dla pola \texttt{referenceMarker}. Dla poprawnego działania tej metody najpierw trzeba wywołać właściwe znajdowanie pozycji, czyli metodę \texttt{calculatePosition}. To właśnie w czasie jej wykonania ustawiane jest pole \texttt{referenceMarker}, które zwraca metoda \texttt{getReferenceMarker}.

\section{ThreeMarkersLocalizationStrategy}

Klasa ta została pozostawiona ze względów historycznych, gdyż jej aktualna implementacja nie zgadza się z interfejsem (brak metody \texttt{getReference\-Marker}, a metoda \texttt{calculatePosition} nie działa w prawidłowy sposób). Mimo wszystko warto przedstawić zasadę działania tego sposobu pozycjonowania, gdyż dostosowanie go do aktualnej wersji systemu powinno być dużo mniej czasochłonne niż napisanie go od początku.

\subsection{Metoda calculatePosition}

Na początku następuje explicite wczytanie mapy z pliku (pominięte jest \texttt{MappingStrategy}) i wypełnienie pól w markerach. Następnie markery są sortowane rosnąco wg odległości od środka obrazu przy użyciu metody \texttt{sort\-Markers\-By\-DistanceFromScreenCenter}. Później wyznaczane są współrzędne kamery z użyciem funkcji \texttt{calculate\-Crossing\-Points}.\\

Metoda \texttt{sortMarkersByDistanceFromScreenCenter} jest punktem wejściowym do funkcji \texttt{quickSortMarkers}. Jedynym jej zadaniem jest podanie argumentów początkowych do \texttt{quickSortMarkers}:
\begin{lstlisting}
quickSortMarkers(markers, 0, markers->size() - 1);
\end{lstlisting}

Metoda \texttt{quickSortMarkers} to standardowy quicksort porównujący sortowane markery na podstawie ich odległości od środka obrazu. W tym celu wykorzystywana jest metoda \texttt{getDistanceFromCenter} z klasy \texttt{Marker}.\\

Metoda \texttt{calculateCrossingPoints} przyjmuje trzy najbliższe środkowi markery i wykonuje na nich triangulację. Polega ona na wyznaczeniu punktu przecięcia trzech okręgów o środkach w tych markerach i promieniach równych odległości od środka obrazu.

Mamy odległości od markerów $m_1, m_2, m_3$ wynoszące odpowiednio $r_1, r_2, r_3$. Wystarczy wyliczyć współrzędne punktu przecięcia trzech okręgów o środkach w punktach $m_i$ i promieniach $r_i$, gdzie $i=\{1, 2, 3\}$. 

Opis obliczania przecięć dwóch okręgów można znaleźć na przykład w serwisie Mathworld \cite{circle_intersection}. Wzory tam przedstawione zostały bezpośrednio przeniesione do kodu i powinny być zrozumiałe po przeczytaniu wspomnianego artykułu. Trzeci okrąg w przypadku implementacji metody \texttt{calculatePosition} służy do sprawdzenia, który punkt jest tym szukanym (przecięcie dwóch okręgów daje dwa punkty wynikowe - algorytm oczekujemy tylko jednego).

