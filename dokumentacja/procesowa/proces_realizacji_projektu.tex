\chapter{Proces realizacji projektu}

W tym rozdziale zostały opisane najważniejsze etapy tworzenia projektu. Szczególną uwagę poświęcono zmianom koncepcji, które zaszły w późniejszej fazie projektu. Warto je zaznaczyć, ponieważ spostrzeżenia i wnioski wyciągnięte z tych zmian mogą oszczędzić sporo czasu przy implementacji podobnych projektów w przyszłości. Wspomniano też o podziale między członków projektu, jednak należy pamiętać, że granice między obowiązkami były dosyć płynne.

\section{Wstępne założenia i cele}

Celem projektu było stworzenie systemu lokalizacji mobilnego robota działającego w zamkniętym pomieszczeniu. Szczególnie skupiono się na zapewnieniu poprawnego działania w laboratorium robotów mobilnych w Katedrze Informatyki. Było to też miejsce, gdzie następowały wstępne testy modułu, więc naturalnie został on najlepiej przystosowany do tego środowiska. W szczególności, przystosowano moduł do poprawnego działania nawet przy włączonym silnym oświetleniu na suficie. Z oświetleniem tym nie radził sobie, używany w testach, komercyjny StarGazer firmy Hagisonic.

Oczywiście system powinien również działać w innych warunkach. Testowano go później również w innych pomieszczeniach, o innych wymiarach, wysokości oraz warunkach oświetleniowych.

Jednym z celów było przygotowanie jak najbardziej kompaktowego i łatwo konfigurowalnego modułu, który mógłby być umieszczany na robotach mobilnych. Pierwszy cel był związany z wyborem platformy sprzętowej, więc został osiągnięty już na początku projektu. Przyjazny użytkownikowi interfejs został jednak stworzony już pod sam koniec prac nad projektem. Przez cały czas tworzenia systemu korzystano raczej z niskopoziomowych rozwiązań (łączenie z robotem przez SSH, stamtąd uruchamianie i konfiguracja systemu z linii komend). Z perspektywy czasu wczesne stworzenie aplikacji klienckiej z przyjaznym interfejsem użytkownika mogłoby przyspieszyć postępy prac, gdyż, jak się później okazało, znacznie ułatwił on debugowanie kodu.

\subsection{Wymagania funkcjonalne systemu}

Wymagania funkcjonalne odnoszą się głównie do samego algorytmu i możliwości, które ma dostarczać:

\begin{itemize}

\item możliwość wybrania środka układu współrzędnych w pomieszczeniu i~stworzenia mapy markerów, które będą w tym układzie posiadały odpowiednie koordynaty,

\item obliczanie pozycji i kąta obrotu robota na podstawie zmapowanych wcześniej markerów,

\item możliwość dynamicznej konfiguracji parametrów algorytmu (wysokość sufitu, próg jasności markerów, itp.) przez przyjazny dla użytkownika interfejs,

\item możliwość włączenia podglądu obrazu rejestrowanego przez kamerę w celu kalibracji wyżej wspomnianych parametrów.

\end{itemize}

\subsection{Wymagania niefunkcjonalne systemu}

Wymagania niefunkcjonalne są bardziej ogólne i wykraczają, w przeciwieństwie do wymagań funkcjonalnych, poza ramy samej implementacji algorytmu:

\begin{itemize}

\item dostarczenie jednolitego, samowystarczalnego modułu sprzętowego, który będzie można w łatwy sposób zamontować na większości robotów,

\item zapewnienie systemu operacyjnego i skonfigurowanej platformy sprzętowej pod implementację i rozszerzanie algorytmów do pozycjonowania robota,

\item dostarczenie implementacji algorytmu pozycjonowania z możliwością późniejszego rozszerzania go lub podmian (algorytm składający się z wielu modułów korzystających ze wzorca projektowego Strategy),

\item wysoka efektywność algorytmu pozycjonowania, pozwalająca na działanie w czasie rzeczywistym,

\item jak najlepsza dokładność pozycjonowania, otwarta na zmiany i ulepszenia,

\item niezależność od sposobu, w jaki użytkownik rozmieścił znaczniki na suficie, wykorzystanie odrębnego układu współrzędnych.

\end{itemize}

\section{Przebieg prac}

Poniżej opisano proces tworzenia systemu od samego początku prac i krystalizowania koncepcji do finalnej wersji dostarczonej w tym projekcie. Warto zaznaczyć, że początkowa faza projektu zajęła bardzo dużo czasu, gdyż testowanie kolejnych wersji było możliwe tylko w laboratorium (do którego dostęp możliwy był tylko w ograniczonym zakresie). Było to spowodowane koniecznością użycia zewnętrznego sprzętu (np. do emisji światła podczerwonego), gdyż sam projektowany przez nas moduł nie był w pełni samodzielny.

\subsection{Rozpoczęcie projektu}

Pomysł na projekt był wzorowany na komercyjnym rozwiązaniu StarGazer \cite{stargazer_manual} firmy Hagisonic, pokazany na rysunku \ref{fig:stargazer}. Rozpoczynając projekt autorzy mieli do dyspozycji egzemplarz tegoż urządzenia, co umożliwiło zapoznanie się z jego własnosciami i wykrycie jego potencjalnych wad. Pozwoliło to również stosunkowo precyzyjnie oreślić najbardziej pożądane cechy tworzonego produktu.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.6]{img/stargazer.jpg}
  \caption{System StarGazer firmy Hagisonic, na którym wzorowana była implementacja stworzona w niniejszym projekcie.}
  \label{fig:stargazer}
\end{figure}

Jako platformy sprzętowej, zostało narzucone użycie komputera jednopłytkowego Overo Air \cite{overo_website} firmy Gumstix. Platforma ta oferowała wydajność potencjalnie wystarczającą do zrealizowania projektu, zapewniając dodatkowo możliwość łatwego dołączenia urządzeń peryferyjnych (kamera, itp.) niezbędnych do realizacji wymagań projektu.

\subsection{Przygotowanie platformy sprzętowej}

Elementem niezbędnym do wykonania projektu była kamera, która miała przesyłać obraz sufitu, na którym znajdowały się znaczniki, do algorytmu wyliczającego pozycję. Priorytetem więc było skonfigurowanie platformy w~taki sposób, aby obsługiwana była komunikacja z kamerą.

Pierwszą próbę takiej konfiguracji podjęto standardowo, korzystając z~artykułu na wiki produktu \cite{caspa_configuration_wiki}. Procedura sprowadziła się do ściągnięcia gotowych obrazów systemu operacyjnego zawierających wszystkie elementy niezbędne do poprawnego działania kamery. Okazało się jednak, że obraz z kamery uzyskany na tej konfiguracji był relatywnie słabej jakości. Użyta gotowa wersja systemu Linux Angstrom posiadała ograniczoną funkcjonalność i~wachlarz dostępnych programów. Dodatkowo, oparta była o jądro w wersji 2.6.34, które wtedy wychodziło już powoli z użytku.

Autorzy zdecydowali się własnoręcznie przygotować całe środowisko, które można by potem użyć do uruchomienia systemu. Okazało się to bardzo czasochłonnym zadaniem i trwało od czerwca do końca lipca 2012.

Autorzy chcieli, aby wykorzystywany system posiadał jądro z linii 3.X, tak aby platforma działała w opraciu o w miarę nowoczesne rozwiązania. Dodatkową zaletą było oficjalne wpsarcie dla sensora MT9V032, w oparciu o który jest zbudowana kamera.

Do otrzymania w pełni działającego systemu potrzebowano dwóch rzeczy: systemu plików oraz samego jądra i plików bootujących. Wymagany system plików został wygenerowany za pomocą frameworka OpenEmbedded, większy problem był ze stworzeniem jądra systemowego. Należało w odpowiedni sposób wybrać jakie moduły oraz elementy jądra mają być w nim zawarte, tak aby kamera działała na procesorach z rodziny OMAP3. Dodatkowo okazało się, że plik źródłowy, stworzony dla układu Overo w źródłach jądra Linuxa nie zawiera fragmentów wymaganych w celu uruchomienia kamery na tej platformie sprzętowej.

W momencie prac nad działającym systemem, integracja wybranej kamery z nowymi wersjami jądra Linuxa nie była nigdzie w pełni opisana. Wiedzy należało szukać na wielu stronach internetowych oraz oficjalnej liście mailingowej dla użytkowników platformy Overo. Często podczas pracy wykorzystywana była metoda prób i błędów, zwłaszcza przy doborze tego, które moduły oraz elementy jądra mają być skompilowane.

Obecnie sytuacja przedstawia się zgoła inaczej. Dzięki wykorzystaniu przez Overo projektu Yocto, kompilacja systemu który wspiera kamerę Caspa FS jest bardzo prosta  i w pełni opisana na domowej stronie producenta układów Overo. Polega ona jedynie na sklonowaniu odpowiedniego repozytorium gita, wywołaniu odpowiednich skryptów oraz ściągnięciu programów wymaganych do cross-compilacji. 

Ostatecznie wykorzystano źródła jądra Linuxa udostępniane przez Steve'a Sakomana \cite{Sakoman}, które zawierały wiele poprawek dla systemów wbudowanych. Dodaliśmy również patch stworzony przez Laurenta Pincharta \cite{Pinchart}, (jednego z autorów obecnego sterownika dla sensora MT9V032), który dodawał możliwość korzystania z kamery na platformach Overo.

\subsection{Rozpoznawanie sztucznych znaczników}

Po udanym uruchomieniu kamery przyszedł czas na próby wyekstraktowania znaczników z obrazu przez nią zapisywanego. W pierwszym podejściu do tego zadania przygotowano znaczniki na podobę tych zastosowanych w~StarGazerze, ale zwyczajnie wydrukowane na kartce (nie odblaskowe). Po kilku próbach znalezienia odpowiedniego algorytmu okazało się jednak, że w~dłuższej perspektywie takie rozwiązanie będzie bardzo uciążliwe. Mimo tego, że dla ludzkiego oka takie znaczniki były łatwe do rozpoznania, na kamerze, gdzie obraz był niezbyt dobrej jakości, mieszały się z otoczeniem. Zmiany oświetlenia czy obiektów w otoczeniu znacznika diametralnie zmieniały wyniki algorytmów detekcji cech charakterystycznych obrazu.

Wyniki wykrywania nieodblaskowych markerów na obrazie prezentuje rysunek \ref{fig:nonreflectiveMarkers}. Po lewej u góry pokazano oryginalny obraz zarejestrowany przez kamerę. Po prawej u góry wynik zastosowania funkcji \texttt{cvHoughCircles} z~biblioteki OpenCV wykonująej transformację Hougha używaną do wyszukiwania okrągłych obiektów na obrazie. Po lewej na dole ten sam obraz po przetworzeniu funkcją binaryzującą \texttt{cvAdaptiveThreshold}. Po prawej na dole kontury obrazu zbinaryzowanego zaznaczone na oryginalnym obrazie przy użyciu funkcji \texttt{cvFindContours}.


Jak widać na rysunku \ref{fig:nonreflectiveMarkers}, wykrywanie nieodblaskowego markera dawało bardzo słabe rezultaty. W ogólnym przypadku działałoby jeszcze zdecydowanie gorzej, ponieważ na powyższym przykładzie wszystkie parametry do funkcji OpenCV zostały ustawione tak, aby dać jak najlepsze rezultaty tylko na tym konkretnym zdjęciu. Wyniki uzyskane dzięki połączeniu adaptywnego progowania i wykrywania konturów były zdecydowanie lepsze niż efekty zastosowania transformacji Hougha, jednak nadal dalekie od ideału. Poza tym, adaptywne progowanie jest dosyć czasochłonną operacją (progowanie jednego piksela wymaga dodatkowo sprawdzenia kolorów pikseli z nim sąsiadujących) i dlatego w dłuższej perspektywie zapewne uniemożliwiłoby uzyskanie przetwarzania w czasie rzeczywistym. Testowanie nieodblaskowych markerów przeprowadzono w sierpniu 2012.

\begin{figure}[H]
  \centering
    \includegraphics[width=\textwidth]{img/non_reflective_markers.png}
  \caption{Próby detekcji zwykłych (nieodblaksowych) markerow na suficie.}
  \label{fig:nonreflectiveMarkers}
\end{figure}

Dopiero po sprawdzeniu (i odrzuceniu) pomysłu z drukowanymi markerami postanowiono sprawdzić, jak będzie działać system, gdy zastosowane zostaną znaczniki używane przez StarGazera. Wymagało to też użycia samego StarGazera do oświetlania znaczników na suficie światłem podczerwonym. Z tego powodu testy te można było przeprowadzać tylko w laboratorium, bo tylko tam możliwy był dostęp do StarGazera.

Wyniki testów okazały się lepsze niż oczekiwali autorzy. Nawet mimo tego, że diody podczerwone nie były umieszczone bezpośrednio wokół obiektywu kamery, lecz znajdowały się około 10cm od niego, światło odbite zapewniło wyraźny obraz markerów, odznaczający go od reszty otoczenia. Natężenie światła odbitego od odblaskowych kropek markerów było praktycznie maksymalne i dzięki temu można było zastosować zwykłe progowanie (nie adaptywne) w celu zbinaryzowania obrazu.

Jedynym problemem okazały się być lampy na suficie, które dawały bardzo silne światło i były nadal widoczne po binaryzacji. Ich odfiltrowanie jednak okazało się bardzo proste ze względu na to, że wyraźnie odróżniały się od markerów (wielokrotnie większa powierzchnia). Zdecydowano się pozostać przy tej formie markerów i przez długi czas używano StarGazera jako urządzenia do rzucania światła podczerwonego na sufit. Dopiero w późniejszej fazie projektu połączono cały wykorzystywany sprzęt (w tym diody podczerwone) na jednym module. Testy markerów odblaskowych trwały we wrześniu 2012.

\subsection{Wyznaczanie pozycji na podstawie sztucznych znaczników}

Fakt, że znaleziono prosty i efektywny sposób ekstrakcji znaczników z~obrazu znacznie usprawnił prace nad projektem. Od teraz cała reszta algorytmu nie zależała już od otoczenia, zmiennych warunków oświetlenia i wielu innych rzeczy, które mają wpływ na wyniki w realnym świecie, lecz wymagała jedynie zastosowania odpowiednich narzędzi geometrii analitycznej do wyznaczenia pozycji na podstawie znalezionych markerów.

W pierwszej wersji wyznaczanie pozycji korzystało z trzech markerów widocznych na obrazie. Zastosowana została przy nim triangulacja, służąca na przykład do wykrywania położenia telefonu w sieciach GSM. Znając odległość (w pikselach) środka obrazu od trzech markerów, można było przeliczyć tę odległość na metry. Używając zmapowanych wcześniej (w tej fazie projektu tworzono mapę markerów ręcznie) pozycji markerów w laboratorium i~stosując triangulację, można było wyliczyć pozycję kamery w laboratorium.

\begin{figure}[H]
  \centering
    \includegraphics[width=\textwidth]{img/reflective_markers.png}
  \caption{Wyniki wykrywania odblaskowych znaczników na suficie.}
  \label{fig:reflectiveMarkers}
\end{figure}

Wyniki wykrywania odblaskowych markerów na obrazie prezentuje rysunek \ref{fig:reflectiveMarkers}. Po lewej u góry pokazano oryginalny obraz zarejestrowany przez kamerę. Po prawej u góry obraz po binaryzacji (próg ustawiony na maksymalną jasność piksela). Duże białe plamy to lampy. Po lewej na dole zaznaczone zostały wykryte markery. Po prawej na dole pokazano wizualizację triangulacji za pomocą trzech markerów najbliższych środkowi. Punkt przecięcia okręgów to środek obrazu.

Na podstawie obserwacji działania StarGazera autorzy doszli do wniosku, że wykonalne i efektywne jest zaimplementowanie wyznaczania pozycji przy użyciu zaledwie jednego markera. Dokładność takiego rozwiązania jest być może nieco gorsza, jednak daje ono dużo większą swobodę w rozmieszczaniu markerów na suficie. Zapewnienie, że w każdym momencie muszą być widoczne na suficie trzy markery wymaga, any były one dosyć gęsto rozmieszczone, a ze względu na ograniczoną ich liczbę, bardzo ogranicza rozmiar pomieszczenia, w którym możemy efektywnie zastosować tę metodę.

Z tego względu zaimplementowano wersję algorytmu dla wykrywania pozycji przy użyciu jednego markera. Zastosowany aparat matematyczny jest w tym przypadku troszkę bardziej zaawansowany niż zwykła triangulacja, jednak możliwy do pokazania przy użyciu niezbyt skomplikowanej geometrii analitycznej. Wykorzystane zależności i równania zostały zaprezentowane w~dokumentacji deweloperskiej.

Od czasu implementacji metody używającej jednego markera, stała się ona priorytetem jeżeli chodzi o rozwój algorytmu. W końcowej wersji projektu jest ona jedyną przetestowaną i w pełni zaimplementowaną metodą. Istnieje wprawdzie możliwość użycia strategii korzystającej z trzech markerów, jednak całkiem prawdopodobne, że zawiera ona błędy lub nie jest najbardziej efektywna obliczeniowo.

Implementacja i testowanie obu metod przypadły na okres listopada 2012.

\subsection{Przechylenie kamery i zastosowanie akcelerometru}

Mając zaimplementowane wstępne wersje algorytmów wyznaczania pozycji, autorzy mogli przeprowadzić pierwsze testy ich działania. Już na początku okazało się, że wyniki nie są do końca takie, jakich można by się spodziewać. Przesuwanie kamery po podłodze, bez zmiany jej kąta obrotu, dawało przyzwoite rezultaty. Problem pojawiał się, gdy zatrzymywano kamerę w jednym miejscu i obracano ją wokół osi optycznej obiektywu. Mimo, że jej pozycja nie powinna się zmieniać, a jedynie kąt obrotu, to wyraźnie można było zaobserwować zmiany w zwracanych wynikach.

Pierwszą diagnozą, było wychylenia kamery od pionu. W tej fazie projektu sprzęt nie był jeszcze odpowiednio zmontowany, co powodowało odchylenia od pionu. Jednym z rozwiązań mogło być odpowiednie zamocowanie wszystkich elementów już w tej fazie tworzenia projektu, jednak stwierdzono, że jest to rozwiązanie krótkofalowe. W przyszłości, jeżeli moduł miałby być wykorzystywany na różnego typu robotach, mogłoby dojść do sytuacji, że nie można go ustawić na idealnie równej powierzchni na robocie, co skutkowałoby niepoprawnymi wynikami.

Samo sprawdzanie hipotezy o wychyleniu kamery i poszukiwanie rozwiązań zajęło około dwa miesiące (grudzień 2012, styczeń 2013). Aby potwierdzić tę tezę należało wykonać setki zdjęć porównawczych na stabilnym podłożu, które można byłoby obracać wokół osi optycznej kamery. Ponadto dosyć długo poszukiwano istniejących rozwiązań tego problemu, rozważając przy tym podejścia zarówno mechaniczne (np. ustawianie pionu przy użyciu poziomicy oczkowej) jak i programowe. Szczególnie ciekawą publikacją opisującą sposób niwelowania efektu wychylenia o dużym kącie była praca Lee i Songa \cite{lee}.

Postanowiono zniwelować efekt wychylenia programowo. Zdecydowano się na pomiar wychylenia przy użyciu akcelerometru. Wejście w tę fazę projektu wymagało przygotowania spójnego modułu, na którym można by zamontować akcelerometr na jednej płaszczyźnie z kamerą. Z tego powodu od lutego, z dużymi przerwami i przestojami, aż do czerwca poszukiwano wszelkiego niezbędnego sprzętu. Oprócz akcelerometru były to płytki z miękkiego pleksiglasu, dystanse, wiertła, itp. Efektem tego był zbudowany w czerwcu prototyp. Nie był on jeszcze do końca samowystarczalny (nie posiadał diod podczerwonych), jednak umożliwiał bezpieczne i wygodne połączenie wszystkich dostępnych części.

W międzyczasie trwały prace nad poprawą jakości i możliwości systemu. Pojawiła się możliwość strumieniowania obrazu z kamery do komputera, co znacznie ułatwiło wyszukiwanie błędów i rozwój aplikacji. Poszukiwano też sposobów jak najlepszej poprawy wyników przy użyciu odczytów z akcelerometru.

\subsection{Przejście na C++ i rozwój dodatkowych funkcji}

W wakacje 2013 chciano zamknąć część implementacyjną, albo przynajmniej doprowadzić ją do stanu prawie finalnego. Przeszkodą na drodze do tego okazała się dosyć niechlujna struktura dotychczas napisanego kodu. Był on pisany w C, co nie zapewniało dobrej enkapsulacji ani wyraźnego podziału algorytmu na niezależne części.

Zdecydowano się przepisać cały kod na C++, starając się trzymać najlepszych technik programowania obiektowego i reusability. Mimo, że zajęło to prawie cały lipiec (obaj autorzy pracowali, więc ilość czasu była ograniczona), to okazało się bardzo dobrym pomysłem w dalszej perspektywie. Nowy kod okazał się dużo łatwiejszy do zrozumienia, rozwijania i modyfikowania.

Szczególnie dobrym posunięciem było rozbicie poszczególnych części algorytmu pozycjonowania (wykrycie kropek na obrazie, pogrupowanie kropek w markery, poprawa współrzędnych markerów na podstawie odczytów z~akelerometru, wyznaczenie pozycji) na osobne strategie. Pozwoliło to łatwo podmieniać implementacje części algorytmu, co w znacznym stopniu ułatwiło testowanie nowych sposobów przetwarzania surowego obrazu czy korekty wychylenia z nowszym modelem akcelerometru.

Sierpień i wrzesień to bardzo szybki rozwój systemu w kierunku ostatecznej wersji. Wydzielono wiele rzeczy, które były na stałe wpisane w kodzie, do opcji podawanych podczas uruchamiania systemu, co pozwoliło szybko i~bez rekompilacji testować działanie systemu w różnych konfiguracjach. Później dodatkowo pojawił się webowy interfejs użytkownika, który pozwalał nie tylko na obserwację obrazu, odczytów i wyników w przeglądarce w czasie rzeczywistym, lecz również na dynamiczną zmianę części ustawień algorytmu. Ta funkcjonalność również znacznie przyspieszyła testowanie i obserwację zachowania algorytmu.

Jednocześnie z implementacją wysokopoziomowej komunikacji i interfejsu użytkownika trwały prace nad niskopoziomowymi optymalizacjami. Najważniejszą z nich była nowa strategia wykrywania kropek markerów na obrazie. Dotychczasowe rozwiązanie, konwertujące surowy obraz do formatu BGR, a następnie na nim wyszukujące kropek pozwalało przetwarzać około 8 klatek na sekundę. Nowa strategia działała bezpośrednio na obrazie surowym, co pozwoliło uzyskać nawet do 32 klatek na sekundę.

\subsection{Prace wykończeniowe}

Ostatnie miesiące 2013 roku to doszlifowywanie kodu i dodawanie lub ulepszanie funkcjonalności, które nie były priorytetami przy rozwoju algorytmu. Dotyczy to głównie poprawy interfejsu użytkownika. Prowadzona była też refaktoryzacja kodu w miejscach, w których nie był on wystarczająco elastyczny (np. instancjonowanie obiektów w strategii zamiast wstrzykiwania przez konstruktor). Zmiany nastąpiły również tam, gdzie wymuszały to dodane do interfejsu użytkownika nowe opcje kontroli ustawień algorytmu.

Ostatnią modyfikacją sprzętową było wymienienie płytki z diodami oświetlającymi sufit. W testach okazało się, że diody nie mogą być skierowanie pionowo do góry, gdyż różnica w intensywności oświetlenia sufitu nie pozwala na poprawną binaryzację obrazu. Płytka została zmodyfikowana w ten sposób, że możliwe było rozchylenie diód na zewnątrz od kamery, dzięki czemu poprawiła się równomierność oświetlenia obszaru rejestrowanego przez kamerę. Została na nie również nałożona plastikowa płytka taka, jaką zastosowane w Stargazerze, aby dodatkowo rozpraszać światło emitowane przez diody i wpadające do kamery. Dzięki tym modyfikacjom możliwe było ponowne prawidłowe binaryzowanie obrazu. Finalna wersja sprzętu widoczna jest na rysunku \ref{fig:overoFinal}.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{img/overo_final.jpg}
  \caption{Finalna wersja tworzonego systemu. Zdjęta została płytka rozpraszająca światło, która nakładana jest na kamerę.}
  \label{fig:overoFinal}
\end{figure}

\section{Zmiany koncepcji}

Ze względu na badawczy charakter projektu, w czasie jego realizacji nastąpiło wiele zmian w stosunku do początkowej jego wizji. Spodziewano się, że koncepcja projektu będzie się bardzo dynamicznie zmieniać, a wiele etapów realizacji przyniesie nieoczekiwane wnioski. Z tego względu decyzje o kierunkach rozwoju projektu były podejmowane na bieżąco w oparciu o uzyskiwane efekty i wyniki prowadzonych badań.

Poniżej podsumowano wszystkie zmiany koncepcji, które dokonały się podczas całego okresu prac nad projektem. Dokładniej o powodach, kontekście i wynikach tych zmian można przeczytać w poprzednim podrozdziale. Tutaj zamieszczono je tylko poglądowo i jako podsumowanie.

\begin{itemize}

\item Przejście z gotowego, skonfigurowanego systemu na system zbudowany własnoręcznie przy użyciu cross-kompilatora. Pozwoliło to na zastosowanie nowszego jądra, nowszych sterowników kamery oraz większą kontrolę nad zainstalowanymi modułami jądra.

\item Zmiana markerów drukowanych na kartkach na markery odblaskowe zastosowane w StarGazerze. Pozwoliło to na dużo łatwiejszą binaryzację obrazu i bardziej niezawodną identyfikację markerów.

\item Zastąpienie algorytmu wyznaczania pozycji przy użyciu trzech markerów na algorytm wyznaczania pozycji przy użyciu jednego markera. Pozwoliło to zmniejszyć wymaganą liczbę widocznych w każdym momencie markerów z trzech do jednego.

\item Wykorzystanie akcelerometru do korekty zniekształceń spowodowanych przez przechylenie kamery. W początkowej koncepcji w ogóle nie wzięto pod uwagę tego, że przechylona kamera może powodować błędy w zwracanych wynikach. Dopiero podczas pierwszych testów przekonano się, że jest to realny problem.

\item Przepisanie kodu z C na C++. Pozwoliło to lepiej ustrukturalizować kod. Enkapsulacja odpowiedzialności za poszczególne zadania w osobnych klasach ułatwiła późniejszą analizę i rozwój kodu. Użycie strategii pozwoliło na łatwą podmianę części implementacji algorytmu.

\item Przeprowadzenie binaryzacji na surowym obrazie, a nie na BGR. Pozwoliło to uniknąć konieczności każdorazowej konwersji obrazu surowego do BGR, co spowodowało skok wydajności algorytmu z 8 klatek na sekundę do 32 klatek na sekundę.

\end{itemize}

\section{Podział prac}

Poniżej przedstawiono podział prac między członkami projektu. Należy jednak zaznaczyć, że żaden etap projektu nie był robiony wyłącznie przez jedną osobę. Przedstawiony tutaj podział pokazuje raczej, kto miał większy wpływ na daną część projektu.

\subsubsection*{Andrzej Dębski}
\begin{itemize}
\item początkowe uruchomienie platformy sprzętowej,
\item aktualizacja i rekonfiguracja systemu operacyjnego urządzenia,
\item testy algorytmu w laboratorium,
\item implementacja strumieniowania obrazu z kamery na komputer,
\item bezpośrednie testy podczas wakacji 2013,
\item implementacja webowego interfejsu użytkownika,
\item testy automatyczne.
\end{itemize}

\subsubsection*{Wojciech Grajewski}
\begin{itemize}
\item początkowe uruchomienie platformy sprzętowej,
\item implementacja wstępnej wersji algorytmu pozycjonowania,
\item testy algorytmu w laboratorium,
\item podłączenie i komunikacja z akcelerometrem,
\item końcowy montaż prototypu urządzenia,
\item przepisanie kodu z C na C++,
\item bieżące poprawki i ulepszenia w algorytmie.
\end{itemize}