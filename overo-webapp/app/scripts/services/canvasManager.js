'use strict';

angular.module('overoWebappApp').service('canvasManager', function canvasManager() {
  var canvas = document.getElementById('absoluteCoordinatesCanvas');
  var canvasContext = canvas.getContext('2d');

  canvasContext.font = 'bold 10px sans-serif';
  function redrawCordinates() {
    canvasContext.beginPath();
    //y axis
    canvasContext.moveTo(0, 0);
    canvasContext.lineTo(0, 10);
    //leaving place for y letter
    canvasContext.moveTo(0, 30);
    canvasContext.lineTo(0, 40);
    canvasContext.fillText('y', -2, 10 + 12);

    //x axis
    canvasContext.moveTo(0, 0);
    canvasContext.lineTo(10, 0);
    //leaving place for x letter
    canvasContext.moveTo(30, 0);
    canvasContext.lineTo(40, 0);
    canvasContext.fillText('x', 10 + 8, 3);

    canvasContext.strokeStyle = '#000';
    canvasContext.stroke();
  }

  function clearCanvas() {
    canvasContext.save();
    canvasContext.translate(0, 0);
    canvasContext.setTransform(1, 0, 0, 1, 0, 0);
    canvasContext.clearRect(0, 0, canvas.width, canvas.height);
    canvasContext.restore();
  }

  function redraw(angle) {
    canvasContext.save();

    canvasContext.translate(canvas.width / 2, canvas.height / 2);
    canvasContext.rotate(angle);
    redrawCordinates();

    canvasContext.restore();
  }

  this.setAngle = function (newAngle) {
    clearCanvas();
    redraw(newAngle);
  };
  redraw(0);
});
