'use strict';

angular.module('overoWebappApp')
  .service('websocketService', function websocketService() {

    this.initializeWebsocket = function (url, protocol, onOpenCallback, onMessageCallback, onErrorCallback) {
      return new WebsocketWrapper(url, protocol, onOpenCallback, onMessageCallback, onErrorCallback);
    };

    function WebsocketWrapper(url, protocol, onOpenCallback, onMessageCallback, onErrorCallback) {
      var that = this;
      that.socket = new WebSocket(url, protocol);
      that.socket.onopen = onOpenCallback;
      that.socket.onmessage = onMessageCallback;
      that.socket.onerror = onErrorCallback;
      that.socket.binaryType = 'arraybuffer';

      that.isOpened = function () {
        return that.socket && (that.socket.readyState === WebSocket.OPEN);
      };

      that.send = function (data) {
        that.socket.send(data);
      };

      that.removeCallbacks = function () {
        if (that.socket) {
          that.socket.onopen = undefined;
          that.socket.onmessage = undefined;
          that.socket.onerror = undefined;
        }
      };
    }

  });
