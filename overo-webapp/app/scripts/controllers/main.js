'use strict';

angular.module('overoWebappApp')
  .controller('MainCtrl', ['$scope', '$window', 'websocketService', 'base64ImageEncoder', function ($scope, $window, websocketService, base64ImageEncoder) {


    $scope.connectionError = false;
    /*
     OVERO STREAM
     */
    $scope.tilt = {x: 0.0, y: 0.0, z: 0.0};
    $scope.coordinates = {x: 0.0, y: 0.0, angle: 0.0, referenceMarkerId: 0};
    $scope.detectedMarkers = [];
    $scope.detectedMarkers2 = '';
    $scope.imageStreamOptions = {
      normalImageStream: {isChecked: false, suffix: '_NORMAL_IMAGE_STREAM', radio: true},
      binarizedImageStream: {isChecked: false, suffix: '_BINARIZED_IMAGE_STREAM', radio: true},
      undistortImage: {isChecked: false, suffix: '_UNDISTORT_IMAGE'},
      paintMarkers: {isChecked: false, suffix: '_PAINT_MARKERS'},
      paintCenter: {isChecked: false, suffix: '_PAINT_CENTER'}
    };
    $scope.dataStreamOptions = {
      accelerometerStream: {isChecked: false, suffix: '_ACC_STREAM'},
      coordinatesStream: {isChecked: false, suffix: '_COORDINATES_STREAM'},
      detectedMarkersStream: {isChecked: false, suffix: '_DETECTED_MARKERS_STREAM'}
    };

    $scope.overoStreamOpenCallback = function () {
      $scope.$apply(function () {
        $scope.connectionOptions.overoStream = true;
      });
    };

    $scope.changeImageStreamOption = function (streamOption) {
      var imageStreamOptionObject = $scope.imageStreamOptions[streamOption];
      var prefix = imageStreamOptionObject.isChecked === false ? 'START' : 'STOP';
      var message = prefix + imageStreamOptionObject.suffix;

      if (imageStreamOptionObject.radio) {
        preserveRadioButton(imageStreamOptionObject, $scope.imageStreamOptions);
      }

      $scope.sendOveroStreamMessage(message);
    };

    $scope.sendOveroStreamMessage = function (message) {
      var socket = $scope.overoStreamSocket;

      if (socket && socket.isOpened()) {
        socket.send(message);
      } else {
        $scope.handleErrors();
      }
    };

    $scope.overoStreamCallback = function (msg) {
      var header = $scope.parseOveroStreamMessageHeader(new $window.jDataView(msg.data, 0, 4));
      var offset = 4;
      var numberOfMarkers;
      var markersDataInBytes;

      if (header.accelerometerStream) {
        $scope.parseTilt(new $window.jDataView(msg.data, offset, 12, false));
        offset += 12;
      }

      if (header.coordinatesStream) {
        $scope.parseCoordinates(new $window.jDataView(msg.data, offset, 16, false));
        offset += 16;
      }

      if (header.detectedMarkersStream) {
        numberOfMarkers = (new $window.jDataView(msg.data, offset, 4, false)).getUint32(0);
        offset += 4;

        //for each marker we are receiving center X,Y and marker ID, each coded in one Uint32
        markersDataInBytes = numberOfMarkers * 3 * 4;
        $scope.parseDetectedMarkers(numberOfMarkers, new $window.jDataView(msg.data, offset, markersDataInBytes, false));

        offset += markersDataInBytes;
      }

      if (header.imageStream) {
        var data = new Uint8Array(msg.data, offset);
        var enc = base64ImageEncoder.encodeImage(data);
        var image = document.getElementById('overoImage');
        image.src = 'data:image/jpg;base64,' + enc;
      }
    };

    $scope.parseOveroStreamMessageHeader = function (header) {
      return{
        imageStream: (header.getUint8(0) === 1),
        accelerometerStream: (header.getUint8(1) === 1),
        coordinatesStream: (header.getUint8(2) === 1),
        detectedMarkersStream: (header.getUint8(3) === 1)
      };
    };

    $scope.parseTilt = function (accelerometerData) {
      $scope.$apply(function () {
        $scope.tilt.x = accelerometerData.getFloat32(0);
        $scope.tilt.y = accelerometerData.getFloat32(4);
        $scope.tilt.z = accelerometerData.getFloat32(8);
      });
    };

    $scope.parseCoordinates = function (coordinatesData) {
      console.log(5);
      $scope.$apply(function () {
        $scope.coordinates.x = coordinatesData.getFloat32(0);
        $scope.coordinates.y = coordinatesData.getFloat32(4);
        $scope.coordinates.angle = coordinatesData.getFloat32(8);
        $scope.coordinates.referenceMarkerId = coordinatesData.getUint32(12);
      });
    };

    $scope.parseDetectedMarkers = function (numberOfMarkers, markersData) {
      var i;
      $scope.detectedMarkers = '';
      for (i = 0; i < numberOfMarkers; ++i) {
        /*$scope.detectedMarkers.push({
         centerX: markersData.getFloat32(),
         centerY: markersData.getFloat32(),
         id: markersData.getUint32()
         });*/
        $scope.detectedMarkers += ('Marker: ' + markersData.getUint32().toString() + ' Center X: ' + markersData.getFloat32().toString() +
          ' Center Y: ' + markersData.getFloat32().toString() + '\n');
      }
      $scope.$apply('detectedMarkers');
    };
    /*
     OVERO CONTROL
     */
    $scope.currentMap = '';
    $scope.algorithmParameters = {undistort: false, cellingHeightMeters: 0.0, dotBrightnessThreshold: 0.0, minDotArea: 0.0, maxDotArea: 0.0};
    $scope.brightness = {value: 0};
    $scope.controlOptions = {
      detection: {isChecked: false, suffix: '_DETECTION', radio: true},
      mapping: {isChecked: false, suffix: '_MAP', radio: true}
    };

    $scope.overoControlOpenCallback = function () {
      $scope.$apply(function () {
        $scope.connectionOptions.overoControl = true;
      });
    };

    $scope.changeDataStreamOption = function (streamOption) {
      var dataStreamOptionObject = $scope.dataStreamOptions[streamOption];
      var prefix = dataStreamOptionObject.isChecked === false ? 'START' : 'STOP';
      var message = prefix + dataStreamOptionObject.suffix;

      $scope.sendOveroStreamMessage(message);
    };

    $scope.changeControlOption = function (streamOption) {
      var controlOptionObject = $scope.controlOptions[streamOption];
      var prefix = controlOptionObject.isChecked === false ? 'START' : 'STOP';
      var message = prefix + controlOptionObject.suffix;

      if (controlOptionObject.radio) {
        preserveRadioButton(controlOptionObject, $scope.controlOptions);
      }

      $scope.sendOveroControlMessage(message);
    };

    $scope.sendalgorithmParameters = function () {
      console.log('sending current parameters');

      var algorithmParameters = $scope.algorithmParameters;
      var dataView = new $window.jDataView(20);
      dataView.writeUint32(algorithmParameters.undistort, false);
      dataView.writeFloat32(algorithmParameters.dotBrightnessThreshold, false);
      dataView.writeFloat32(algorithmParameters.minDotArea, false);
      dataView.writeFloat32(algorithmParameters.maxDotArea, false);
      dataView.writeFloat32(algorithmParameters.ceilingHeightMeters, false);

      $scope.sendOveroControlMessage(dataView.buffer);
    };

    $scope.sendBrightness = function () {
      console.log('sending brightness');
      var value = parseInt($scope.brightness.value);
      var dataView = new $window.jDataView(4);

      if (value < 0 || value > 100) {
        console.log('value not in [0,100] range');
        return;
      }
      dataView.writeUint32(value, false);

      $scope.sendOveroControlMessage(dataView.buffer);
    };

    $scope.sendOveroControlMessage = function (message) {
      var socket = $scope.overoControlSocket;

      if (socket && socket.isOpened()) {
        socket.send(message);
      } else {
        $scope.handleErrors();
      }
    };

    $scope.overoControlCallback = function (msg) {
      console.log('overoControlCallback');
      $scope.parseOveroStreamMessageHeader(new $window.jDataView(msg.data, 0, 4));
      var header = $scope.parseOveroControlMessageHeader(new $window.jDataView(msg.data, 0, 2));
      var offset = 2;
      var parsedMessage;
      var numberOfMarkers;
      var markersDataInBytes;
      var i;

      if (header.algorithmParameters) {
        parsedMessage = new $window.jDataView(msg.data, offset, 20, false);
        offset += 20;
        $scope.$apply(function () {
          $scope.algorithmParameters.undistort = parsedMessage.getUint32(0) !== 0;
          $scope.algorithmParameters.dotBrightnessThreshold = parsedMessage.getFloat32(4);
          $scope.algorithmParameters.minDotArea = parsedMessage.getFloat32(8);
          $scope.algorithmParameters.maxDotArea = parsedMessage.getFloat32(12);
          $scope.algorithmParameters.ceilingHeightMeters = parsedMessage.getFloat32(16);
        });
      }

      if (header.currentMap) {
        numberOfMarkers = (new $window.jDataView(msg.data, offset, 4, false)).getUint32();
        offset += 4;
        //for every marker we receive id, X, Y, angle, each coded in 4 bytes
        markersDataInBytes = numberOfMarkers * 4 * 4;

        parsedMessage = new $window.jDataView(msg.data, offset, markersDataInBytes, false);

        $scope.currentMap = '';
        for (i = 0; i < numberOfMarkers; ++i) {
          $scope.currentMap += ('Marker: ' + parsedMessage.getUint32().toString() + ' absX: ' +
            parsedMessage.getFloat32().toString() + ' absY: ' + parsedMessage.getFloat32().toString() +
            ' angle: ' + parsedMessage.getFloat32().toString() + '\n');
        }

        $scope.$apply('currentMap');
      }
    };


    $scope.parseOveroControlMessageHeader = function (header) {
      return{
        algorithmParameters: (header.getUint8(0) === 1),
        currentMap: (header.getUint8(1) === 1)
      };
    };
    /*
     GENERAL FUNCTIONS
     */
    $scope.connectionOptions = {
      overoIP: 'localhost:15000',
      overoControl: false,
      overoStream: false
    };
    $scope.overoControlSocket = null;
    $scope.overoStreamSocket = null;

    function preserveRadioButton(changedOptionObject, optionObjects) {
      var isChecked = changedOptionObject.isChecked;

      for (var optionObject in optionObjects) {
        if (optionObjects[optionObject].radio) {
          optionObjects[optionObject].isChecked = false;
        }
      }
      changedOptionObject.isChecked = isChecked;
    }

    $scope.clearErrors = function () {
      $scope.connectionError = false;
    };

    $scope.websocketErrorCallback = function (event) {
      console.log(event);
      $scope.$apply(function () {
        $scope.handleErrors();
      });
    };

    $scope.handleErrors = function () {
      $scope.connectionOptions.overoControl = false;
      $scope.connectionOptions.overoStream = false;
      $scope.connectionError = true;
    };

    $scope.resetModel = function () {
      var optionObject;

      for (optionObject in $scope.imageStreamOptions) {
        $scope.imageStreamOptions[optionObject].isChecked = false;
      }

      for (optionObject in $scope.dataStreamOptions) {
        $scope.dataStreamOptions[optionObject].isChecked = false;
      }

      for (optionObject in $scope.controlOptions) {
        $scope.controlOptions[optionObject].isChecked = false;
      }

      $scope.detectedMarkers = '';
      $scope.tilt = {x: 0.0, y: 0.0, z: 0.0};
      $scope.algorithmParameters = {dotBrightnessThreshold: 0.0, minDotArea: 0.0, maxDotArea: 0.0, ceilingHeightMeters: 0.0};
      $scope.coordinates = {x: 0.0, y: 0.0, angle: 0.0, referenceMarkerId: 0};
    };

    $scope.connect = function () {
      $scope.resetModel();
      $scope.clearErrors();

      if ($scope.overoControlSocket) {
        $scope.overoControlSocket.removeCallbacks();
      }
      if ($scope.overoStreamSocket) {
        $scope.overoStreamSocket.removeCallbacks();
      }

      $scope.overoControlSocket = websocketService.initializeWebsocket('ws://' + $scope.connectionOptions.overoIP, 'overoControl',
        $scope.overoControlOpenCallback, $scope.overoControlCallback, $scope.websocketErrorCallback);
      $scope.overoStreamSocket = websocketService.initializeWebsocket('ws://' + $scope.connectionOptions.overoIP, 'overoStream',
        $scope.overoStreamOpenCallback, $scope.overoStreamCallback, $scope.websocketErrorCallback);
    };
  }]);
